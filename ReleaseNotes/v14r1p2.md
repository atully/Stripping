2019-05-26 Stripping v14r1p2
========================================

Production release (after overtiming fixes - see LBOPG-109) for 2018 pp incremental re-stripping (Stripping34r0p1) production.

----------------------------------------

Based on Phys v25r10p1. This version is released on 2018-patches branch.

### Modifications and new additions:

- Fix overtimes 2018 patches, !1150 (@cvazquez)   
  

- Remove StrippingTrackEffDownMuonZLine for timing issues, !1148 (@lohenry)   
  Remove StrippingTrackEffDownMuonZLine for timing issues

- add fixes to B2SS, and update LineConfig, !1147 (@mmarinan)   
  FYI @cvazquez @jcidvida

- Update RD dictionaries, !1146 (@vlisovsk)   
  Follows the changes in Bu2LLK and B2XMuMu lines.

- Tighten StrippingBu2LLK.py, !1145 (@vlisovsk)   
  - Separate problematic hadron containers of Bu2LLK_mmLine and Bu2LLK_eeLine(2) into "extra" lines, which have a tighter lepton PT and MaxCandidates requirements, and no RelInfo13  
  - Tighten the 2-body and 3-body hadron selections  
  - Reduce the number of hadron containers and set MaxCandidates for other lines

- Reduce processing time in B2XMuMu: Change Pi0ForOmegaMINPT from 500 to 800, !1144 (@skretzsc)   
  Rare busy events caused timeouts in processing of 2018 data. Such a busy event was identified in   
    
  $STRIPPINGSELECTIONSROOT/tests/data/april_2019/Reco18_RunFile_33.py    
    
  (ev. no 5420) where B2XMuMu finds **5679 candidates** and takes **>15s to process**.   
    
  Several config changes were studied in order to reduce this number of candidates, and a tightening of pi0ForOmegaMINPT, which is the minimum pt of pions that are used to form an omega candidate, is found to be effective. With   
    
  pi0ForOmegaMINPT = 800  
    
  (currently: 500) we find **3955 candidates** and need **10.5s to process** this event.   
    
  This value of 800 is then also consistent with pi0MINPT which is also at 800.   
    
  Therefore, we suggest to tighten this pi0 pt selection in order to reduce the processing time for busy events like this and avoid the timeouts that now cause problems in data processing.

- B2OC Selections s34r0p1 update for 9 lines taking more than 10 s to process busy events, !1143 (@abertoli)   
  

- Add ChronoDelta script, !1140 (@cvazquez)   
  

- Reorder B mother cuts in StrippingDarkBoson for timing, !1136 (@dcraik)   
  Reordered mother cuts for B candidates to speed up DarkBoson lines.  
    
  Tested for StrippingB2KpiX22K2PiDarkBosonLine on file 10, events 15500-15750 (contains one of the most problematic events). Timing of stripping line reduced by 40% (20% including related info).  
    
  Before:  
  ```  
  INFO          StrippingB2KpiX22K2PiDarkBosonLine_TIMING          |    42.680 |    42.701 |    0.031   10664.3   674.47 |     250 |    10.675 |  
  INFO           StrippingB2KpiX22K2PiDarkBosonLine_TIMINGPreScaler|     0.000 |     0.003 |    0.002       0.0     0.00 |     250 |     0.001 |  
  INFO           StrippingB2KpiX22K2PiDarkBosonLine_TIMINGFilterSeq|    42.680 |    42.675 |    0.010   10664.2   674.46 |     250 |    10.669 |  
  INFO            StrippingB2KpiX22K2PiDarkBosonLine_TIMINGVOIDFilt|     0.000 |     0.004 |    0.003       0.0     0.00 |     250 |     0.001 |  
  INFO            B2KpiX22K2PiDarkBosonLine_TIMING                 |   936.000 |   934.944 |    0.082    4673.3  2089.79 |       5 |     4.675 |  
  INFO            RelatedInfo1_B2KpiX22K2PiDarkBosonLine_TIMING    |  1280.000 |  1274.547 | 1274.547    1274.5     0.00 |       1 |     1.275 |  
  INFO            RelatedInfo2_B2KpiX22K2PiDarkBosonLine_TIMING    |  1290.000 |  1290.302 | 1290.302    1290.3     0.00 |       1 |     1.290 |  
  INFO            RelatedInfo3_B2KpiX22K2PiDarkBosonLine_TIMING    |  1260.000 |  1265.126 | 1265.126    1265.1     0.00 |       1 |     1.265 |  
  INFO            RelatedInfo4_B2KpiX22K2PiDarkBosonLine_TIMING    |   530.000 |   521.653 |  521.653     521.7     0.00 |       1 |     0.522 |  
  INFO            RelatedInfo5_B2KpiX22K2PiDarkBosonLine_TIMING    |  1430.000 |  1439.263 | 1439.263    1439.3     0.00 |       1 |     1.439 |  
  INFO            RelatedInfo6_B2KpiX22K2PiDarkBosonLine_TIMING    |   140.000 |   137.656 |  137.656     137.7     0.00 |       1 |     0.138 |  
  INFO           StrippingB2KpiX22K2PiDarkBosonLine_TIMINGPostScale|     0.000 |     0.005 |    0.005       0.0     0.00 |       1 |     0.000 |  
  ```  
    
  After:  
  ```  
  INFO          StrippingB2KpiX22K2PiDarkBosonLine_TIMING          |    33.920 |    33.986 |    0.030    8486.6   536.74 |     250 |     8.497 |  
  INFO           StrippingB2KpiX22K2PiDarkBosonLine_TIMINGPreScaler|     0.000 |     0.003 |    0.002       0.0     0.00 |     250 |     0.001 |  
  INFO           StrippingB2KpiX22K2PiDarkBosonLine_TIMINGFilterSeq|    33.920 |    33.961 |    0.009    8486.5   536.73 |     250 |     8.490 |  
  INFO            StrippingB2KpiX22K2PiDarkBosonLine_TIMINGVOIDFilt|     0.000 |     0.004 |    0.003       0.0     0.00 |     250 |     0.001 |  
  INFO            B2KpiX22K2PiDarkBosonLine_TIMING                 |   558.000 |   560.157 |    0.074    2800.1  1252.16 |       5 |     2.801 |  
  INFO            RelatedInfo1_B2KpiX22K2PiDarkBosonLine_TIMING    |  1200.000 |  1191.947 | 1191.947    1191.9     0.00 |       1 |     1.192 |  
  INFO            RelatedInfo2_B2KpiX22K2PiDarkBosonLine_TIMING    |  1190.000 |  1196.830 | 1196.830    1196.8     0.00 |       1 |     1.197 |  
  INFO            RelatedInfo3_B2KpiX22K2PiDarkBosonLine_TIMING    |  1200.000 |  1197.351 | 1197.351    1197.4     0.00 |       1 |     1.197 |  
  INFO            RelatedInfo4_B2KpiX22K2PiDarkBosonLine_TIMING    |   510.000 |   514.688 |  514.688     514.7     0.00 |       1 |     0.515 |  
  INFO            RelatedInfo5_B2KpiX22K2PiDarkBosonLine_TIMING    |  1410.000 |  1402.763 | 1402.763    1402.8     0.00 |       1 |     1.403 |  
  INFO            RelatedInfo6_B2KpiX22K2PiDarkBosonLine_TIMING    |   130.000 |   134.658 |  134.658     134.7     0.00 |       1 |     0.135 |  
  INFO           StrippingB2KpiX22K2PiDarkBosonLine_TIMINGPostScale|     0.000 |     0.005 |    0.005       0.0     0.00 |       1 |     0.000 |  
  ```  
    
  Tested for StrippingB2KpiX24KDarkBosonLine on file 46, events 0-100k. Timing reduced by 45%.  
    
  Before:  
  ```  
  INFO          StrippingB2KpiX24KDarkBosonLine_TIMING             |     0.182 |     0.186 |    0.029   10877.2    39.52 |   75857 |    14.162 |  
  INFO           StrippingB2KpiX24KDarkBosonLine_TIMINGPreScaler   |     0.002 |     0.003 |    0.002       0.1     0.00 |   75857 |     0.246 |  
  INFO           StrippingB2KpiX24KDarkBosonLine_TIMINGFilterSequen|     0.160 |     0.163 |    0.009   10877.1    39.52 |   75857 |    12.422 |  
  INFO            StrippingB2KpiX24KDarkBosonLine_TIMINGVOIDFilter |     0.002 |     0.003 |    0.003       0.2     0.00 |   75857 |     0.296 |  
  INFO            B2KpiX24KDarkBosonLine_TIMING                    |    79.084 |    79.205 |    0.061   10758.1   902.91 |     142 |    11.247 |  
  INFO            RelatedInfo1_B2KpiX24KDarkBosonLine_TIMING       |    25.000 |    20.324 |    4.974      35.7    21.71 |       2 |     0.041 |  
  INFO            RelatedInfo2_B2KpiX24KDarkBosonLine_TIMING       |    20.000 |    21.437 |    4.878      38.0    23.42 |       2 |     0.043 |  
  INFO            RelatedInfo3_B2KpiX24KDarkBosonLine_TIMING       |    20.000 |    20.971 |    4.873      37.1    22.77 |       2 |     0.042 |  
  INFO            RelatedInfo4_B2KpiX24KDarkBosonLine_TIMING       |    10.000 |    11.932 |    5.225      18.6     9.49 |       2 |     0.024 |  
  INFO            RelatedInfo5_B2KpiX24KDarkBosonLine_TIMING       |    25.000 |    21.906 |    8.112      35.7    19.51 |       2 |     0.044 |  
  INFO            RelatedInfo6_B2KpiX24KDarkBosonLine_TIMING       |     0.000 |     2.166 |    0.819       3.5     1.90 |       2 |     0.004 |  
  INFO           StrippingB2KpiX24KDarkBosonLine_TIMINGPostScaler  |     0.000 |     0.004 |    0.004       0.0     0.00 |       2 |     0.000 |  
  ```  
    
  After:  
  ```  
  INFO          StrippingB2KpiX24KDarkBosonLine_TIMING             |     0.115 |     0.117 |    0.028    5891.3    21.42 |   75857 |     8.927 |  
  INFO           StrippingB2KpiX24KDarkBosonLine_TIMINGPreScaler   |     0.001 |     0.003 |    0.002       0.0     0.00 |   75857 |     0.241 |  
  INFO           StrippingB2KpiX24KDarkBosonLine_TIMINGFilterSequen|     0.096 |     0.095 |    0.009    5891.2    21.42 |   75857 |     7.261 |  
  INFO            StrippingB2KpiX24KDarkBosonLine_TIMINGVOIDFilter |     0.002 |     0.003 |    0.003       0.1     0.00 |   75857 |     0.283 |  
  INFO            B2KpiX24KDarkBosonLine_TIMING                    |    43.732 |    43.509 |    0.063    5810.9   487.90 |     142 |     6.178 |  
  INFO            RelatedInfo1_B2KpiX24KDarkBosonLine_TIMING       |    20.000 |    19.322 |    4.847      33.8    20.47 |       2 |     0.039 |  
  INFO            RelatedInfo2_B2KpiX24KDarkBosonLine_TIMING       |    20.000 |    19.405 |    4.796      34.0    20.66 |       2 |     0.039 |  
  INFO            RelatedInfo3_B2KpiX24KDarkBosonLine_TIMING       |    20.000 |    19.933 |    4.796      35.1    21.41 |       2 |     0.040 |  
  INFO            RelatedInfo4_B2KpiX24KDarkBosonLine_TIMING       |    15.000 |    11.864 |    4.254      19.5    10.76 |       2 |     0.024 |  
  INFO            RelatedInfo5_B2KpiX24KDarkBosonLine_TIMING       |    20.000 |    20.194 |    7.369      33.0    18.14 |       2 |     0.040 |  
  INFO            RelatedInfo6_B2KpiX24KDarkBosonLine_TIMING       |     0.000 |     2.163 |    0.739       3.6     2.01 |       2 |     0.004 |  
  INFO           StrippingB2KpiX24KDarkBosonLine_TIMINGPostScaler  |     0.000 |     0.004 |    0.004       0.0     0.00 |       2 |     0.000 |  
  ```

- Fixing StrippingKshort2Leptons, !1135 (@acasaisv)   
  Adding MaxCombination cuts on 4e Lines with VELO tracks, as suggested by @vlisovsk

- B&Q Omega_b decays fix timing, !1132 (@vazhovko)   
  Problematic lines:  
    
  ```  
  StrippingOmegabDecaysOmegacpipipiLine   
  StrippingOmegabDecaysOmegacpipipiWSLine  
  StrippingOmegabDecaysXicgammaKpiLine   
  StrippingOmegabDecaysXicgammaKpiWSLine   
  StrippingOmegabDecaysXic0piKpiLine   
  StrippingOmegabDecaysXic0piKpiWSLine  
  ```  
    
  Line tested using Reco18_RunFile_46, because it was the most problematic one ([log_before](/uploads/7e64f00e9fd90373414541cf0beacfa0/Strip34_TestSel_Omegab_0.log))  
  Test with 20K files with applied changes:  
  [log_after](/uploads/e54f289b5bfcf11c9fb8529ba53525b1/Strip34_TestSel_Omegab_V3_46.log)  
    
    
  **StrippingOmegabDecaysXicgammaKpiLine**   
  ```  
  TimingAuditor.TIMER                                            INFO          StrippingOmegabDecaysXicgammaKpiLine               |     0.655 |     0.668 |    0.036    2477.2    17.82 |   20000 |    13.366 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysXicgammaKpiLinePreScaler     |     0.008 |     0.006 |    0.005       0.1     0.00 |   20000 |     0.138 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysXicgammaKpiLineFilterSequence|     0.613 |     0.629 |    0.006    2477.1    17.82 |   20000 |    12.591 |  
  TimingAuditor.TIMER                                            INFO            SELECT:Phys/StdLooseAllPhotons                   |    11.340 |    11.669 |    0.171      94.7    10.37 |     776 |     9.056 |  
  TimingAuditor.TIMER                                            INFO            OmegabDecaysXicgammaKpiLine                      |     4.613 |     4.641 |    0.167    2468.6    91.74 |     724 |     3.361 |  
  ```  
    
    
  **StrippingOmegabDecaysXicgammaKpiWSLine**   
  ```  
  TimingAuditor.TIMER                                            INFO          StrippingOmegabDecaysXicgammaKpiWSLine             |     0.236 |     0.236 |    0.037    2845.3    20.13 |   20000 |     4.727 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysXicgammaKpiWSLinePreScaler   |     0.004 |     0.006 |    0.005       0.1     0.00 |   20000 |     0.138 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysXicgammaKpiWSLineFilterSequen|     0.201 |     0.197 |    0.006    2845.1    20.13 |   20000 |     3.953 |  
  TimingAuditor.TIMER                                            INFO            OmegabDecaysXicgammaKpiWSLine                    |     5.372 |     5.199 |    0.138    2845.0   105.75 |     724 |     3.765 |  
  ```  
    
    
  **StrippingOmegabDecaysOmegacpipipiLine**   
  ```  
  TimingAuditor.TIMER                                            INFO          StrippingOmegabDecaysOmegacpipipiLine              |     0.172 |     0.168 |    0.035    2383.3    16.85 |   20000 |     3.379 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysOmegacpipipiLinePreScaler    |     0.004 |     0.006 |    0.005       0.1     0.00 |   20000 |     0.137 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysOmegacpipipiLineFilterSequenc|     0.134 |     0.131 |    0.006    2383.2    16.85 |   20000 |     2.627 |  
  TimingAuditor.TIMER                                            INFO            OmegabDecaysOmegacpipipiLine                     |    25.625 |    25.611 |    0.094    2383.2   243.15 |      96 |     2.459 |  
  ```  
    
    
  **StrippingOmegabDecaysOmegacpipipiWSLine**  
  ```  
  TimingAuditor.TIMER                                            INFO          StrippingOmegabDecaysOmegacpipipiWSLine            |     0.180 |     0.189 |    0.036    2794.1    19.76 |   20000 |     3.790 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysOmegacpipipiWSLinePreScaler  |     0.003 |     0.006 |    0.005       0.1     0.00 |   20000 |     0.136 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysOmegacpipipiWSLineFilterSeque|     0.151 |     0.152 |    0.006    2794.0    19.76 |   20000 |     3.046 |  
  TimingAuditor.TIMER                                            INFO            OmegabDecaysOmegacpipipiWSLine                   |    29.791 |    29.980 |    0.101    2793.9   285.07 |      96 |     2.878 |  
  ```  
    
    
  **StrippingOmegabDecaysXic0piKpiLine**   
  ```  
  TimingAuditor.TIMER                                            INFO          StrippingOmegabDecaysXic0piKpiLine                 |     0.354 |     0.352 |    0.036    1286.0     9.09 |   20000 |     7.051 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysXic0piKpiLinePreScaler       |     0.004 |     0.006 |    0.005       0.1     0.00 |   20000 |     0.138 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysXic0piKpiLineFilterSequence  |     0.314 |     0.307 |    0.006    1285.9     9.09 |   20000 |     6.154 |  
  TimingAuditor.TIMER                                            INFO            Xic0ForOmegabDecays                              |     0.222 |     0.223 |    0.113      17.6     0.17 |   19421 |     4.335 |  
  TimingAuditor.TIMER                                            INFO            OmegabDecaysXic0piKpiLine                        |    23.703 |    23.937 |    0.153    1267.7   172.45 |      54 |     1.293 |  
  ```  
    
    
  **StrippingOmegabDecaysXic0piKpiWSLine**  
  ```  
  TimingAuditor.TIMER                                            INFO          StrippingOmegabDecaysXic0piKpiWSLine               |     0.094 |     0.090 |    0.036     821.3     5.81 |   20000 |     1.810 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysXic0piKpiWSLinePreScaler     |     0.006 |     0.007 |    0.005       0.1     0.00 |   20000 |     0.143 |  
  TimingAuditor.TIMER                                            INFO           StrippingOmegabDecaysXic0piKpiWSLineFilterSequence|     0.048 |     0.050 |    0.006     821.2     5.81 |   20000 |     1.011 |  
  TimingAuditor.TIMER                                            INFO            OmegabDecaysXic0piKpiWSLine                      |    15.370 |    15.528 |    0.148     821.1   111.69 |      54 |     0.839 |  
  ```

- B2LLXBDT, removed isolation tool 5&13 to fix timing problem, !1131 (@jhe)   
  Removed Isolation tool 5&13, to fix the timing problem of processing 2018 data
