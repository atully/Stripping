###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Configurables

success_list=[]
fail_list=[]

for conf in sorted(Configurables.__all__):
    try:
        print "# " + conf
        exec("Configurables.%s()" % conf)
        success_list.append(conf)
    except:
        import sys
        import traceback
        traceback.print_exc(file=sys.stdout)
        fail_list.append(conf)

print "== successful =="
print len(success_list)
print success_list
print "== failed =="
print len(fail_list)
print fail_list
print "== done =="

