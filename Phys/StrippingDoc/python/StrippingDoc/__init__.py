###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Python readable doc for the stripping.'''
import os, glob, re

class StrippingDoc(object) :
    '''Helper class to access the python stripping doc.'''

    def __init__(self, version) :
        '''Get the doc for the given stripping version.'''

        if isinstance(version, str) :
            try :
                self.module = __import__('StrippingDoc.' + version.lower(), fromlist = ['lines'])
            except ImportError :
                modules = []
                for fname in sorted(glob.glob(os.path.join(os.environ['STRIPPINGDOCROOT'], 'python', 'StrippingDoc', 'stripping*.py'))) :
                    module = os.path.split(fname)[1][:-3]
                    modules.append(module[0].upper() + module[1:])
                raise ValueError('No doc for version {0!r}. Known versions are:\n{1!r}'.format(version, modules))
        # Assume it's a module/class that has the desired attributes.
        else :
            self.module = version

        # Could use 
        # filter(lambda attr : not attr.startswith('_'), dir(self.module))
        # so as not to have to maintain this list, but this checks that the given 
        # object has the required attribues.
        for attr in 'DaVinciVersion', 'platform', 'TCK', 'datatype', 'CondDBtag', \
                'DDDBtag', 'stepid', 'description' :
            setattr(self, attr, getattr(self.module, attr))

        self.lines = {}
        for name, line in self.module.lines.items() :
            self.lines[name] = StrippingLineDoc(datatype = self.datatype, **line)

    def filter_lines(self, test) :
        '''Get lines for which the method 'test' returns True.'''
        return filter(test, self.lines.values())

    def get_line(self, name) :
        '''Get the doc on the line of the given name.'''
        try :
            return self.lines[name]
        except KeyError :
            return None

    def find_lines(self, pattern) :
        '''Get the doc on lines whose name contains the given regex pattern'''
        return self.filter_lines(lambda line : re.search(pattern, line.name))

def add_carets(Decay) :
    '''Add carets to a DecayDescriptor for DecayTreeTuple.'''
    # Add carets, except on the head particle, which should always be preceded by open bracket.
    Decay = re.sub('([^(]) ([A-Za-z])', '\\1 ^\\2', Decay)
    # Add carets on sub decays, where it needs to go outside the bracket.
    Decay = re.sub('([^|]) \\( ([A-Za-z])', '\\1 ^( \\2', Decay)
    return Decay

class StrippingLineDoc(object) :
    '''Helper class to access doc on a stripping line.'''

    def __init__(self, **kwargs) :
        '''Takes the dict of info for a line stored in the stripping doc module.'''
        for attr, val in kwargs.items() :
            setattr(self, attr, val)

    def tuple_config(self, fulldescriptors = True) :
        '''Get the configuration of a DecayTreeTuple instance for this line. If fulldescriptors = False,
        just the top level decay descriptors will be used.'''
        
        if fulldescriptors :
            Decay = ' || '.join(self.decaydescriptors)
        else :
            Decay = ' || '.join(self.topdecaydescriptors)
        Decay = add_carets(Decay)
        Inputs = [self.outputlocation]
        return dict(Inputs = Inputs, Decay = Decay)

    def related_info_config(self, stream = None) :
        '''Get the configuration of a LoKi__Hybrid__TupleTool to read the output of the RelatedInfo tools
        of this line.'''

        stream = self._stream(stream)

        Variables = {}
        config = dict(Variables = Variables)
        if not self.RelatedInfoTools :
            return config
        for varinfo in self.RelatedInfoTools :
            locations = []
            if 'Location' in varinfo :
                locations.append(varinfo['Location'])
            if 'DaughterLocations' in varinfo :
                locations += varinfo['DaughterLocations'].values()
            for loc in locations :
                # The RELINFO functor seems to ignore RootInTES, so give it the full path.
                location = '/'.join([self.root_in_TES(stream)] + self.outputlocation.split('/')[:-1] + [loc])
                for var in varinfo['Variables'] :
                    Variables[loc + '_' + var] = 'RELINFO({0!r}, {1!r}, -999.)'.format(location, var)
        return config

    def _stream(self, stream) :
        if not stream :
            stream = filter(lambda name : name.lower() not in ('ftag.dst', 'mdst.dst'), self.streams)[0]
        return stream
        
    def root_in_TES(self, stream = None) :
        '''Get the RootInTES for reading the output of this line.'''
        stream = self._stream(stream)
        return '/Event' + ('/' + stream[:-5] if stream.lower().endswith('.mdst') else '')

    def input_type(self, stream = None) :
        '''Get the InputType for DaVinci.'''
        stream = self._stream(stream)
        if stream.lower().endswith('.mdst') :
            return 'MDST'
        return 'DST'

    def davinci_config(self, stream = None) :
        '''Get the InputType, DataType, and RootInTES for DaVinci.'''
        return dict(InputType = self.input_type(stream),
                    DataType = self.datatype,
                    RootInTES = self.root_in_TES(stream))

    def decision_name(self) :
        '''Get the decision name in the Stripping SelReports.'''
        return 'Stripping' + self.name + 'Decision'

    def decision_filter_config(self) :
        '''Get the config for a LoKi__HDRFilter on this line.'''
        decname = self.decision_name()
        return dict(Code = 'HLT_PASS({0!r})'.format(decname),
                    Location = '/Event/Strip/Phys/DecReports',
                    RootInTES = '/Event')
