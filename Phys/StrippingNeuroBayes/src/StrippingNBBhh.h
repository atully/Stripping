/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef STRIPPINGNBBHH_H
#define STRIPPINGNBBHH_H 1

#include "Kernel/DaVinciHistoAlgorithm.h"
#include "Kernel/FPEGuard.h"

#include <string>

// NeuroBayes (only available)
#ifdef HAVE_NEUROBAYES
#include "NeuroBayesExpert.hh"
#include "nb_param.hh"
#endif

/** @class StrippingNBBhh.cpp StrippingNBBhh.h
 *
 *
 *  @author Ulrich Kerzel
 *  @date   2011-01-25
 */
class StrippingNBBhh : public DaVinciHistoAlgorithm
{

public:

  /// Standard constructor
  StrippingNBBhh( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~StrippingNBBhh( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:

  bool       getInputVar(const LHCb::Particle& particle);

  double     m_NetworkCut;
  bool       m_PlotHisto;

  double     m_PlotMassMin;
  double     m_PlotMassMax;
  int        m_PlotNBins;

#ifdef HAVE_NEUROBAYES
  Expert*  m_NBExpert = nullptr;
  float*   m_inArray = nullptr;
#endif

  std::string m_ExpertiseName;
  std::string m_netVersion;

};

#endif // STRIPPINGNBBHH_H
