###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: StrippingConf
################################################################################
gaudi_subdir(StrippingConf v4r27p1)

gaudi_depends_on_subdirs(GaudiAlg
                         Kernel/SelectionLine
                         Phys/DaVinciKernel
                         Phys/LoKiCore
                         Phys/SelPy
                         Phys/StrippingAlgs)

find_package(AIDA)
find_package(Boost)

gaudi_install_python_modules()


gaudi_add_test(QMTest QMTEST)
