###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ['Xabier Cid Vidal', 'Carlos Vazquez Sierra', 'Igor Kostiuk']
__date__   = '27/01/2019'

__all__ = ('GluinosJetsConf', 'default_config')

from Gaudi.Configuration import *
from StandardParticles import StdJets
from PhysSelPython.Wrappers import FilterSelection, PassThroughSelection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import GeV
from LoKiPhys.functions import SOURCE, SIZE
from CommonParticles.Utils import *
from Configurables import FilterDesktop, JetVertexAlg

default_config = {
    'NAME'              : 'GluinosJets',
    'WGs'               : ['QEE'],
    'BUILDERTYPE'       : 'GluinosJetsConf',
    'CONFIG'            : { 'JET_PT'                   : 20*GeV 
                          , 'MIN_IPCHI2'               : 25
                          , 'DG_NDISPLONG'             : 1
                          , 'HF_NDISPLONG'             : 3 
                          , 'HF_GDAUGS'                : 5
                          , 'DisplacedGluinoPrescale'  : 1.
                          , 'SixLightPrescale'         : 1.
                          , 'TwoLightOneHFPrescale'    : 1.
                          , 'RawBanks'                 : ['Calo', 'Trigger', 'Velo', 'Tracker']
                          },
    'STREAMS'             : [ 'BhadronCompleteEvent' ]             
    }

### Lines stored in this file:  
# StrippingGluinosJetsSixJetsLine
# StrippingGluinosJetsDisplacedGluinoLine
# StrippingGluinosJetsThreeJetsOneHFLine

class GluinosJetsConf(LineBuilder) :
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        ### Define the jet cuts:

        _displacedGluinoCuts = ("(PT > %(JET_PT)s) "
                                "& (NINTREE(ISBASIC & HASTRACK & ISLONG & (%(MIN_IPCHI2)s < MIPCHI2DV(PRIMARY))) > %(DG_NDISPLONG)s)") % self.config

        _heavyFlavourCuts  = ("(PT > %(JET_PT)s) "
                              "& (NINTREE(ISBASIC & HASTRACK & ISLONG & (%(MIN_IPCHI2)s < MIPCHI2DV(PRIMARY))) > %(HF_NDISPLONG)s) "
                              "& (SUMTREE( ('D0'==ABSID) & (0.9*mm < abs(VFASPF(VX_BEAMSPOTRHO(6*mm)))), NDAUGS ) >= %(HF_GDAUGS)s)") % self.config

        ## Add daughter vertices to jets:

        jetVertexAlg  = JetVertexAlg( self.name + "jetVertexAlg", 
                                      Input=StdJets.outputLocation(), Output="Phys/StdJetsVtxAlgGluinos/Particles" )
        _StdJetVtxAlg = updateDoD( jetVertexAlg, name="StdJetsVtxAlgGluinos" )

        ## Create the containers:

        displacedGluinoSel     = FilterDesktop( self.name + "displacedGluino", Inputs = [_StdJetVtxAlg.keys()[0]], Code = _displacedGluinoCuts )
        self.displacedGluinoLoc = updateDoD ( displacedGluinoSel )

        heavyFlavourJetsSel        = FilterDesktop( self.name + "heavyFlavourJets", Inputs = [_StdJetVtxAlg.keys()[0]], Code = _heavyFlavourCuts )
        self.heavyFlavourJetsLoc   = updateDoD ( heavyFlavourJetsSel )

        ### Create the container for the candidates to be saved:

        self.looseJets = FilterSelection( self.name + "looseJets", StdJets, Code = "(PT > %(JET_PT)s)" % self.config )

        ### Six light jets line:

        sixJetSelection = self.MultiJetSelection( self.name + "SixJetSelection", self.looseJets, 'SixJets')
        sixJetLine      = StrippingLine( self.name + "SixJetsLine", algos = [sixJetSelection],
                                         RequiredRawEvents = self.config['RawBanks'], prescale = self.config['SixLightPrescale'] )
        self.registerLine(sixJetLine)

        ### Displaced gluino into three light jets:

        dGluinoSelection = self.MultiJetSelection( self.name + "DisplacedGluinoSelection", self.looseJets, 'DisplacedGluino')
        dGluinoLine      = StrippingLine( self.name + "DisplacedGluinoLine", algos = [dGluinoSelection],
                                         RequiredRawEvents = self.config['RawBanks'], prescale = self.config['DisplacedGluinoPrescale'] )
        self.registerLine(dGluinoLine)

        ### Three jets from where at least one heavy flavour jet:

        TwoLightOneHFSelection = self.MultiJetSelection( self.name + "TwoLightOneHFSelection", self.looseJets, 'TwoLightOneHF')
        TwoLightOneHFLine      = StrippingLine( self.name + "ThreeJetsOneHFLine", algos = [TwoLightOneHFSelection],
                                                RequiredRawEvents = self.config['RawBanks'], prescale = self.config['TwoLightOneHFPrescale'] )
        self.registerLine(TwoLightOneHFLine)

    def MultiJetSelection (self, name, input, typeCombination):
        if typeCombination == "SixJets":
          _Preambulo = [ "lightJets = SOURCE ( '%s', ALL ) >> SIZE" % input.outputLocation() ]
          _Code      = "(lightJets >= 6)"
        if typeCombination == "DisplacedGluino":
          _Preambulo = [ "DGluinoJets  = SOURCE ( '%s', ALL ) >> SIZE" % self.displacedGluinoLoc.keys()[0] ]
          _Code      = "(DGluinoJets >= 3)"
        if typeCombination == "TwoLightOneHF":
          _Preambulo = [ "HFJets       = SOURCE ( '%s', ALL ) >> SIZE" % self.heavyFlavourJetsLoc.keys()[0],
                         "lightJets = SOURCE ( '%s', ALL ) >> SIZE" % input.outputLocation() ]
          _Code      = "(lightJets >= 3) & (HFJets >= 1)"

        from GaudiConfUtils.ConfigurableGenerators import LoKi__VoidFilter as _VFilter_
        return PassThroughSelection (name,
                                     Algorithm         = _VFilter_(Code = _Code , Preambulo = _Preambulo) ,
                                     RequiredSelection = input )