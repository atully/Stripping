###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
(1) Bd,s->Jpsi h+ h-, h=pi or K with biased selection
(2) Bd->Jpsi K*(892) similar selection for decay time acceptance
(3) prompt Jpsi h+h- for time resolution [prescaled]
(4) Lb->Jpsi Lambda(1520)0 (p+ K-/pi-) for pentaquark 
'''

__author__ = ['Liming Zhang', 'Xuesong Liu']
__date__ = '10/11/2016'

__all__ = ('B2JpsiHHConf','default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from CommonParticles.Utils import updateDoD
from StandardParticles import StdLoosePions
from StandardParticles import StdLooseKaons
from StandardParticles import StdLooseProtons
from StandardParticles import StdAllLooseKaons
from StandardParticles import StdAllLooseProtons
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV

default_config = {
    'NAME'              : 'B2JpsiHH',
    'WGs'               : ['B2CC'],
    'BUILDERTYPE'       : 'B2JpsiHHConf',
    'CONFIG'            : {
                         'TRCHI2DOF'                 :       5
		     ,	 'DTF_CTAU'			     :       0.2*0.299
                 ,       'JpsiMassWindow'            :       80
                 ,       'VCHI2PDOF'                 :       10
                 ,       'HLTCuts'                   :       "HLT_PASS_RE('Hlt2DiMuonJPsiDecision')"
                 ,       'Bs2Jpsif0Prescale'         :       0.30     #2015(S24r0p1) 2016(S28) 2017(S29) 0.30 # 2016(S26): 0.10, 2011: 0.13, 2012: 0.62
		     ,	 'PROBNNk'			     :       0.05
		     ,	 'PROBNNp'			     :       0.05
		     ,	 'PROBNNpi'			     :       0.05
		     ,	 'PIDKforKaon'		     :       0.
		     ,	 'PIDKforPion'		     :       10.
		     ,	 'PIDpforProton'		     :       0.
		     ,	 'PIDKpforKaon'		     :       -10.
		     ,	 'PIDpKforProton'		     :       -10.
                 ,       'MVACut'                    :       "-0.1"
                 ,       'XmlFile'                   :       "$TMVAWEIGHTSROOT/data/Bs2Jpsif0_BDT_v1r1.xml"
                         },
    'STREAMS'           : {
            'Leptonic' : [
            'StrippingB2JpsiHHBs2Jpsif0PrescaledLine',
            'StrippingB2JpsiHHBs2JpsiKstarLine',
            'StrippingB2JpsiHHLb2JpsipHLine'
            ],
            'Dimuon'   : [
            'StrippingB2JpsiHHBs2Jpsif0Line',
            'StrippingB2JpsiHHBs2Jpsif0KaonLine',
            'StrippingB2JpsiHHBs2Jpsif0wsLine'
            ]
    } 
    }

### Lines stored in this file:

class B2JpsiHHConf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
      LineBuilder.__init__(self, name, config)
      self.name = name
      self.config = config
    
        #### Input selections:

        # ------------------------------------------------------------ J/Psi:
        
      self.WideJpsiList = DataOnDemand(Location = "Phys/StdMassConstrainedJpsi2MuMu/Particles")

      self.DetachedJpsiList = self.createSubSel( OutputList = 'NarrowDetachedJpsiForJpsiHH' + self.name,
                                                 InputList  = self.WideJpsiList,
                                                 Cuts       = "(PFUNA(ADAMASS('J/psi(1S)')) < %(JpsiMassWindow)s * MeV) " %self.config)

        # ------------------------------------------------------------ Hardons:
       
      self.NoIPKaonList = self.createSubSel( OutputList = "PIDKaonsForJpsipK" + self.name,
                                             InputList  = DataOnDemand(Location = "Phys/StdAllLooseKaons/Particles"),
                                             Cuts       = "(TRCHI2DOF < %(TRCHI2DOF)s ) & (((PIDK > %(PIDKforKaon)s ) & ((PIDK-PIDp) > %(PIDKpforKaon)s )) | (PROBNNk > %(PROBNNk)s ))  & (PT > 250.*MeV) & (TRGHOSTPROB < 0.5)" % self.config )
      
      self.NoIPProtonList = self.createSubSel( OutputList = "PIDprotonsForJpsipK" + self.name,
                                               InputList  = DataOnDemand(Location = "Phys/StdAllLooseProtons/Particles"),
                                               Cuts       = "(TRCHI2DOF < %(TRCHI2DOF)s ) &  ( ((PIDp > %(PIDpforProton)s ) & ((PIDp-PIDK) > %(PIDpKforProton)s )) | (PROBNNp > %(PROBNNp)s ) )  & (PT > 250.*MeV) & (TRGHOSTPROB < 0.5)" % self.config )
      
      self.NoIPPionList = self.createSubSel( OutputList = "PIDpionsForJpsippi" + self.name,
                                             InputList  = DataOnDemand(Location = "Phys/StdAllLoosePions/Particles"),
                                             Cuts       = "(TRCHI2DOF < %(TRCHI2DOF)s ) & ( (PIDK < %(PIDKforPion)s ) | (PROBNNpi > %(PROBNNpi)s ) )  & (PT > 250.*MeV) & (TRGHOSTPROB < 0.5)" % self.config )

      # -------------------------------------------------------------- Kstar:     

      self.DetachedKstarList = self.createCombinationSel( OutputList      = "DetachedKstarListForJpsiHH" + self.name,
                                                          DaughterLists   = [ self.NoIPKaonList, self.NoIPPionList ],
                                                          DecayDescriptor =  "[K*(892)0 -> K+ pi-]cc" ,
                                                          PreVertexCuts   = "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 2700 *MeV) & (ADOCACHI2CUT(20., ''))",
#                                                          PreVertexCuts   = "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (ADAMASS('K*(892)0') < 200 *MeV) & (ADOCACHI2CUT(20., ''))",
                                                          PostVertexCuts  = "(VFASPF(VCHI2) < 16)",   
                                                          ReFitPVs         = False ) # Note that this is false to save CPU time, for the future check with Liming if the kaon list is fine

      # -------------------------------------------------------------- f0:     

      self.Detachedf0pipiList = self.createCombinationsSel( OutputList       = "Detachedf02pipiForJpsiHH" + self.name,
                                                        DaughterLists    = [ self.NoIPKaonList, self.NoIPPionList ],
                                                        DecayDescriptors = ["f_0(980) -> pi+ pi-"],
                                                        DaughterCuts     = { "pi+" : " (MIPCHI2DV(PRIMARY)>4) " },
#                                                  DaughterCuts = { "pi+" : " (MIPCHI2DV(PRIMARY)>9) " % self.config,
#                                                                   "K+"  : " (MIPCHI2DV(PRIMARY)>9) " % self.config },
                                                        PreVertexCuts    = "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 2700 *MeV) & (ADOCACHI2CUT(20., ''))",
                                                        PostVertexCuts   = "(VFASPF(VCHI2) < 16) ",
                                                        ReFitPVs         = False ) 

      # -------------------------------------------------------------- f0:     

      self.Detachedf0KKList = self.createCombinationsSel( OutputList       = "Detachedf02KKForJpsiHH" + self.name,
                                                        DaughterLists    = [ self.NoIPKaonList, self.NoIPPionList ],
                                                        DecayDescriptors = [ "f_0(980) -> K+ K-"],
                                                        PreVertexCuts    = "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 2700 *MeV) & (ADOCACHI2CUT(20., ''))",
                                                        PostVertexCuts   = "(VFASPF(VCHI2) < 16) ",
                                                        ReFitPVs         = False ) 

      # -------------------------------------------------------------- f0:     

      self.Detachedf0wspipiList = self.createCombinationsSel( OutputList       = "Detachedf02wspipiForJpsiHH" + self.name,
                                                        DaughterLists    = [ self.NoIPKaonList, self.NoIPPionList ],
                                                        DecayDescriptors = [ "f_0(980) -> pi- pi-", "f_0(980) -> pi+ pi+"],
                                                        DaughterCuts     = { "pi+" : " (MIPCHI2DV(PRIMARY)>4) " },
                                                        PreVertexCuts    = "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 2700 *MeV) & (ADOCACHI2CUT(20., ''))",
                                                        PostVertexCuts   = "(VFASPF(VCHI2) < 16) ",
                                                        ReFitPVs         = False ) 

      self.Unbiasedf0List = self.createCombinationsSel( OutputList       = "Unbiasedf02HHForJpsiHH" + self.name,
                                                        DaughterLists    = [ self.NoIPKaonList, self.NoIPPionList ],
                                                        DecayDescriptors = ["f_0(980) -> pi+ pi-", "f_0(980) -> K+ K-"],
                                                        PreVertexCuts    = "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 2700 *MeV) & (ADOCACHI2CUT(20., ''))",
                                                        PostVertexCuts   = "(VFASPF(VCHI2) < 16)",
                                                        ReFitPVs         = False ) 

      # -------------------------------------------------------------- Lambda0:     

      self.Lambda0List = self.createCombinationsSel( OutputList       = "Lambda02pHForJpsiHH" + self.name,
                                                     DaughterLists    = [ self.NoIPKaonList, self.NoIPPionList, self.NoIPProtonList ],
                                                     DecayDescriptors = ["[Lambda(1520)0 -> p+ K-]cc", "[Lambda(1520)0 -> p+ K+]cc", "[Lambda(1520)0 -> p+ pi-]cc", "[Lambda(1520)0 -> p+ pi+]cc"],
#                                                     DaughterCuts     = { "p+" : " (MIPCHI2DV(PRIMARY)>4) ", "K-" : " (MIPCHI2DV(PRIMARY)>4) " },
                                                     DaughterCuts     = { "p+" : " (MIPCHI2DV(PRIMARY)>4) ", "K-" : " (MIPCHI2DV(PRIMARY)>4) ", "pi-" : " (MIPCHI2DV(PRIMARY)>4) " },
                                                     PreVertexCuts    = "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 3100 *MeV) & (ADOCACHI2CUT(20., ''))",
                                                     PostVertexCuts   = "(VFASPF(VCHI2) < 16)",
                                                     ReFitPVs         = False )  

      # -------------------------------------------------------------- Training variables:

      self.Vars = {
            'BCHI2'               :    "VFASPF(VCHI2)",
            'BPT'                 :    "PT",
            'SumPT'               :    "CHILD(PT,2,1)+CHILD(PT,2,2)",
            'DIRA'                :    "BPVDIRA",
            'BIPCHI2'             :    "BPVIPCHI2()"
        }

      self.makeBs2Jpsif0()
      self.makeBs2JpsiKstar()  
      self.makeLb2JpsipH()

    ### Stripping lines:

    # ------------------------- Bs->J/Psif0:

    def makeBs2Jpsif0( self ):
      Bs2Jpsif0 = self.createCombinationSel( OutputList      = "Bs2Jpsif0" + self.name,
                                             DecayDescriptor = "B_s0 -> J/psi(1S) f_0(980)",
                                             DaughterLists   = [ self.DetachedJpsiList, self.Detachedf0pipiList],
                                             PreVertexCuts   = "in_range(5100,AM,5720)",
                                             PostVertexCuts  = "in_range(5150,M,5670) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s ) & (BPVIPCHI2()<25.0) & (DTF_CTAU(0,True) > %(DTF_CTAU)s ) & (BPVDIRA >0.999) " % self.config ) # for the other particles is 10.
#                                             PostVertexCuts  = "in_range(5150,M,5670) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s ) & (BPVIPCHI2()<25.0) & (BPVLTIME() > 0.2*ps) & (BPVDIRA >0.999) & (BPVVD > 1.5 *mm)" % self.config ) # for the other particles is 10.
      
      MvaBs2Jpsif0 = self.applyMVA( "MvaBs2Jpsif0" + self.name,
                                    SelB            = Bs2Jpsif0,
                                    MVAVars         = self.Vars,
                                    MVACutValue     = "%(MVACut)s" % self.config, 
                                    MVAxmlFile      = "%(XmlFile)s" % self.config )

      Bs2Jpsif0Line  = StrippingLine( self.name + "Bs2Jpsif0Line", algos = [ MvaBs2Jpsif0 ])
        
      Bs2Jpsif0KK = self.createCombinationSel( OutputList      = "Bs2Jpsif0KK" + self.name,
                                             DecayDescriptor = "B_s0 -> J/psi(1S) f_0(980)",
                                             DaughterLists   = [ self.DetachedJpsiList, self.Detachedf0KKList],
                                             PreVertexCuts   = "in_range(5100,AM,5720)",
                                             PostVertexCuts  = "in_range(5150,M,5670) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s ) & (BPVIPCHI2()<25.0) & (DTF_CTAU(0,True) > %(DTF_CTAU)s ) & (BPVDIRA >0.999)" % self.config ) # for the other particles is 10.
      
      MvaBs2Jpsif0KK = self.applyMVA( "MvaBs2Jpsif0KK" + self.name,
                                    SelB            = Bs2Jpsif0KK,
                                    MVAVars         = self.Vars,
                                    MVACutValue     = "%(MVACut)s" % self.config, 
                                    MVAxmlFile      = "%(XmlFile)s" % self.config )

      Bs2Jpsif0KaonLine  = StrippingLine( self.name + "Bs2Jpsif0KaonLine", algos = [ MvaBs2Jpsif0KK ])
        
      Bs2Jpsif0ws = self.createCombinationSel( OutputList      = "Bs2Jpsif0ws" + self.name,
                                             DecayDescriptor = "B_s0 -> J/psi(1S) f_0(980)",
                                             DaughterLists   = [ self.DetachedJpsiList, self.Detachedf0wspipiList],
                                             PreVertexCuts   = "in_range(5100,AM,5720)",
                                             PostVertexCuts  = "in_range(5150,M,5670) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s ) & (BPVIPCHI2()<25.0) & (DTF_CTAU(0,True) > %(DTF_CTAU)s ) & (BPVDIRA >0.999) " % self.config ) # for the other particles is 10.
#                                             PostVertexCuts  = "in_range(5150,M,5670) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s ) & (BPVIPCHI2()<25.0) & (BPVLTIME() > 0.2*ps) & (BPVDIRA >0.999) & (BPVVD > 1.5 *mm)" % self.config ) # for the other particles is 10.
      
      MvaBs2Jpsif0ws = self.applyMVA( "MvaBs2Jpsif0ws" + self.name,
                                    SelB            = Bs2Jpsif0ws,
                                    MVAVars         = self.Vars,
                                    MVACutValue     = "%(MVACut)s" % self.config, 
                                    MVAxmlFile      = "%(XmlFile)s" % self.config )

      Bs2Jpsif0wsLine  = StrippingLine( self.name + "Bs2Jpsif0wsLine", algos = [ MvaBs2Jpsif0ws ])
        
      Bs2Jpsif0Prescaled = self.createCombinationSel( OutputList      = "Bs2Jpsif0Prescale" + self.name,
                                                      DecayDescriptor = "B_s0 -> J/psi(1S) f_0(980)",
                                                      DaughterLists   = [ self.WideJpsiList, self.Unbiasedf0List ],
                                                      PreVertexCuts   = "in_range(5170,AM,5470)",
                                                      PostVertexCuts  = "(VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s)" % self.config ) # for the other particles is 10.

      Bs2Jpsif0PrescaledLine  = StrippingLine( self.name + "Bs2Jpsif0PrescaledLine", algos = [ Bs2Jpsif0Prescaled ], EnableFlavourTagging = False, HLT2 = self.config['HLTCuts'], prescale = self.config["Bs2Jpsif0Prescale"]  )#, MDSTFlag = True )

      self.registerLine(Bs2Jpsif0Line)
      self.registerLine(Bs2Jpsif0KaonLine)
      self.registerLine(Bs2Jpsif0wsLine)
      self.registerLine(Bs2Jpsif0PrescaledLine)

    # -------------------------- Bs->J/PsiK*:

    def makeBs2JpsiKstar( self ):
        Bs2JpsiKstar = self.createCombinationSel( OutputList      = "Bs2JpsiKstar" + self.name,
                                                  DecayDescriptor = "[B_s~0 -> J/psi(1S) K*(892)0]cc",
                                                  DaughterLists   = [ self.DetachedJpsiList, self.DetachedKstarList ],
                                                  PreVertexCuts   = "in_range(4900,AM,5600)",
                                                  PostVertexCuts  = "in_range(5000,M,5500) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s) & (BPVIPCHI2()<25.0) & (DTF_CTAU(0,True) > %(DTF_CTAU)s ) & (BPVDIRA >0.999)" %self.config )

        MvaBs2JpsiKstar = self.applyMVA( "MvaBs2JpsiKstar" + self.name,
                                         SelB            = Bs2JpsiKstar,
                                         MVAVars         = self.Vars,
                                         MVACutValue     = "%(MVACut)s" % self.config, 
                                         MVAxmlFile      = "%(XmlFile)s" % self.config )

        Bs2JpsiKstarLine = StrippingLine( self.name + "Bs2JpsiKstarLine", algos = [ MvaBs2JpsiKstar ], EnableFlavourTagging = True )

        self.registerLine(Bs2JpsiKstarLine)

    # -------------------------- Lb->J/PsipH:

    def makeLb2JpsipH( self ):
        Lb2JpsipH = self.createCombinationSel( OutputList      = "Lb2JpsipK" + self.name,
                                                   DecayDescriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0 ]cc",
                                                   DaughterLists   = [ self.DetachedJpsiList, self.Lambda0List ],
                                                   PreVertexCuts   = "in_range(5020,AM,6220)",
                                                   PostVertexCuts  = "in_range(5120,M,6120) & (VFASPF(VCHI2PDOF) < %(VCHI2PDOF)s) & (BPVIPCHI2()<25.0) & (DTF_CTAU(0,True) > %(DTF_CTAU)s ) & (BPVDIRA >0.999)" %self.config )

        MvaLb2JpsipH = self.applyMVA( "MvaLb2JpsiLambda" + self.name,
                                          SelB            = Lb2JpsipH,
                                          MVAVars         = self.Vars,
                                          MVACutValue     = "%(MVACut)s" % self.config, 
                                          MVAxmlFile      = "%(XmlFile)s" % self.config )

        Lb2JpsipHLine = StrippingLine( self.name + "Lb2JpsipHLine", algos = [ MvaLb2JpsipH ], EnableFlavourTagging = True )

        self.registerLine(Lb2JpsipHLine)

    ### Common tools:

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = ReFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
        
    def createCombinationsSel( self, OutputList,
                          DecayDescriptors,
                          DaughterLists,
                          DaughterCuts = {} ,
                          PreVertexCuts = "ALL",
                          PostVertexCuts = "ALL",
                          ReFitPVs = True ) :
        '''For taking in multiple decay descriptors'''
        combiner = CombineParticles( DecayDescriptors = DecayDescriptors,
                                 DaughtersCuts = DaughterCuts,
                                 MotherCut = PostVertexCuts,
                                 CombinationCut = PreVertexCuts,
                                 ReFitPVs = ReFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def applyMVA(self, name,
             SelB,
             MVAVars,
             MVAxmlFile,
             MVACutValue) :
      from MVADictHelpers import addTMVAclassifierValue
      from Configurables import FilterDesktop as MVAFilterDesktop

      _FilterB = MVAFilterDesktop( name + "Filter",
                                   Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue )

      addTMVAclassifierValue( Component  = _FilterB,
                              XMLFile    = MVAxmlFile,
                              Variables  = MVAVars,
                              ToolName   = name )

      return Selection( name,
                  Algorithm = _FilterB,
                  RequiredSelections = [ SelB ] )

    def filterTisTos( self, name, DiMuonInput, myTisTosSpecs ) :
        from Configurables import TisTosParticleTagger
    
        myTagger = TisTosParticleTagger(name + "_TisTosTagger")
        myTagger.TisTosSpecs = myTisTosSpecs
        
        # To speed it up, TisTos only with tracking system:
        myTagger.ProjectTracksToCalo = False
        myTagger.CaloClustForCharged = False
        myTagger.CaloClustForNeutral = False
        myTagger.TOSFrac = { 4:0.0, 5:0.0 }
        
        return Selection(name + "_SelTisTos",
                     Algorithm = myTagger,
                     RequiredSelections = [ DiMuonInput ] )

