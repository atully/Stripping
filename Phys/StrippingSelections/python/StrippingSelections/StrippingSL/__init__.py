###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module importing stripping selection line builder modules
for SL WG.
"""

_selections = ['StrippingB2DMuNuX',
               #'StrippingJPsiForSL',
               #'StrippingB2DHForTauMu',
               #'StrippingB2DMuForTauMu',
               #'StrippingB2DstMuNuIncl',
               'StrippingB2Dst0MuNu',
               'StrippingB2XTauNu',
               'StrippingB2XuMuNu',
               'StrippingB2XuENu',
               #'StrippingB23MuNu',
               #'StrippingCharmFromBSemiForHadronAsy',
               #'StrippingD0ForBXX',
               #'StrippingLb2pMuNuVub',
               #'Strippingbhad2PMuX',
               'StrippingB2PPbarMuForTauMu',
               #'StrippingB2Dlnu',
               'StrippingLb2LcMuNuX_Lc2L0Pi'
               #'StrippingTrackEffD0ToK3Pi
               ]

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]

