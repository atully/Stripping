###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = ['Carla Marin', 'Xabier Cid Vidal',  'Adrian Casais Vidal']
__date__ = '23/02/2018'
__version__ = '$2.0 $'

'''
    stripping code for Kshort --> eePiPi
'''

default_config =  {
    'NAME'        : 'Kshort2eePiPi',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'Kshort2eePiPiConf',
    'CONFIG'      : {   'Kshort2eePiPi_eeFromTracksLinePrescale'       : 1 ,
                        'Kshort2eePiPi_eeFromTracksLinePostscale'      : 1 ,
                        'Kshort2eePiPi_eeFromTracks_TOSLinePrescale'   : 1 ,
                        'Kshort2eePiPi_eeFromTracks_TOSLinePostscale'  : 1 ,
                        'Kshort2eePiPi_UULinePrescale'                : 1 ,
                        'Kshort2eePiPi_UULinePostscale'               : 1 ,
                        'Kshort2eePiPi_VVLinePrescale'                : 1 ,
                        'Kshort2eePiPi_VVLinePostscale'               : 1 ,
                        'Kshort2eePiPi_LULinePrescale'                : 1 ,
                        'Kshort2eePiPi_LULinePostscale'               : 1 ,
                        'Kshort2eePiPi_LVLinePrescale'                : 1 ,
                        'Kshort2eePiPi_LVLinePostscale'               : 1 ,
                        'Kshort2eePiPi_UVLinePrescale'                : 1 ,
                        'Kshort2eePiPi_UVLinePostscale'               : 1 ,
                        
                        'ePT'           : 100.          , #MeV
                        'eMINIPCHI2'    : 16            , #adimensional
                        'ePIDe'         : -4            , #adimensional
                        'eGhostProb'    : 0.5           , #adimensional
                        'PionPT'        : 100           , #MeV
                        'PionMINIPCHI2' : 16            , #adimensional
                        'PionPIDK'      : 5             , #adimensional
                        'PionGhostProb' : 0.5           , #adimensional    
                        'KsMAXDOCA'     : 1.            , #mm
                        'KsLifetime'    : 0.01*89.53    , #0.01*10^-12s
                        'KsIP'          : 1             , #mm
                        'MaxKsMass'     : 800.          , #MeV, comb mass high limit
                        'KsVtxChi2'     : 50            ,


                        # TOS line
                        'ePT_soft'             : 50.           , #MeV # 80 at Hlt2
                        'eMINIPCHI2_soft'      : 0.            , #adimensional # no cut at Hlt2
                        'ePIDe_soft'           : -100          , #adimensional # -2 at Hlt2
                        'eGhostProb_soft'      : 0.5           , #adimensional
                        'PionPT_soft'          : 400.          , #MeV # 500 at Hlt2
                        'PionMINIPCHI2_soft'   : 4.            , #adimensional # 9 at Hlt2
                        'PionPIDK_soft'        : 100.          , #adimensional # no cut at Hlt2
                        'PionGhostProb_soft'   : 0.5           , #adimensional
                        'KsMAXDOCA_soft'       : 1.            , #mm # 0.5 at Hlt2
                        'KsLifetime_soft'      : 0.            , #s # no cut at Hlt2
                        'KsIP_soft'            : 1.            , #mm # 0.5 at Hlt2
                        'MaxKsMass_soft'       : 650.          , #MeV # 550 at Hlt2
                        'KsVtxChi2_soft'       : 25.           , # 16 at Hlt2

                        ## LONG tracks
                        'ePT_L'                : 100.          ,#MeV                        
                        'eMINIPCHI2_L'         : 50            ,#adimensional
                        'ePIDe_L'              : -3.5          ,#adimensional
                        'eGhostProb_L'         : 0.35          ,#adimensional
                        
                        ## UPSTREAM tracks
                        'ePT_U'                : 50            ,#MeV
                        'eMINIPCHI2_U'         : 10            ,#adimensional
                        'ePIDe_U'              : -3.5          ,#adimensional
                        'eGhostProb_U'         : 0.35          ,#adimensional
                        
                        ## VELO tracks
                        'eMINIPCHI2_V'         : 40            ,#adimensional
                        'eGhostProb_V'         : 0.5           ,#adimensional                      
                        
                        ## UU line
                        'KsMAXDOCA_UU'         : 1.            ,#mm
                        'KsDisChi2_UU'         : 2500          ,#adimensional
                        'KsIP_UU'              : 1             ,#mm
                        'MaxKsMass_UU'         : 800.          ,#MeV, comb mass high limit
                        'KsVtxChi2_UU'         : 35            ,#adimensional

                        ## VV line
                        'KsMAXDOCA_VV'         : 1.            ,#mm
                        'KsDisChi2_VV'         : 2500          ,#adimensional
                        'KsIP_VV'              : 8             ,#mm
                        'MaxKsMass_VV'         : 900.          ,#MeV, comb mass high limit
                        'KsVtxChi2_VV'         : 40            ,#adimensional

                        ## LU line
                        'KsMAXDOCA_LU'         : 1.            ,#mm
                        'KsDisChi2_LU'         : 2500          ,#adimensional
                        'KsIP_LU'              : 5             ,#mm
                        'MaxKsMass_LU'         : 800.          ,#MeV, comb mass high limit
                        'KsVtxChi2_LU'         : 25            ,#adimensional
                        
                        ## LV line
                        'KsMAXDOCA_LV'         : .5            ,#mm
                        'KsDisChi2_LV'         : 2500          ,#adimensional
                        'KsIP_LV'              : 10            ,#mm
                        'MaxKsMass_LV'         : 900.          ,#MeV, comb mass high limit
                        'KsVtxChi2_LV'         : 37            ,#adimensional
                        
                        ## UV line
                        'KsMAXDOCA_UV'         : 0.8           ,#mm
                        'KsDisChi2_UV'         : 2000          ,#adimensional
                        'KsIP_UV'              : 15            ,#mm
                        'MaxKsMass_UV'         : 878.          ,#MeV, comb mass high limit
                        'KsVtxChi2_UV'         : 19            ,#adimensional
                                                
                        'TISTOSDict'     : {'Hlt2RareStrangeKsPiPiEETOSDecision%TOS': 0,
                                            },

                    },
    'STREAMS'     : ['Leptonic']
    }

__all__ = ('Kshort2eePiPiConf',
           'default_config'
           )

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays as Combine3Particles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllNoPIDsPions
from StandardParticles import StdAllNoPIDsElectrons
from StandardParticles import StdDiElectronFromTracks
from StandardParticles import StdNoPIDsUpElectrons
from Configurables import ChargedProtoParticleMaker, NoPIDsParticleMaker, DelegatingTrackSelector, CombineParticles, LoKi__VertexFitter
from SelPy.utils import ( UniquelyNamedObject,
                          ClonableObject,
                          SelectionBase )

## VELO PARTICLES SETUP COPIED FROM StrippingTrackEffD0ToK3Pi
class GSWrapper(UniquelyNamedObject,
                ClonableObject,
                SelectionBase) :
    
    def __init__(self, name, sequencer, output, requiredSelections) :
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        SelectionBase.__init__(self,
                               algorithm = sequencer,
                               outputLocation = output,
                               requiredSelections = requiredSelections )
    
##############################

class Kshort2eePiPiConf(LineBuilder) :
    """
    Builder for Kshort --> pi,pi,e,e
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()
                              
    def __init__(self, name, config):

        self.name = name
        LineBuilder.__init__(self, name, config)


        ########################
        ## 0: make VELO particles by hand
        # first make velo protoparticles (not default for VELO Tracks)
        self.makeparts = ChargedProtoParticleMaker(name = "MyVeloProtoParticles"+self.name,
                                                   Inputs = ["Rec/Track/Best"],
                                                   Output = "Rec/ProtoP/MyVeloProtoParticles")
        
        self.makeparts.addTool( DelegatingTrackSelector, name="TrackSelector" )
        self.makeparts.TrackSelector.TrackTypes = ["Velo"]
        
        self.myveloprotos = GSWrapper(name = "MyVeloProtos"+self.name,
                                      sequencer = self.makeparts,
                                      output = "Rec/ProtoP/MyVeloProtoParticles",
                                      requiredSelections =  [])

        #now make the velo particles
        self.particleMaker =  NoPIDsParticleMaker ( name = "veloParticleMaker"+self.name,
                                                    DecayDescriptor = 'Electron' ,
                                                    Particle = 'electron',
                                                    AddBremPhotonTo = [],
                                                    Input = self.myveloprotos.outputLocation())
        
        
        self.StdNoPIDsVeloElectrons = Selection('StdNoPIDsVeloElectrons'+self.name,
                                                Algorithm = self.particleMaker, 
                                                RequiredSelections = [self.myveloprotos],
                                                InputDataSetter=None)
        
        
        # 1 : Make e's for Kshort2eePiPi
        selElecsFromTracksForeePiPi = makeElecsFromTracksForeePiPi("ElecsFromTracksFor"+self.name,
                                                                   StdDiElectronFromTracks,
                                                                   config["ePT"],
                                                                   config["eMINIPCHI2"],
                                                                   config["eGhostProb"],
                                                                   config["ePIDe"]
                                                                   )

        selElecsFromTracksForeePiPiSoft = makeElecsFromTracksForeePiPi("ElecsFromTracksFor"+self.name+"_soft",
                                                                       StdDiElectronFromTracks,
                                                                       config["ePT_soft"],
                                                                       config["eMINIPCHI2_soft"],
                                                                       config["eGhostProb_soft"],
                                                                       config["ePIDe_soft"]
                                                                      )


        selElecsFromLongForeePiPi = makeElecsFromVeloUpLongForeePiPi("ElecsFromLongFor"+self.name,
                                                                     StdAllNoPIDsElectrons,
                                                                     config["ePT_L"],
                                                                     config["eMINIPCHI2_L"],
                                                                     config["eGhostProb_L"],
                                                                     config["ePIDe_L"]
                                                                     )
        
        selElecsFromUpForeePiPi = makeElecsFromVeloUpLongForeePiPi("ElecsFromUpFor"+self.name,
                                                                   StdNoPIDsUpElectrons,
                                                                   config["ePT_U"],
                                                                   config["eMINIPCHI2_U"],
                                                                   config["eGhostProb_U"],
                                                                   config["ePIDe_U"])
        
        selElecsFromVeloForeePiPi = makeElecsFromVeloUpLongForeePiPi("ElecsFromVeloFor"+self.name,
                                                                     self.StdNoPIDsVeloElectrons,
                                                                     0, ## PT
                                                                     config["eMINIPCHI2_V"],
                                                                     config["eGhostProb_V"],
                                                                     -100000.) ## PID
        

        # 2 : Make Pions for Kshort2eePiPi
        selPionsForeePiPi = makePionsForeePiPi("PionsFor"+self.name,
                                               StdAllNoPIDsPions,
                                               config["PionPT"],
                                               config["PionMINIPCHI2"],
                                               config["PionGhostProb"],
                                               config["PionPIDK"]
                                               )

        selPionsForeePiPiSoft = makePionsForeePiPi("PionsFor"+self.name+"_soft",
                                                   StdAllNoPIDsPions,
                                                   config["PionPT_soft"],
                                                   config["PionMINIPCHI2_soft"],
                                                   config["PionGhostProb_soft"],
                                                   config["PionPIDK_soft"]
                                               )
        
        # 3 : Combine
        selKshort2eePiPiFromTracks = self._makeKshort2eePiPi(self.name+"_eeFromTracks",
                                                             "KS0 -> pi+ pi- J/psi(1S)",
                                                             selPionsForeePiPi,
                                                             selElecsFromTracksForeePiPi,
                                                             config["MaxKsMass"],
                                                             config["KsMAXDOCA"],
                                                             config["KsIP"],
                                                             config["KsLifetime"],
                                                             config["KsVtxChi2"]
                                                             )
 
        selKshort2eePiPiFromTracks_forTOS = self._makeKshort2eePiPi(self.name+"_eeFromTracks"+"_forTOS",
                                                                    "KS0 -> pi+ pi- J/psi(1S)",
                                                                    selPionsForeePiPiSoft,
                                                                    selElecsFromTracksForeePiPiSoft,
                                                                    config["MaxKsMass_soft"],
                                                                    config["KsMAXDOCA_soft"],
                                                                    config["KsIP_soft"],
                                                                    config["KsLifetime_soft"],
                                                                    config["KsVtxChi2_soft"]
                                                                    )

        decay_Ks = "KS0 -> pi+ pi- e+ e-"
        decays_Ks = ["KS0 -> pi+ pi- e+ e-","KS0 -> pi+ pi- e- e-","KS0 -> pi+ pi- e+ e+"]
        selKshort2eePiPiUU = self._makeKshort2eePiPi(self.name+"_eeUU",
                                                     decay_Ks,
                                                     selPionsForeePiPi,
                                                     selElecsFromUpForeePiPi,
                                                     config["MaxKsMass_UU"],
                                                     config["KsMAXDOCA_UU"],
                                                     config["KsIP_UU"],
                                                     config["KsDisChi2_UU"],
                                                     config["KsVtxChi2_UU"],
                                                     "( ANUM( ( TRTYPE == 4 ) &  ( ABSID == 'e-' ) ) == 2 )" # 2 UPSTREAM
                                                     )

        selKshort2eePiPiVV = self._makeKshort2eePiPi(self.name+"_eeVV",
                                                     decays_Ks,
                                                     selPionsForeePiPi,
                                                     selElecsFromVeloForeePiPi,
                                                     config["MaxKsMass_VV"],
                                                     config["KsMAXDOCA_VV"],
                                                     config["KsIP_VV"],
                                                     config["KsDisChi2_VV"],
                                                     config["KsVtxChi2_VV"],
                                                     "( ANUM( ( TRTYPE == 1 ) & ( ABSID == 'e-' ) ) == 2 )" # 2 VELO
                                                     )
        
        selKshort2eePiPiLU = self._makeKshort2eePiPi(self.name+"_eeLU",
                                                     decay_Ks,
                                                     selPionsForeePiPi,
                                                     [selElecsFromLongForeePiPi,selElecsFromUpForeePiPi],
                                                     config["MaxKsMass_LU"],
                                                     config["KsMAXDOCA_LU"],
                                                     config["KsIP_LU"],
                                                     config["KsDisChi2_LU"],
                                                     config["KsVtxChi2_LU"],
                                                     "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 4 ) & ( ABSID == 'e-' ) ) == 1 ) )" ## 1 LONG AND 1 UP
                                                     )

        selKshort2eePiPiLV = self._makeKshort2eePiPi(self.name+"_eeLV",
                                                     decays_Ks,
                                                     selPionsForeePiPi,
                                                     [selElecsFromLongForeePiPi,selElecsFromVeloForeePiPi],
                                                     config["MaxKsMass_LV"],
                                                     config["KsMAXDOCA_LV"],
                                                     config["KsIP_LV"],
                                                     config["KsDisChi2_LV"],
                                                     config["KsVtxChi2_LV"],
                                                     "( ( ANUM( ( TRTYPE == 3 ) &  ( ABSID == 'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 1 ) & ( ABSID == 'e-' ) ) == 1 ) )" ## 1 LONG AND 1 VELO
                                                     )

        selKshort2eePiPiUV = self._makeKshort2eePiPi(self.name+"_eeUV",
                                                     decays_Ks,
                                                     selPionsForeePiPi,
                                                     [selElecsFromVeloForeePiPi,selElecsFromUpForeePiPi],
                                                     config["MaxKsMass_UV"],
                                                     config["KsMAXDOCA_UV"],
                                                     config["KsIP_UV"],
                                                     config["KsDisChi2_UV"],
                                                     config["KsVtxChi2_UV"],
                                                     "( ( ANUM( ( TRTYPE == 4 ) &  ( ABSID ==  'e-' ) ) == 1 ) & ( ANUM( ( TRTYPE == 1 ) & ( ABSID == 'e-' ) ) == 1 ) )" ## 1 VELO AND 1 UP
                                                     )


        # 3b : trigger filter
        selKshort2eePiPiFromTracks_TOS = tisTosSelection(self.name+"_eeFromTracks"+"_TOS",
                                                         selKshort2eePiPiFromTracks_forTOS,
                                                         config['TISTOSDict'])
        # 4 : Declare Lines
        self.eePiPiFromTracksLine = StrippingLine(self.name+"_eeFromTracks"+"Line",
                                                  prescale  = config['Kshort2eePiPi_eeFromTracksLinePrescale'],
                                                  postscale = config['Kshort2eePiPi_eeFromTracksLinePostscale'],
                                                  selection = selKshort2eePiPiFromTracks,
                                                  MDSTFlag  = False
                                                  )

        self.eePiPiFromTracksTOSLine = StrippingLine(self.name+"_eeFromTracks"+"_TOSLine",
                                                     prescale  = config['Kshort2eePiPi_eeFromTracks_TOSLinePrescale'],
                                                     postscale = config['Kshort2eePiPi_eeFromTracks_TOSLinePostscale'],
                                                     selection = selKshort2eePiPiFromTracks_TOS,
                                                     MDSTFlag  = False
                                                     )

        self.eePiPi_UULine = StrippingLine(self.name+"_UU"+"Line",
                                           prescale  = config['Kshort2eePiPi_UULinePrescale'],
                                           postscale = config['Kshort2eePiPi_UULinePostscale'],
                                           selection = selKshort2eePiPiUU,
                                           MDSTFlag  = False
                                           )

        self.eePiPi_VVLine = StrippingLine(self.name+"_VV"+"Line",
                                           prescale  = config['Kshort2eePiPi_VVLinePrescale'],
                                           postscale = config['Kshort2eePiPi_VVLinePostscale'],
                                           selection = selKshort2eePiPiVV,
                                           MDSTFlag  = False
                                           )

        self.eePiPi_LULine = StrippingLine(self.name+"_LU"+"Line",
                                           prescale  = config['Kshort2eePiPi_LULinePrescale'],
                                           postscale = config['Kshort2eePiPi_LULinePostscale'],
                                           selection = selKshort2eePiPiLU,
                                           MDSTFlag  = False
                                           )

        self.eePiPi_LVLine = StrippingLine(self.name+"_LV"+"Line",
                                           prescale  = config['Kshort2eePiPi_LVLinePrescale'],
                                           postscale = config['Kshort2eePiPi_LVLinePostscale'],
                                           selection = selKshort2eePiPiLV,
                                           MDSTFlag  = False
                                           )
        
        self.eePiPi_UVLine = StrippingLine(self.name+"_UV"+"Line",
                                           prescale  = config['Kshort2eePiPi_UVLinePrescale'],
                                           postscale = config['Kshort2eePiPi_UVLinePostscale'],
                                           selection = selKshort2eePiPiUV,
                                           MDSTFlag  = False
                                           )

        # 5 : register Line
        self.registerLine( self.eePiPiFromTracksLine )
        self.registerLine( self.eePiPiFromTracksTOSLine )
        self.registerLine( self.eePiPi_UULine)
        self.registerLine( self.eePiPi_VVLine)
        self.registerLine( self.eePiPi_LULine)
        self.registerLine( self.eePiPi_LVLine)
        self.registerLine( self.eePiPi_UVLine)


#####################################################
    def _makeKshort2eePiPi(self, name, decay, pionSel, elecSel, MaxKsMass, KsMAXDOCA, KsIP, KsLifetime, KsVtxChi2, trackComb = 0):
        """
        Handy interface for Kshort2eePiPi
        """

        if trackComb:
            return makeKshort2eePiPiUpVelo(name,
                                           decay,
                                           pionSel,
                                           elecSel,
                                           MaxKsMass,
                                           KsMAXDOCA,
                                           KsIP,
                                           KsLifetime, ## this is really KsDisChi2
                                           KsVtxChi2,
                                           trackComb
                                           )
        else:
            return makeKshort2eePiPi(name,
                                     decay,
                                     pionSel,
                                     elecSel,
                                     MaxKsMass,
                                     KsMAXDOCA,
                                     KsIP,
                                     KsLifetime,
                                     KsVtxChi2
                                     )

            
#####################################################
def makeKshort2eePiPi(name, decay, pionSel, elecSel, MaxKsMass, KsMAXDOCA, KsIP, KsLifetime, KsVtxChi2):
    """
    Makes the KS0 -> pi+ pi- (J/psi(1S) -> e+ e-)
    """

    _comb12cut =    "(AMAXDOCA('') < %(KsMAXDOCA)s *mm)" % locals()
    
    _combcut =      "(AM < %(MaxKsMass)s *MeV) & "\
                    "(AMAXDOCA('') < %(KsMAXDOCA)s *mm)" % locals()
    
    
    _mothercut =    "(M < %(MaxKsMass)s *MeV) &"\
                    "(MIPDV(PRIMARY) < %(KsIP)s *mm) & "\
                    "((BPVVDSIGN*M/P) > %(KsLifetime)s*2.9979e-01) & "\
                    "(VFASPF(VCHI2/VDOF) < %(KsVtxChi2)s) " % locals()

    _Combine = Combine3Particles(DecayDescriptor=decay,
                                 Combination12Cut=_comb12cut,
                                 CombinationCut=_combcut,
                                 MotherCut=_mothercut
                                 )

    return Selection(name,
                     Algorithm=_Combine,
                     RequiredSelections=[elecSel,pionSel]
                     )


def makeKshort2eePiPiUpVelo(name, decay, pionSel, elecSel, MaxKsMass, KsMAXDOCA, KsIP, KsDisChi2, KsVtxChi2, trackComb):
    """
    Makes the KS0 -> pi+ pi- e+ e-)
    """
    
    _combcut =      "(AM < %(MaxKsMass)s *MeV) & "\
                    "(AMAXDOCA('') < %(KsMAXDOCA)s *mm) &" % locals()
    _combcut += trackComb
    
    _mothercut =    "( M < %(MaxKsMass)s *MeV) &"\
                    "( MIPDV(PRIMARY) < %(KsIP)s *mm) & "\
                    "( BPVVDCHI2 > %(KsDisChi2)s) & "\
                    "( VFASPF(VCHI2/VDOF) < %(KsVtxChi2)s) " % locals()

    ## then, there is a V TRACK
    if type(decay)==list:
        _Combine = CombineParticles(name = "Combine"+name,
                                    DecayDescriptors=decay,
                                    CombinationCut=_combcut,
                                    MotherCut=_mothercut
                                    )
        _Combine.ParticleCombiners = {"" : "LoKi::VertexFitter"}
        _Combine.addTool( LoKi__VertexFitter, name="LoKi::VertexFitter" )
    
    else:
        _Combine = CombineParticles(name = "Combine"+name,
                                    DecayDescriptor=decay,
                                    CombinationCut=_combcut,
                                    MotherCut=_mothercut
                                    )
    
    if type(elecSel)==list:
        mysels = elecSel
        mysels.append(pionSel)
    else: mysels = [elecSel,pionSel]

    return Selection(name,
                     Algorithm=_Combine,
                     RequiredSelections=mysels
                     )

#####################################################
def makeElecsFromTracksForeePiPi(name, elecs, ePT, eMINIPCHI2, eGhostProb, ePIDe):
    """
    Electron selection from StdDiElectronFromTracks
    """

    _code = "(MINTREE(ABSID<14,PT) > %(ePT)s) &"\
            "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(eMINIPCHI2)s) &"\
            "(MAXTREE(ABSID<14,TRGHOSTPROB) < %(eGhostProb)s) &"\
            "(MINTREE(ABSID<14,PIDe) > %(ePIDe)s)"  % locals()

    _Filter = FilterDesktop(Code = _code)

    return Selection(name,
                     Algorithm = _Filter,
                     RequiredSelections = [ elecs ]
                     )


def makeElecsFromVeloUpLongForeePiPi(name, elecs, ePT, eMINIPCHI2, eGhostProb , ePIDe):
        """
        Electron selection from ElectronFromTracks
        """
        
        _code = "( MIPCHI2DV(PRIMARY) > %(eMINIPCHI2)s ) &"\
                "( PT > %(ePT)s ) &"\
                "( TRGHOSTPROB < %(eGhostProb)s ) & "\
                "( PIDe > %(ePIDe)s )"  % locals()


        _Filter = FilterDesktop(Code = _code)
        
        return Selection(name,
                         Algorithm = _Filter,
                         RequiredSelections = [ elecs ]
                         )
                

#####################################################
def makePionsForeePiPi(name, pions, PionPT, PionMINIPCHI2, PionGhostProb, PionPIDK):
    """
    Pion selection from StdNoPIDsPions
    """
    _code = "(PT > %(PionPT)s) &"\
            "(MIPCHI2DV(PRIMARY) > %(PionMINIPCHI2)s) &"\
            "(TRGHOSTPROB < %(PionGhostProb)s) &"\
            "(PIDK < %(PionPIDK)s)" % locals()
    
    _Filter = FilterDesktop(Code = _code)
    
    return Selection(name,
                     Algorithm = _Filter,
                     RequiredSelections = [ pions ]
                     )

#####################################################
def tisTosSelection(name, sel, dict_TISTOS):
    '''Filters Selection sel to be TOS OR TIS.'''
    from Configurables import TisTosParticleTagger
    tisTosFilter = TisTosParticleTagger(name+'Algo')
    tisTosFilter.TisTosSpecs = dict_TISTOS

    return Selection(name+'Sel',
                     Algorithm=tisTosFilter,
                     RequiredSelections=[sel])

# EOF
