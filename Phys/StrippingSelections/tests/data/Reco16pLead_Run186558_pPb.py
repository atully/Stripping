###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Wed Apr  5 16:45:41 2017
#-- Contains event types : 
#--   90000000 - 13 files - 1149181 events - 14.55 GBytes


#--  Extra information about the data processing phases:

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000165_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000166_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000167_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000168_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000169_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000170_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000171_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000172_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000173_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000174_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000175_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000176_1.full.dst',
'LFN:/lhcb/LHCb/Protonion16/FULL.DST/00055643/0000/00055643_00000177_1.full.dst'
], clear=True)

from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs = [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco16pLead_Run186558_pPb.xml' ]

