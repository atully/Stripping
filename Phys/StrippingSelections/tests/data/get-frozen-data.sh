#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

freezer=CERN-FREEZER-EOS

function get_lfns() {
    # Take the arguments of dirac-bookkeeping-get-files then echo a 
    # list of lfns with all the other info stripped out. 
    output=$(lb-run LHCbDirac/prod dirac-bookkeeping-get-files $@)
    lfns=""
    for lfn in $(echo "$output" | awk '{print $1;}') ; do
	lfns+="$lfn "
    done
    echo $lfns
}

function get_frozen_files() {
    # Take a list of LFNs and echo a list of those that're replicated
    # to the freezer (defined by the "freezer" variable). 

    # Calling dirac-dms-lfn-replicas for every lfn is pretty slow,
    # but the output for more than one lfn isn't easily parsed.
    cmd="lfns=''
for lfn in $@ ; do
    replicas=\$(dirac-dms-lfn-replicas \$lfn | grep $freezer)
    if [ ! -z \"\$replicas\" ] ; then
        lfns+=\"\$lfn \"
    fi
done
echo \"\$lfns\"
"
    lb-run LHCbDirac/prod bash -c "$cmd"
}

function save_frozen_files() {
    # Takes an output file name and the arguments of dirac-bookkeeping-get-files
    # and saves to the file those that're replicated to the freezer, in the usual
    # python readable format. 
    outputfname=$1
    bkargs=${@:1}
    lfns=$(get_lfns $bkargs)
    echo "# lb-run LHCbDirac/prod dirac-bookkeeping-get-files $bkargs

from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([" > $outputfname
    # Need to use eval else the list of lfns isn't split. 
    for lfn in $(eval "get_frozen_files $lfns") ; do
	echo "'LFN:${lfn}'," >> $outputfname
    done
    echo "], clear=True)
" >> $outputfname
}

if [ $# != 0 ] ; then 
    save_frozen_files $@
else
    echo 'Usage:
./get-frozen-data.sh <outputfname> --BKQuery <path> ... 
following args can be anything accepted by dirac-bookkeeping-get-files'
fi
