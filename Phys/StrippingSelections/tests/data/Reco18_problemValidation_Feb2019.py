###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles(['LFN:/lhcb/LHCb/Collision18/RDST/00079431/0001/00079431_00011340_1.rdst'], clear=True)

from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs += ['xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco18_problemValidation_Feb2019.xml']
