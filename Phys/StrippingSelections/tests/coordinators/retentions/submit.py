###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, sys, string
currentPath = os.getcwd()
DirPath = '/'.join(s.strip('/') for s in currentPath.split('/')[:-5])
mydataset="Reco16_Run182594.py"
mydatasetxml="Reco16_Run182594.xml"
mystripping="stripping26r1"
wg = ''
job_name = 'Test'
if len(sys.argv) > 1 :
  wg = str(sys.argv[1])
  job_name = 'Str26r1_'+str(wg)
print "TEST FOR "+wg+" WORKING GROUP"
ge = GaudiExec()
ge.directory = DirPath
f = open('TestFromSettings_all.py','r')
opts = f.read()
opts = opts.replace('___wg___',wg)
opts = opts.replace('__mystripping__',mystripping)
f.close()
f2 = open('temp.py','w')
f2.write(opts)
f2.close()
ge.options = [LocalFile(namePattern='temp.py')]
j = Job(name = job_name,
        application = ge,
        outputfiles = [LocalFile(namePattern='*.root'),
                       MassStorageFile(namePattern='*.dst'),
                       MassStorageFile(namePattern='*.mdst')],
        inputfiles = [mydatasetxml],
        splitter = SplitByFiles(filesPerJob = 2, maxFiles = 50, ignoremissing = True),
        backend = LSF(queue = '1nd')
)
j.application.readInputData(mydataset)
j.inputdata.XMLCatalogueSlice = mydatasetxml
j.submit()
print "Removing the temp file"
os.system("rm temp.py")
