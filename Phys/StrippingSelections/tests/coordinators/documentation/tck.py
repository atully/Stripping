###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import HltGenConfig, ConfigCDBAccessSvc

confsvc = ConfigCDBAccessSvc( File = 'config.cdb', Mode = 'ReadWrite' )

gen = HltGenConfig( ConfigTop = [ "StrippingGlobal" ]
                  , ConfigSvc = [ "ToolSvc" ]
                  , ConfigAccessSvc = confsvc.getName()
                  , HltType = 'Stripping21r1p1'
                  , MooreRelease = 'DAVINCI_v39r1'
                  , Label = '2011 Incremental stripping')

from Configurables import DaVinci
DaVinci().EventPreFilters = [ gen ]

DaVinci().EvtMax = 10000
