#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test to check that all the code in StrippingSelections does not cause breaks
"""

__author__ = 'Stefano Perazzini stefano.perazzini@cern.ch'


import subprocess, sys

if __name__ == '__main__' :

  proc = subprocess.Popen(['python','-c','from StrippingSelections import *'],stdout=subprocess.PIPE)

  out = proc.communicate()[0]

  summary = '\n'
  if out != '': 
    summary += out+'\n'
    wr = sys.stderr.write
  else:          
    summary += 'PASS\n'
    wr = sys.stdout.write

  wr(summary)
