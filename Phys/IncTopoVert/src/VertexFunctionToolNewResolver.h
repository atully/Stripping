/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef VERTEXFUNCTIONTOOLNEWRESOLVER_H
#define VERTEXFUNCTIONTOOLNEWRESOLVER_H 1

// Include files
#include "VertexFunctionTool.h"

/** @class VertexFunctionToolNewResolver VertexFunctionToolNewResolver.h
 *   This tool implements IVertexFunctionTool. See the interface class for a description
 *   of the purpose of these classes.
 *
 *  @author Julien Cogan and Mathieu Perrin-Terrin
 *  @date   2012-10-15
 */


class VertexFunctionToolNewResolver : public VertexFunctionTool{
public:
  /// Standard constructor
  VertexFunctionToolNewResolver( const std::string& type,
                                 const std::string& name,
                                 const IInterface* parent);
  //VertexFunctionToolNewResolver( );


  ~VertexFunctionToolNewResolver( ); ///< Destructor

  bool   areResolved(LHCb::RecVertex & V1,LHCb::RecVertex & V2) override;


protected:

private:

};



#endif // VERTEXFUNCTIONTOOLNEWRESOLVER_H
