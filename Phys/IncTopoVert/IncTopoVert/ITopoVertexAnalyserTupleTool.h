/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef ITOPOVERTEXANALYSERTUPLETOOL_H 
#define ITOPOVERTEXANALYSERTUPLETOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"



/** @class ITopoVertexAnalyserTupleTool ITopoVertexAnalyserTupleTool.h
 *  This class is an interface for the tool analysing the output of 
 *  the IncTopoVertAlg
 *
 *  @author Mathieu Perrin-Terrin
 *  @date   2013-08-09
 */
class GAUDI_API ITopoVertexAnalyserTupleTool : virtual public IAlgTool 
{

public: 

  DeclareInterfaceID(ITopoVertexAnalyserTupleTool, 2, 0);

  ///analyse the vertices at the given tesLocation
  virtual StatusCode analyseVertices( Tuples::Tuple* tuple, 
                                      const std::string& tesLocation ) = 0;

};
#endif // ITOPOVERTEXANALYSERTUPLETOOL_H
