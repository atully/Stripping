/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RELINFOMUONIDPLUS_H
#define RELINFOMUONIDPLUS_H 1
#define MAXMUSTATIONS 5

// Include files
#include "Kernel/IRelatedInfoTool.h"            // Interface
#include "Event/RelatedInfoMap.h"

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "MuonID/IMuonIDTool.h"
#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "Event/RecSummary.h"
#include "Event/ODIN.h"
#include "Event/RichPID.h"
#include "CaloUtils/CaloParticle.h"

// from TMVA
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include "MVADictTools/TMVATransform.h"
#include "Kernel/IParticleDictTool.h"

/** @class RelInfoMuonIDPlus RelInfoMuonIDPlus.h
 *
 *  @author Ricardo Vazquez Gomez, Marianna Fontana, Giacomo Graziani
 *  @date   2016-11-04
 */

using namespace TMVA;
using namespace std;

//class IMuonIDTool;
//class Reader;

class RelInfoMuonIDPlus : public GaudiTool, virtual public IRelatedInfoTool
{

public:

  /// Standard constructor
  RelInfoMuonIDPlus( const std::string& type,
                     const std::string& name,
                     const IInterface* parent);

  LHCb::RelatedInfoMap* getInfo(void) override;

  virtual std::string infoPath(void);

  StatusCode calculateRelatedInfo(const LHCb::Particle*,
                                  const LHCb::Particle*) override;

  virtual ~RelInfoMuonIDPlus(); ///< Destructor

  StatusCode initialize() override;

private:
  IMuonIDTool* m_newtool;

  std::vector<std::string> m_variables;
  std::vector<short int> m_keys;
  LHCb::RelatedInfoMap m_map;

  double myBDT;

  // Check if a pure CALO Particle
  inline bool isPureNeutralCalo(const LHCb::Particle* P)const
  {
    LHCb::CaloParticle caloP( (LHCb::Particle*) P );
    return caloP.isPureNeutralCalo();
  }

  float CombDLL2BDT(double x);

  // variables for BDT
  TMVATransform m_tmva ;
  TMVATransform::optmap m_optmap ;
  IParticleDictTool::DICT m_varmap ;
  IParticleDictTool::DICT m_out ;

  float m_fP;
  float m_fPt;
  float m_fnTracks;
  float m_fMuMatch[MAXMUSTATIONS];
  float m_fMuSigma[MAXMUSTATIONS];
  float m_fMuIso[MAXMUSTATIONS];
  float m_fMuTime[MAXMUSTATIONS];
  float m_fChi2Cag;

  float m_fTrChi2Ndof;
  float m_fGhostProb;
  float m_fTrMatchChi2;
  float m_fTlik;
  float m_fTrFitVeloChi2Ndof;
  float m_fTrFitTChi2Ndof;
  float m_fUsedRich1Gas;
  float m_fUsedRich2Gas;
  float m_fAboveKThr;
  float m_fRichDLLmu;
  float m_fRichDLLk;
  float m_fVeloCharge;
  float m_fEcalE;
  float m_fHcalE;

  //options
  std::string m_MuonIDPlusToolName;
  std::string m_transformName;
  std::string m_Weights_BDT_MuonID;
};

#endif // RELINFOMUONIDPLUS_H
