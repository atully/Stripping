/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RELINFOVELOTRACKMATCH_H
#define RELINFOVELOTRACKMATCH_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/IRelatedInfoTool.h"
#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>
#include "Event/RelatedInfoMap.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IDistanceCalculator.h"
//needed for TMVA
#include "Kernel/IParticleDictTool.h"
#include "Event/Particle.h"

/** @class RelInfoVeloTrackMatch RelInfoVeloTrackMatch.h
 *
 * \brief Match Velo track to composite candidate.
 *
 *    m_bdt1 : bdt value
 *
 * Options:
 *
 *   None
 *
 *
 *  @author Andrea Contu
 *  @date   2016-09-22
 *
 */

// class IDVAlgorithm;
// class IDistanceCalculator;
// class IParticleDictTool ;

class RelInfoVeloTrackMatch : public GaudiTool, virtual public IRelatedInfoTool {
public:
    /// Standard constructor
    RelInfoVeloTrackMatch( const std::string& type,
                           const std::string& name,
                           const IInterface* parent );

    StatusCode initialize() override;

    StatusCode calculateRelatedInfo( const LHCb::Particle*,
                                     const LHCb::Particle*) override;

    LHCb::RelatedInfoMap* getInfo(void) override;

    virtual std::string infoPath(void);

    virtual ~RelInfoVeloTrackMatch( ); ///< Destructor

private:

    std::vector<std::string> m_variables;
    LHCb::RelatedInfoMap m_map;
    std::vector<short int> m_keys;
    std::vector<const LHCb::Particle*> m_decayParticles;

    /// Save all the particles in the decay descriptor in a vector
    void saveDecayParticles( const LHCb::Particle *top );
    IDVAlgorithm* m_dva;
    double m_slopediff;
    bool m_momentumcone;
    double m_maxVbestPVdist;
    std::string m_inputTracksLocation;
    LHCb::Tracks* m_inputTracks = nullptr;

};

#endif // RELINFOVELOTRACKMATCH_H
