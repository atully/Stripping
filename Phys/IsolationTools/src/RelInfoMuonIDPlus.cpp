/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/ToolFactory.h"
#include "RelInfoMuonIDPlus.h"
#include "Kernel/RelatedInfoNamed.h"
#include "Event/Particle.h"
// Kernel
#include "GaudiKernel/PhysicalConstants.h"
#include <Kernel/IDistanceCalculator.h>

#include "TMath.h"

#include <TROOT.h>
#include <TObject.h>

#include "Math/Boost.h"
#include "Event/VertexBase.h"
#include "Event/RecVertex.h"
#include "Event/ProtoParticle.h"
#include "Event/Particle.h"
#include "Event/TrackTypes.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/RelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"
#include "Relations/RelationWeighted2D.h"

#include <TMVA/Tools.h>
#include <TMVA/Reader.h>

//#if not defined(__CINT__) || defined(__MAKECINT__)

//#endif

//-----------------------------------------------------------------------------
// Implementation file for class : RelInfoMuonIDPlus
//
// 2016-11-04 : Ricardo Vazquez Gomez, Marianna Fontana, Giacomo Graziani
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_TOOL_FACTORY( RelInfoMuonIDPlus )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RelInfoMuonIDPlus::RelInfoMuonIDPlus( const std::string& type,
                                      const std::string& name,
                                      const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  m_keys.clear();
  
  declareInterface<IRelatedInfoTool>(this);
  declareProperty("MuonIDPlusToolName", m_MuonIDPlusToolName="MuonIDPlusTool");
  declareProperty("MVATransform" , m_transformName="MuonIDBDT", "path/name of the DictTransform tool");
  declareProperty("Weights_BDT_MuonID", m_Weights_BDT_MuonID="MuonTMVA_BDTv3.weights.xml");
  declareProperty("Variables", m_variables, "List of variables to store (store all if empty)");

  declareProperty( "P", m_fP = 1. );
  declareProperty( "PT", m_fPt = 1. );
  declareProperty( "nTracks", m_fnTracks = 1. );
  declareProperty( "MuMatch1", (m_fMuMatch[1]) = 1. );
  declareProperty( "MuMatch2", (m_fMuMatch[2]) = 1. );
  declareProperty( "MuMatch3", (m_fMuMatch[3]) = 1. );
  declareProperty( "MuMatch4", (m_fMuMatch[4]) = 1. );
  declareProperty( "MuSigma1", (m_fMuSigma[1]) = 1. );
  declareProperty( "MuSigma2", (m_fMuSigma[2]) = 1. );
  declareProperty( "MuSigma3", (m_fMuSigma[3]) = 1. );
  declareProperty( "MuSigma4", (m_fMuSigma[4]) = 1. );
  declareProperty( "MuIso1", (m_fMuIso[1]) = 1. );
  declareProperty( "MuIso2", (m_fMuIso[2]) = 1. );
  declareProperty( "MuIso3", (m_fMuIso[3]) = 1. );
  declareProperty( "MuIso4", (m_fMuIso[4]) = 1. );
  declareProperty( "Chi2Cag", m_fChi2Cag = 1. );
  declareProperty( "MuTime1", (m_fMuTime[1]) = 1. );
  declareProperty( "MuTime2", (m_fMuTime[2]) = 1. );
  declareProperty( "MuTime3", (m_fMuTime[3]) = 1. );
  declareProperty( "MuTime4", (m_fMuTime[4]) = 1. );
  declareProperty( "TrChi2Ndof", m_fTrChi2Ndof = 1. );
  declareProperty( "GhostProb", m_fGhostProb = 1. );
  declareProperty( "TrMatchChi2", m_fTrMatchChi2 = 1. );
  declareProperty( "Tlik", m_fTlik = 1. );
  declareProperty( "TrFitVeloChi2Ndof", m_fTrFitVeloChi2Ndof = 1. );
  declareProperty( "TrFitTChi2Ndof", m_fTrFitTChi2Ndof = 1. );
  declareProperty( "UsedRich1Gas", m_fUsedRich1Gas = 1. );
  declareProperty( "UsedRich2Gas", m_fUsedRich2Gas = 1. );
  declareProperty( "AboveKThr", m_fAboveKThr = 1. );
  declareProperty( "RichDLLmu", m_fRichDLLmu = 1. );
  declareProperty( "RichDLLk", m_fRichDLLk = 1. );
  declareProperty( "VeloCharge", m_fVeloCharge = 1. );
  declareProperty( "EcalE", m_fEcalE = 1. );
  declareProperty( "HcalE", m_fHcalE = 1. ); 
}

//=============================================================================
// Destructor
//=============================================================================
RelInfoMuonIDPlus::~RelInfoMuonIDPlus() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode RelInfoMuonIDPlus::initialize(){
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_newtool = tool<IMuonIDTool>(m_MuonIDPlusToolName, this);
  if(!m_newtool) return Error("Couldn't get parent MuonIDPlusTool",StatusCode::FAILURE);

  m_keys.push_back( RelatedInfoNamed::MU_BDT );

  //------------------Initialize TMVA reader A.Shires Method--------------------------------

  m_optmap["Name"] = m_transformName ;
  m_optmap["KeepVars"] = "0" ;
  m_optmap["XMLFile"] = System::getEnv("TMVAWEIGHTSROOT") + "/data/" + m_Weights_BDT_MuonID;
  m_tmva.Init( m_optmap, debug().stream(), msgLevel(MSG::DEBUG) ) ; //

  return sc;
}

float RelInfoMuonIDPlus::CombDLL2BDT(double x) {
  // apply empirical transformation to CombDLL variable to give same probability of BDT for low energy pions
  return (float) (-0.0993107 +
                  0.0165849 * x +
                  -0.000707536 * pow(x,2)  +
                  0.000109344 * pow(x,3)  +
                  6.44315e-06 * pow(x,4)  +
                  -2.26864e-07 * pow(x,5)) ;
}

//=============================================================================
// Fill Extra Info structure
//=============================================================================
StatusCode RelInfoMuonIDPlus::calculateRelatedInfo( const LHCb::Particle* top, 
                                                    const LHCb::Particle* part ){

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Filling particle with ID " << top->particleID().pid() << endmsg;

  if (!part) return Info("RelInfoMuonIDPlus::calculateRelInfo called with null Particle pointer!", StatusCode::SUCCESS);
  if ( !part->isBasicParticle() ) return StatusCode::SUCCESS;
  // no PID info for composite!
  if ( isPureNeutralCalo(part)  ) return StatusCode::SUCCESS;
  // no PID information for calo neutrals

  const LHCb::ProtoParticle* proto = part->proto();
  if (!proto) return Info("RelInfoMuonIDPlus::calculateRelInfo called with null ProtoParticle pointer!",StatusCode::SUCCESS);
  const LHCb::Track *pTrack=proto->track();
  if (!pTrack) return StatusCode::SUCCESS;

  const LHCb::RecSummary *rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default);
  if ( !rS ) {
    rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default,false);
  }

  myBDT = -0.5;
  m_varmap.clear();
  m_map.clear();

  // variables to be used in BDT
  // note that input variables for TMVA Reader must be float for some reason 

  m_fP = pTrack->p();
  m_fPt = pTrack->pt();
  m_fnTracks = (float) ( rS ? rS->info(LHCb::RecSummary::nTracks,-1) : -2) ;

  m_fTrChi2Ndof = (pTrack->nDoF() >0) ? ( pTrack->chi2()/pTrack->nDoF() ) : -1.;
  m_fGhostProb  = pTrack->ghostProbability() ;
  m_fTrMatchChi2 = pTrack->info( LHCb::Track::FitMatchChi2, -1 );
  m_fTlik = pTrack->likelihood();
  m_fTrFitVeloChi2Ndof = ( pTrack->info(LHCb::Track::FitVeloNDoF,0) > 0 ) ?
    ( pTrack->info(LHCb::Track::FitVeloChi2, -1.)/pTrack->info(LHCb::Track::FitVeloNDoF, 0) )
    : -1.;
  m_fTrFitTChi2Ndof = ( pTrack->info(LHCb::Track::FitTNDoF,0) > 0 ) ?
    ( pTrack->info(LHCb::Track::FitTChi2, -1.)/pTrack->info(LHCb::Track::FitTNDoF, 0) )
    : -1.;

  const LHCb::RichPID* richPID = proto->richPID();
  m_fUsedRich1Gas = richPID ? ((float) richPID->usedRich1Gas()) : 0.;
  m_fUsedRich2Gas = richPID ? ((float) richPID->usedRich2Gas()) : 0.;
  m_fAboveKThr = richPID ? ((float) richPID->kaonHypoAboveThres()) : 0.;
  m_fRichDLLmu = proto->info(LHCb::ProtoParticle::RichDLLmu,-1000);
  m_fRichDLLk = proto->info(LHCb::ProtoParticle::RichDLLk,-1000);
  m_fVeloCharge = proto->info(LHCb::ProtoParticle::VeloCharge,-10000.);
  m_fEcalE = proto->info(LHCb::ProtoParticle::CaloEcalE,-10000.);
  m_fHcalE = proto->info(LHCb::ProtoParticle::CaloHcalE,-10000.);

  LHCb::MuonPID *pid = m_newtool->getMuonID(pTrack);

  for (int s=0;s<MAXMUSTATIONS;s++) {
    m_fMuMatch[s] = m_newtool->muonIDPropertyI(pTrack, "match", s);
    if(m_fMuMatch[s] == 2) { 
      m_fMuMatch[s] = 1 + m_newtool->muonIDPropertyI(pTrack, "clusize", s);
    }
    m_fMuSigma[s]=m_newtool->muonIDPropertyD(pTrack, "matchSigma", s);
    m_fMuIso[s]=m_newtool->muonIDPropertyD(pTrack, "iso", s);
    m_fMuTime[s] = m_newtool->muonIDPropertyD(pTrack, "time", s);
  }

  int ndof=0;
  m_fChi2Cag = (150.-0.000001);
  const LHCb::Track *mTrack= (pid ? pid->muonTrack() : NULL);
  if ( mTrack ) {
    ndof=mTrack->nDoF();
    if(ndof>0) m_fChi2Cag = TMath::Min( m_fChi2Cag, (float) (mTrack->chi2()/ndof) );
  }

  m_varmap.insert("P",m_fP);
  m_varmap.insert("PT",m_fPt);
  m_varmap.insert("nTracks",m_fnTracks);
  m_varmap.insert("TrChi2Ndof",m_fTrChi2Ndof);
  m_varmap.insert("GhostProb",m_fGhostProb);
  m_varmap.insert("TrMatchChi2",m_fTrMatchChi2);
  m_varmap.insert("Tlik",m_fTlik);
  m_varmap.insert("TrFitVeloChi2Ndof",m_fTrFitVeloChi2Ndof);
  m_varmap.insert("TrFitTChi2Ndof",m_fTrFitTChi2Ndof);
  m_varmap.insert("UsedRich1Gas",m_fUsedRich1Gas);
  m_varmap.insert("UsedRich2Gas",m_fUsedRich2Gas);
  m_varmap.insert("AboveKThr",m_fAboveKThr);
  m_varmap.insert("RichDLLmu",m_fRichDLLmu);
  m_varmap.insert("RichDLLk",m_fRichDLLk);
  m_varmap.insert("VeloCharge",m_fVeloCharge);
  m_varmap.insert("EcalE",m_fEcalE);
  m_varmap.insert("HcalE",m_fHcalE);
  m_varmap.insert("Chi2Cag",m_fChi2Cag);
  m_varmap.insert("MuMatch[1]",m_fMuMatch[1]);
  m_varmap.insert("MuMatch[2]",m_fMuMatch[2]);
  m_varmap.insert("MuMatch[3]",m_fMuMatch[3]);
  m_varmap.insert("MuMatch[4]",m_fMuMatch[4]);
  m_varmap.insert("MuSigma[1]",m_fMuSigma[1]);
  m_varmap.insert("MuSigma[2]",m_fMuSigma[2]);
  m_varmap.insert("MuSigma[3]",m_fMuSigma[3]);
  m_varmap.insert("MuSigma[4]",m_fMuSigma[4]);
  m_varmap.insert("MuIso[1]",m_fMuIso[1]);
  m_varmap.insert("MuIso[2]",m_fMuIso[2]);
  m_varmap.insert("MuIso[3]",m_fMuIso[3]);
  m_varmap.insert("MuIso[4]",m_fMuIso[4]);
  m_varmap.insert("MuTime[1]",m_fMuTime[1]);
  m_varmap.insert("MuTime[2]",m_fMuTime[2]);
  m_varmap.insert("MuTime[3]",m_fMuTime[3]);
  m_varmap.insert("MuTime[4]",m_fMuTime[4]);

  if ( msgLevel(MSG::DEBUG) ) {
    const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(evtSvc(),LHCb::ODINLocation::Default);
    if ( !odin ) { 
      odin = getIfExists<LHCb::ODIN>(evtSvc(),LHCb::ODINLocation::Default,false); 
    }

    debug() << "Run " << odin->runNumber() << ", Event " << odin->eventNumber() << std::endl;
    debug() << "P = " << m_fP << ", PT = " << m_fPt << ", Ntr = " << int(m_fnTracks+0.01) << std::endl;
    for (int M=2; M<6; M++){
      debug() << "Station M"<< M << ", Match = " << int(m_fMuMatch[M-1]+.01) << ", Sigma = " << m_fMuSigma[M-1] <<
                 ", Iso = " << m_fMuIso[M-1] << ", Time = " << m_fMuTime[M-1] << std::endl;
    }
    debug() << "Chi2Cag = " << m_fChi2Cag << std::endl;
    debug() << "Track chi2/dof " << m_fTrChi2Ndof << ", Velo = " << m_fTrFitVeloChi2Ndof << ", Trk = " << m_fTrFitTChi2Ndof <<
               ", match = " << m_fTrMatchChi2 << ", ghostP = " << m_fGhostProb << ", lik = " << m_fTlik << std::endl;
    debug() << "Rich use gas 1 " << (int)(m_fUsedRich1Gas+.001) << ", gas2 " << (int)(m_fUsedRich2Gas+.001) << ", above Kthr " << (int)(m_fAboveKThr+.001) <<
               " DLLmu = " << m_fRichDLLmu << ", DLLk = " << m_fRichDLLk << std::endl;
    debug() << "Velo Charge = " << m_fVeloCharge << ", Ecal E = " << m_fEcalE << ", Hcal E = " << m_fHcalE << std::endl; 
  } 
 
  if ( m_fP < 3.*Gaudi::Units::GeV ) { // BDT not valid in this region, use CombDLLMu with appropriate transformation
    double CombDLLMu = TMath::Max(proto->info(LHCb::ProtoParticle::CombDLLmu,-1000), -17.+0.000001);
    myBDT = CombDLL2BDT(CombDLLMu);
  }
  else { // BDT can be computed
    m_tmva(m_varmap,m_out);
    myBDT = TMath::Max(-0.5, m_out[m_transformName]);
  }
  if ( msgLevel(MSG::DEBUG) ) debug() << "MuIDFiCa says " << myBDT << std::endl;

  // fill related info
  std::vector<short int>::const_iterator ikey;
  for (ikey = m_keys.begin(); ikey != m_keys.end(); ikey++) {
    float value = 0;
    switch (*ikey) {
      case  RelatedInfoNamed::MU_BDT : value = myBDT; break;
    }
    m_map.insert( std::make_pair( *ikey, value) );
  } 

  return StatusCode::SUCCESS;
}

LHCb::RelatedInfoMap* RelInfoMuonIDPlus::getInfo(void) {
    return &m_map;
}

std::string RelInfoMuonIDPlus::infoPath(void){
  std::stringstream ss;
  ss << std::string("ParticleMuonIDPlusRelations");
  return ss.str();
}
