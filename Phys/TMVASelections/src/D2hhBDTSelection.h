/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef D2HHBDTSELECTION_H
#define D2HHBDTSELECTION_H 1

#include <vector>
// Include files
// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiAlg/GaudiTool.h"

#include "Kernel/IParticleFilter.h"

#include "TMVA/Reader.h"
#include "TString.h"

#include "LoKi/LoKiPhys.h"
#include "LoKi/Child.h"
#include "LoKi/ParticleCuts.h"
#include "LoKi/ParticleContextCuts.h"
#include "LoKi/CoreCuts.h"

class IDistanceCalculator;
class DVAlgorithm;

/** @class D2HHBDTSelection D2hhBDTSelection.h
 *
 *
 *  @author Stefano Perazzini
 *  @date   2012-11-11
 */
class D2hhBDTSelection : public extends1<GaudiTool,IParticleFilter>{

  friend class ToolFactory<D2hhBDTSelection>;

public:
  /** initialize tool */
  StatusCode initialize() override;


  /** finalize tool */
  StatusCode finalize() override;


  /**
   *  @see IParticleFilter
   */
  bool operator()(const LHCb::Particle* p) const override;

protected:
  /// Standard constructor
  D2hhBDTSelection( const std::string& type,
                          const std::string& name,
                          const IInterface* parent);

  virtual ~D2hhBDTSelection( ); ///< Destructor

private:

  /// default constructor is disabled
  D2hhBDTSelection();

  /// copy constructor is disabled
  D2hhBDTSelection(const D2hhBDTSelection&);

  /// assignemet operator is disabled
  D2hhBDTSelection& operator=(const D2hhBDTSelection&);

  bool set(const LHCb::Particle* p) const;

private:

  const IDistanceCalculator* m_dist;
  IDVAlgorithm *m_dva;
  TMVA::Reader *m_BDTReader;

  unsigned int m_nVars;
  float *m_values;

  double m_cut; // BDT cut value
  std::string m_weightsFile; // weights file

  const LoKi::Cuts::MIPDV     m_MIPDV;
  const LoKi::Cuts::BPVIP     m_BPVIP;
  const LoKi::Cuts::VD        m_BPVVD;

};
#endif // D2HHBDTSELECTION_H
