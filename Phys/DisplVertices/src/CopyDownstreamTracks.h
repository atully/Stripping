/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
// Include files

#ifndef COPYDOWNSTREAMTRACKS_H
#define COPYDOWNSTREAMTRACKS_H 1

// from Gaudi


#include "GaudiKernel/AlgFactory.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Track.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CopyDownstreamTracks
//
// 2011-02-22
//
// A little class to copy the downstream track pointer to another container
//
//-----------------------------------------------------------------------------

class CopyDownstreamTracks : public GaudiAlgorithm {
public:
  /// Standard constructor
  CopyDownstreamTracks( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CopyDownstreamTracks( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization


private:
  std::vector<std::string> m_TracksLocations; ///< where the input tracks are located
  std::string m_downstreamTrackLocation;      ///< where the downstream tracks are saved
};


#endif // COPYDOWNSTREAMTRACKS_H

