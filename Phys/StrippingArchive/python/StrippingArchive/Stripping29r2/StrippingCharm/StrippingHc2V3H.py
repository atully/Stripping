###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping lines for selection of general decay topologies
    [Lambda_c+ -> (V0 -> h+ h-) h+ h+ h-]CC
In this file the stripping line for this decay is build 
    [Lambda_c+ -> (Lambda0 -> p+ pi-) pi+ pi+ pi-]CC
    [Lambda_c+ -> (Lambda0 -> p+ pi-) K+ pi+ pi-]CC
Throughout this file, 'Bachelor' refers to the children of the Lambda_c+ which is
not part of the V0 decay.
"""

__author__ = ['Louis Henry']
__date__ = '13/10/2017'

__all__ = (
    'default_config',
    'StrippingHc2V3HConf'
)

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, mrad
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop, DaVinci__N4BodyDecays
from StandardParticles import StdNoPIDsDownPions as InputDownPions
from StandardParticles import StdNoPIDsPions as InputPions
from StandardParticles import StdNoPIDsKaons as InputKaons
from StandardParticles import StdLooseLambdaLL as InputLambdasLL
from StandardParticles import StdLooseLambdaDD as InputLambdasDD

from PhysSelPython.Wrappers import Selection, MergedSelection, DataOnDemand
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME': 'Hc2V3H',
    'WGs': ['Charm'],
    'BUILDERTYPE': 'StrippingHc2V3HConf',
    'STREAMS': ['CharmCompleteEvent'],
    'CONFIG': {
        # Minimum Lc+ bachelor momentum
        'Bach_P_MIN': 2.0*GeV,
        # PID of the Lambda proton
        "ProbNNpMin_LL" : 0.10,
        "ProbNNpMin_DD" : 0.00,
        # Minimum L0 momentum
        'Lambda0_P_MIN': 2000*MeV,
        # Minimum L0 transverse momentum
        'Lambda0_PT_MIN': 250*MeV,
        # Minimum flight distance chi^2 of L0 from the primary vertex
        'Lambda0_FDCHI2_MIN': 25,
        # Maximum L0 vertex chi^2 per vertex fit DoF
        'Lambda0_VCHI2VDOF_MAX': 12.0,
        # Minimum Xi- momentum
        'Xim_P_MIN': 2000*MeV,
        # Minimum Xi- transverse momentum
        'Xim_PT_MIN': 250*MeV,
        # Minimum flight distance chi^2 of Xi- from the primary vertex
        'Xim_FDCHI2_MIN': 9,
        # Maximum Xi- vertex chi^2 per vertex fit DoF
        'Xim_VCHI2VDOF_MAX': 12.0,
        # Xi_c0 mass window around the nominal Xi_c0 mass before the vertex fit
        'Comb_ADAMASS_WIN': 120.0*MeV,
        # Xi_c0 mass window around the nominal Xi_c0 mass after the vertex fit
        'Xic_ADMASS_WIN'    : 90.0*MeV,
        # Maximum distance of closest approach of Xi_c0 children
        'Comb_ADOCAMAX_MAX': 0.5*mm,
        # Maximum Xi_c0 vertex chi^2 per vertex fit DoF
        'Xic_VCHI2VDOF_MAX_LLL'    : 5.0,
        'Xic_VCHI2VDOF_MAX_DDL'    : 5.0,
        'Xic_VCHI2VDOF_MAX_DDD'    : 5.0,
        # Maximum angle between Xi_c0 momentum and Xi_c0 direction of flight
        'Xic_acosBPVDIRA_MAX_LLL': 140.0*mrad,
        'Xic_acosBPVDIRA_MAX_DDL': 140.0*mrad,
        'Xic_acosBPVDIRA_MAX_DDD': 140.0*mrad,
        # Primary vertex displacement requirement, either that the Xi_c0 is some
        # sigma away from the PV, or it has a minimum flight time
        'Xic_PVDispCut_LLL': '(BPVVDCHI2 > 16.0)',
        'Xic_PVDispCut_DDL': '(BPVVDCHI2 > 9.0)',
        'Xic_PVDispCut_DDD': '(BPVVDCHI2 > 9.0)',
        # HLT filters, only process events firing triggers matching the RegEx
        'Hlt1Filter': None,
        'Hlt2Filter': None,
        # Fraction of candidates to randomly throw away before stripping
        'PrescaleXic2XimPiPiPiLLL': 1.0,
        'PrescaleXic2XimPiPiPiDDL': 1.0,
        'PrescaleXic2XimPiPiPiDDD': 1.0,
        # Fraction of candidates to randomly throw away after stripping
        'PostscaleXic2XimPiPiPiLLL': 1.0,
        'PostscaleXic2XimPiPiPiDDL': 1.0,
        'PostscaleXic2XimPiPiPiDDD': 1.0,
    }
}


class StrippingHc2V3HConf(LineBuilder):
    """Creates LineBuilder object containing the stripping lines."""
    # Allowed configuration keys
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        """Initialise this LineBuilder instance."""
        self.name = name
        self.config = config
        LineBuilder.__init__(self, name, config)
        
        # Decay descriptors
        self.Xic2XimPiPiPi     = ['[Xi_c0 -> Xi- pi+ pi- pi+]cc' ]
        self.Xi2Lambdapi = ['[Xi- -> Lambda0 pi-]cc']
        
        # Line names
        # 'LLL', 'DDL', and 'DDD' will be appended to these names for
        # the corresponding selection and strippingLine instances
        self.Xic2XimPiPiPi_name     = '{0}_Xic2XimPiPiPi'    .format(name)
        
        # Build bachelor pion and kaon cut strings
        # Cuts MIPCHI2DV(PRIMARY)>4 & PT>250*MeV already present in the InputsParticles
        childCuts = (
            '(P > {0[Bach_P_MIN]})'
        ).format(self.config)
        
        kineticCuts = '{0}'.format(childCuts)

        # Build Lambda0 cut strings
        lambda0Cuts = (
            '(P > {0[Lambda0_P_MIN]})'
            '& (PT > {0[Lambda0_PT_MIN]})'
            '& (BPVVDCHI2 > {0[Lambda0_FDCHI2_MIN]})'
            '& (VFASPF(VCHI2/VDOF) < {0[Lambda0_VCHI2VDOF_MAX]})'
        ).format(self.config)

        # Build Xi- cut strings
        ximCuts = (
            '(P > {0[Xim_P_MIN]})'
            '& (PT > {0[Xim_PT_MIN]})'
            '& (BPVVDCHI2 > {0[Xim_FDCHI2_MIN]})'
            '& (VFASPF(VCHI2/VDOF) < {0[Xim_VCHI2VDOF_MAX]})'
        ).format(self.config)

        # Define any additional cuts on LL/DD difference
        lambda0LLCuts = lambda0Cuts# +(" & (Lambda0_FD_ORIVX > {0[Lambda0_FD_LL_MIN]}])").format(self.config)
        lambda0DDCuts = lambda0Cuts
        ximLLLCuts = ximCuts
        ximDDDCuts = ximCuts
        ximDDLCuts = ximCuts

        # Filter Input particles
        self.Pions = Selection(
            'PionsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                 Code = kineticCuts
            ),
            RequiredSelections=[InputPions]
        )

        self.Kaons = Selection(
            'KaonsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                  Code = kineticCuts
            ),
            RequiredSelections=[InputKaons]
        )
        self.DownPions = Selection(
            'DownPionsFor{0}'.format(name),
            Algorithm=FilterDesktop(
                Code = kineticCuts
                ),
            RequiredSelections=[InputDownPions]
        )

        # Filter Input Lambdas
        self.LambdaListLooseDD = MergedSelection("StdLooseDDLambdaFor" + self.name,
                                                 RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles")])
        
        self.LambdaListLooseLL = MergedSelection("StdLooseLLLambdaFor" + self.name,
                                                 RequiredSelections =  [DataOnDemand(Location = "Phys/StdLooseLambdaLL/Particles")])
        
        self.LambdaListLL =  self.createSubSel(OutputList = "LambdaLLFor" + self.name,
                                               InputList = self.LambdaListLooseLL ,
                                               Cuts = "(MAXTREE('p+'==ABSID,PROBNNp) > %(ProbNNpMin_LL)s ) " %self.config
                                               )
        
        self.LambdaListDD =  self.createSubSel(OutputList = "LambdaDDFor" + self.name,
                                               InputList = self.LambdaListLooseDD ,
                                               Cuts = "(MAXTREE('p+'==ABSID,PROBNNp) > %(ProbNNpMin_DD)s ) " %self.config
                                               )
        
        self.Lambda0LL = Selection(
            'Lambda0LLFor{0}'.format(name),
            Algorithm=FilterDesktop(
                Code=lambda0LLCuts
            ),
            RequiredSelections=[self.LambdaListLL]
        )
        self.Lambda0DD = Selection(
            'Lambda0DDFor{0}'.format(name),
            Algorithm=FilterDesktop(
                Code=lambda0DDCuts
            ),
            RequiredSelections=[self.LambdaListDD]
        )

        # Make Xim
        self.Xim = self.makeXi2LambdaPi(
            name = name,
            inputSelLLL = [self.LambdaListLL,self.Pions],
            inputSelDDL = [self.LambdaListDD,self.Pions],
            inputSelDDD = [self.LambdaListDD,self.DownPions],
            LLLFilter = ximLLLCuts,
            DDLFilter = ximDDLCuts,
            DDDFilter = ximDDDCuts,
            decDescriptors=self.Xi2Lambdapi
        )


        # Build selection for Xic -> Xi- 3pi
        self.selXic2XimPiPiPi = self.makeXic2V3H(
            name=self.Xic2XimPiPiPi_name,
            inputSelLLL=[self.Xim["LLL"], self.Pions],
            inputSelDDL=[self.Xim["DDL"], self.Pions],
            inputSelDDD=[self.Xim["DDD"], self.Pions],
            decDescriptors=self.Xic2XimPiPiPi
        )

        # Make line for Xic -> Xi- 3pi
        self.line_Xic2XimPiPiPiLLL = self.make_line(
            name='{0}LLLLine'.format(self.Xic2XimPiPiPi_name),
            selection=self.selXic2XimPiPiPi[0],
            prescale=config['PrescaleXic2XimPiPiPiLLL'],
            postscale=config['PostscaleXic2XimPiPiPiLLL'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Xic2XimPiPiPiDDL = self.make_line(
            name='{0}DDLLine'.format(self.Xic2XimPiPiPi_name),
            selection=self.selXic2XimPiPiPi[1],
            prescale=config['PrescaleXic2XimPiPiPiDDL'],
            postscale=config['PostscaleXic2XimPiPiPiDDL'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

        self.line_Xic2XimPiPiPiDDD = self.make_line(
            name='{0}DDDLine'.format(self.Xic2XimPiPiPi_name),
            selection=self.selXic2XimPiPiPi[2],
            prescale=config['PrescaleXic2XimPiPiPiDDD'],
            postscale=config['PostscaleXic2XimPiPiPiDDD'],
            HLT1=config['Hlt1Filter'],
            HLT2=config['Hlt2Filter']
        )

    def make_line(self, name, selection, prescale, postscale, **kwargs):
        """Create the stripping line defined by the selection.

        Keyword arguments:
        name -- Base name for the Line
        selection -- Selection instance
        prescale -- Fraction of candidates to randomly drop before stripping
        postscale -- Fraction of candidates to randomly drop after stripping
        **kwargs -- Keyword arguments passed to StrippingLine constructor
        """
        # Only create the line with positive pre- and postscales
        # You can disable each line by setting either to a negative value
        if prescale > 0 and postscale > 0:
            line = StrippingLine(
                name,
                selection=selection,
                prescale=prescale,
                postscale=postscale,
                **kwargs
            )
            self.registerLine(line)
            return line
        else:
            return False

    def makeXi2LambdaPi(self, name, inputSelLLL, inputSelDDL, inputSelDDD, LLLFilter, DDLFilter, DDDFilter, decDescriptors):
        """Return the dictionary for three Selection instances for a Xi- -> V0 h- decay.
        The return value is a two-tuple of Selection instances as
               {'LLL':LLL Selection, 'DDD':DDD Selection, 'DDL': DDL Selection}.
        Keyword arguments:
        name -- Name to give the Selection instance
        inputSelLLL -- List of inputs passed to Selection.RequiredSelections for the LLL Selection (LambdaLL and Pions Long)
        inputSelDDD -- List of inputs passed to Selection.RequiredSelections for the DDD Selection (LambdaDD and Pions Downstream)
        inputSelDDL -- List of inputs passed to Selection.RequiredSelections for the DDL Selection (LambdaDD and Pions Long)
        decDescriptors -- List of decay descriptors for CombineParticles

        The cuts are equal to LambdaDD and LambdaLL
        """

        # Xim StandardCuts as Lambda0 + Filters
        _XimLLL = CombineParticles(
            DecayDescriptors=decDescriptors,
            DaughtersCuts = {'pi+': '(P>2*GeV) & (MIPCHI2DV(PRIMARY)>9)'},
            CombinationCut = "(ADAMASS('Xi-')<50*MeV) & (ADOCACHI2CUT(30, ''))",
            MotherCut = "(ADMASS('Xi-')<35*MeV) & (VFASPF(VCHI2)<30) & {0}".format(LLLFilter),
        )

        _XimDDL = CombineParticles(
            DecayDescriptors=decDescriptors,
            DaughtersCuts = {'pi+': '(P>2*GeV) & (MIPCHI2DV(PRIMARY)>4)'},
            CombinationCut = "(ADAMASS('Xi-')<80*MeV) & (ADOCACHI2CUT(25, ''))",
            MotherCut = "(ADMASS('Xi-')<64*MeV) & (VFASPF(VCHI2)<25) & {0}".format(DDLFilter),
        )

        _XimDDD = CombineParticles(
            DecayDescriptors=decDescriptors,
            DaughtersCuts = {'pi+': '(P>2*GeV) & (MIPCHI2DV(PRIMARY)>4)'},
            CombinationCut = "(ADAMASS('Xi-')<80*MeV) & (ADOCACHI2CUT(25, ''))",
            MotherCut = "(ADMASS('Xi-')<64*MeV) & (VFASPF(VCHI2)<25) & {0}".format(DDDFilter),
        )

        selXimLLL = Selection(
            'XimLLLFor{0}'.format(name),
            Algorithm=_XimLLL,
            RequiredSelections=inputSelLLL
        )
        selXimDDL = Selection(
            'XimDDLFor{0}'.format(name),
            Algorithm=_XimDDL,
            RequiredSelections=inputSelDDL
        )
        selXimDDD = Selection(
            'XimDDDFor{0}'.format(name),
            Algorithm=_XimDDD,
            RequiredSelections=inputSelDDD
        )
        
        return {'LLL':selXimLLL,'DDL':selXimDDL,'DDD':selXimDDD}

    def makeXic2V3H(self, name, inputSelLLL, inputSelDDL, inputSelDDD, decDescriptors):
        """Return two Selection instances for a Xi_c0 -> V- h+ h- h+ decay.

        The return value is a three-tuple of Selection instances as
        (LLL Selection, DDL selection, DDD Selection)
        where LLL, DDL, and DDD are the method of reconstruction for the V-.
        Keyword arguments:
        name -- Name to give the Selection instance
        inputSelLLL -- List of inputs passed to Selection.RequiredSelections
                      for the LLL Selection
        inputSelDDL -- List of inputs passed to Selection.RequiredSelections
                      for the DDL Selection
        inputSelDDD -- List of inputs passed to Selection.RequiredSelections
                      for the DDD Selection
        decDescriptors -- List of decay descriptors for CombineParticles
        """
        lclPreambulo = [
            'from math import cos'
        ]

        combCuts = (
            "(ADAMASS('Xi_c0') < {0[Comb_ADAMASS_WIN]})"
            "& (ADOCA(1,2) < {0[Comb_ADOCAMAX_MAX]})"
            "& (ADOCA(1,3) < {0[Comb_ADOCAMAX_MAX]})"
            "& (ADOCA(1,4) < {0[Comb_ADOCAMAX_MAX]})"
            ).format(self.config)
        
        xicCuts_LLL = (
            "(VFASPF(VCHI2/VDOF) < {0[Xic_VCHI2VDOF_MAX_LLL]})"
            "& ({0[Xic_PVDispCut_LLL]})"
            "& (BPVDIRA > cos({0[Xic_acosBPVDIRA_MAX_LLL]}))"
            "& (ADMASS('Xi_c0') < {0[Xic_ADMASS_WIN]})"
        ).format(self.config)

        xicCuts_DDL = (
            "(VFASPF(VCHI2/VDOF) < {0[Xic_VCHI2VDOF_MAX_DDL]})"
            "& ({0[Xic_PVDispCut_DDL]})"
            "& (BPVDIRA > cos({0[Xic_acosBPVDIRA_MAX_DDL]}))"
            "& (ADMASS('Xi_c0') < {0[Xic_ADMASS_WIN]})"
        ).format(self.config)

        xicCuts_DDD = (
            "(VFASPF(VCHI2/VDOF) < {0[Xic_VCHI2VDOF_MAX_DDD]})"
            "& ({0[Xic_PVDispCut_DDD]})"
            "& (BPVDIRA > cos({0[Xic_acosBPVDIRA_MAX_DDD]}))"
            "& (ADMASS('Xi_c0') < {0[Xic_ADMASS_WIN]})"
        ).format(self.config)

        comb12Cuts = (
            "(DAMASS('Xi_c0') < {0[Comb_ADAMASS_WIN]})"
            "& (ADOCA(1,2) < {0[Comb_ADOCAMAX_MAX]})"
            ).format(self.config)

        comb123Cuts = (
            "(DAMASS('Xi_c0') < {0[Comb_ADAMASS_WIN]})"
            "& (ADOCA(1,2) < {0[Comb_ADOCAMAX_MAX]})"
            "& (ADOCA(1,3) < {0[Comb_ADOCAMAX_MAX]})"
            ).format(self.config)

        _Xic_LLL = DaVinci__N4BodyDecays(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            CombinationCut=combCuts,
            MotherCut=xicCuts_LLL,
            Combination12Cut=comb12Cuts,
            Combination123Cut=comb123Cuts
            )

        _Xic_DDL = DaVinci__N4BodyDecays(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            CombinationCut=combCuts,
            MotherCut=xicCuts_DDL,
            Combination12Cut=comb12Cuts,
            Combination123Cut=comb123Cuts
            )

        _Xic_DDD = DaVinci__N4BodyDecays(
            DecayDescriptors=decDescriptors,
            Preambulo=lclPreambulo,
            CombinationCut=combCuts,
            MotherCut=xicCuts_DDD,
            Combination12Cut=comb12Cuts,
            Combination123Cut=comb123Cuts
            )

        selLLL = Selection(
            '{0}LLL'.format(name),
            Algorithm=_Xic_LLL,
            RequiredSelections=inputSelLLL
        )

        selDDL = Selection(
            '{0}DDL'.format(name),
            Algorithm=_Xic_DDL,
            RequiredSelections=inputSelDDL
        )

        selDDD = Selection(
            '{0}DDD'.format(name),
            Algorithm=_Xic_DDD,
            RequiredSelections=inputSelDDD
        )

        return selLLL, selDDL, selDDD

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] ) 
