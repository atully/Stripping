###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting converted photons
'''

__author__ = ['Tom Boettcher']
__date__ = ''
__version__ = '$Revision: 1.0 $'

__all__ = (
    'ConvertedPhotonConf',
    'default_config'
    )

from Gaudi.Configuration import *
from CommonParticles.Utils import *
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from Configurables import ( DiElectronMaker,
                            ProtoParticleCALOFilter,
                            ParticleTransporter,
                            LoKi__VertexFitter,
                            )
from PhysSelPython.Wrappers import (Selection,
                                    SimpleSelection,
                                    MergedSelection,
                                    AutomaticData)
from StrippingUtils.Utils import LineBuilder
from StrippingConf.StrippingLine import StrippingLine

default_config = {
    'NAME' : 'ConvertedPhoton',
    'BUILDERTYPE' : 'ConvertedPhotonConf',
    'WGs' : ['QEE'],
    'STREAMS' : ['EW'],
    'CONFIG' : {
        'odin' : ['NoBeam','Beam1','Beam2','BeamCrossing'],

        #==================
        # Low pT selection
        'LowPt' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : ("HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision') | "
                            "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')"),
            'Prescale' : 0.1,
            'AddBrem' : True,
            'MinPt' : 5000.0*MeV,
            'MaxPt' : 10000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : 0.0,
            'MaxIPChi2' : 200.0
            },

        #==================
        # High pT selection
        'HighPt' : {
            'Hlt1Filter' : None,
            'Hlt2Filter' : ("HLT_PASS('Hlt2EWConvPhotonLLHighPtDecision') | "
                            "HLT_PASS('Hlt2EWConvPhotonDDHighPtDecision')"),
            'Prescale' : 0.25,
            'AddBrem' : True,
            'MinPt' : 10000.0*MeV,
            'MaxPt' : 100000.0*MeV,
            'MaxMass' : 100.0*MeV,
            'MinPIDe' : 0.0,
            'MaxIPChi2' : 200.0
            }
        }
    }


class ConvertedPhotonConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):

        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config
        odin = "|".join(["(ODIN_BXTYP == LHCb.ODIN.%s)"%(odin_type,)
                         for odin_type in config['odin']
                         if odin_type in ['NoBeam','Beam1','Beam2','BeamCrossing']])

        #=============
        # Low pT line
        lowPtLL = self.makeConvertedPhoton('SelConvPhotonLowPtLL',
                                           config['LowPt'],
                                           'Long')
        lowPtDD = self.makeConvertedPhoton('SelConvPhotonLowPtDD',
                                           config['LowPt'],
                                           'Downstream')
        self.LowPtLine = StrippingLine(
            name = 'ConvPhotonLowPt',
            prescale = self.config['LowPt']['Prescale'],
            HLT1 = self.config['LowPt']['Hlt1Filter'],
            HLT2 = self.config['LowPt']['Hlt2Filter'],
            selection = MergedSelection(
                'SelConvPhotonLowPt',
                RequiredSelections = [lowPtLL, lowPtDD]
                ),
            ODIN = odin
            )
        self.registerLine(self.LowPtLine)

        #=============
        # High pT line
        highPtLL = self.makeConvertedPhoton('SelConvPhotonHighPtLL',
                                            config['HighPt'],
                                            'Long')
        highPtDD = self.makeConvertedPhoton('SelConvPhotonHighPtDD',
                                            config['HighPt'],
                                            'Downstream')
        self.HighPtLine = StrippingLine(
            name = 'ConvPhotonHighPt',
            prescale = self.config['HighPt']['Prescale'],
            HLT1 = self.config['LowPt']['Hlt1Filter'],
            HLT2 = self.config['LowPt']['Hlt2Filter'],
            selection = MergedSelection(
                'SelConvPhotonHighPt',
                RequiredSelections = [highPtLL, highPtDD]
                ),
            ODIN = odin
            )
        self.registerLine(self.HighPtLine)

    #================================
    # Create selections for each line
    def makeConvertedPhoton(self, name, config, tracks):
        alg = DiElectronMaker(name+'Maker')
        alg.DecayDescriptor = 'gamma -> e+ e-'
        selector = trackSelector(alg, trackTypes=[tracks])
        alg.addTool(ProtoParticleCALOFilter, name='Electron')
        alg.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'%s'"
                                  % config['MinPIDe']]
        alg.DeltaY = 3.0
        alg.DeltaYmax = 200.0*mm
        alg.DiElectronMassMax = config['MaxMass']
        alg.DiElectronPtMin = 200.0*MeV


        # Extra setup for better performance on DD pairs
        if tracks == 'Downstream':
            alg.ParticleCombiners.update({"" : "LoKi::VertexFitter"})
            alg.addTool(LoKi__VertexFitter)
            alg.LoKi__VertexFitter.addTool(ParticleTransporter,
                                           name='Transporter')
            alg.LoKi__VertexFitter.Transporter.TrackExtrapolator = (
                "TrackRungeKuttaExtrapolator")
            alg.LoKi__VertexFitter.DeltaDistance = 100*mm

        code = ("(BPVVDZ > 0)"
                " & (BPVIPCHI2() < %s)"
                " & (PT > %s)"
                " & (PT < %s)"
                " & (MM < (20 + 0.04*VFASPF(VZ))*MeV)"
                % (config['MaxIPChi2'],config['MinPt'],config['MaxPt']))



        preSel = Selection('Pre'+name, Algorithm=alg)
        sel = Selection(name,
                        Algorithm=FilterDesktop(Code=code),
                        RequiredSelections=[preSel])
        return sel
