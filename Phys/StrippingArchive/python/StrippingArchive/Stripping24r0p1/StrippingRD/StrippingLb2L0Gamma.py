#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file   StrippingLb2L0Gamma.py
# @author Albert Puig (albert.puig@epfl.ch)
# @date   20.08.2014
# =============================================================================
"""Stripping lines for Lambda_b -> Lambda0(->p pi) gamma.

These lines cover (a) calorimetric and (b) converted photons.

"""

__author__ = ["Albert Puig Navarro", "Carla Marin Benito"]
__date__ = "14.11.2016"
__version__ = "2.0"
__all__ = ("default_config", "StrippingLb2L0GammaConf")

import os

from Gaudi.Configuration import *

from Configurables import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop

from PhysSelPython.Wrappers      import Selection, MergedSelection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils        import LineBuilder

from StandardParticles import StdAllNoPIDsPions, StdAllNoPIDsProtons
from StandardParticles import StdLooseAllPhotons, StdAllLooseGammaDD, StdAllLooseGammaLL
from StandardParticles import StdVeryLooseDetachedKst2Kpi
from StandardParticles import StdLooseJpsi2MuMu

default_config = {'NAME'        : 'Lb2L0Gamma',
                  'WGs'         : ['RD'],
                  'BUILDERTYPE' : 'StrippingLb2L0GammaConf',
                  'CONFIG'      : {# Prescales
                                   'Lb2L0GammaPrescale'          : 1.0,
                                   'Bd2KstGammaPrescale'         : 1.0,
                                   'Lb2L0JpsiPrescale'           : 1.0,
                                   'Bd2KstJpsiPrescale'          : 1.0,
                                   'Lb2L0GammaConvertedPrescale' : 1.0,
                                   # Trigger cuts
                                   'L0'                          : ['Photon', 'Electron', 'Hadron'],
                                   'L0_Jpsi'                     : [],
                                   'L0_Conv'                     : [],
                                   'HLT1'                        : [],
                                   'HLT2'                        : [],
                                   # Track cuts
                                   'Track_Chi2ndf_Max'           : 3.0,
                                   'Track_MinChi2ndf_Max'        : 2.0,
                                   'Track_GhostProb_Max'         : 0.4,
                                   'TrackLL_IPChi2_Min'          : 16.0,
                                   'Pion_P_Min'                  : 2000.0,
                                   'Proton_P_Min'                : 7000.0, # Can increase
                                   'Pion_Pt_Min'                 : 300.0,
                                   'Proton_Pt_Min'               : 800.0, # Can increase
                                   # Lambda0 cuts
                                   'Lambda0_VtxChi2_Max'         : 9.0,
                                   'Lambda0LL_IP_Min'            : 0.05,
                                   'Lambda0LL_MassWindow'        : 20.0,
                                   'Lambda0DD_MassWindow'        : 30.0,
                                   'Lambda0_Pt_Min'              : 1000.0,
                                   # Kst ctus
                                   'Kst_MassWindow'              : 100.0,
                                   # Photon cuts
                                   'Photon_PT_Min'               : 2500.0,
                                   'Photon_CL_Min'               : 0.2,
                                   'PhotonCnv_PT_Min'            : 1000.0,
                                   'PhotonCnv_MM_Max'            : 100.0,
                                   'PhotonCnv_VtxChi2_Max'       : 9.0,
                                   # Lambda_b cuts
                                   'Lambdab_VtxChi2_Max'         : 9.0,
                                   'Lambdab_Pt_Min'              : 1000.0,
                                   'Lambdab_SumPt_Min'           : 5000.0,
                                   'Lambdab_IPChi2_Max'          : 25.0,
                                   'Lambdab_MTDOCAChi2_Max'      : 7.0,
                                   'Lambdab_MassWindow'          : 1100.0,
                                  },
                  'STREAMS' : ['Leptonic'],
                 }

class StrippingLb2L0GammaConf(LineBuilder):
    __configuration_keys__ = ('Lb2L0GammaPrescale'         ,
                              'Bd2KstGammaPrescale'        ,
                              'Lb2L0JpsiPrescale'          ,
                              'Bd2KstJpsiPrescale'         ,
                              'Lb2L0GammaConvertedPrescale',
                              'L0'                         ,
                              'L0_Jpsi'                    ,
                              'L0_Conv'                    ,
                              'HLT1'                       ,
                              'HLT2'                       ,
                              'Track_Chi2ndf_Max'          ,
                              'Track_GhostProb_Max'        ,
                              'Track_MinChi2ndf_Max'       ,
                              'TrackLL_IPChi2_Min'         ,
                              'Pion_P_Min'                 ,
                              'Proton_P_Min'               ,
                              'Pion_Pt_Min'                ,
                              'Proton_Pt_Min'              ,
                              'Lambda0_VtxChi2_Max'        ,
                              'Lambda0LL_IP_Min'           ,
                              'Lambda0LL_MassWindow'       ,
                              'Lambda0DD_MassWindow'       ,
                              'Lambda0_Pt_Min'             ,
                              'Kst_MassWindow'             ,
                              'Photon_PT_Min'              ,
                              'Photon_CL_Min'              ,
                              'PhotonCnv_PT_Min'           ,
                              'PhotonCnv_MM_Max'           ,
                              'PhotonCnv_VtxChi2_Max'      ,
                              'Lambdab_VtxChi2_Max'        ,
                              'Lambdab_Pt_Min'             ,
                              'Lambdab_SumPt_Min'          ,
                              'Lambdab_IPChi2_Max'         ,
                              'Lambdab_MTDOCAChi2_Max'     ,
                              'Lambdab_MassWindow'         ,
                              #'RelatedInfoTools'           ,
                             )

    def __init__(self, name, config):
        self.name = name
        LineBuilder.__init__(self, name, config)
        #################################################################################
        # Configure trigger
        #################################################################################
        l0 = None
        if config['L0']:
            l0 = "L0_CHANNEL_RE('%s')" % ('|'.join(config['L0']))
        l0_jpsi = None
        if config['L0_Jpsi']:
            l0_jpsi = "L0_CHANNEL_RE('%s')" % ('|'.join(config['L0_Jpsi']))
        l0_conv = None
        if config['L0_Conv']:
            l0_conv = "L0_CHANNEL_RE('%s')" % ('|'.join(config['L0_Conv']))
        hlt1 = None
        if config['HLT1']:
            hlt1 = "HLT_PASS_RE('%s')" % ('|'.join(config['HLT1']))
        hlt2 = None
        if config['HLT2']:
            hlt2 = "HLT_PASS_RE('%s')" % ('|'.join(config['HLT2']))

        #################################################################################
        # Build Lambda_0
        #################################################################################
        tracks_cuts = """(MAXTREE(TRCHI2DOF, HASTRACK) < %(Track_Chi2ndf_Max)s) &
                         (MINTREE(TRCHI2DOF, HASTRACK) < %(Track_MinChi2ndf_Max)s) &
                         (MAXTREE(TRGHOSTPROB, HASTRACK) < %(Track_GhostProb_Max)s) &
                         (INTREE(('p+'==ABSID) & (PT > %(Proton_Pt_Min)s))) &
                         (INTREE(('pi+'==ABSID) & (PT > %(Pion_Pt_Min)s))) &
                         (INTREE(('p+'==ABSID) & (P > %(Proton_P_Min)s))) &
                         (INTREE(('pi+'==ABSID) & (P > %(Pion_P_Min)s)))"""
        lambda0_ll_dod = DataOnDemand(Location='Phys/StdLooseLambdaLL/Particles')
        lambda0_ll_cuts = """(PT>%(Lambda0_Pt_Min)s*MeV) &
                             (VFASPF(VCHI2/VDOF)<%(Lambda0_VtxChi2_Max)s) &
                             (MINTREE(MIPCHI2DV(PRIMARY), ISLONG) > %(TrackLL_IPChi2_Min)s) &
                             (MIPDV(PRIMARY) > %(Lambda0LL_IP_Min)s*mm) &
                             (ADMASS('Lambda0') < %(Lambda0LL_MassWindow)s*MeV)"""
        lambda0_ll_code = (lambda0_ll_cuts + " & " + tracks_cuts) % config
        lambda0_ll_filter = FilterDesktop(Code=lambda0_ll_code)
        lambda0_ll = Selection("LooseLambda0LL",
                               Algorithm=lambda0_ll_filter,
                               RequiredSelections=[lambda0_ll_dod])
        lambda0_dd_dod = DataOnDemand(Location='Phys/StdLooseLambdaDD/Particles')
        lambda0_dd_cuts = """(PT>%(Lambda0_Pt_Min)s*MeV) &
                             (VFASPF(VCHI2/VDOF)<%(Lambda0_VtxChi2_Max)s) &
                             (ADMASS('Lambda0') < %(Lambda0DD_MassWindow)s*MeV)"""
        lambda0_dd_code = (lambda0_dd_cuts + " & " + tracks_cuts) % config
        lambda0_dd_filter = FilterDesktop(Code=lambda0_dd_code)
        lambda0_dd = Selection("LooseLambda0DD",
                               Algorithm=lambda0_dd_filter,
                               RequiredSelections=[lambda0_dd_dod])
        lambda0 = MergedSelection("LooseLambda0",
                                  RequiredSelections=[lambda0_ll, lambda0_dd])

        #################################################################################
        # Build Kst (LL only)
        #################################################################################
        tracks_cuts_Kst = tracks_cuts.replace("p+", "K+")
        Kst_ll_dod = DataOnDemand(Location='Phys/StdVeryLooseDetachedKst2Kpi/Particles')
        Kst_ll_cuts = lambda0_ll_cuts.replace("(ADMASS('Lambda0') < %(Lambda0LL_MassWindow)s*MeV)",
                                              "(ADMASS('K*(892)0') < %(Kst_MassWindow)s*MeV)")
        Kst_ll_code = (Kst_ll_cuts + " & " + tracks_cuts_Kst) % config
        Kst_ll_filter = FilterDesktop(Code=Kst_ll_code)
        Kst_ll = Selection("LooseKstLL",
                           Algorithm=Kst_ll_filter,
                           RequiredSelections=[Kst_ll_dod])
        
        #################################################################################
        # Filter photons
        #################################################################################
        photons_noncnv_filter = FilterDesktop(Code="(PT > %(Photon_PT_Min)s*MeV) & (CL > %(Photon_CL_Min)s)" % config)
        photons_noncnv = Selection("Photons_NonCnv",
                                   Algorithm=photons_noncnv_filter,
                                   RequiredSelections=[StdLooseAllPhotons])
        photons_cnv_merged = MergedSelection("Photons_Cnv_Merge",
                                             RequiredSelections=[StdAllLooseGammaDD,StdAllLooseGammaLL])
        photons_cnv_code = """(HASVERTEX) &
                              (MM < %(PhotonCnv_MM_Max)s*MeV) &
                              (PT > %(PhotonCnv_PT_Min)s*MeV) &
                              (VFASPF(VCHI2/VDOF)<%(PhotonCnv_VtxChi2_Max)s)""" % config
        photons_cnv_filter = FilterDesktop(Code=photons_cnv_code % config)
        photons_cnv = Selection("Photons_Cnv",
                                Algorithm=photons_cnv_filter,
                                RequiredSelections=[photons_cnv_merged])

        #################################################################################
        # Build Lambda_b
        #################################################################################
        ### With non-converted photons
        # Lb2L0Gamma
        lambda_b_combine = CombineParticles("Lambdab_NonConv_Combine")
        lambda_b_combine.DecayDescriptor = "[Lambda_b0 -> Lambda0 gamma]cc"
        lambda_b_combine.DaughtersCuts = {'Lambda0': 'ALL', 'gamma': 'ALL'}
        lambda_b_combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'}
        lambda_b_combCut = """(ADAMASS('Lambda_b0') < %(Lambdab_MassWindow)s*MeV) &
                              (ASUM(PT) > %(Lambdab_SumPt_Min)s )"""  % config
        lambda_b_mothCut = """(PT > %(Lambdab_Pt_Min)s*MeV) &
                              (MTDOCACHI2(1) < %(Lambdab_MTDOCAChi2_Max)s)""" % config
        lambda_b_combine.CombinationCut = lambda_b_combCut
        lambda_b_combine.MotherCut = lambda_b_mothCut
        lambda_b = Selection("Lambdab_NonConv_Sel",
                             Algorithm=lambda_b_combine,
                             RequiredSelections=[photons_noncnv, lambda0])

        # B02KstGamma
        b0_combine = CombineParticles("B0_Combine")
        b0_combine.DecayDescriptor = "[B0 -> K*(892)0 gamma]cc"
        b0_combine.DaughtersCuts = {'K*(892)0': 'ALL', 'gamma': 'ALL'}
        b0_combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'}
        b0_combine.CombinationCut = lambda_b_combCut.replace('Lambda_b0', 'B0')
        b0_combine.MotherCut = lambda_b_mothCut
        b0 = Selection("B0_Sel",
                       Algorithm=b0_combine,
                       RequiredSelections=[photons_noncnv, Kst_ll])

        # Lb2L0Jpsi
        lambda_b_jpsi_combine = CombineParticles("Lambdab_JPsi_Combine")
        lambda_b_jpsi_combine.DecayDescriptor = "[Lambda_b0 -> Lambda0 J/psi(1S)]cc"
        lambda_b_jpsi_combine.DaughtersCuts = {'Lambda0': 'ALL', 'J/psi(1S)': 'ALL'}
        lambda_b_jpsi_combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'}
        lambda_b_jpsi_combine.CombinationCut = lambda_b_combCut
        lambda_b_jpsi_combine.MotherCut = lambda_b_mothCut
        lambda_b_jpsi = Selection("Lambdab_Jpsi_Sel",
                                  Algorithm=lambda_b_jpsi_combine,
                                  RequiredSelections=[StdLooseJpsi2MuMu, lambda0])

        # B02KstJpsi
        b0_jpsi_combine = CombineParticles("B0_Jpsi_Combine")
        b0_jpsi_combine.DecayDescriptor = "[B0 -> K*(892)0 J/psi(1S)]cc"
        b0_jpsi_combine.DaughtersCuts = {'K*(892)0': 'ALL', 'J/psi(1S)': 'ALL'}
        b0_jpsi_combine.ParticleCombiners = {'' : 'MomentumCombiner:PUBLIC'}
        b0_jpsi_combine.CombinationCut = lambda_b_combCut.replace('Lambda_b0', 'B0')
        b0_jpsi_combine.MotherCut = lambda_b_mothCut
        b0_jpsi = Selection("B0_Jpsi_Sel",
                            Algorithm=b0_jpsi_combine,
                            RequiredSelections=[StdLooseJpsi2MuMu, Kst_ll])

        ### With converted photons
        # Lb2L0Gamma
        lambda_b_cnv_combine = CombineParticles("Lambdab_Conv_Combine")
        lambda_b_cnv_combine.DecayDescriptor = "[Lambda_b0 -> Lambda0 gamma]cc"
        lambda_b_cnv_combine.DaughtersCuts = {'Lambda0': 'ALL', 'gamma': 'ALL'}
        lambda_b_cnv_combine.ParticleCombiners = {"": "OfflineVertexFitter:PUBLIC"}
        lambda_b_cnv_combine.CombinationCut = "(ADAMASS('Lambda_b0') < 1.5*%(Lambdab_MassWindow)s*MeV)" % config
        lambda_b_cnv_combine.MotherCut = """(HASVERTEX) & (VFASPF(VCHI2/VDOF)<%(Lambdab_VtxChi2_Max)s) &
                                            (PT > %(Lambdab_Pt_Min)s*MeV) &
                                            (BPVIPCHI2() < %(Lambdab_IPChi2_Max)s) &
                                            (ADMASS('Lambda_b0') < %(Lambdab_MassWindow)s*MeV)""" % config
                                            #(MTDOCACHI2(1) < %(Lambdab_MTDOCAChi2_Max)s) &
        lambda_b_cnv = Selection("Lambdab_Conv_Sel",
                                 Algorithm=lambda_b_cnv_combine,
                                 RequiredSelections=[photons_cnv, lambda0])

        #################################################################################
        # Build lines
        #################################################################################
        children_Lb = {'L0': '[Lambda_b0 -> ^Lambda0 gamma]CC',
                       'gamma': '[Lambda_b0 -> Lambda0 ^gamma]CC'}
        self.line = StrippingLine("Lb2L0Gamma",
                                  prescale=config["Lb2L0GammaPrescale"],
                                  L0DU=l0,
                                  HLT1=hlt1,
                                  HLT2=hlt2,
                                  checkPV=True,
                                  RelatedInfoTools=[self.get_cone_relinfo(1.7, children=children_Lb),
                                                    self.get_cone_relinfo(1.35, children=children_Lb),
                                                    self.get_cone_relinfo(1.0, children=children_Lb),
                                                   ],
                                  RequiredRawEvents=['Calo'],
                                  MDSTFlag = False,
                                  selection=lambda_b)
        self.registerLine(self.line)

        children_Bd = {'Kst': '[B0 -> ^K*(892)0 gamma]CC',
                       'gamma': '[B0 -> K*(892)0 ^gamma]CC'}
        self.line_b0 = StrippingLine("Lb2L0Gamma_Bd2KstGamma",
                                     prescale=config["Bd2KstGammaPrescale"],
                                     L0DU=l0,
                                     HLT1=hlt1,
                                     HLT2=hlt2,
                                     checkPV=True,
                                     RelatedInfoTools=[self.get_cone_relinfo(1.7, children=children_Bd),
                                                       self.get_cone_relinfo(1.35, children=children_Bd),
                                                       self.get_cone_relinfo(1.0, children=children_Bd),
                                                       ],
                                     RequiredRawEvents=['Calo'],
                                     MDSTFlag = False,
                                     selection=b0)
        self.registerLine(self.line_b0)

        children_Lb_Jpsi = {'L0': '[Lambda_b0 -> ^Lambda0 J/psi(1S)]CC',
                            'Jpsi': '[Lambda_b0 -> Lambda0 ^J/psi(1S)]CC'}
        self.line_jpsi = StrippingLine("Lb2L0Gamma_Lb2L0Jpsi",
                                       prescale=config["Lb2L0JpsiPrescale"],
                                       L0DU=l0_jpsi,
                                       HLT1=hlt1,
                                       HLT2=hlt2,
                                       checkPV=True,
                                       RelatedInfoTools=[self.get_cone_relinfo(1.7, children=children_Lb_Jpsi),
                                                         self.get_cone_relinfo(1.35, children=children_Lb_Jpsi),
                                                         self.get_cone_relinfo(1.0, children=children_Lb_Jpsi),
                                                         ],
                                       RequiredRawEvents=[],
                                       MDSTFlag = False,
                                       selection=lambda_b_jpsi)
        self.registerLine(self.line_jpsi)

        children_B0_Jpsi = {'Kst': '[B0 -> ^K*(892)0 J/psi(1S)]CC',
                           'Jpsi': '[B0 -> K*(892)0 ^J/psi(1S)]CC'}
        self.line_b0_jpsi = StrippingLine("Lb2L0Gamma_Bd2KstJpsi",
                                         prescale=config["Bd2KstJpsiPrescale"],
                                         L0DU=l0_jpsi,
                                         HLT1=hlt1,
                                         HLT2=hlt2,
                                         checkPV=True,
                                         RelatedInfoTools=[self.get_cone_relinfo(1.7, children=children_B0_Jpsi),
                                                           self.get_cone_relinfo(1.35, children=children_B0_Jpsi),
                                                           self.get_cone_relinfo(1.0, children=children_B0_Jpsi),
                                                           ],
                                         RequiredRawEvents=[], 
                                         MDSTFlag = False,
                                         selection=b0_jpsi)
        self.registerLine(self.line_b0_jpsi)

        self.line_cnv = StrippingLine("Lb2L0GammaConverted",
                                      prescale=config["Lb2L0GammaPrescale"],
                                      L0DU=l0_conv,
                                      HLT1=hlt1,
                                      HLT2=hlt2,
                                      checkPV=True,
                                      RelatedInfoTools=[self.get_cone_relinfo(1.7, lambda_b_cnv, children_Lb),
                                                        self.get_cone_relinfo(1.35, lambda_b_cnv, children_Lb),
                                                        self.get_cone_relinfo(1.0, lambda_b_cnv, children_Lb),
                                                        self.get_vtxisol_relinfo(lambda_b_cnv),
                                                        self.get_vtxisol_radiative_relinfo(lambda_b_cnv)
                                                        ],
                                      RequiredRawEvents=['Calo'],
                                      MDSTFlag = False,
                                      selection=lambda_b_cnv)
        self.registerLine(self.line_cnv)


    @staticmethod
    def get_cone_relinfo(angle, head=None, children=None):
        tool = {'Type'     : 'RelInfoConeVariables',
                'ConeAngle': angle,
                'Variables': ['CONEANGLE', 'CONEMULT', 'CONEP', 'CONEPASYM', 'CONEPT', 'CONEPTASYM']}
        # Some shortcuts
        base_location = 'ConeVarsInfo/%%s/%s' % angle
        # Head
        if head:
            tool.update({'Location'    : base_location % 'Lb',
                         'TopSelection': head})
        if children:
            tool.update({'DaughterLocations': dict([(sel_string, base_location % name)
                                                    for name, sel_string in children.items()])})
        return tool


    @staticmethod
    def get_vtxisol_relinfo(selection):
        return {'Type'        : 'RelInfoVertexIsolation',
                'Variables'   : ['VTXISONUMVTX',
                                 'VTXISODCHI2ONETRACK', 'VTXISODCHI2MASSONETRACK',
                                 'VTXISODCHI2TWOTRACK', 'VTXISODCHI2MASSTWOTRACK'],
                'Location'    : 'VertexIsolInfo',
                'TopSelection': selection}

    @staticmethod
    def get_vtxisol_radiative_relinfo(selection):
        return {'Type'        : 'RelInfoVertexIsolationRadiative',
                'Variables'   : ['NEWVTXISONUMVTX','NEWVTXISOTRKRELD0',
                                 'NEWVTXISOTRKDCHI2','NEWVTXISODCHI2MASS'],
                'Location'    : 'VertexIsolInfoRadiative',
                'TopSelection': selection}
    
# EOF

