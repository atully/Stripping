###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = ['Mark Smith']
__date__ = '17/10/2016'
__version__ = '$Revision: 1.0 $'

'''
B+->p p~ tau nu exclusive reconstruction. A mash up of the Strippingbhad2PMuX and
StrippingB2DMuForTauMu lines.
'''
# =============================================================================
##
#  B+ -> p p~ tau nu exclusive reconstruction. 
#
#  Stripping lines for the charmless semileptonic decay B+ -> p p~ tau nu.
#
#  The lines will lead to a  measurement of the ratio of branching fractions
#  of B+ -> p p~ tau nu and B+ -> p p~ mu nu
#
#  This together with form factor predictions from either light cone
#  sum rules or lattice QCD will allow a measurement of R(ppbar).
# 
#  Three lines are included:
#  Two of which strip the opposite sign (right sign) proton and muon and combinations.
#  One of which has no prescale and strips protons and muons with a corrected mass >
#  4 GeV.  The other line is prescaled by a factor of 0.5 and strips < 4 GeV in corrected
#  mass.  This exploits the fact that the corrected mass for Lb->pmunu peaks at the Lambda_b
#  mass.  The final line strips same sign proton and muon cominations over the whole corrected 
#  mass region.  This will be ussed to extract shapes for the combinatorial background.
#
#  Stripping XX, with requirements that the
#  rate <0.05% and timing <0.5ms/evt.
## 

"""
  b->p p mu nu X reconstruction. 

  Stripping lines for the charmless semileptonic decay B+ -> p p~ mu nu.

  The lines will lead to a  measurement of the ratio of branching frations
  B+->p p~ tau nu and B+-> p p~ mu nu. 

  This together with form factor predictions from either light cone
  sum rules or lattice QCD will allow a measurement of R(ppbar).
 
  Last modification $Date: 2016-October-17 $
               by $Author: Mark Smith $
"""

default_config = {
  'B2PPbarMuForTauMu' : {
        'WGs'         : ['Semileptonic'],
	'BUILDERTYPE' : 'B2PPbarMuForTauMuBuilder',
        'CONFIG'      :{
      "GEC_nSPDHits"        : 600.   ,#adimensional
	  "TRGHOSTPROB"         : 0.35   ,#adimensional
	  #Muon Cuts
	  "MuonGHOSTPROB"       : 0.35   ,#adimensional
	  "MuonTRCHI2"          : 4.     ,#adimensional
	  "MuonP"               : 3000.  ,#MeV
	  "MuonMINIPCHI2"       : 16.    ,#adminensional
	  #Proton Cuts 
	  "ProtonTRCHI2"        : 6.     ,#adimensional
	  "ProtonP"             : 15000. ,#MeV
	  "ProtonPT"            : 800.  , 
	  "ProtonPIDK"          : 2.     ,#adimensional 
	  "ProtonPIDp"          : 2.     ,#adimensional
	  "ProtonMINIPCHI2"     : 9.    ,#adminensional
	  #B Mother Cuts
	  "BVCHI2DOF"           : 15.     ,#adminensional
	  "BDIRA"               : 0.999  ,#adminensional
	  "BFDCHI2HIGH"         : 50.   ,#adimensional
	  "PPbarVCHI2DOF"           : 10.     ,#adminensional
	  "PPbarDIRA"               : 0.999  ,#adminensional
	  "PPbarFDCHI2HIGH"         : 25.   ,#adimensional
	  "pMuMassLower"        : 1000.  ,#MeV
	  "ppMuPT"               : 1500.,  #MeV
	  "DECAYS"               : ["[B- -> J/psi(1S) mu-]cc"],
	  "SSDECAYS"               : ["[B- -> J/psi(1S) mu-]cc"]
	},
       'STREAMS'     : ['Semileptonic']	  
      }
    }

from Gaudi.Configuration import *
from StrippingUtils.Utils import LineBuilder

import logging

default_name="B2PPbarMuForTauMu"

class B2PPbarMuForTauMuBuilder(LineBuilder):
    """
    Definition of B+->p p~ mu nu stripping
    """
    
    __configuration_keys__ = [
        "GEC_nSPDHits"
        ,"TRGHOSTPROB"          
        ,"MuonGHOSTPROB"
        ,"MuonTRCHI2"          
        ,"MuonP"               
        ,"MuonMINIPCHI2"       
        ,"ProtonTRCHI2"          
        ,"ProtonP"               
        ,"ProtonPT"              
        ,"ProtonPIDK"             
        ,"ProtonPIDp"            
        ,"ProtonMINIPCHI2"
        ,"BVCHI2DOF"
        ,"BDIRA"               
        ,"BFDCHI2HIGH"         
        ,"PPbarVCHI2DOF"
        ,"PPbarDIRA"               
        ,"PPbarFDCHI2HIGH"         
        ,"pMuMassLower"     
	      ,"ppMuPT"     
	      ,"DECAYS"     
	      ,"SSDECAYS"     
        ]

    def __init__(self,name,config):
        LineBuilder.__init__(self, name, config)

        from PhysSelPython.Wrappers import Selection, DataOnDemand

        self.GECs = { "Code":"( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(GEC_nSPDHits)s )" % config,
                      "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        self._muonSel=None
        self._muonFilter()

        self._protonSel=None
        self._protonFilter()

        self._daughtersSel=None
        self._daughters()

        self._daughters_SSSel=None
        self._daughters_SS()


        self._fakeprotonSel=None
        self._fakeprotonFilter()

        self._fakedaughtersSel=None
        self._fakedaughters()

        self._fakemuonSel=None
        self._fakemuonFilter()

        self._Definitions()

        self.registerLine(self._Bplus_line())
        self.registerLine(self._BplusTopo_line())
        self.registerLine(self._SS_Bplus_line())
        self.registerLine(self._Bplus_fakeP_line())
        self.registerLine(self._SS_Bplus_fakeP_line())
        self.registerLine(self._Bplus_fakeMu_line())
        self.registerLine(self._SS_Bplus_fakeMu_line())
        
    def _NominalMuSelection( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
               "& (ISMUON)"\
               "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"

    def _FakeMuSelection( self ):
        return "(TRCHI2DOF < %(MuonTRCHI2)s ) &  (P> %(MuonP)s *MeV)"\
               "& (TRGHOSTPROB < %(MuonGHOSTPROB)s)"\
	       "& (~ISMUON) & (INMUON)"\
               "& (MIPCHI2DV(PRIMARY)> %(MuonMINIPCHI2)s )"
    
    def _NominalPSelection( self ):
        return "(TRCHI2DOF < %(ProtonTRCHI2)s )&  (P> %(ProtonP)s *MeV) &  (PT> %(ProtonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDp-PIDpi> %(ProtonPIDp)s )& (PIDp-PIDK> %(ProtonPIDK)s ) "\
               "& (MIPCHI2DV(PRIMARY)> %(ProtonMINIPCHI2)s )"\
               "& (switch(ISMUON,1,0) < 1)"

    def _FakePSelection( self ):
        return "(TRCHI2DOF < %(ProtonTRCHI2)s )&  (P> %(ProtonP)s *MeV) &  (PT> %(ProtonPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (MIPCHI2DV(PRIMARY)> %(ProtonMINIPCHI2)s )"\
               "& (switch(ISMUON,1,0) < 1)"

    def _PPbarSelection( self ):
        return "(VFASPF(VCHI2/VDOF)< %(PPbarVCHI2DOF)s) & (BPVDIRA> %(PPbarDIRA)s)"\
                     "& (BPVVDCHI2 >%(PPbarFDCHI2HIGH)s)"

    def _fakePPbarSelection( self ):
        return "(VFASPF(VCHI2/VDOF)< %(PPbarVCHI2DOF)s) & (BPVDIRA> %(PPbarDIRA)s)"\
                     "& (INTREE((ABSID == 'p+')& (PIDp-PIDpi> %(ProtonPIDp)s )&(PIDp-PIDK> %(ProtonPIDK)s ))) "\
                     "& (BPVVDCHI2 >%(PPbarFDCHI2HIGH)s)"

    def _BMotherSelection( self ):
        return "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s) & (BPVDIRA> %(BDIRA)s)"\
                     "& (PT > %(ppMuPT)s)"\
                     "& (BPVVDCHI2 >%(BFDCHI2HIGH)s)"




    ###### Signal Line  ######

    def _Bplus_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = "HLT_PASS_RE('Hlt2XcMuXForTauB2XcMuDecision')"
        ldu = ''
        return StrippingLine(self._name+'Line', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._bhad2PMuX_B()], HLT2 = hlt, L0DU = ldu)


    ###### Signal Line from TOPO  ######

    def _BplusTopo_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = "HLT_PASS_RE('Hlt2TopoMu2Body.*Decision')"
        ldu = ''
        return StrippingLine(self._name+'TopoLine', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._bhad2PMuX_BTopo()], HLT2 = hlt, L0DU = ldu)



    ##### Same-sign protons line #####

    def _SS_Bplus_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = "HLT_PASS_RE('Hlt2TopoMu2Body.*Decision')"
        ldu = ''
        return StrippingLine(self._name+'SSLine', prescale = 1.0,
                             FILTER=self.GECs,
                             algos = [ self._bhad2PMuX_B_SS()], HLT2 = hlt, L0DU = ldu)

    ##### Fake muon line #####

    def _Bplus_fakeMu_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = "HLT_PASS_RE('Hlt2XcMuXForTauB2XcFakeMuDecision')"
        ldu = ''
        return StrippingLine(self._name+'fakeMuLine', prescale = 0.02,
                             FILTER=self.GECs,
                             algos = [ self._bhad2PMuX_B_fakeMu()], HLT2 = hlt, L0DU = ldu)

    ##### Fake proton line #####

    def _Bplus_fakeP_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = "HLT_PASS_RE('Hlt2TopoMu2Body.*Decision')"
        ldu = ''
        return StrippingLine(self._name+'fakePLine', prescale = 0.02,
                             FILTER=self.GECs,
                             algos = [ self._bhad2PMuX_B_fakeP()], HLT2 = hlt, L0DU = ldu)

    ##### Same-sign protons with one a fake #####

    def _SS_Bplus_fakeP_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = "HLT_PASS_RE('Hlt2TopoMu2Body.*Decision')"
        ldu = ''
        return StrippingLine(self._name+'SSfakePLine', prescale = 0.02,
                             FILTER=self.GECs,
                             algos = [ self._bhad2PMuX_B_SSfakeP()], HLT2 = hlt, L0DU = ldu)

    ##### Same-sign protons with a fake muon #####

    def _SS_Bplus_fakeMu_line( self ):
        from StrippingConf.StrippingLine import StrippingLine
        hlt = "HLT_PASS_RE('Hlt2TopoMu2Body.*Decision')"
        ldu = ''
        return StrippingLine(self._name+'SSfakeMuLine', prescale = 0.02,
                             FILTER=self.GECs,
                             algos = [ self._bhad2PMuX_B_SSfakeMu()], HLT2 = hlt, L0DU = ldu)


    ##### Muon Filter ######
    def _muonFilter( self ):
        if self._muonSel is not None:
            return self._muonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseMuons
        _mu = FilterDesktop( Code = self._NominalMuSelection() % self._config )

        _muSel=Selection("Mu_for"+self._name,
                         Algorithm=_mu,
                         RequiredSelections = [StdLooseMuons])
        
        self._muonSel=_muSel
        
        return _muSel

    ##### Fake Muon Filter ######
    def _fakemuonFilter( self ):
        if self._fakemuonSel is not None:
            return self._fakemuonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllNoPIDsMuons
        _mu = FilterDesktop( Code = self._FakeMuSelection() % self._config )

        _muSel=Selection("fakeMu_for"+self._name,
                         Algorithm=_mu,
                         RequiredSelections = [StdAllNoPIDsMuons])
        
        self._fakemuonSel=_muSel
        
        return _muSel

    ###### Proton Filter ######
    def _protonFilter( self ):
        if self._protonSel is not None:
            return self._protonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseProtons
        
        _pr = FilterDesktop( Code = self._NominalPSelection() % self._config )
        _prSel=Selection("p_for"+self._name,
                         Algorithm=_pr,
                         RequiredSelections = [StdLooseProtons])
        
        self._protonSel=_prSel
        
        return _prSel

    ##### Fake Proton Filter ######
    def _fakeprotonFilter( self ):
        if self._fakeprotonSel is not None:
            return self._fakeprotonSel
	
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllNoPIDsProtons
        
        _pr = FilterDesktop( Code = self._FakePSelection() % self._config )
        _prSel=Selection("fakep_for"+self._name,
                         Algorithm=_pr,
                         RequiredSelections = [StdAllNoPIDsProtons])
        
        self._fakeprotonSel=_prSel
        
        return _prSel


    def _Definitions(self):
        return [ 
            "from LoKiPhys.decorators import *",
                           ]

    ###### Signal ppbar selection ######
    def _PPbar( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        PPbar = CombineParticles(DecayDescriptors = ["J/psi(1S) -> p+ p~-"], ReFitPVs = True)
        PPbar.Preambulo = self._Definitions()
        PPbar.MotherCut = self._PPbarSelection() % self._config
        PPbar.ReFitPVs = True
            
        PPbarSel=Selection("PPbar_B_for"+self._name,
                         Algorithm=PPbar,
                         RequiredSelections = [self._protonFilter()])
         
        return PPbarSel

    ###### Same-sign ppbar selection ######
    def _PPbar_SS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        PPbar_SS = CombineParticles(DecayDescriptors = ["[J/psi(1S) -> p+ p+]cc"], ReFitPVs = True)
        PPbar_SS.Preambulo = self._Definitions()
        PPbar_SS.MotherCut = self._PPbarSelection() % self._config
        PPbar_SS.ReFitPVs = True
            
        PPbar_SSSel=Selection("PPbar_SS_B_for"+self._name,
                         Algorithm=PPbar_SS,
                         RequiredSelections = [self._protonFilter()])
         
        return PPbar_SSSel

    ##### Fake proton selection #####

    def _fakePPbar( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        PPbar = CombineParticles(DecayDescriptors = ["J/psi(1S) -> p+ p~-"], ReFitPVs = True)
        PPbar.Preambulo = self._Definitions()
        PPbar.MotherCut = self._fakePPbarSelection() % self._config
        PPbar.ReFitPVs = True
            
        PPbarSel=Selection("fakePPbar_B_for"+self._name,
                         Algorithm=PPbar,
                         RequiredSelections = [self._fakeprotonFilter()])
         
        return PPbarSel



    ###### Hadron daughters selection ######

    def _daughters( self ):
        if self._daughtersSel is not None:
            return self._daughtersSel
        from PhysSelPython.Wrappers import MergedSelection
        _sel_Daughters = MergedSelection("Selection_mergeddaughters"+self._name,
                                         RequiredSelections = [self._protonFilter(),self._PPbar()])
        self._daughtersSel=_sel_Daughters
        return _sel_Daughters

    ###### Same-sign Hadron daughters selection ######

    def _daughters_SS( self ):
        if self._daughters_SSSel is not None:
            return self._daughters_SSSel
        from PhysSelPython.Wrappers import MergedSelection
        _sel_Daughters = MergedSelection("Selection_same-sign_mergeddaughters"+self._name,
                                         RequiredSelections = [self._protonFilter(),self._PPbar_SS()])
        self._daughters_SSSel=_sel_Daughters
        return _sel_Daughters

    ##### Daughters, PID on only one #####

    def _fakedaughters( self ):
        if self._fakedaughtersSel is not None:
            return self._fakedaughtersSel
        from PhysSelPython.Wrappers import MergedSelection
        _sel_Daughters = MergedSelection("Selection_fakemergeddaughters"+self._name,
                                         RequiredSelections = [self._fakeprotonFilter(),self._fakePPbar()])
        self._fakedaughtersSel=_sel_Daughters
        return _sel_Daughters


    ###### B->ppMuNu Signal ######
    def _bhad2PMuX_B( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection,MergedSelection
        
        _pMu = CombineParticles(DecayDescriptors = self._config["DECAYS"], ReFitPVs = True)
        _pMu.Preambulo = self._Definitions()
        _pMu.CombinationCut = "(AM>%(pMuMassLower)s*MeV)" % self._config
        _pMu.MotherCut = self._BMotherSelection() % self._config
        _pMu.ReFitPVs = True
            
        _pMuSel=Selection("ppMu_B_for"+self._name,
                         Algorithm=_pMu,
                         RequiredSelections = [self._muonFilter(), self._daughters()])
        return _pMuSel


    ###### B->ppMuNu Signal for Topo Line ######
    def _bhad2PMuX_BTopo( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection,MergedSelection
        
        _pMu = CombineParticles(DecayDescriptors = self._config["DECAYS"], ReFitPVs = True)
        _pMu.Preambulo = self._Definitions()
        _pMu.CombinationCut = "(AM>%(pMuMassLower)s*MeV)" % self._config
        _pMu.MotherCut = self._BMotherSelection() % self._config
        _pMu.ReFitPVs = True
            
        _pMuSel=Selection("ppMu_BTopo_for"+self._name,
                         Algorithm=_pMu,
                         RequiredSelections = [self._muonFilter(), self._daughters()])
        return _pMuSel


    ###### B->ppMuNu Same-Sign protons ######
    def _bhad2PMuX_B_SS( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection,MergedSelection
        
        _pMu = CombineParticles(DecayDescriptors = self._config["SSDECAYS"], ReFitPVs = True)
        _pMu.Preambulo = self._Definitions()
        _pMu.CombinationCut = "(AM>%(pMuMassLower)s*MeV)" % self._config
        _pMu.MotherCut = self._BMotherSelection() % self._config
        _pMu.ReFitPVs = True
            
        _pMuSel=Selection("ppMu_SS_B_for"+self._name,
                         Algorithm=_pMu,
                         RequiredSelections = [self._muonFilter(), self._daughters_SS()])
        return _pMuSel



    ###### Lb->pMuNu Fake Muon ######
    def _bhad2PMuX_B_fakeMu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _pMu = CombineParticles(DecayDescriptors = self._config["DECAYS"], ReFitPVs = True)
        _pMu.Preambulo = self._Definitions()
        _pMu.CombinationCut = "(AM>%(pMuMassLower)s*MeV)" % self._config
        _pMu.MotherCut = self._BMotherSelection() % self._config
        _pMu.ReFitPVs = True
            
        _pMuSel=Selection("ppMu_fakeMu_B_for"+self._name,
                         Algorithm=_pMu,
                         RequiredSelections = [self._fakemuonFilter(), self._daughters()])
        return _pMuSel

    ###### Lb->pMuNu Fake Proton ######
    def _bhad2PMuX_B_fakeP( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _pMu = CombineParticles(DecayDescriptors = self._config["DECAYS"], ReFitPVs = True)
        _pMu.Preambulo = self._Definitions()
        _pMu.CombinationCut = "(AM>%(pMuMassLower)s*MeV)" % self._config
        _pMu.MotherCut = self._BMotherSelection() % self._config
        _pMu.ReFitPVs = True
            
        _pMuSel=Selection("ppMu_fakeP_B_for"+self._name,
                         Algorithm=_pMu,
                         RequiredSelections = [self._muonFilter(), self._fakedaughters()])
        return _pMuSel

    ###### Lb->pMuNu same-sign protons with a Fake Proton ######
    def _bhad2PMuX_B_SSfakeP( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _pMu = CombineParticles(DecayDescriptors = self._config["SSDECAYS"], ReFitPVs = True)
        _pMu.Preambulo = self._Definitions()
        _pMu.CombinationCut = "(AM>%(pMuMassLower)s*MeV)" % self._config
        _pMu.MotherCut = self._BMotherSelection() % self._config
        _pMu.ReFitPVs = True
            
        _pMuSel=Selection("ppMu_SSfakeP_B_for"+self._name,
                         Algorithm=_pMu,
                         RequiredSelections = [self._muonFilter(), self._fakedaughters()])
        return _pMuSel

    ###### Lb->pMuNu same-sign protons with a Fake Muon ######
    def _bhad2PMuX_B_SSfakeMu( self ):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        
        _pMu = CombineParticles(DecayDescriptors = self._config["SSDECAYS"], ReFitPVs = True)
        _pMu.Preambulo = self._Definitions()
        _pMu.CombinationCut = "(AM>%(pMuMassLower)s*MeV)" % self._config
        _pMu.MotherCut = self._BMotherSelection() % self._config
        _pMu.ReFitPVs = True
            
        _pMuSel=Selection("ppMu_SSfakeMu_B_for"+self._name,
                         Algorithm=_pMu,
                         RequiredSelections = [self._fakemuonFilter(), self._daughters_SS()])
        return _pMuSel
