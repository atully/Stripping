###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting B0->a1 pi
'''

__author__=['Daniel Vieira, Jibo He']
__date__ = '01/12/2016'
__version__= '$Revision: 1.0 $'


__all__ = (
    'B02a1PiBDTConf',
    'default_config'
    )

default_config = {
    'NAME'                 :  'B02a1PiBDT',
    'BUILDERTYPE'          :  'B02a1PiBDTConf',
    'CONFIG'    : {
        'a1PionCuts'       : "(PROBNNpi > 0.1) & (PT > 200*MeV) & (TRGHOSTPROB<0.4) &  (MIPCHI2DV(PRIMARY) > 2.)",
        'BachelorPionCuts' : "(PROBNNpi > 0.1) & (PT > 1000*MeV) & (TRGHOSTPROB<0.4) &  (MIPCHI2DV(PRIMARY) > 2.)",
        'a1ComCuts'        : "(ADAMASS('a_1(1260)+') < 800 *MeV)",
        'a1Cuts'           : """
                             (MIPCHI2DV(PRIMARY) > 2.)
                             & (VFASPF(VCHI2) < 50.)
                             & (PT > 1.*GeV)
                             """ ,
        'B0ComCuts'        : "(ADAMASS('B0') < 500 *MeV)",
        'B0MomCuts'        : """
		             (VFASPF(VCHI2) < 50.) 
			     & (BPVDIRA> 0.99) 
			     & (BPVIPCHI2()<30) 
			     & (VFASPF(VMINVDCHI2DV(PRIMARY)) > 10)
			     & (PT > 2.*GeV )
	                     """,
        'B02a1PiMVACut'    :  "-0.0",
        'B02a1PiXmlFile'   :  "$TMVAWEIGHTSROOT/data/B02a1Pi_BDT_v1r1.xml", 
        'Prescale'         : 1.
        },
    'STREAMS'              : ['Bhadron'],
    'WGs'                  : ['BnoC']
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import DaVinci__N4BodyDecays

class B02a1PiBDTConf(LineBuilder):
    
    __configuration_keys__ = (
        'a1PionCuts',
        'BachelorPionCuts',
        'a1ComCuts',
        'a1Cuts',
        'B0ComCuts',
        'B0MomCuts',
        'Prescale',
        'B02a1PiMVACut',
        'B02a1PiXmlFile'
        )

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        """
        a1
        """

        self.Sela1Pions = self.createSubSel( OutputList = self.name + "Sela1Pions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsPions/Particles' ), 
                                           Cuts = config['a1PionCuts']
                                           )
        
        self.Sela12PiPiPi = self.createCombinationSel( OutputList = self.name + "Sela12PiPiPi",
                                                    DaughterLists = [ self.Sela1Pions ],
                                                    DecayDescriptor = "[a_1(1260)+ -> pi- pi+ pi+]cc",
						    PreVertexCuts  = config['a1ComCuts'],
                                                    PostVertexCuts = config['a1Cuts']
                                                    )

        """
        Bachelor Pion 
        """
        self.SelBachelorPion = self.createSubSel( OutputList = self.name + "SelBachelorPion",
                                         InputList =  DataOnDemand(Location = 'Phys/StdAllNoPIDsPions/Particles' ), 
                                         Cuts = config['BachelorPionCuts']
                                         )

        """
        B0 -> a1(1260)+ pi-  
        """
        self.SelB02a1Pi = self.createCombinationSel( OutputList = self.name + "SelB02a1Pi",
                                                        DecayDescriptor = "[B0 -> pi- a_1(1260)+]cc",
                                                        DaughterLists = [ self.SelBachelorPion, self.Sela12PiPiPi ],                    
                                                        PreVertexCuts  = config['B0ComCuts'],
                                                        PostVertexCuts = config['B0MomCuts'] )
                
        """
        Apply MVA
        """
        self.B02a1PiVars = {
            'Pion1_IPCHI2_OWNPV'        : " CHILD(MIPCHI2DV(), 1) ",
            'Pion2_IPCHI2_OWNPV'        : " CHILD(MIPCHI2DV(), 2, 1) ",
            'Pion3_IPCHI2_OWNPV'        : " CHILD(MIPCHI2DV(), 2, 2) ",
            'Pion4_IPCHI2_OWNPV'        : " CHILD(MIPCHI2DV(), 2, 3) ", 
            'Pion1_PT'                  : " CHILD(PT, 1) ",
            'Pion2_PT'                  : " CHILD(PT, 2, 1) ",
            'Pion3_PT'                  : " CHILD(PT, 2, 2) ",
            'Pion4_PT'                  : " CHILD(PT, 2, 3) ",

            'sqrt(Pion1_IPCHI2_OWNPV+Pion2_IPCHI2_OWNPV+Pion3_IPCHI2_OWNPV+Pion4_IPCHI2_OWNPV)'                 : "sqrt( CHILD(MIPCHI2DV(), 1)+CHILD(MIPCHI2DV(), 2, 1)+CHILD(MIPCHI2DV(), 2, 2)+CHILD(MIPCHI2DV(), 2, 3))",

            'a1_PT'                     : " CHILD(PT, 2 ) ",
            'a1_IPCHI2_OWNPV'           : " CHILD(MIPCHI2DV(), 2 ) ",
            'a1_ENDVERTEX_CHI2'         : " CHILD(VFASPF(VCHI2), 2)",
            
            'B0_PT'                     : " PT ",
            'B0_IPCHI2_OWNPV'           : " BPVIPCHI2() ", 
            'B0_ENDVERTEX_CHI2'         : " VFASPF(VCHI2) ",
            'B0_FDCHI2_OWNPV'           : " VFASPF(VMINVDCHI2DV(PRIMARY)) ",
            'B0_DIRA_OWNPV'             : " BPVDIRA "
            }


        self.MvaB02a1Pi = self.applyMVA( self.name + "MvaB02a1Pi",
                                             SelB        = self.SelB02a1Pi,
                                             MVAVars     = self.B02a1PiVars,
                                             MVACutValue = config['B02a1PiMVACut'], 
                                             MVAxmlFile  = config['B02a1PiXmlFile']
                                             )
        
        self.B02a1PiBDTLine = StrippingLine( self.name + 'Line',                                                
                                                prescale  = config['Prescale'],
                                                #algos     = [ self.SelB02a1Pi ],
                                                algos     = [ self.MvaB02a1Pi ],
                                                EnableFlavourTagging = True
                                                )
        self.registerLine( self.B02a1PiBDTLine )
        

    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )

        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

    def applyMVA( self, name, 
                  SelB,
                  MVAVars,
                  MVAxmlFile,
                  MVACutValue
                  ):
        from MVADictHelpers import addTMVAclassifierValue
        from Configurables import FilterDesktop as MVAFilterDesktop

        _FilterB = MVAFilterDesktop( name + "Filter",
                                     Code = "VALUE('LoKi::Hybrid::DictValue/" + name + "')>" + MVACutValue  )

        addTMVAclassifierValue( Component = _FilterB,
                                XMLFile   = MVAxmlFile,
                                Variables = MVAVars,
                                ToolName  = name )
        
        return Selection( name,
                          Algorithm =  _FilterB,
                          RequiredSelections = [ SelB ] )
