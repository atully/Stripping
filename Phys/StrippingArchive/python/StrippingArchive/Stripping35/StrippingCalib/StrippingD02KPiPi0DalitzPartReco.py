###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = ['Greg Ciezarek','Liang Sun','Adam Davis']
__date__ = '18/05/2017'
__version__ = '$Revision: 0.01 $'

'''
D* -> D0 pis, D0 -> K- pi+ pi0, pi0 -> e e gamma, photon and one electron unreconstructed 
'''
# =============================================================================
##
# D* -> D0 pis, D0 -> K- pi+ pi0, pi0 -> e e gamma, photon and one electron unreconstructed
#

"""
Stripping lines for  

Last modification $Date: 18/05/2017 $
               by $Author: gciezare$
"""
__all__ = ('D2KPiPi0_PartRecoBuilder',
           'TOSFilter',
           'default_config')

default_config = {
    'D2KPiPi0_PartReco' : {
        'WGs'         : ['Calib'],
        'BUILDERTYPE' : 'D2KPiPi0_PartRecoBuilder',
        'CONFIG'      :  {
            "KLepMassLow"     : 500  , #MeV
            "KLepMassHigh"    : 2500 , #MeV
            "DELTA_MASS_MAX"  : 190  , #MeV
            "TRGHOSTPROB"     : 0.35 ,#adimensional
            "ElectronPIDe"    : 5.  ,
            "ElectronPT"      : 000  ,#MeV
            "HadronPT"          : 800. ,#MeV
            "KaonPIDK"        : 5.   ,#adimensional
            "PionPIDK"        : -1.   ,#adimensional
            "HadronMINIPCHI2"   : 9    ,#adimensional
            "BVCHI2DOF"       : 6   ,#adminensional
            "BDIRA"           : 0.999,#adminensional
            "BFDCHI2HIGH"     : 100. ,#adimensional
            "BPVVDZcut"       : 0.0  , #mm
            #slow pion
            "Slowpion_PT"     : 300 #MeV
            ,"Slowpion_TRGHOSTPROB" : 0.35 #adimensional
            ,"Slowpion_PIDe" : 5 #adimensional
            ,"TOSFilter" : { 'Hlt2CharmHad.*HHX.*Decision%TOS' : 0}  #adimensional
            },
        'STREAMS'     : ['CharmCompleteEvent']	  
        }
    }
from Gaudi.Configuration import *
from StrippingUtils.Utils import LineBuilder

import logging

class D2KPiPi0_PartRecoBuilder(LineBuilder):
    """
    Definition of D* tagged D0 -> H- mu+(e+) nu stripping
    """
    
    __configuration_keys__ = default_config['D2KPiPi0_PartReco']['CONFIG'].keys()
    
    def __init__(self,name,config):
        LineBuilder.__init__(self, name, config)
        self._config=config
        from PhysSelPython.Wrappers import Selection, DataOnDemand
        
        #self.GECs = { "Code":"( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < %(GEC_nLongTrk)s )" % config,
        #              "Preambulo": ["from LoKiTracks.decorators import *"]}
        
        self._electronSel=None
        self._electronFilter()
        
        self._slowpionSel =None
        self._pionSel=None
        self._kaonSel=None
        self._kaonFilter()
        self._pionFilter()
        self._slowpionFilter()
        
        self.registerLine(self._D2KENuLine())
        #self.registerLine(self._D2KENuSSLine())

    def _NominalKSelection( self ):
        return "  (PT> %(HadronPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK  > %(KaonPIDK)s )"\
               "& (MIPCHI2DV(PRIMARY)> %(HadronMINIPCHI2)s )"

    def _NominalPiSelection( self ):
        #        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
        return " (PT> %(HadronPT)s *MeV)"\
               "& (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
               "& (PIDK < %(PionPIDK)s) "\
               "& (MIPCHI2DV(PRIMARY)> %(HadronMINIPCHI2)s )"

    def _NominalSlowPiSelection( self ):
        #        return "(TRCHI2DOF < %(KaonTRCHI2)s )&  (P> %(KaonP)s *MeV) &  (PT> %(KaonPT)s *MeV)"\
        return " (PT> %(Slowpion_PT)s *MeV)"\
               "& (TRGHOSTPROB < %(Slowpion_TRGHOSTPROB)s)"\
               "& (PIDe < %(Slowpion_PIDe)s) & (MIPCHI2DV(PRIMARY)< 9.0) "

    def _NominalElectronSelection( self ):
        return " (PT> %(ElectronPT)s *MeV) & (TRGHOSTPROB < %(TRGHOSTPROB)s)"\
                "& (PIDe > %(ElectronPIDe)s)"

    ######--######
    def _electronFilter( self ):
        if self._electronSel is not None:
            return self._electronSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseElectrons
        _el = FilterDesktop( Code = self._NominalElectronSelection() % self._config )
        _electronSel=Selection("Electron_for"+self._name,
                         Algorithm=_el,
                         RequiredSelections = [StdLooseElectrons])
        
        self._electronSel=_electronSel
        
        return _electronSel

    ######--######

    def _pionFilter( self ):
        if self._pionSel is not None:
            return self._pionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLoosePions
        
        _ka = FilterDesktop( Code = self._NominalPiSelection() % self._config )
        _kaSel=Selection("Pi_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdLoosePions])
        self._pionSel=_kaSel
        return _kaSel

    ######Kaon Filter######
    def _kaonFilter( self ):
        if self._kaonSel is not None:
            return self._kaonSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdLooseKaons
        
        _ka = FilterDesktop( Code = self._NominalKSelection() % self._config )
        _kaSel=Selection("K_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdLooseKaons])
        self._kaonSel=_kaSel
        return _kaSel

    ######Slow Pion Filter######
    def _slowpionFilter( self ):
        if self._slowpionSel is not None:
            return self._slowpionSel
        
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysSelPython.Wrappers import Selection
        from StandardParticles import StdAllLoosePions
        
        _ka = FilterDesktop( Code = self._NominalSlowPiSelection() % self._config )
        _kaSel=Selection("pis_for"+self._name,
                         Algorithm=_ka,
                         RequiredSelections = [StdAllLoosePions])
        self._slowpionSel=_kaSel
        return _kaSel

    ## OPPOSITE SIGN hl (SIGNAL)
    def _D2KENuLine( self ):
        return self.DstarMaker("D2KPiPi0_PartReco",["[D0 -> K- pi+ e+]cc","[D0 -> K- pi+ e-]cc"], self._kaonFilter(), self._pionFilter(), self._electronFilter())

    #def _D2KENuSSLine( self ):
    #    return self.DstarMaker("D2KESS",["[D0 -> K- pi+ e-]cc"], self._kaonFilter(), self._pionFilter(), self._electronFilter())

    def DstarMaker(self, _name, _D0Decays, kfiler, pifilter, lfilter):
        from GaudiConfUtils.ConfigurableGenerators import CombineParticles
        from PhysSelPython.Wrappers import Selection
        from StrippingConf.StrippingLine import StrippingLine
        
        _KMu = CombineParticles(
            DecayDescriptors = _D0Decays,
            CombinationCut = "(AM>%(KLepMassLow)s*MeV) & (AM<%(KLepMassHigh)s*MeV)" % self._config,
            MotherCut = "(VFASPF(VCHI2/VDOF)< %(BVCHI2DOF)s )"\
            "& (BPVVDCHI2 >%(BFDCHI2HIGH)s)"\
            "& (BPVDIRA > %(BDIRA)s)"\
            "& (BPVVDZ > %(BPVVDZcut)s)"
            #            "& (BPVVD > %(VDCut)s)"
            #" & (ratio > 0.0)"
            % self._config,
            ReFitPVs = True
            )
            
        _D0Sel=Selection("SelD0_for"+_name,
                         Algorithm=_KMu,
                         RequiredSelections = [lfilter, kfiler, pifilter])


        DstComb = CombineParticles( #name = "CombDst"+_name,
                DecayDescriptors = ['[D*(2010)+ -> D0 pi+]cc'],
                CombinationCut = "(AM - ACHILD(M,1) < %(DELTA_MASS_MAX)s+5 *MeV) & (ADOCACHI2CUT(20,''))" %self._config,
                MotherCut = "(M - CHILD(M,1) < %(DELTA_MASS_MAX)s *MeV) & (VFASPF(VCHI2/VDOF) < 9.0)" %self._config
                )
        DstSel = Selection("SelDst"+_name,
                Algorithm = DstComb,
                RequiredSelections = [_D0Sel,self._slowpionFilter()])

        _tosFilter = self._config['TOSFilter']
        DstSelTOS = TOSFilter( "SelDstHMu_Hlt2TOS"+_name
                ,DstSel
                ,_tosFilter)
    
        Line = StrippingLine(_name+'Line',
                prescale = 1.,
                RequiredRawEvents = ["Calo","Tracker","Velo"],
                selection = DstSelTOS) 
        return Line
                                                    
def TOSFilter( name, _input, _specs ) :
    from Configurables import TisTosParticleTagger
    from PhysSelPython.Wrappers import Selection 
    _tisTosFilter = TisTosParticleTagger( name + "Tagger" )
    _tisTosFilter.TisTosSpecs = _specs 
    return Selection( name
                      , Algorithm = _tisTosFilter
                      , RequiredSelections = [ _input ]
                      )        
