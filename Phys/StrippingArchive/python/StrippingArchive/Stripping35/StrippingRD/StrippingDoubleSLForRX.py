###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Stripping for double SL decays for
control mode for R(X) analyses.
"""

__author__  = 'M. Kreps, T. Blake, D. Loh'
__date__    = '10/11/2016'
__version__ = '$Revision: 1.0$'

__all__ = ( 'DoubleSLForRXConf', 'default_config' )

#
#  Define Cuts here
#

default_config = {
    'NAME'        : 'DoubleSLForRX',
    'BUILDERTYPE' : 'DoubleSLForRXConf',
    'CONFIG'      :
    {
    "BMassLow"          : 1000.0, 
    "BMassUpp"          : 5400.0,
    "DMassLow"          : 500.0 ,
    "DMassUpp"          : 2000.0,
    "DstarDeltaMass"    : 300,  
    "BDIRA"             : 0.995,
    "DDIRA"             : 0.995,
    "BDOCA"             : 10,
    "BVCHI2"            : 100,
    "BPT"               : 2000.  ,
    "DIPChisq"          : 4. ,
    "DFDChisq"          : 36.,
    "LeptonPT"          : 800. ,
    "LeptonIPChisq"     : 9,
    "KaonPT"            : 500. ,
    "KaonIPChisq"       : 9,
    "PionIPChisq"       : 4,
    "MuonProbNN"        : 0,
    "ElectronProbNN"    : 0.10,
    "KaonProbNN"        : 0.10,
    "DVertexChisq"      : 10,
    "DSeparation"       : -10, 
    "DPT"               : 800., 
    "TrackGhostProb"    : 0.5, 
    "HLT1_FILTER"       : None, 
    "HLT2_FILTER"       : None,
    "L0DU_FILTER"       : None,
    "SpdMult"           : 600, 
    "UseNoPIDParticles" : False,
    },
    'WGs'     : [ 'RD' ],
    'STREAMS' : ['Leptonic'] 
    }

#
# Global imports
#

from Gaudi.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import  ( CombineParticles,
                                                     FilterDesktop,
                                                     DaVinci__N3BodyDecays )

from PhysSelPython.Wrappers import ( Selection, MergedSelection )

from StrippingConf.StrippingLine import StrippingLine

from StrippingUtils.Utils import LineBuilder


#
#  LineBuiler 
#

class DoubleSLForRXConf(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config) :
        
        LineBuilder.__init__(self, name, config)
        self.name = name
        
        # Selection objects for particles 
        self.Leptons = self.__makeLeptons__( config )
        self.Kaons   = self.__makeKaons__( config )
        self.Pions   = self.__makePions__( config )
        self.D       = self.__makeD__( self.Kaons, self.Leptons, config )
        self.B       = self.__makeB__( self.D, self.Pions, self.Leptons, config )
        
        # GEC filter on SPD multiplicty 
        self.Filter = {
            'Code'      : " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < %(SpdMult)s )" % config ,
            'Preambulo' : [
            "from LoKiNumbers.decorators import *",
            "from LoKiCore.basic import LHCb"
            ]
            }
        
        # Define stripping line 
        self.Line = StrippingLine(
            self.name + "_Line",
            prescale = 1,
            #RelatedInfoTools = config['RelatedInfoTools'],
            HLT2     = config['HLT2_FILTER'],
            HLT1     = config['HLT1_FILTER'],
            L0DU     = config['L0DU_FILTER'],
            FILTER   = self.Filter,
            algos    = [ self.B ],
            MDSTFlag = False
            )
        
        self.registerLine( self.Line )

    def __ElectronCuts__( self, conf ):
        cuts = "(PT > %(LeptonPT)s) & " \
               "(MIPCHI2DV(PRIMARY) > %(LeptonIPChisq)s)" % conf
        if conf['UseNoPIDParticles']:
            return cuts
        return cuts + " & (HASRICH) & (PROBNNe > %(ElectronProbNN)s)" % conf  
        
    def __MuonCuts__( self, conf ):
        cuts = "(PT > %(LeptonPT)s) & " \
               "(MIPCHI2DV(PRIMARY) > %(LeptonIPChisq)s) & " \
               "(TRGHP < %(TrackGhostProb)s)" % conf
        if conf['UseNoPIDParticles']:
            return cuts
        return cuts + " & (HASRICH) & (PROBNNmu > %(MuonProbNN)s)" % conf

    def __KaonCuts__( self, conf ):
        cuts = "(PT > %(KaonPT)s) & " \
               "(MIPCHI2DV(PRIMARY) > %(KaonIPChisq)s) & " \
               "(TRGHP < %(TrackGhostProb)s)" % conf
        if conf['UseNoPIDParticles']:
            return cuts
        return cuts + " & (HASRICH) & (PROBNNk > %(KaonProbNN)s)" % conf 

    def __PionCuts__( self, conf ):
        return "(MIPCHI2DV(PRIMARY) > %(PionIPChisq)s) & " \
               "(TRGHP < %(TrackGhostProb)s)" % conf 
    
    def __DCuts__( self, conf ):
        return "(PT > %(DPT)s) & " \
               "(MIPCHI2DV(PRIMARY) > %(DIPChisq)s) & " \
               "(VFASPF(VCHI2PDOF) < %(DVertexChisq)s) & " \
               "(BPVVDCHI2 > %(DFDChisq)s)" % conf 
    
    def __DCombinationCuts__( self, conf ):
        return "(AM > %(DMassLow)s) & (AM < %(DMassUpp)s)" % conf 

    def __DstarCuts__( self, conf ):
        return "(M-MAXTREE('D0'==ABSID,M) < %(DstarDeltaMass)s) " % conf 
    
    def __BCuts__( self, conf ):
        return "(PT > %(BPT)s) & " \
               "(VFASPF(VCHI2) < %(BVCHI2)s) & " \
               "((MINTREE(ABSID=='D0', VFASPF(VZ))-VFASPF(VZ)) > %(DSeparation)s) & " \
               "(BPVDIRA > %(BDIRA)s)" % conf 

    def __BCombinationCuts__( self, conf ):
        return "(AM > %(BMassLow)s) & (AM < %(BMassUpp)s)" % conf 
    
    def __makeLeptons__( self, conf ):
        '''
        Create muons/electrons 
        '''
        from StandardParticles import ( StdAllLooseMuons,
                                        StdAllNoPIDsMuons,
                                        StdAllLooseElectrons,
                                        StdAllNoPIDsElectrons )
        muons     = None
        electrons = None
        
        if conf['UseNoPIDParticles']:
            muons     = StdAllNoPIDsMuons
            electrons = StdAllNoPIDsElectrons
        else:
            muons     = StdAllLooseMuons
            electrons = StdAllLooseElectrons

        # Filter electrons/muons
        filterElectrons = FilterDesktop( Code = self.__ElectronCuts__( conf ) )
        filterMuons     = FilterDesktop( Code = self.__MuonCuts__( conf ) )
                 
        selElectrons    = Selection( self.name + "_Electrons",
                                     Algorithm = filterElectrons,
                                     RequiredSelections = [ electrons ] )
        selMuons        = Selection( self.name + "_Muons" ,
                                     Algorithm = filterMuons,
                                     RequiredSelections = [ muons ] )

        # Merge electrons/muons into a single container
        selLeptons      = MergedSelection( self.name + "_Leptons",
                                           RequiredSelections = [ selElectrons, selMuons ] )
        return selLeptons


    
    def __makeKaons__(self, conf):
        '''
        Create Kaons
        '''
        from StandardParticles import ( StdAllLooseKaons, 
                                        StdAllNoPIDsKaons )

        # Filter kaons
        
        kaons = StdAllNoPIDsKaons if conf['UseNoPIDParticles'] else StdAllLooseKaons 
        
        filterKaons = FilterDesktop( Code = self.__KaonCuts__( conf ) )

        selKaons    = Selection( self.name + "_Kaons",
                                 RequiredSelections = [ kaons ] ,
                                 Algorithm = filterKaons )
        return selKaons


    def __makePions__( self, conf = {} ):
        """
        Make slow pions
        """
        from StandardParticles import ( StdAllNoPIDsPions )
        
        filterPions = FilterDesktop( Code = self.__PionCuts__( conf ) )

        selPions    = Selection( self.name + "_Pions",
                                 RequiredSelections = [ StdAllNoPIDsPions ] ,
                                 Algorithm = filterPions )
        return selPions

    def __makeD__( self, kaons = None , leptons = None , conf = {} ):
        """
        Make D --> K l 
        """
        # CombineParticles for the SL D decay

        combineD = CombineParticles()
        combineD.DecayDescriptors = [ "[D0 -> K- mu+]cc", "[D0 -> K- e+]cc" ] 
        combineD.CombinationCut   = self.__DCombinationCuts__( conf )
        combineD.MotherCut        = self.__DCuts__( conf )
        
        selD = Selection( self.name + "_D",
                          Algorithm = combineD,
                          RequiredSelections = [ kaons, leptons ] )
        return selD

    def __makeB__( self, dmesons = None, pions = None, leptons = None, conf  = {} ):
        """
        Make B --> D pi l 
        """
        
        combineB = DaVinci__N3BodyDecays()
        
        combineB.DecayDescriptors = [ "[B0 -> D~0 pi- mu+]cc", "[B0 -> D~0 pi- e+]cc" ]

        combineB.Combination12Cut = "((AM - AM1) < %(DstarDeltaMass)s)" % conf

        combineB.CombinationCut   = "(AM > %(BMassLow)s) & " \
                                    "(AM < %(BMassUpp)s) & " \
                                    "(ACHI2DOCA(2,3) < %(BDOCA)s)" % conf 

        combineB.MotherCut        = self.__BCuts__( conf )
        
        selB = Selection( self.name + "_B",
                          Algorithm = combineB,
                          RequiredSelections = [ dmesons, pions, leptons ] )
        return selB




#  def __makeDstar__( self, dmesons = None, pions = None,  conf = {} ):
#        """
#        Make D* --> D pi 
#       """
#     
#       # CombineParticles for the D* decay
#
#       combineDstar = CombineParticles()
#       combineDstar.DecayDescriptor = "[D*(2010)+ -> D0 pi+]cc"
#       combineDstar.CombinationCut  = "(AM < 2100*MeV)"
#       combineDstar.MotherCut       = self.__DstarCuts__( conf )
#       
#       selDstar = Selection( self.name + "_Dstar",
#                             Algorithm = combineDstar,
#                             RequiredSelections = [ dmesons, pions ] )
#       
#       return selDstar

#    def __makeB__( self, dstars = None , leptons = None , conf = {} ):
#        """
#        Make B --> D l 
#        """
#
#       # CombineParticles for the B decay
#       
#       combineB = CombineParticles()
#       combineB.DecayDescriptors = [ "[B0 -> D*(2010)- mu+]cc", "[B0 -> D*(2010)- e+]cc" ] 
#       combineB.CombinationCut   = self.__BCombinationCuts__( conf )
#       combineB.MotherCut        = self.__BCuts__( conf )
#       
#       selB = Selection( self.name + "_B",
#                         Algorithm = combineB,
#                         RequiredSelections = [ dstars, leptons ] )
#       return selB
