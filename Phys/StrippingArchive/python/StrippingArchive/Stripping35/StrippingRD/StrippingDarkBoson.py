###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
'''
Dark Bosons (ALPs, inflatons, WTFs, etc)

Author: M. Williams, D. Craik
'''
__author__ = ['Mike Williams', 'Dan Craik']

moduleName = 'DarkBoson'

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

import re
from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from PhysSelPython.Wrappers import EventSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from Configurables import LoKi__VoidFilter
from StandardParticles import StdAllNoPIDsPions, StdAllNoPIDsKaons, \
     StdAllNoPIDsProtons, StdNoPIDsUpPions, StdAllLooseMuons, \
     StdLooseDownMuons
from Beauty2Charm_LoKiCuts import LoKiCuts
from StandardParticles import StdAllLooseGammaLL, StdAllLooseGammaDD, \
    StdAllLooseElectrons, StdLooseAllPhotons#, StdDiElectronFromTracks
from Configurables import SubstitutePID
from copy import deepcopy
from CommonParticles.Utils import *
from Configurables import DiElectronMaker,ProtoParticleCALOFilter
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

# Default configuration dictionary
default_config = {
    'NAME' : 'DarkBoson',
    'BUILDERTYPE' : 'DarkBosonConf' ,
    'STREAMS' : {
        'Bhadron' : [
           'StrippingB2KX2KKPiDarkBosonLine',
           'StrippingB2KpiX2KKPiDarkBosonLine',
           'StrippingB2KX2KKPiMDarkBosonLine',
           'StrippingB2KpiX2KKPiMDarkBosonLine',
           'StrippingB2KX2PiPiPiDarkBosonLine',
           'StrippingB2KpiX2PiPiPiDarkBosonLine',
           'StrippingB2KX2PiPiPiMDarkBosonLine',
           'StrippingB2KpiX2PiPiPiMDarkBosonLine',
           'StrippingB2KX2EtaPiPi23PIDarkBosonLine',
           'StrippingB2KpiX2EtaPiPi23PIDarkBosonLine',
           'StrippingB2KX2EtaPiPi2GGDarkBosonLine',
           'StrippingB2KpiX2EtaPiPi2GGDarkBosonLine',
           'StrippingB2KX24PiDarkBosonLine',
           'StrippingB2KpiX24PiDarkBosonLine',
           'StrippingB2KX26PiDarkBosonLine',
           'StrippingB2KpiX26PiDarkBosonLine',
           'StrippingB2KX22K2PiDarkBosonLine',
           'StrippingB2KpiX22K2PiDarkBosonLine',
           'StrippingB2KX24KDarkBosonLine',
           'StrippingB2KpiX24KDarkBosonLine',
           ],
        'Dimuon' : [
           'StrippingB2KpiX2MuMuDDDarkBosonLine',
           'StrippingB2KpiX2MuMuDDSSDarkBosonLine',
           #'StrippingB2KKX2MuMuDDDarkBosonLine',
           #'StrippingB2KKX2MuMuDDSSDarkBosonLine',
           'StrippingB2KX2MuMuDDDarkBosonLine',
           'StrippingB2KX2MuMuDDSSDarkBosonLine',
           #'StrippingB2KpiX2PiPiDDDarkBosonLine',
           #'StrippingB2KKX2PiPiDDDarkBosonLine',
           #'StrippingB2KX2PiPiDDDarkBosonLine',
           #'StrippingB2KX2PiPiDDSSDarkBosonLine',
           #'StrippingB2RhoX2MuMuDDDarkBosonLine',
           #'StrippingB2KpiX2KKDDDarkBosonLine',
           #'StrippingB2KKX2KKDDDarkBosonLine',
           #'StrippingB2KX2KKDDDarkBosonLine',
           #'StrippingB2KX2KKDDSSDarkBosonLine',
           ],
        'Leptonic' : [
           'StrippingB2KpiX2MuMuDarkBosonLine',
           'StrippingB2KpiX2MuMuSSDarkBosonLine',
           #'StrippingB2KKX2MuMuDarkBosonLine',
           #'StrippingB2KKX2MuMuSSDarkBosonLine',
           'StrippingB2KX2MuMuDarkBosonLine',
           'StrippingB2KX2MuMuSSDarkBosonLine',
           'StrippingB2KpiX2PiPiDarkBosonLine',
           #'StrippingB2KKX2PiPiDarkBosonLine',
           'StrippingB2KX2PiPiDarkBosonLine',
           'StrippingB2KX2PiPiSSDarkBosonLine',
           #'StrippingB2RhoX2MuMuDarkBosonLine',
           'StrippingB2KpiX2KKDarkBosonLine',
           #'StrippingB2KKX2KKDarkBosonLine',
           'StrippingB2KX2KKDarkBosonLine',
           'StrippingB2KX2KKSSDarkBosonLine',
           'StrippingB2KpiX2EEDarkBosonLine',
           'StrippingB2KpiX2EESSDarkBosonLine',
           #'StrippingB2KKX2EEDarkBosonLine',
           #'StrippingB2KKX2EESSDarkBosonLine',
           'StrippingB2KX2EEDarkBosonLine',
           'StrippingB2KX2EESSDarkBosonLine',
           'StrippingB2JKDarkBosonLine',
           'StrippingB2JKstDarkBosonLine',
           'StrippingB2KstX2GammaGammaDarkBosonLine',
           'StrippingB2KstX2PiGammaDarkBosonLine',
           'StrippingB2KstX2PiGammaMDarkBosonLine',
           ]
    },
    'WGs'     : [ 'RD' ],
    'CONFIG'  : {
    "KB" : { # K directly from B
    'TRCHI2DOF_MAX' : 3,
    'PT_MIN'        : '250*MeV',
    'P_MIN'         : '2000*MeV',
    'MIPCHI2DV_MIN' : 9,
    'TRGHP_MAX'     : 0.3,
    'PROBNNK_MIN'   : 0.1,
    },
    "KBhard" : { # K directly from B only for B -> KS0(pipi) K
    'TRCHI2DOF_MAX' : 3,
    'PT_MIN'        : '250*MeV',
    'P_MIN'         : '3000*MeV',
    'MIPCHI2DV_MIN' : 36,
    'TRGHP_MAX'     : 0.3,
    'PROBNNK_MIN'   : 0.2,
    },
    "PiB" : { # pi directly from B
    'TRCHI2DOF_MAX' : 3,
    'PT_MIN'        : '250*MeV',
    'P_MIN'         : '2000*MeV',
    'MIPCHI2DV_MIN' : 9,
    'TRGHP_MAX'     : 0.3,
    'PROBNNpi_MIN'  : 0.2
    },
    "KX" : { # K from X
    'TRCHI2DOF_MAX' : 3,
    'PT_MIN'        : '250*MeV',
    'P_MIN'         : '3000*MeV',
    'MIPCHI2DV_MIN' : 25,
    'TRGHP_MAX'     : 0.3,
    'PROBNNK_MIN'   : 0.1
    },
    "PiX" : { # pi from X
    'TRCHI2DOF_MAX' : 3,
    'PT_MIN'        : '250*MeV',
    'P_MIN'         : '3000*MeV',
    'MIPCHI2DV_MIN' : 36,
    'TRGHP_MAX'     : 0.3,
    'PROBNNpi_MIN'  : 0.2
    },
    "KDX" : { # K DOWN from X
    'TRCHI2DOF_MAX' : 4,
    'PT_MIN'        : '125*MeV',
    'P_MIN'         : '0*MeV',
    'MIPCHI2DV_MIN' : 25,
    'TRGHP_MAX'     : 0.3,
    'PROBNNK_MIN'   : 0.1
    },
    "PiDX" : { # pi DOWN from X
    'TRCHI2DOF_MAX' : 4,
    'PT_MIN'        : '125*MeV',
    'P_MIN'         : '0*MeV',
    'MIPCHI2DV_MIN' : 25,
    'TRGHP_MAX'     : 0.3,
    'PROBNNpi_MIN'  : 0.1
    },
    "MuX" : { # muon from X
    'TRCHI2DOF_MAX' : 3,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '0*MeV',
    'MIPCHI2DV_MIN' : 9,
    'TRGHP_MAX'     : 0.3,
    'PIDmu_MIN'     : -5
    },
    "MuJ" : { # muon from J/psi
    'TRCHI2DOF_MAX' : 4,
    'PT_MIN'        : '125*MeV',
    'P_MIN'         : '0*MeV',
    'MIPCHI2DV_MIN' : 25,
    'TRGHP_MAX'     : 0.3,
    'PIDmu_MIN'     : -4
    },
    "E" : { # electron cuts
    'TRCHI2DOF_MAX' : 5,
    'MIPCHI2DV_MIN' : 9,
    'PT_MIN'        : '100*MeV',
    'TRGHP_MAX'     : 0.4,
    'PIDe_MIN'      : 0
    },
    "gX" : { # gamma from X
    'PT_MIN'        : '500*MeV',
    'P_MIN'         : '1000*MeV',
    'CL_MIN'        : '0.3',
    },
    "etaX" : { # eta->gg from X
    'MM_MIN'        : '450*MeV',
    'MM_MAX'        : '650*MeV',
    'PT_MIN'        : '500*MeV',
    'P_MIN'         : '2000*MeV',
    'CL_MIN'        : '0.2',
    },
    "pizMX" : { # merged pi0 from X
    'PT_MIN'        : '500*MeV',
    'P_MIN'         : '3000*MeV',
    'CL_MIN'        : '0.1',
    },
    "pizRX" : { # resolved pi0 from X
    'PT_MIN'        : '500*MeV',
    'CL_MIN'        : '0.1',
    },
    "XLL" : { # making the X for LL
    'VCHI2DOF_MAX'  : 10,
    'BPVVDCHI2_MIN' : 25,
    'PT_MIN'        : '250*MeV'
    },
    "XLLhard" : { # making the X for LL hard vertex for the pions
    'VCHI2DOF_MAX'  : 5,
    'BPVVDCHI2_MIN' : 25,
    'PT_MIN'        : '250*MeV'
    },
    "XDD" : { # making X for DD
    'VCHI2DOF_MAX'  : 15,
    'BPVVDCHI2_MIN' : 25,
    'PT_MIN'        : '0*MeV'
    },
    "XGG" : { # making the X for diphoton
    'PT_MIN'        : '2000*MeV'
    },
    "X3H" : { # making the X for 3H decays
    'VCHI2DOF_MAX'  : 10,
    'BPVVDCHI2_MIN' : 25,
    'PT_MIN'        : '500*MeV',
    'SUMPT_MIN'     : '0*MeV',
    'HAD_MINIPCHI2_MIN' : 16,
    },
    "XETAHH" : { # making the X for eta pipi decays
    'VCHI2DOF_MAX'  : 10,
    'BPVVDCHI2_MIN' : 25,
    'PT_MIN'        : '2000*MeV',
    'SUMPT_MIN'     : '0*MeV',
    'HAD_MINIPCHI2_MIN' : 16,
    },
    "J" : { # cuts on J/psi
    'VCHI2DOF_MAX'  : 12,
    'ADAMASS_MAX'   : '100*MeV',
    },
    "B2KX" : { # B -> K X
    'SUMPT_MIN'     : '0*MeV',
    'VCHI2DOF_MAX'  : 15,
    'BPVIPCHI2_MAX' : 10,
    'BPVLTIME_MIN'  : '0.2*ps',
    'AM_MIN'        : '4800*MeV',
    'AM_MAX'        : '5800*MeV',
    'HAD_MINIPCHI2_MIN' : 25,
    'PT_MIN'        : '1000*MeV'
    },
    "B2KXTight" : { # B -> K X
    'SUMPT_MIN'     : '0*MeV',
    'VCHI2DOF_MAX'  : 15,
    'BPVIPCHI2_MAX' : 10,
    'BPVLTIME_MIN'  : '0.2*ps',
    'AM_MIN'        : '4800*MeV',
    'AM_MAX'        : '5800*MeV',
    'HAD_MINIPCHI2_MIN' : 25,
    'PT_MIN'        : '3000*MeV'
    },
    "B2HHX" : { # B -> Kpi X, KK X
    'SUMPT_MIN'     : '0*MeV',
    'VCHI2DOF_MAX'  : 15,
    'BPVIPCHI2_MAX' : 10,
    'BPVLTIME_MIN'  : '0.2*ps',
    'AM_MIN'        : '4800*MeV',
    'AM_MAX'        : '5800*MeV',
    'HAD_MINIPCHI2_MIN' : 9,
    'PT_MIN'        : '1000*MeV'
    },
    "B2HHXTight" : { # B -> Kpi X, KK X
    'SUMPT_MIN'     : '0*MeV',
    'VCHI2DOF_MAX'  : 15,
    'BPVIPCHI2_MAX' : 10,
    'BPVLTIME_MIN'  : '0.2*ps',
    'AM_MIN'        : '4800*MeV',
    'AM_MAX'        : '5800*MeV',
    'HAD_MINIPCHI2_MIN' : 9,
    'PT_MIN'        : '3000*MeV'
    },
    "B2KstX_X2neutrals" : { # B -> Kpi X, X -> gg, gpi0
    'SUMPT_MIN'     : '0*MeV',
    'BPVIPCHI2_MAX' : 10,
    'BPVLTIME_MIN'  : '0.2*ps',
    #'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '4800*MeV',
    'AM_MAX'        : '5800*MeV',
    'PT_MIN'        : '3000*MeV'
    },
    "Prescales" : { "SS" : 0.1, "DD" : 1.0, "KK": 0.25},
    "GECNTrkMax" : 250,
    "CommonRelInfoTools" : [ { "Type": "RelInfoVertexIsolation", "Location":"VtxIsoInfo" },
                             { "Type": "RelInfoVertexIsolationBDT", "Location":"VtxIsoInfoBDT" },
                             { "Type" : "RelInfoBs2MuMuBIsolations",
                               "Variables" : [],
                               "Location"  : "BsMuMuBIsolation",
                               "tracktype" : 3,
                               "makeTrackCuts" : False, },
                             ]
    }
}

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

class DarkBosonConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, moduleName, default_config) :

        LineBuilder.__init__(self, moduleName, default_config)
        piD = DataOnDemand(Location="Phys/StdNoPIDsDownPions/Particles")
        kD = DataOnDemand(Location="Phys/StdLooseDownKaons/Particles")
        pizM = DataOnDemand(Location="Phys/StdLooseMergedPi0/Particles")
        pizR = DataOnDemand(Location="Phys/StdLooseResolvedPi0/Particles")
        eta = DataOnDemand(Location="Phys/StdLooseResolvedEta/Particles")
        gC = MergedSelection("gC",RequiredSelections=[StdAllLooseGammaLL,StdAllLooseGammaDD])

        # pre-filter inputs
        kB = filterInputs('KB',default_config['KB'],[StdAllNoPIDsKaons])
        kBhard = filterInputs('KBhard',default_config['KBhard'],[StdAllNoPIDsKaons])
        piB = filterInputs('PiB',default_config['PiB'],[StdAllNoPIDsPions])
        kX = filterInputs('KX',default_config['KX'],[StdAllNoPIDsKaons])
        piX = filterInputs('PiX',default_config['PiX'],[StdAllNoPIDsPions])
        kDX = filterInputs('KDX',default_config['KDX'],[kD])
        piDX = filterInputs('PiDX',default_config['PiDX'],[piD])
        muX = filterInputs('MuX',default_config['MuX'],[StdAllLooseMuons])
        muDX = filterInputs('MuDX',default_config['MuX'],[StdLooseDownMuons])
        muJ = filterInputs('MuJ',default_config['MuJ'],[StdAllLooseMuons])

        gX = filterInputs('gX',default_config['gX'],[StdLooseAllPhotons])
        pizMX = filterInputs('pizMX',default_config['pizMX'],[pizM])
        pizRX = filterInputs('pizRX',default_config['pizRX'],[pizR])
        etaX = filterInputs('etaX',default_config['etaX'],[eta])

        eta23piX = makeEta([piX,pizRX],"ForX")

        # make/filter X
        eestd = DataOnDemand(Location='Phys/StdDiElectronFromTracks/Particles')
        ee = filterEE('OS',default_config['E'],default_config['XLL'],[eestd])
        #eeDD = makeEEdd(config)
        mmX = makeX('MuMu',default_config['XLL'],'KS0 -> mu+ mu-',[muX])
        mmDDX = makeX('MuMuDD',default_config['XDD'],'KS0 -> mu+ mu-',[muDX])
        kkX = makeX('KK',default_config['XLL'],'KS0 -> K+ K-',[kX])
        kkDDX = makeX('KKDD',default_config['XDD'],'KS0 -> K+ K-',[kDX])
        pipiX = makeX('PiPi',default_config['XLLhard'],'KS0 -> pi+ pi-',[piX])
        pipiDDX = makeX('PiPiDD',default_config['XDD'],'KS0 -> pi+ pi-',[piDX])
        ## 4-6H modes
        fourpiX = makeX('4Pi',  default_config['XLL'],'KS0 -> pi+ pi+ pi- pi-',[piX])
        sixpiX  = makeX('6Pi',  default_config['XLL'],'KS0 -> pi+ pi+ pi- pi- pi0 pi0',[piX,pizRX])
        KKpipiX = makeX('2K2Pi',default_config['XLL'],'KS0 -> K+ K- pi+ pi-',[kX,piX])
        fourKX  = makeX('4K',   default_config['XLL'],'KS0 -> K+ K+ K- K-',[kX])
        ## gamma modes
        ggX   = makeX2neutrals('GammaGamma',default_config['XGG'],'pi0 -> gamma gamma',[gX])
        pigX  = makeX2neutrals('PiGamma',   default_config['XGG'],'eta -> pi0 gamma',[gX,pizRX])
        pigMX = makeX2neutrals('PiGammaM',  default_config['XGG'],'eta -> pi0 gamma',[gX,pizMX])
        ##HHH modes
        kkpiX       = makeX('KKPi',       default_config['X3H'],'KS0 -> K+ K- pi0',[kX,pizRX])
        kkpiMX      = makeX('KKPiM',      default_config['X3H'],'KS0 -> K+ K- pi0',[kX,pizMX])
        pipipiX     = makeX('PiPiPi',     default_config['X3H'],'KS0 -> pi+ pi- pi0',[piX,pizRX])
        pipipiMX    = makeX('PiPiPiM',    default_config['X3H'],'KS0 -> pi+ pi- pi0',[piX,pizMX])
        #kkpiDDX     = makeX('KKPiDD',     default_config['XDD'],'KS0 -> K+ K- pi0',[kDX,pizRX])
        #kkpiDDMX    = makeX('KKPiDDM',    default_config['XDD'],'KS0 -> K+ K- pi0',[kDX,pizMX])
        #pipipiDDX   = makeX('PiPiPiDD',   default_config['XDD'],'KS0 -> pi+ pi- pi0',[piDX,pizRX])
        #pipipiDDMX  = makeX('PiPiPiDDM',  default_config['XDD'],'KS0 -> pi+ pi- pi0',[piDX,pizMX])
        etapipiGGX  = makeX('EtaPiPi2GG', default_config['XETAHH'],'KS0 -> eta pi+ pi-',[etaX,piX])
        etapipi3PIX = makeX('EtaPiPi23PI',default_config['XETAHH'],'KS0 -> eta pi+ pi-',[eta23piX,piX])

        # SS X
        eeSS = filterEE('SS',default_config['E'],default_config['XLL'],[makeEESS()])
        mmSSX = makeX('MuMuSS',default_config['XLL'],'[KS0 -> mu+ mu+]cc',[muX])
        mmSSDDX = makeX('MuMuSSDD',default_config['XDD'],'[KS0 -> mu+ mu+]cc',[muDX])
        kkSSX = makeX('KKSS',default_config['XLL'],'[KS0 -> K+ K+]cc',[kX])
        kkSSDDX = makeX('KKSSDD',default_config['XDD'],'[KS0 -> K+ K+]cc',[kDX])
        pipiSSX = makeX('PiPiSS',default_config['XLL'],'[KS0 -> pi+ pi+]cc',[piX])
        pipiSSDDX = makeX('PiPiSSDD',default_config['XDD'],'[KS0 -> pi+ pi+]cc',[piDX])
        preSS = default_config['Prescales']['SS']

        preDD = default_config['Prescales']['DD']
        preKK = default_config['Prescales']['KK']

        j = makeJ(default_config['J'],[muJ])
        kpiB = makeKst([kB,piB])

        # B -> K pi lines
        self._makeBLine('B2KpiX2MuMu',['[B0 -> KS0 K+ pi-]cc'],
                        [mmX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2MuMuDD',['[B0 -> K+ pi- KS0]cc'],
                        [mmDDX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2EE',['[B0 -> K+ pi- KS0]cc'],
                        [ee,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2KK',['[B0 -> K+ pi- KS0]cc'],
                        [kkX,kB,piB],default_config['B2HHX'],preKK)

        #self._makeBLine('B2KpiX2KKDD',['[B0 -> K+ pi- KS0]cc'],
        #                [kkDDX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2KKPi',['[B0 -> K+ pi- KS0]cc'],
                        [kkpiX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2KKPiM',['[B0 -> K+ pi- KS0]cc'],
                        [kkpiMX,kB,piB],default_config['B2HHX'])

        #self._makeBLine('B2KpiX2KKPiDD',['[B0 -> K+ pi- KS0]cc'],
        #                [kkpiDDX,kB,piB],default_config['B2HHX'])

        #self._makeBLine('B2KpiX2KKPiDDM',['[B0 -> K+ pi- KS0]cc'],
        #                [kkpiDDMX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2PiPi',['[B0 -> K+ pi- KS0]cc'],
                        [pipiX,kB,piB],default_config['B2HHX'],preSS)

        #self._makeBLine('B2KpiX2PiPiDD',['[B0 -> K+ pi- KS0]cc'],
        #                [pipiDDX,kB,piB],default_config['B2HHX'],preDD)

        self._makeBLine('B2KpiX2PiPiPi',['[B0 -> K+ pi- KS0]cc'],
                        [pipipiX,kB,piB],default_config['B2HHXTight'])

        self._makeBLine('B2KpiX2PiPiPiM',['[B0 -> K+ pi- KS0]cc'],
                        [pipipiMX,kB,piB],default_config['B2HHX'])

        #self._makeBLine('B2KpiX2PiPiPiDD',['[B0 -> K+ pi- KS0]cc'],
        #                [pipipiDDX,kB,piB],default_config['B2HHX'])

        #self._makeBLine('B2KpiX2PiPiPiDDM',['[B0 -> K+ pi- KS0]cc'],
        #                [pipipiDDMX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2EtaPiPi2GG',['[B0 -> K+ pi- KS0]cc'],
                        [etapipiGGX,kB,piB],default_config['B2HHXTight'])

        self._makeBLine('B2KpiX2EtaPiPi23PI',['[B0 -> K+ pi- KS0]cc'],
                        [etapipi3PIX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX24Pi',['[B0 -> K+ pi- KS0]cc'],
                        [fourpiX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX26Pi',['[B0 -> K+ pi- KS0]cc'],
                        [sixpiX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX22K2Pi',['[B0 -> K+ pi- KS0]cc'],
                        [KKpipiX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX24K',['[B0 -> K+ pi- KS0]cc'],
                        [fourKX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2MuMuSS',['[B0 -> K+ pi- KS0]cc'],
                        [mmSSX,kB,piB],default_config['B2HHX'],preSS)

        self._makeBLine('B2KpiX2MuMuDDSS',['[B0 -> K+ pi- KS0]cc'],
                        [mmSSDDX,kB,piB],default_config['B2HHX'])

        self._makeBLine('B2KpiX2EESS',['[B0 -> K+ pi- KS0]cc'],
                        [eeSS,kB,piB],default_config['B2HHX'],preSS)

        self._makeBLine('B2KstX2GammaGamma',['[B0 -> K*(892)0 pi0]cc'],
                        [ggX,kpiB],default_config['B2KstX_X2neutrals'])

        self._makeBLine('B2KstX2PiGamma',['[B0 -> K*(892)0 eta]cc'],
                        [pigX,kpiB],default_config['B2KstX_X2neutrals'])

        self._makeBLine('B2KstX2PiGammaM',['[B0 -> K*(892)0 eta]cc'],
                        [pigMX,kpiB],default_config['B2KstX_X2neutrals'])

        # B -> K K lines
        #self._makeBLine('B2KKX2MuMu',['B0 -> K+ K- KS0'],
        #                [mmX,kB],default_config['B2HHX'])

        #self._makeBLine('B2KKX2MuMuDD',['B0 -> K+ K- KS0'],
        #                [mmDDX,kB],default_config['B2HHX'])

        #self._makeBLine('B2KKX2EE',['B0 -> K+ K- KS0'],
        #                [ee,kB],default_config['B2HHX'])

        #self._makeBLine('B2KKX2MuMuSS',['B0 -> K+ K- KS0'],
        #                [mmSSX,kB],default_config['B2HHX'],preSS)

        #self._makeBLine('B2KKX2MuMuDDSS',['B0 -> K+ K- KS0'],
        #                [mmSSDDX,kB],default_config['B2HHX'])

        #self._makeBLine('B2KKX2EESS',['B0 -> K+ K- KS0'],
        #                [eeSS,kB],default_config['B2HHX'],preSS)

        #self._makeBLine('B2KKX2KK',['B0 -> K+ K- KS0'],
        #                [kkX,kB],default_config['B2HHX'])

        #self._makeBLine('B2KKX2KKDD',['B0 -> K+ K- KS0'],
        #                [kkDDX,kB],default_config['B2HHX'])

        #self._makeBLine('B2KKX2PiPi',['B0 -> K+ K- KS0'],
        #                [pipiX,kB],default_config['B2HHX'],preDD)

        #self._makeBLine('B2KKX2PiPiDD',['B0 -> K+ K- KS0'],
        #                [pipiDDX,kB],default_config['B2HHX'])

        # B -> K lines
        self._makeBLine('B2KX2MuMu',['[B+ -> K+ KS0]cc'],
                        [mmX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2MuMuDD',['[B+ -> K+ KS0]cc'],
                        [mmDDX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2EE',['[B+ -> K+ KS0]cc'],
                        [ee,kB],default_config['B2KX'])

        self._makeBLine('B2KX2KK',['[B+ -> K+ KS0]cc'],
                        [kkX,kB],default_config['B2KX'])

        #self._makeBLine('B2KX2KKDD',['[B+ -> K+ KS0]cc'],
        #                [kkDDX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2KKPi',['[B+ -> K+ KS0]cc'],
                        [kkpiX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2KKPiM',['[B+ -> K+ KS0]cc'],
                        [kkpiMX,kB],default_config['B2KX'])

        #self._makeBLine('B2KX2KKPiDD',['[B+ -> K+ KS0]cc'],
        #                [kkpiDDX,kB],default_config['B2KX'])

        #self._makeBLine('B2KX2KKPiDDM',['[B+ -> K+ KS0]cc'],
        #                [kkpiDDMX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2PiPi',['[B+ -> K+ KS0]cc'],
                        [pipiX,kBhard],default_config['B2KX'])

        #self._makeBLine('B2KX2PiPiDD',['[B+ -> K+ KS0]cc'],
        #                [pipiDDX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2PiPiPi',['[B+ -> K+ KS0]cc'],
                        [pipipiX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2PiPiPiM',['[B+ -> K+ KS0]cc'],
                        [pipipiMX,kB],default_config['B2KX'])

        #self._makeBLine('B2KX2PiPiPiDD',['[B+ -> K+ KS0]cc'],
        #                [pipipiDDX,kB],default_config['B2KX'])

        #self._makeBLine('B2KX2PiPiPiDDM',['[B+ -> K+ KS0]cc'],
        #                [pipipiDDMX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2EtaPiPi2GG',['[B+ -> K+ KS0]cc'],
                        [etapipiGGX,kB],default_config['B2KXTight'])

        self._makeBLine('B2KX2EtaPiPi23PI',['[B+ -> K+ KS0]cc'],
                        [etapipi3PIX,kB],default_config['B2KX'])

        self._makeBLine('B2KX24Pi',['[B+ -> K+ KS0]cc'],
                        [fourpiX,kB],default_config['B2KX'])

        self._makeBLine('B2KX26Pi',['[B+ -> K+ KS0]cc'],
                        [sixpiX,kB],default_config['B2KX'])

        self._makeBLine('B2KX22K2Pi',['[B+ -> K+ KS0]cc'],
                        [KKpipiX,kB],default_config['B2KX'])

        self._makeBLine('B2KX24K',['[B+ -> K+ KS0]cc'],
                        [fourKX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2MuMuSS',['[B+ -> K+ KS0]cc'],
                        [mmSSX,kB],default_config['B2KX'],preSS)

        self._makeBLine('B2KX2MuMuDDSS',['[B+ -> K+ KS0]cc'],
                        [mmSSDDX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2EESS',['[B+ -> K+ KS0]cc'],
                        [eeSS,kB],default_config['B2KX'],preSS)

        self._makeBLine('B2KX2KKSS',['[B+ -> K+ KS0]cc'],
                        [kkSSX,kB],default_config['B2KX'],preSS)

        #self._makeBLine('B2KX2KKDDSS',['[B+ -> K+ KS0]cc'],
        #                [kkSSDDX,kB],default_config['B2KX'])

        self._makeBLine('B2KX2PiPiSS',['[B+ -> K+ KS0]cc'],
                        [pipiSSX,kB],default_config['B2KX'],preSS)

        #self._makeBLine('B2KX2PiPiDDSS',['[B+ -> K+ KS0]cc'],
        #                [pipiSSDDX,kB],default_config['B2KX'])

        # B -> J/psi lines
        self._makeBLine('B2JK',['[B+ -> J/psi(1S) K+]cc'],
                        [j,kB],default_config['B2KX'])

        kst = filterKst()
        self._makeBLine('B2JKst',['[B0 -> J/psi(1S) K*(892)0]cc'],
                        [j,kst],default_config['B2KX'])
        rho = makeRho([piB])
        #self._makeBLine('B2RhoX2MuMu',['B0 -> rho(770)0 KS0'],[rho,mmX],
        #                default_config['B2HHX'])
        #self._makeBLine('B2RhoX2MuMuDD',['B0 -> rho(770)0 KS0'],[rho,mmDDX],
        #                default_config['B2HHX'])


    def _makeBLine(self,tag,dec,inputs,default_config,pre=1.0):
        sel = makeB(tag,dec,inputs,default_config)
        self._makeLine(tag,sel,pre)

    def _makeLine(self,name,sel,pre):
        filt = {'Code' :
                "(recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG)"\
                " < %s )" \
                % default_config['CONFIG']['GECNTrkMax'],
                'Preambulo' : [ "from LoKiTracks.decorators import *",
                                'from LoKiCore.functions    import *' ]}
        name = name + "DarkBosonLine"
        hlt=""
        if name.find('KstX2PiGamma') > -1 or name.find('KstX2GammaGamma') > -1:
            hlt = "HLT_PASS_RE('Hlt2Topo2Body.*Decision')"
        elif name.find('MuMu'):
            pass
        else:
            hlt = "HLT_PASS_RE('Hlt2Topo.*Body.*Decision')"
        relInfoTools=self._getRelInfo(name)
        #if not relInfoTools:
        #    relInfoTools = default_config['CONFIG']['CommonRelInfoTools']

        sline = StrippingLine(name,pre,selection=sel,HLT2=hlt,checkPV=True,FILTER=filt,
                              EnableFlavourTagging=False,
                              RelatedInfoTools = relInfoTools ) #default_config['CONFIG']['CommonRelInfoTools'] )
        self.registerLine(sline)

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
    def _getRelInfo(self,name):
        relInfoTools = []
        if name.find('DD')>-1:
            return relInfoTools ## DD lines go to DST so no relInfoTools
        decStr = self._getDecStr(name)
        if decStr=="":
            return relInfoTools ## decay not known
        dls = self._getDaugLocs(decStr,"ConeIso05p")
        relInfoTools.append(
        { "Type" : "RelInfoConeVariables",
          "ConeAngle" : 0.5,
          "Variables" : [],
          "Location"  : 'ConeIso05B0',
          "DaughterLocations" : dls, } )
        dls = self._getDaugLocs(decStr,"ConeIso10p")
        relInfoTools.append(
        { "Type" : "RelInfoConeVariables",
          "ConeAngle" : 1.0,
          "Variables" : [],
          "Location"  : 'ConeIso10B0',
          "DaughterLocations" : dls, } )
        dls = self._getDaugLocs(decStr,"ConeIso15p")
        relInfoTools.append(
        { "Type" : "RelInfoConeVariables",
          "ConeAngle" : 1.5,
          "Variables" : [],
          "Location"  : 'ConeIso15B0',
          "DaughterLocations" : dls, } )
#        dls = self._getDaugLocs(name,"TrackIsoBDTp")
#        relInfoTools.append(
#        { "Type": "RelInfoTrackIsolationBDT",
#          "Variables" : 0,
#          "DaughterLocations" : dls, } )
#        dls = self._getDaugLocs(name,"BsMuMuTrackIsop")
#        relInfoTools.append(
#        { "Type" : "RelInfoBs2MuMuTrackIsolations",
#          "Variables" : [],
#          "IsoTwoBody" : True,
#          "DaughterLocations" : dls, } )
        relInfoTools.extend(default_config['CONFIG']['CommonRelInfoTools'])
        return relInfoTools

    def _getDaugLocs(self,decay,locName):
        dls = {}
        decay = decay.split('^')
        for i in range(1,len(decay)):
            dec="^".join([" ".join(decay[:i])," ".join(decay[i:])])
            loc=locName+str(i)
            dls[dec] = loc
        return dls

    def _getDecStr(self,name):
        decay = ""
        if name.find('JKst')>-1:
            decay = "[ Beauty -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(K*(892)0 -> ^K+ ^pi-) ]CC"
        elif name.find('JK')>-1:
            decay = "[ Beauty -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+ ]CC"
        else:
            bpart = self._getBpart(name)
            xpart = self._getXpart(name)
            if bpart and xpart:
                decay = "".join(["[ ",bpart,"^(",xpart,") ]CC"])
        #print "getDecStr: ",name," ",decay
        return decay

    def _getXpart(self,name):
        ret=None
        if name.find('X26Pi')>-1:
            ret= 'KS0 -> ^pi+ ^pi+ ^pi- ^pi- ^(pi0 -> ^gamma ^gamma) ^(pi0 -> ^gamma ^gamma) '
        elif name.find('X24Pi')>-1:
            ret= 'KS0 -> ^pi+ ^pi+ ^pi- ^pi-'
        elif name.find('X22K2Pi')>-1:
            ret= 'KS0 -> ^K+ ^pi+ ^K- ^pi-'
        elif name.find('X24K')>-1:
            ret= 'KS0 -> ^K+ ^K+ ^K- ^K-'
        elif name.find('X2PiPiPi')>-1:
            ret= 'KS0 -> ^pi+ ^pi- ^pi0'
            if name.find('M')==-1:
                ret = ret.replace("pi0","(pi0 -> ^gamma ^gamma) ")
        elif name.find('X2KKPi')>-1:
            ret= 'KS0 -> ^K+ ^K- ^pi0'
            if name.find('M')==-1:
                ret = ret.replace("pi0","(pi0 -> ^gamma ^gamma) ")
        elif name.find('X2EtaPiPi')>-1:
            ret= 'KS0 -> ^eta ^pi+ ^pi-'
            if name.find('2GG')>-1:
                ret = ret.replace("eta","(eta -> ^gamma ^gamma) ")
            else:
                ret = ret.replace("eta","(eta -> ^pi+ ^pi- ^(pi0 -> ^gamma ^gamma) ) ")
        elif name.find('X2MuMu')>-1:
            ret= 'KS0 -> ^mu+ ^mu-'
            if name.find('SS')>-1:
                ret = "[ "+ret[:-1]+"+ ]CC "
        elif name.find('X2EE')>-1:
            ret= 'KS0 -> ^e+ ^e-'
            if name.find('SS')>-1:
                ret = "[ "+ret[:-1]+"+ ]CC "
        elif name.find('X2PiPi')>-1:
            ret= 'KS0 -> ^pi+ ^pi-'
            if name.find('SS')>-1:
                ret = "[ "+ret[:-1]+"+ ]CC "
        elif name.find('X2KK')>-1:
            ret= 'KS0 -> ^K+ ^K-'
            if name.find('SS')>-1:
                ret = "[ "+ret[:-1]+"+ ]CC "
        elif name.find('X2GammaGamma')>-1:
            ret= 'pi0 -> ^gamma ^gamma'
        elif name.find('X2PiGamma')>-1:
            ret= 'eta -> ^pi0 ^gamma'
            if name.find('M')==-1:
                ret = ret.replace("pi0","(pi0 -> ^gamma ^gamma) ")
        return ret

    def _getBpart(self,name):
        if name.find('B2KX')>-1:
            return "Beauty -> ^K+ "
        elif name.find('B2KpiX')>-1:
            return "Beauty -> ^K+ ^pi- "
        elif name.find('B2KstX')>-1:
            return "Beauty -> ^(K*(892)0 ->  ^K+  ^pi-) "

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

def filterInputs(which,conf,inputs):
    keys = [key.split('_')[0] for key in conf.keys()]
    code = LoKiCuts(keys,conf).code()
    #code = LoKiCuts.combine(['INMUON',code])
    photonCut = "(NINGENERATION( (ID=='gamma') & ((PT < 500*MeV) | (CL < 0.3)),1)==0)"
    code = LoKiCuts.combine([photonCut,code])
    #print "filterInputs: ", which, code
    return Selection(which+'DarkBosonFilter',Algorithm=FilterDesktop(Code=code),
                     RequiredSelections=inputs)

def filterEE(which,econf,xconf,inputs):
    code = "(ID=='J/psi(1S)') & (PT > %s) & (MINTREE('e+'==ABSID,PIDe) > %s)" \
           % (xconf['PT_MIN'],econf['PIDe_MIN'])
    code += " & (MINTREE('e+'==ABSID,MIPCHI2DV(PRIMARY)) > %s)" \
            % econf['MIPCHI2DV_MIN']
    code += " & (MINTREE('e+'==ABSID,PT) > %s)" % econf['PT_MIN']
    code += " & (MAXTREE('e+'==ABSID,TRGHP) < %s)" % econf['TRGHP_MAX']
    code = LoKiCuts.combine([code,LoKiCuts(['HASVERTEX','BPVVDCHI2','VCHI2DOF'],
                                           xconf).code()])
    #print "filterEE: ", code
    sel = Selection(which+'FilterEEDarkBosonFilter',
                    Algorithm=FilterDesktop(Code=code),
                    RequiredSelections=inputs)
    sub = SubstitutePID(which+'EESubPIDDarkBoson',
                        Code="DECTREE('J/psi(1S) -> e+ e-')")
    sub.MaxChi2PerDoF = -666
    sub.Substitutions = {'J/psi(1S) -> e+ e-'  : 'KS0'}
    return Selection(which+'EESubPIDDarkBosonSel',Algorithm=sub,
                     RequiredSelections=[sel])

def makeEEdd(default_config):
    eedd = DiElectronMaker('EEDDForDarkBoson')
    eedd.Particle = "KS0"
    selector = trackSelector(eedd, trackTypes=["Downstream"])
    eedd.addTool(ProtoParticleCALOFilter, name='Electron')
    eedd.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>'%s'" \
                               % default_config['PID']['E']['PIDe_MIN']]
    eedd.DiElectronMassMax = 5000
    eedd.DiElectronMassMin = 0
    eedd.DiElectronPtMin = 250
    eedd.ElectronPtMin = 100
    eedd.ElectronPIDcut = default_config['PID']['E']['PIDe_MIN']

    sel = Selection('EEDDDarkBosonSel',Algorithm=eedd)
    code = LoKiCuts(['HASVERTEX','BPVVDCHI2','VCHI2DOF'],default_config['V']).code()
    #print 'makeEEdd', code
    return Selection('FilterEEDDDarkBoson',Algorithm=FilterDesktop(Code=code),
                     RequiredSelections=[sel])

def makeEESS():
    eess = DiElectronMaker('EESSForDarkBoson')
    eess.Particle = "J/psi(1S)"
    selector = trackSelector(eess, trackTypes=["Long"])
    eess.addTool(ProtoParticleCALOFilter, name='Electron')
    eess.Electron.Selection = ["RequiresDet='CALO' CombDLL(e-pi)>-2"]
    eess.DiElectronMassMax = 5000
    eess.DiElectronMassMin = 0
    eess.DiElectronPtMin = 250
    eess.ElectronPtMin = 100
    eess.OppositeSign = False
    return Selection('EESSDarkBosonSel',Algorithm=eess)

def filterKst():
    kst = DataOnDemand(Location='Phys/StdLooseDetachedKst2Kpi/Particles')
    code = '(MIPCHI2DV(PRIMARY) > 25)'
    return Selection('KstDarkBosonFilter',Algorithm=FilterDesktop(Code=code),
                     RequiredSelections=[kst])

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

def makeX(which,default_config,dec,inputs):
    comboCuts = "(AM < 5000*MeV) & (APT > %s)" % default_config['PT_MIN']
    if(which.find('DD') < 0):
        comboCuts += " & (ACUTDOCA(0.2*mm,''))"
    comboCuts += "& (ADOCACHI2CUT(25,''))"
    momCuts = LoKiCuts(['HASVERTEX','BPVVDCHI2','VCHI2DOF'],default_config).code()
    momCuts = LoKiCuts.combine(['(BPVDIRA > 0)',momCuts])
    if default_config.has_key('HAD_MINIPCHI2_MIN'):
        momCuts += '& (NINGENERATION(ISBASIC & HASTRACK & (MIPCHI2DV(PRIMARY) < %s),1)==0)' \
                   % default_config['HAD_MINIPCHI2_MIN']
    x = CombineParticles(DecayDescriptors=[dec],
                         CombinationCut=comboCuts,MotherCut=momCuts)
    #print 'makeX:',which,comboCuts,momCuts
    return Selection("X2"+which+"DarkBosonSel",Algorithm=x,
                     RequiredSelections=inputs)

def makeX2neutrals(which,default_config,dec,inputs):
    #momCuts = "(MINTREE('e+'==ABSID,PIDe)>0.0)"
    momCuts = "(M < 5000*MeV) & (PT > %s)" % default_config['PT_MIN']
    #if(which.find('C') > -1):
    #    momCuts += " & (INTREE( (ID=='gamma') & (ISBASIC) )) & (INTREE( HASTRACK ))"
    x = CombineParticles(DecayDescriptors=[dec],
                         MotherCut=momCuts,
                         ParticleCombiners = {"" : "MomentumCombiner:PUBLIC"},
                         ReFitPVs=False)#True)
    #print 'makeX2neutrals:',which,momCuts
    return Selection("X2"+which+"DarkBosonSel",Algorithm=x,
                     RequiredSelections=inputs)


def makeJ(default_config,inputs):
    comboCuts = "(ADAMASS('J/psi(1S)') < %s)" % default_config['ADAMASS_MAX']
    comboCuts += " & (ACUTDOCA(0.2*mm,''))"
    comboCuts += "& (ADOCACHI2CUT(25,''))"
    momCuts = LoKiCuts(['HASVERTEX','VCHI2DOF'],default_config).code()
    momCuts = LoKiCuts.combine(['(BPVDIRA > 0)',momCuts])
    j = CombineParticles(DecayDescriptors=['J/psi(1S) -> mu+ mu-'],
                         CombinationCut=comboCuts,MotherCut=momCuts)
    #print 'makeJ:',comboCuts,momCuts
    return Selection("J2MuMuDarkBosonSel",Algorithm=j,
                     RequiredSelections=inputs)

def makeRho(inputs,which=""):
    comboCuts = "(AM > 550*MeV) & (AM < 1050*MeV) & ADOCACHI2CUT(25,'')"
    momCuts = "(VFASPF(VCHI2/VDOF)<10) & (MIPCHI2DV(PRIMARY)> 16) & HASVERTEX"
    momCuts += " & (M > 600*MeV) & (M < 1000*MeV)"
    rho = CombineParticles(DecayDescriptors=['rho(770)0 -> pi+ pi-'],
                           CombinationCut=comboCuts,MotherCut=momCuts)
    #print 'makeRho:',comboCuts,momCuts
    return Selection("Rho2PiPi"+which+"DarkBosonSel",Algorithm=rho,
                     RequiredSelections=inputs)

def makeKst(inputs,which=""):
    comboCuts = "ADOCACHI2CUT(30,'') & (ADAMASS('K*(892)0') < 100*MeV)" #(AM < 2000*MeV)"
    momCuts = "(VFASPF(VCHI2/VDOF)<10) & (MIPCHI2DV(PRIMARY)> 16) & HASVERTEX"
    kst = CombineParticles(DecayDescriptors=['[K*(892)0 -> K+ pi-]cc'],
                           CombinationCut=comboCuts,MotherCut=momCuts)
    #print 'makeKst:',comboCuts,momCuts
    return Selection("Kst2KPi"+which+"DarkBosonSel",Algorithm=kst,
                     RequiredSelections=inputs)

def makeEta(inputs,which=""):
    comboCuts = "(AM < 750*MeV) & (ACHI2DOCA(1,2) < 25)"
    momCuts = "(VFASPF(VCHI2/VDOF)<10) & (MIPCHI2DV(PRIMARY)> 16) & HASVERTEX"
    momCuts += " & (M < 650*MeV)"
    eta = CombineParticles(DecayDescriptors=['eta -> pi+ pi- pi0'],
                           CombinationCut=comboCuts,MotherCut=momCuts)
    #print 'makeOmega:',comboCuts,momCuts
    return Selection("Eta2PiPiPi"+which+"DarkBosonSel",Algorithm=eta,
                     RequiredSelections=inputs)

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

def makeB(name,decays,inputs,default_config):
    comboCuts = LoKiCuts(['AM','SUMPT'],default_config).code()
    momCuts = LoKiCuts(['HASVERTEX','PT','VCHI2DOF','BPVLTIME','BPVIPCHI2','BPVDIRA'],default_config).code()
    cuts = ""
    if default_config.has_key('BPVDIRA_MIN'):
        cuts += '(BPVDIRA > %s)' % default_config['BPVDIRA_MIN']
    else:
        cuts += '(BPVDIRA > 0)'
    if default_config.has_key('HAD_MINIPCHI2_MIN'):
        cuts += ' & (NINGENERATION(ISBASIC & HASTRACK & (MIPCHI2DV(PRIMARY) < %s),1)==0)' \
                % default_config['HAD_MINIPCHI2_MIN']
    momCuts = LoKiCuts.combine([cuts,momCuts])
    momCuts += ' & (MM > %s) & (MM < %s)' % (default_config['AM_MIN'],default_config['AM_MAX'])
    n=2
    if name.find('B2KpiX') >= 0: n=3
    alg = None
    if n==2:
        alg = CombineParticles(DecayDescriptors=decays)
    else:
        alg = DaVinci__N3BodyDecays(DecayDescriptors=decays)
        alg.Combination12Cut = 'AM < %s' % default_config['AM_MAX']

    alg.ReFitPVs = True
    alg.CombinationCut=comboCuts
    alg.MotherCut=momCuts
    #print 'makeB:',name,comboCuts,momCuts,decays
    return Selection(name+"DarkBosonSel",Algorithm=alg,
                     RequiredSelections=inputs)

#\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#

