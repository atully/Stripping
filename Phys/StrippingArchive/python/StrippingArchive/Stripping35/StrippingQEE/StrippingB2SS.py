###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for construction of B-->S(hh)S(hh) and
B-->S(hh)S(hh) (detached) stripping selections and lines. Can be Bs->SS, B0->K*SS or B+->K+SS
'''

__author__ = ['Xabier Cid Vidal']
## based on StrippingB2MuMuMuMuLines by Diego Martinez Santos, Johannes Albrecht, Konstantinos A. Petridis, Alexander Baranov
__date__ = '20/01/2019'
__version__ = '$Revision: 1.0 $'
__all__ = ('B2SSConf','default_config')


default_config = {
    'NAME' : 'B2SS',
    'WGs' : ['QEE'],
    'BUILDERTYPE' : 'B2SSConf',
    'CONFIG'      : {
        ## Kaon
        'B2KstSSKaonLinePrescale'    : 1 ,
        'B2KstSSKaonLinePostscale'   : 1,
        'B2KstSSKaonDetLinePrescale'    : 1,
        'B2KstSSKaonDetLinePostscale'   : 1,

        'Bu2KSSKaonLinePrescale'    : 1,
        'Bu2KSSKaonLinePostscale'   : 1,
        'Bu2KSSKaonDetLinePrescale'    : 1,
        'Bu2KSSKaonDetLinePostscale'   : 1,

        'Bs2SSKaonLinePrescale'    : 1,
        'Bs2SSKaonLinePostscale'   : 1,
        'Bs2SSKaonDetLinePrescale'    : 1,
        'Bs2SSKaonDetLinePostscale'   : 1,

        ## Pion
        'B2KstSSPionLinePrescale'    : 1 ,
        'B2KstSSPionLinePostscale'   : 1,
        'B2KstSSPionDetLinePrescale'    : 1,
        'B2KstSSPionDetLinePostscale'   : 1,
    
        'Bu2KSSPionLinePrescale'    : 1,
        'Bu2KSSPionLinePostscale'   : 1,
        'Bu2KSSPionDetLinePrescale'    : 1,
        'Bu2KSSPionDetLinePostscale'   : 1,

        'Bs2SSPionLinePrescale'    : 1,
        'Bs2SSPionLinePostscale'   : 1,
        'Bs2SSPionDetLinePrescale'    : 1,
        'Bs2SSPionDetLinePostscale'   : 1,

        ## Proton
        'B2KstSSProtonLinePrescale'    : 1 ,
        'B2KstSSProtonLinePostscale'   : 1,
        'B2KstSSProtonDetLinePrescale'    : 1,
        'B2KstSSProtonDetLinePostscale'   : 1,
    
        'Bu2KSSProtonLinePrescale'    : 1,
        'Bu2KSSProtonLinePostscale'   : 1,
        'Bu2KSSProtonDetLinePrescale'    : 1,
        'Bu2KSSProtonDetLinePostscale'   : 1,

        'Bs2SSProtonLinePrescale'    : 1,
        'Bs2SSProtonLinePostscale'   : 1,
        'Bs2SSProtonDetLinePrescale'    : 1,
        'Bs2SSProtonDetLinePostscale'   : 1,

        ## Muon
        'B2KstSSMuonLinePrescale'    : 1 ,
        'B2KstSSMuonLinePostscale'   : 1,
        'B2KstSSMuonDetLinePrescale'    : 1,
        'B2KstSSMuonDetLinePostscale'   : 1,
    
        'Bu2KSSMuonLinePrescale'    : 1,
        'Bu2KSSMuonLinePostscale'   : 1,
        'Bu2KSSMuonDetLinePrescale'    : 1,
        'Bu2KSSMuonDetLinePostscale'   : 1,

        ## BS24Mu lines effectively already there in StrippingB2MuMuMuMuLines
 
        ## Electron
        'B2KstSSElectronLinePrescale'    : 1 ,
        'B2KstSSElectronLinePostscale'   : 1,
        'B2KstSSElectronDetLinePrescale'    : 1,
        'B2KstSSElectronDetLinePostscale'   : 1,
    
        'Bu2KSSElectronLinePrescale'    : 1,
        'Bu2KSSElectronLinePostscale'   : 1,
        'Bu2KSSElectronDetLinePrescale'    : 1,
        'Bu2KSSElectronDetLinePostscale'   : 1,

        'Bs2SSElectronLinePrescale'    : 1,
        'Bs2SSElectronLinePostscale'   : 1,
        'Bs2SSElectronDetLinePrescale'    : 1,
        'Bs2SSElectronDetLinePostscale'   : 1,

        # PIONS
        "DAUGHPIIPCHI2": 25,
        "DAUGHPIPT": "500*MeV",

        # HADRONS
        "DAUGHHADIPCHI2": 25,
        "DAUGHHADPT": "250*MeV",

        # LEPTONS
        "DAUGHLEPIPCHI2": 15,
        "DAUGHLEPPT": "25*MeV",
        
        # KAON
        "DAUGHKAONPID": 0,
        "DAUGHPROTONPID": 0,

        # KSTAR
        "KSTARPT": "750*MeV",
        "KSTARMASSWIN": "130*MeV",
        "KSTARFDCHI2": 25,
        "KSTARVCHI2DOF": 5,

        # PROMPT
        "SPrompt_BVCHI2DOF": 9,
        "SPrompt_BDIRA": 0,
        "SPrompt_BFDCHI2": 100,
        "SPrompt_BIPCHI2": 15,
        "SPrompt_BMAXDOCA": " 0.3*mm",
        'SPrompt_BMASS_MIN': '4600*MeV',
        'SPrompt_BMASS_MAX': '6000*MeV',
        'SPrompt_SDELTAM': '100*MeV',

        # PROMPT PION
        "SPrompt_BPIVCHI2DOF": 3,
        "SPrompt_BPIDIRA": 0,
        "SPrompt_BPIFDCHI2": 125,
        "SPrompt_BPIIPCHI2": 10,
        "SPrompt_BPIMAXDOCA": " 0.2*mm",
        'SPrompt_BPIMASS_MIN': '4900*MeV',
        'SPrompt_BPIMASS_MAX': '5800*MeV',
        'SPrompt_SDELTAMPI': '50*MeV',
        
        # DETACHED PAIRS
        'SDet_PAIRMAXDOCA'  : '0.5*mm',
        'SDet_PAIRSUMPT'    : '1000*MeV',
        'SDet_PAIRVCHI2DOF'  : 16,
        'SDet_PAIRFDCHI2'    : 25,
        'SDet_PAIRIPCHI2'    : 25,

        # DETACHED PAIRS PIONS
        'SDet_PAIRPIMAXDOCA'  : '0.25*mm',
        'SDet_PAIRPISUMPT'    : '2000*MeV',
        'SDet_PAIRPIVCHI2DOF' : 5,
        'SDet_PAIRPIFDCHI2'   : 25,
        'SDet_PAIRPIIPCHI2'   : 25,

        # DETACHED PAIRS LEPTONS
        'SDet_PAIRLEMAXDOCA'  : '0.5*mm',
        'SDet_PAIRLESUMPT'    : '50*MeV',
        'SDet_PAIRLEVCHI2DOF' : 16,
        'SDet_PAIRLEFDCHI2'   : 15,
        'SDet_PAIRLEIPCHI2'   : 10,

        # 'BFromDetached'
        'SDet_BSUMPT'       : '2000*MeV',
        'SDet_BVCHI2DOF'    : 5,
        'SDet_BIPCHI2'      : 15,
        'SDet_BFDCHI2'      : 50,
        'SDet_BDIRA'        : 0.0,
        'SDet_BMASS_MIN'    : '4600*MeV',
        'SDet_BMASS_MAX'    : '6000*MeV',
        'SDet_SDELTAM'    : '100*MeV'
        },
    
    'STREAMS' : ['EW'],
    }



from Gaudi.Configuration import *
from Configurables import CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdAllLooseProtons, StdAllLoosePions, StdAllLooseKaons
from StandardParticles import StdAllLooseMuons, StdAllLooseElectrons
from CommonParticles.StdLooseKstar import *


#from StrippingSelections.Utils import checkConfig

class B2SSConf(LineBuilder) :
    """
    B2SS
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config):
        self.name = name
        self.__confdict__ = config
        self.config = config
        
        LineBuilder.__init__(self, name, config)

        finalStates = ["Muon","Electron","Kaon",
                       "Pion","Proton"]

        Bids = ["B0","B+","B_s0"]
        
        # Generaly used input particles
        self.inDaughters = {}
        for daugh in finalStates:
            self.inDaughters[daugh] = self.filterDaughters(daugh+"sFor"+name,daugh)
        self.inDaughters["Kstar"] = self.filterDaughters("KstarsFor"+name,"Kstar")
        
        # Prompt selection
        self.B2Prompt = {}
        for bid in Bids:
            self.B2Prompt[bid]={}
            for daugh in finalStates:
                if daugh=="Muon" and bid=="B_s0": continue ## already covered at another module
                self.B2Prompt[bid][daugh]=self.makeB2Prompt(bid+"2"+daugh+"PromptFor"+name,bid,daugh)

        # Detached selection
        # Pairs
        self.inDetachedPairs = {}
        for daugh in finalStates:
            self.inDetachedPairs[daugh] = self.makeDetachedPairs('Detached'+daugh+"PairFor"+name,
                                                               daugh)
        
        ## build B combination
        self.B2Det = {}
        for bid in Bids:
            self.B2Det[bid]={}
            for daugh in finalStates:
                if daugh=="Muon" and bid=="B_s0": continue ## already covered at another module
                self.B2Det[bid][daugh]=self.makeB2Det(bid+"2"+daugh+"DetFor"+name,bid,daugh)
                
        
        # Stripping lines
        StrippingLines = []
        for bid in Bids:
            if bid=="B0": line_name0 = "B2KstSS"
            if bid=="B+": line_name0 = "Bu2KSS"
            if bid=="B_s0": line_name0 = "Bs2SS"            
            for daugh in finalStates:
                if daugh=="Muon" and bid=="B_s0": continue ## already covered at another module
                line_name1 = line_name0+daugh
                i1=1
                for sel in [self.B2Prompt[bid][daugh],self.B2Det[bid][daugh]]:
                    if i1==2: line_name = line_name1+"DetLine"
                    else: line_name = line_name1+"Line"
                    i1+=1                                        
                    StrippingLines.append(StrippingLine(name+line_name,
                                                        prescale=config[line_name+'Prescale'],
                                                        postscale=config[line_name+'Postscale'],
                                                        algos=[sel]))
                    self.registerLine(StrippingLines[-1])


    def filterDaughters(self,name,daugh):
        """
        Daughters selection
        """

        if daugh=="Muon" or daugh=="Electron":
        ## apply first cuts to the daughters (faster)            
            _code = "( MIPCHI2DV(PRIMARY)> %(DAUGHLEPIPCHI2)s)" \
                    "& ( PT > %(DAUGHLEPPT)s)" %self.config
        elif daugh=="Pion":
            ## apply first cuts to the daughters (faster)
            _code = "( MIPCHI2DV(PRIMARY)> %(DAUGHPIIPCHI2)s)" \
                    "& ( PT > %(DAUGHPIPT)s)" %self.config
        else:
            ## apply first cuts to the daughters (faster)
            _code = "( MIPCHI2DV(PRIMARY)> %(DAUGHHADIPCHI2)s)" \
                    "& ( PT > %(DAUGHHADPT)s)" %self.config

        
        if daugh=="Muon": mysel = StdAllLooseMuons
        if daugh=="Electron": mysel = StdAllLooseElectrons
        if daugh=="Kaon": mysel = StdAllLooseKaons
        if daugh=="Pion": mysel = StdAllLoosePions
        if daugh=="Proton": mysel = StdAllLooseProtons
        ## reduce number of Kstar candidates
        if daugh=="Kstar":
            _code += "& (PT > %(KSTARPT)s )"\
                     "& (M < ( 892*MeV + %(KSTARMASSWIN)s ) )"\
                     "& (M > ( 892*MeV - %(KSTARMASSWIN)s ) )"\
                     "& (BPVVDCHI2 > %(KSTARFDCHI2)s )"\
                     "& (VFASPF(VCHI2/VDOF) < %(KSTARVCHI2DOF)s )"\
                     "& ( CHILD( MIPCHI2DV(PRIMARY), 1) > %(DAUGHHADIPCHI2)s ) "\
                     "& ( CHILD( MIPCHI2DV(PRIMARY), 2) > %(DAUGHHADIPCHI2)s ) " %self.config
            mysel = DataOnDemand(Location="Phys/StdLooseKstar2Kpi/Particles")

        ## for K+, need some sort of PID to reduce number of candidates
        if daugh=="Kaon":
            _code += "& (PIDK > %(DAUGHKAONPID)s)" %self.config

        if daugh=="Proton":
            _code += "& (PIDp > %(DAUGHPROTONPID)s)" %self.config
        
        _Filter = FilterDesktop(Code = _code)
        
        return Selection(name,
                         Algorithm = _Filter,
                         RequiredSelections = [ mysel] )
    
    
    def makeB2Prompt(self,name,bid,daugh):
        """
        B --> 4 identical particles prompt selection
        """

        ## DEFINE MASS SUBCOMBINATIONS
        if bid=="B_s0": Preambulo = ["MYAM12=AMASS(1,2)",
                                     "MYAM13=AMASS(1,3)",
                                     "MYAM14=AMASS(1,4)",
                                     "MYAM23=AMASS(2,3)",
                                     "MYAM24=AMASS(2,4)",
                                     "MYAM34=AMASS(3,4)"]

        else: Preambulo = ["MYAM12=AMASS(2,3)",
                           "MYAM13=AMASS(2,4)",
                           "MYAM14=AMASS(2,5)",
                           "MYAM23=AMASS(3,4)",
                           "MYAM24=AMASS(3,5)",
                           "MYAM34=AMASS(4,5)"]                     


        if daugh=="Pion":
            ## Generic cuts for prompt selection
            comboCuts = "(AMAXDOCA('')<%(SPrompt_BPIMAXDOCA)s) &"\
                        " (AM>%(SPrompt_BPIMASS_MIN)s) & (AM<%(SPrompt_BPIMASS_MAX)s) "%self.config

            comboCuts+= "& ( (abs(MYAM12-MYAM34)< %(SPrompt_SDELTAMPI)s) | "\
                        "    (abs(MYAM13-MYAM24)< %(SPrompt_SDELTAMPI)s) | "\
                        "    (abs(MYAM14-MYAM23)< %(SPrompt_SDELTAMPI)s) )" %self.config

            
            momCuts = "(VFASPF(VCHI2/VDOF)< %(SPrompt_BPIVCHI2DOF)s)"\
                      "& (BPVDIRA > %(SPrompt_BPIDIRA)s)"\
                      "& (BPVVDCHI2> %(SPrompt_BPIFDCHI2)s)"\
                      "& (BPVIPCHI2()< %(SPrompt_BPIIPCHI2)s)"%self.config
            
            
        else:
            ## Generic cuts for prompt selection
            comboCuts = "(AMAXDOCA('')<%(SPrompt_BMAXDOCA)s) &"\
                        " (AM>%(SPrompt_BMASS_MIN)s) & (AM<%(SPrompt_BMASS_MAX)s) "%self.config

            ## build all possible combinations for daughters,
            ## ensure at least one of the S masses is the same
            comboCuts+= "& ( (abs(MYAM12-MYAM34)< %(SPrompt_SDELTAM)s) | "\
                        "    (abs(MYAM13-MYAM24)< %(SPrompt_SDELTAM)s) | "\
                        "    (abs(MYAM14-MYAM23)< %(SPrompt_SDELTAM)s) )" %self.config

            momCuts = "(VFASPF(VCHI2/VDOF)< %(SPrompt_BVCHI2DOF)s)"\
                      "& (BPVDIRA > %(SPrompt_BDIRA)s)"\
                      "& (BPVVDCHI2> %(SPrompt_BFDCHI2)s)"\
                      "& (BPVIPCHI2()< %(SPrompt_BIPCHI2)s)"%self.config

        
        ## first half of decay depends on the mother
        if bid=="B0":
            decay1 = "[B0 -> K*(892)0 "
            inputs = [self.inDaughters["Kstar"]]
        if bid=="B+":
            decay1 = "[B+ -> K+ "
            inputs = [self.inDaughters["Kaon"]]
        if bid=="B_s0":
            decay1 = "B_s0 -> "
            inputs = []

        
        ## second half of decay depends on the daughters
        if daugh=="Muon": decay2 = " mu+ mu- mu+ mu-"
        if daugh=="Electron": decay2 = " e+ e- e+ e-"
        if daugh=="Kaon": decay2 = " K+ K- K+ K-"
        if daugh=="Pion": decay2 = " pi+ pi- pi+ pi-"
        if daugh=="Proton": decay2 = " p+ p~- p+ p~-"

        if bid!="B_s0": decay2+="]cc" # for B_s0, no need for having cc

        ## input corresponds to accompaining particle + daughters
        if not (daugh=="Kaon" and bid=="B+"): inputs.append(self.inDaughters[daugh])
        ## skip B+ and kaon, since kaons are already an input in that case (avoids warning)

        ## build B candidate
        B2PromptS = CombineParticles("Combine"+name)
        B2PromptS.DecayDescriptor = decay1+decay2
        B2PromptS.CombinationCut = comboCuts
        B2PromptS.MotherCut = momCuts
        B2PromptS.Preambulo = Preambulo
        
        return Selection(
            name,
            Algorithm=B2PromptS,
            RequiredSelections=inputs,
            )


    def makeDetachedPairs(self,name,daugh):
        """
        KS0->daugh daugh selection
        """

        ## FOR PIONS, NEED TIGHTER CUTS
        ## Generic cuts for detached pairs
        if daugh=="Pion":
            comboCuts = "(AMAXDOCA('')<%(SDet_PAIRPIMAXDOCA)s) &" \
                        "(ASUM(PT)>%(SDet_PAIRPISUMPT)s)" %self.config
            momCuts = "(VFASPF(VCHI2/VDOF)< %(SDet_PAIRPIVCHI2DOF)s)"\
                      "& (BPVVDCHI2 > %(SDet_PAIRPIFDCHI2)s)"\
                      "& (BPVIPCHI2()> %(SDet_PAIRPIIPCHI2)s)" %self.config

        elif daugh=="Muon" or daugh=="Electron":
            comboCuts = "(AMAXDOCA('')<%(SDet_PAIRLEMAXDOCA)s) &" \
                        "(ASUM(PT)>%(SDet_PAIRLESUMPT)s)" %self.config
            momCuts = "(VFASPF(VCHI2/VDOF)< %(SDet_PAIRLEVCHI2DOF)s)"\
                      "& (BPVVDCHI2 > %(SDet_PAIRLEFDCHI2)s)"\
                      "& (BPVIPCHI2()> %(SDet_PAIRLEIPCHI2)s)" %self.config
        else:
            comboCuts = "(AMAXDOCA('')<%(SDet_PAIRMAXDOCA)s) &" \
                        "(ASUM(PT)>%(SDet_PAIRSUMPT)s)" %self.config
            momCuts = "(VFASPF(VCHI2/VDOF)< %(SDet_PAIRVCHI2DOF)s)"\
                      "& (BPVVDCHI2 > %(SDet_PAIRFDCHI2)s)"\
                      "& (BPVIPCHI2()> %(SDet_PAIRIPCHI2)s)" %self.config

        
        KS2MuMu = CombineParticles("Combine"+name)
        inputs = [self.inDaughters[daugh]]
        if daugh=="Muon": KS2MuMu.DecayDescriptor = "KS0 ->  mu+ mu-"
        if daugh=="Electron": KS2MuMu.DecayDescriptor = "KS0 ->  e+ e-"
        if daugh=="Kaon": KS2MuMu.DecayDescriptor = "KS0 ->  K+ K-"
        if daugh=="Pion": KS2MuMu.DecayDescriptor = "KS0 ->  pi+ pi-"
        if daugh=="Proton": KS2MuMu.DecayDescriptor = "KS0 ->  p+ p~-"
        KS2MuMu.CombinationCut=comboCuts
        KS2MuMu.MotherCut=momCuts
        
        return Selection(
            name,
            Algorithm=KS2MuMu,
            RequiredSelections=inputs
            )
    

    def makeB2Det(self,name,bid,daugh):
        """
        B --> S S selection
        """

        ## DEFINE MASS OF THE DAUGHTERS, FOR B0 AND B+ WE CARE ABOUT DAUGHTERS 2 AND 3
        if bid=="B_s0": Preambulo = ["MS1 = ACHILD(M,1)",
                                     "MS2 = ACHILD(M,2)"]
        else: Preambulo = ["MS1 = ACHILD(M,2)",
                           "MS2 = ACHILD(M,3)"]

        ## Generic cuts for detached selection
        comboCuts = "(ASUM(PT)>%(SDet_BSUMPT)s) " \
                    "& (AM>%(SDet_BMASS_MIN)s) & (AM<%(SDet_BMASS_MAX)s) "\
                    "& ( abs(MS1-MS2)<%(SDet_SDELTAM)s )"%self.config
        
        momCuts = "(VFASPF(VCHI2/VDOF)< %(SDet_BVCHI2DOF)s)"\
                  "& (BPVDIRA > %(SDet_BDIRA)s)"\
                  "& (BPVVDCHI2> %(SDet_BFDCHI2)s)"\
                  "& (BPVIPCHI2()< %(SDet_BIPCHI2)s)"%self.config

        ## first half of decay depends on the mother, inputs depend also on the daughter
        if bid=="B0":
            decay = "[B0 -> K*(892)0 KS0 KS0]cc"
            inputs = [self.inDaughters["Kstar"],self.inDetachedPairs[daugh]]
        if bid=="B+":
            decay = "[B+ -> K+ KS0 KS0]cc"
            inputs = [self.inDaughters["Kaon"],self.inDetachedPairs[daugh]]
        if bid=="B_s0":
            decay = "B_s0 -> KS0 KS0"
            inputs = [self.inDetachedPairs[daugh]]
            

        ## build B candidate
        B2DetS = CombineParticles("Combine"+name)
        B2DetS.DecayDescriptor = decay
        B2DetS.CombinationCut = comboCuts
        B2DetS.MotherCut = momCuts
        B2DetS.Preambulo = Preambulo
        
        return Selection(
            name,
            Algorithm=B2DetS,
            RequiredSelections=inputs,
            )

        
