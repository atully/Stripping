###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
all
''' Xic+ and Theta+ lines Designed by Yury Shcheglov, Alexey Dzyuba, Nelya Sagidova to study Xic+ and Theta+ particles '''

__author__ = ['Yury Shcheglov']
__date__ = '2016/20/05'
__version__ = '$Revision: 2.0 $'

__all__ = ( 'StrippingXic2HHHConf',
            'makeXic2HHH',
            'default_config' )



import re
from Gaudi.Configuration import *
from StrippingConf.StrippingLine import StrippingLine
from GaudiKernel.SystemOfUnits import MeV, mm, ns
from LHCbKernel.Configuration import *
from Configurables import FilterDesktop, CombineParticles
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, AutomaticData, MergedSelection
from PhysSelPython.Wrappers import MultiSelectionSequence

#from Configurables import LoKi__VoidFilte
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsPions, StdNoPIDsKaons,StdNoPIDsProtons, StdAllNoPIDsProtons

from Configurables import TisTosParticleTagger


default_config = {
    'NAME'        : 'BetacHHH',
    "BUILDERTYPE": "StrippingXic2HHHConf", 
    "CONFIG": {
        "Comb_ADOCAMAX_MAX": 0.3, 
        "Comb_MASS_MAX": 2800.0, 
        "Comb_MASS_MIN": 2200.0, 
        "Daug_All_PT_MIN": 300.0, 
        "Daug_P_MIN": 3000.0, 
        "Daug_TRCHI2DOF_MAX": 3.0, 
        "K_IPCHI2_MIN": 4.0, 
        "PostscaleBetac2LcKS0": 1.0, 
        "PostscaleBetac2pDs": 0.0, 
        "PostscaleBetacLb1": 1.0, 
        "PostscaleBetacLb2": 1.0, 
        "PostscaleXic2PKK": 1.0, 
        "PostscaleXic2PKPi": 0.0, 
        "PrescaleBetac2LcKS0": 1.0, 
        "PrescaleBetac2pDs": 0.0, 
        "PrescaleBetacLb1": 1.0, 
        "PrescaleBetacLb2": 1.0, 
        "PrescaleXic2PKK": 1.0, 
        "PrescaleXic2PKPi": 0.0, 
        "RelatedInfoTools": [
            {
                "ConeAngle": 1.5, 
                "Location": "Cone1", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.75, 
                "Location": "Cone2", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.5, 
                "Location": "Cone3", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.25, 
                "Location": "Cone4", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.1, 
                "Location": "Cone5", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }
        ], 
        "Xic_BPVDIRA_MIN": 0.999, 
        "Xic_BPVIPCHI2_MAX": 12.0, 
        "Xic_BPVLTIME_MAX": 0.003, 
        "Xic_BPVLTIME_MIN": 0.0002, 
        "Xic_BPVVDCHI2_MIN": 0.0, 
        "Xic_PT_MIN": 2000.0, 
        "Xic_VCHI2VDOF_MAX": 10.0, 
        "pion_IPCHI2_MIN": 4.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": ["Charm"] # Note, this should always be a list.
}


class StrippingXic2HHHConf(LineBuilder): 

    __configuration_keys__ = default_config['CONFIG'].keys()

    __confdict__={}
    ## Possible parameters and default values copied from the definition
    ##   of StrippingLine
    def _strippingLine ( self,
                          name             ,   # the base name for the Line
                          prescale  = 1.0  ,   # prescale factor
                          ODIN      = None ,   # ODIN predicate
                          L0DU      = None ,   # L0DU predicate
                          FILTER    = None ,   # 'VOID'-predicate, e.g. Global Event Cut
                          checkPV   = True ,   # Check PV before running algos
                          algos     = None ,   # the list of stripping members
                          selection = None ,
                          postscale = 1.0    ,   # postscale factor
                          MaxCandidates = "Override",   # Maximum number
                          MaxCombinations = "Override", # Maximum number
                          HDRLocation = None, # other configuration parameter 
                          RelatedInfoTools = None ) : # RelatedInfoTools to be included  


        if (prescale > 0) and (postscale > 0) : 
            line = StrippingLine( name,
                                  prescale        = prescale,
                                  ODIN            = ODIN,
                                  L0DU            = L0DU,
                                  FILTER          = FILTER,
                                  checkPV         = checkPV,
                                  algos           = algos,
                                  selection       = selection,
                                  postscale       = postscale,
                                  MaxCandidates   = MaxCandidates,
                                  MaxCombinations = MaxCombinations,
                                  HDRLocation     = HDRLocation, 
                                  RelatedInfoTools  = RelatedInfoTools)

            self.registerLine(line)
            return line

        else : 
            return False



    def __init__(self, name, config) : 

        LineBuilder.__init__(self, name, config)
        self.__confdict__= config

        xic_PKPi_name =  name + 'Xic2PKPi'
        xic_pKK_name  =  name + 'Xic2PKK'
        betac_lcks_name  =  name + 'Betac2LcKS0'
        betac_pds_name  =  name + 'Betac2pDs'
        betac_lb1_name  =  name + 'BetacLb1'
        betac_lb2_name  =  name + 'BetacLb2'

        self.KS0LL = DataOnDemand(Location = "Phys/StdLooseKsLL/Particles")
        self.KS0DD = DataOnDemand(Location = "Phys/StdLooseKsDD/Particles")


 ############### PROTON SELECTIONS #########################################


        self.inProtons = Selection( "Protonsfor" + name,
                                  Algorithm = self._protonFilter(),
                                  RequiredSelections = [StdNoPIDsProtons])


 ############### PIONS, Lambda0, KS0 and KAONS SELECTIONS ##################
        
        self.inPions  = Selection( "Pionsfor" + name,
                                  Algorithm = self._pionFilter(),
                                  RequiredSelections = [StdNoPIDsPions])

        self.inKS0LL  = Selection( "KS0LLFor" + name,
                                  Algorithm = self._KS0LLFilter(),
                                  RequiredSelections = [self.KS0LL])
        
        self.inKS0DD  = Selection( "KS0DDFor" + name,
                                  Algorithm = self._KS0DDFilter(),
                                  RequiredSelections = [self.KS0DD])


        self.inKS0   =  MergedSelection( "KS0for" + name,
                                  RequiredSelections = [self.inKS0LL,self.inKS0DD])

        self.inKaons  = Selection( "Kaonsfor" + name,
                                  Algorithm = self._kaonFilter(),
                                  RequiredSelections = [StdNoPIDsKaons])


 ###############################################################
        self.selXic2PKPi = makeXic2PKPi( name = xic_PKPi_name
               , inputSel = [ self.inProtons, self.inKaons, self.inPions]
               , Daug_All_PT_MIN = config['Daug_All_PT_MIN']
               , Daug_P_MIN = config['Daug_P_MIN']
               , Daug_TRCHI2DOF_MAX = config['Daug_TRCHI2DOF_MAX']
               , Comb_MASS_MIN  = 2190. * MeV
               , Comb_MASS_MAX  = 2570. * MeV
               , Comb_ADOCAMAX_MAX = config['Comb_ADOCAMAX_MAX']
               , Xic_PT_MIN = config['Xic_PT_MIN']
               , Xic_VCHI2VDOF_MAX = config['Xic_VCHI2VDOF_MAX']
               , Xic_BPVVDCHI2_MIN = config['Xic_BPVVDCHI2_MIN']
               , Xic_BPVDIRA_MIN = config['Xic_BPVDIRA_MIN']
               , Xic_BPVIPCHI2_MAX = config['Xic_BPVIPCHI2_MAX'] 
               , Xic_BPVLTIME_MAX = config['Xic_BPVLTIME_MAX']
               , Xic_BPVLTIME_MIN = config['Xic_BPVLTIME_MIN']
             )
                                      
        self.selXic2PKK = makeXic2PKPi( name = xic_pKK_name
               , inputSel = [self.inKaons, self.inProtons]
               , Daug_All_PT_MIN = config['Daug_All_PT_MIN']
               , Daug_P_MIN = config['Daug_P_MIN']
               , Daug_TRCHI2DOF_MAX = config['Daug_TRCHI2DOF_MAX']
               , Comb_MASS_MIN  = config['Comb_MASS_MIN']
               , Comb_MASS_MAX  = 2800. * MeV
               , Comb_ADOCAMAX_MAX = config['Comb_ADOCAMAX_MAX']
               , Xic_PT_MIN = config['Xic_PT_MIN']
               , Xic_VCHI2VDOF_MAX = config['Xic_VCHI2VDOF_MAX']  
               , Xic_BPVVDCHI2_MIN = config['Xic_BPVVDCHI2_MIN']
               , Xic_BPVDIRA_MIN = config['Xic_BPVDIRA_MIN']
               , Xic_BPVIPCHI2_MAX = config['Xic_BPVIPCHI2_MAX']
               , Xic_BPVLTIME_MAX = config['Xic_BPVLTIME_MAX']
               , Xic_BPVLTIME_MIN = config['Xic_BPVLTIME_MIN']  
               , decDescriptors = [ "[Lambda_c+ -> p+ K- K+]cc" ]
             )

        self.selBetac2LcKS0 = makeBetac2LcKS0 ( name = betac_lcks_name
               , inputSel = [ self.inProtons, self.inKaons,  self.inPions, self.inKS0]
               , Daug_All_PT_MIN =  config['Daug_All_PT_MIN']
               , Daug_P_MIN = config['Daug_P_MIN']
               , Daug_TRCHI2DOF_MAX = config['Daug_TRCHI2DOF_MAX']
               , Comb_MASS_MIN  = 2770. * MeV            
               , Comb_MASS_MAX  = 3600. * MeV            
               , Comb_ADOCAMAX_MAX = 1.0 * mm
               , Xic_PT_MIN = 3000. * MeV
               , Xic_BPVDIRA_MIN = -100.
               , Xic_BPVIPCHI2_MAX = config['Xic_BPVIPCHI2_MAX']
               , Xic_BPVLTIME_MAX = config['Xic_BPVLTIME_MAX']
               , Xic_BPVLTIME_MIN = config['Xic_BPVLTIME_MIN']
               , decDescriptors = [ "[Lambda_c+ -> p+ K- pi+ KS0]cc" ]
             )


        self.selBetac2pDs = makeBetac2pDs ( name = betac_pds_name
               , inputSel = [ self.inProtons,self.inPions,self.inKaons]
               , Daug_All_PT_MIN =  config['Daug_All_PT_MIN']
               , Daug_P_MIN = config['Daug_P_MIN']
               , Daug_TRCHI2DOF_MAX = config['Daug_TRCHI2DOF_MAX']
               , Comb_MASS_MIN  = 2500. * MeV
               , Comb_MASS_MAX  = 3700. * MeV
               , Comb_ADOCAMAX_MAX = 1.0 * mm
               , Xic_PT_MIN = 5000. * MeV
               , Xic_BPVDIRA_MIN = -100.
               , Xic_BPVIPCHI2_MAX = config['Xic_BPVIPCHI2_MAX']
               , Xic_BPVLTIME_MAX = config['Xic_BPVLTIME_MAX']
               , Xic_BPVLTIME_MIN = config['Xic_BPVLTIME_MIN']
               , decDescriptors = [ "[Sigma_c++ -> p+ pi+ K- pi+]cc" ]
             )


        self.selBetacLb1 = makeBetacLb1 ( name = betac_lb1_name
               , inputSel = [ self.inProtons, self.inKaons, self.inKS0, self.inPions]
               , Daug_All_PT_MIN =  config['Daug_All_PT_MIN']
               , Daug_P_MIN = config['Daug_P_MIN']
               , Daug_TRCHI2DOF_MAX = config['Daug_TRCHI2DOF_MAX']
               , Comb_MASS_MIN  = 5000. * MeV
               , Comb_MASS_MAX  = 6000. * MeV
               , Comb_ADOCAMAX_MAX = 1.0 * mm
               , Xic_PT_MIN = 2000. * MeV
               , Xic_BPVDIRA_MIN = -100.
               , Xic_BPVIPCHI2_MAX = config['Xic_BPVIPCHI2_MAX']
               , Xic_BPVLTIME_MAX = config['Xic_BPVLTIME_MAX']
               , Xic_BPVLTIME_MIN = config['Xic_BPVLTIME_MIN']
               , decDescriptors = [ "[Lambda_b0 -> p+ K- K+ KS0 pi-]cc" ]
             )
                                         
        self.selBetacLb2 = makeBetacLb2 ( name = betac_lb2_name  
               , inputSel = [ self.inProtons, self.inKaons,  self.inPions, self.inKS0, self.inPions]
               , Daug_All_PT_MIN =  config['Daug_All_PT_MIN']
               , Daug_P_MIN = config['Daug_P_MIN']
               , Daug_TRCHI2DOF_MAX = config['Daug_TRCHI2DOF_MAX']
               , Comb_MASS_MIN  = 5000. * MeV
               , Comb_MASS_MAX  = 6000. * MeV
               , Comb_ADOCAMAX_MAX = 1.0 * mm
               , Xic_PT_MIN = 2000. * MeV
               , Xic_BPVDIRA_MIN = -100. 
               , Xic_BPVIPCHI2_MAX = config['Xic_BPVIPCHI2_MAX']
               , Xic_BPVLTIME_MAX = config['Xic_BPVLTIME_MAX']
               , Xic_BPVLTIME_MIN = config['Xic_BPVLTIME_MIN']
               , decDescriptors = [ "[Lambda_b0 -> p+ K- pi+ KS0 KS0 pi-]cc" ]
             )


        self.line_Xic2PKPi = self._strippingLine( name = xic_PKPi_name + 'Line',
                                         prescale  = config['PrescaleXic2PKPi'],
                                         postscale = config['PostscaleXic2PKPi'],
                                         RelatedInfoTools = config['RelatedInfoTools'],
                                         selection = self.selXic2PKPi,
                                       )

        self.line_Xic2PKK = self._strippingLine( name = xic_pKK_name + 'Line',
                                         prescale  = config['PrescaleXic2PKK'],
                                         postscale = config['PostscaleXic2PKK'],
                                         RelatedInfoTools = config['RelatedInfoTools'],
                                         selection = self.selXic2PKK,
                                       )

        self.line_Betac2LcKS0 = self._strippingLine( name = betac_lcks_name + 'Line',
                                         prescale  = config['PrescaleBetac2LcKS0'],
                                         postscale = config['PostscaleBetac2LcKS0'],
                                         RelatedInfoTools = config['RelatedInfoTools'],
                                         selection = self.selBetac2LcKS0,
                                       )

        self.line_Betac2pDs = self._strippingLine( name = betac_pds_name + 'Line',
                                         prescale  = config['PrescaleBetac2pDs'],
                                         postscale = config['PostscaleBetac2pDs'],
                                         RelatedInfoTools = config['RelatedInfoTools'],
                                         selection = self.selBetac2pDs,
                                       )

        self.line_BetacLb1 = self._strippingLine( name = betac_lb1_name + 'Line',
                                         prescale  = config['PrescaleBetacLb1'],
                                         postscale = config['PostscaleBetacLb1'],
                                         RelatedInfoTools = config['RelatedInfoTools'],
                                         selection = self.selBetacLb1,
                                       )

        self.line_BetacLb2= self._strippingLine( name = betac_lb2_name + 'Line', 
                                         prescale  = config['PrescaleBetacLb2'], 
                                         postscale = config['PostscaleBetacLb2'], 
                                         RelatedInfoTools = config['RelatedInfoTools'],
                                         selection = self.selBetacLb2, 
                                       )


    def _protonFilter( self ):
          _code = "(PROBNNp > 0.55) &(TRGHP < 0.4) & (P> 2000.0*MeV) & (TRCHI2DOF < %(Daug_TRCHI2DOF_MAX)s)" % self.__confdict__
          _proton = FilterDesktop( Code = _code )
          return _proton


    def _pionFilter( self ):
          _code = "(PROBNNpi > 0.15) &(TRGHP < 0.4) & (MIPCHI2DV(PRIMARY) > %(pion_IPCHI2_MIN)s) &(P>2000*MeV)& (PT>300*MeV)&(TRCHI2DOF < %(Daug_TRCHI2DOF_MAX)s) " % self.__confdict__
          _pion = FilterDesktop( Code = _code )
          return _pion


    def _kaonFilter( self ):
          _code = "(PROBNNk > 0.4) &(TRGHP < 0.4) & (MIPCHI2DV(PRIMARY) > %(K_IPCHI2_MIN)s) & (P>2000*MeV) & (PT>300*MeV) &(TRCHI2DOF < %(Daug_TRCHI2DOF_MAX)s)" % self.__confdict__          
          _kaon = FilterDesktop( Code = _code )
          return _kaon


    def _KS0LLFilter( self ):
          _code = "(P>3000.*MeV)&(PT>800.*MeV)&(BPVLTIME()>3.0*ps)&CHILDCUT((TRCHI2DOF<4.),1)&CHILDCUT((TRCHI2DOF<4.),2)" \
                                                "& (BPVVDZ > 0.0 * mm) " \
                                                "& (BPVVDZ < 2300.0 * mm) " \
                                                "& (BPVDIRA > 0.99995 ) " \
                                                "& (ADMASS('KS0') < 15.0 *MeV) " \
                                                "& (BPVVDCHI2> 100)" % self.__confdict__
          _KS0LL = FilterDesktop( Code = _code )
          return _KS0LL
                                  
    def _KS0DDFilter( self ):
          _code = "(P>3000.*MeV)&(PT>800.*MeV)&(BPVLTIME()>3.0*ps)&CHILDCUT((TRCHI2DOF<4.),1)&CHILDCUT((TRCHI2DOF<4.),2)" \
                                                "& (BPVVDZ > -1000.0 * mm) " \
                                                "& (BPVVDZ < 650.0 * mm) " \
                                                "& (BPVDIRA > 0.99995 ) " \
                                                "& (ADMASS('KS0') < 15.0 *MeV) " \
                                                "& (BPVVDCHI2> 100)" % self.__confdict__
               
          _KS0DD = FilterDesktop( Code = _code )
          return _KS0DD


def makeXic2PKPi( name
               , inputSel
               , Daug_All_PT_MIN
               , Daug_P_MIN
               , Daug_TRCHI2DOF_MAX
               , Comb_MASS_MIN 
               , Comb_MASS_MAX
               , Comb_ADOCAMAX_MAX
               , Xic_PT_MIN
               , Xic_VCHI2VDOF_MAX
               , Xic_BPVVDCHI2_MIN
               , Xic_BPVDIRA_MIN
               , Xic_BPVIPCHI2_MAX 
               , Xic_BPVLTIME_MAX
               , Xic_BPVLTIME_MIN
               , decDescriptors = [ "[Lambda_c -> p+ K- pi+]cc" ]
             ) : 


    combCuts = "(AM > %(Comb_MASS_MIN)s)" \
               "& (AM < %(Comb_MASS_MAX)s)" \
               "& (AMINCHILD(PT) > %(Daug_All_PT_MIN)s)" \
               "& (AMINCHILD(P)  > %(Daug_P_MIN)s)" \
               "& (ADOCAMAX('') < %(Comb_ADOCAMAX_MAX)s)" % locals()


    xicCuts =  "(PT > %(Xic_PT_MIN)s)" \
               "& (VFASPF(VCHI2/VDOF) < %(Xic_VCHI2VDOF_MAX)s)" \
               "& (BPVIPCHI2() < %(Xic_BPVIPCHI2_MAX)s)" \
               "& (BPVVDCHI2 > %(Xic_BPVVDCHI2_MIN)s)" \
               "& (BPVDIRA > %(Xic_BPVDIRA_MIN)s)" \
               "& (BPVLTIME() > %(Xic_BPVLTIME_MIN)s)" \
               "& (BPVLTIME() < %(Xic_BPVLTIME_MAX)s)" % locals()


    _Xic = CombineParticles(
        DecayDescriptors = decDescriptors

        , CombinationCut = combCuts
        , MotherCut = xicCuts 
    )

    return Selection( name,
                      Algorithm = _Xic,
                      RequiredSelections = inputSel
                    )


def makeBetac2LcKS0( name
               , inputSel
               , Daug_All_PT_MIN
               , Daug_P_MIN
               , Daug_TRCHI2DOF_MAX
               , Comb_MASS_MIN
               , Comb_MASS_MAX
               , Comb_ADOCAMAX_MAX
               , Xic_PT_MIN
               , Xic_BPVDIRA_MIN
               , Xic_BPVIPCHI2_MAX
               , Xic_BPVLTIME_MAX
               , Xic_BPVLTIME_MIN
               , decDescriptors = [ "[Lambda_c+ -> p+ K- pi+ KS0]cc" ]
             ) :

    combCuts = "(AM > %(Comb_MASS_MIN)s)" \
               "& (AM <  %(Comb_MASS_MAX)s)" \
               "& (AMINCHILD(P)  > %(Daug_P_MIN)s)" \
               "& (AMINCHILD(PT) > %(Daug_All_PT_MIN)s)" % locals()

    betacCuts = "(PT > %(Xic_PT_MIN)s)" % locals()

    _Betac2LcKS0 = CombineParticles(
        DecayDescriptors = decDescriptors
        , CombinationCut = combCuts
        , MotherCut = betacCuts
    )

    return Selection( name,
                      Algorithm = _Betac2LcKS0,
                      RequiredSelections = inputSel
                    )


def makeBetac2pDs( name
               , inputSel
               , Daug_All_PT_MIN
               , Daug_P_MIN
               , Daug_TRCHI2DOF_MAX
               , Comb_MASS_MIN
               , Comb_MASS_MAX
               , Comb_ADOCAMAX_MAX
               , Xic_PT_MIN
               , Xic_BPVDIRA_MIN
               , Xic_BPVIPCHI2_MAX
               , Xic_BPVLTIME_MAX
               , Xic_BPVLTIME_MIN
               , decDescriptors = [ "[Sigma_c++ -> p+ pi+ K- pi+]cc" ]
             ) :

    combCuts = "(AM > %(Comb_MASS_MIN)s)" \
               "& (AM <  %(Comb_MASS_MAX)s)" \
               "& (AMINCHILD(P)  > %(Daug_P_MIN)s)" \
               "& (AMINCHILD(PT) > %(Daug_All_PT_MIN)s)" % locals()

    betacCuts = "(PT > %(Xic_PT_MIN)s)" % locals()

    _Betac2pDs = CombineParticles(
        DecayDescriptors = decDescriptors
        , CombinationCut = combCuts
        , MotherCut = betacCuts
    )

    return Selection( name,
                      Algorithm = _Betac2pDs,
                      RequiredSelections = inputSel
                    )    


def makeBetacLb1( name
               , inputSel
               , Daug_All_PT_MIN
               , Daug_P_MIN
               , Daug_TRCHI2DOF_MAX
               , Comb_MASS_MIN
               , Comb_MASS_MAX
               , Comb_ADOCAMAX_MAX 
               , Xic_PT_MIN
               , Xic_BPVDIRA_MIN
               , Xic_BPVIPCHI2_MAX
               , Xic_BPVLTIME_MAX
               , Xic_BPVLTIME_MIN
               , decDescriptors = [ "[Lambda_b0 -> p+ K- K+ KS0 pi-]cc" ]
             ) :
                     
    combCuts = "(AM > %(Comb_MASS_MIN)s)" \
               "& (AM <  %(Comb_MASS_MAX)s)" \
               "& (( ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3)+ ACHILD(MIPCHI2DV(), 6) )>25)" \
               "& (AMINCHILD(P)  > %(Daug_P_MIN)s)" \
               "& (AMINCHILD(PT) > %(Daug_All_PT_MIN)s)" % locals()
                              
    betacCuts = "(PT > %(Xic_PT_MIN)s)" % locals()
               
    _BetacLb1 = CombineParticles(
        DecayDescriptors = decDescriptors
        , CombinationCut = combCuts
        , MotherCut = betacCuts 
    )
               
    return Selection( name,
                      Algorithm = _BetacLb1,
                      RequiredSelections = inputSel
                    )
    
def makeBetacLb2( name
               , inputSel
               , Daug_All_PT_MIN
               , Daug_P_MIN
               , Daug_TRCHI2DOF_MAX
               , Comb_MASS_MIN
               , Comb_MASS_MAX
               , Comb_ADOCAMAX_MAX 
               , Xic_PT_MIN
               , Xic_BPVDIRA_MIN
               , Xic_BPVIPCHI2_MAX
               , Xic_BPVLTIME_MAX
               , Xic_BPVLTIME_MIN
               , decDescriptors = [ "[Lambda_b0 -> p+ K- pi+ KS0 KS0 pi-]cc" ]
             ) :
    
    combCuts = "(AM > %(Comb_MASS_MIN)s)" \
               "& (AM <  %(Comb_MASS_MAX)s)" \
               "& ((ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 6) )>25)" \
               "& (AMINCHILD(P)  > %(Daug_P_MIN)s)" \
               "& (AMINCHILD(PT) > %(Daug_All_PT_MIN)s)" % locals()
               
    betacCuts = "(PT > %(Xic_PT_MIN)s)" % locals()
               
    _BetacLb2 = CombineParticles( 
        DecayDescriptors = decDescriptors
        , CombinationCut = combCuts
        , MotherCut = betacCuts   
    )
               
    return Selection( name,
                      Algorithm = _BetacLb2,
                      RequiredSelections = inputSel
                    )


