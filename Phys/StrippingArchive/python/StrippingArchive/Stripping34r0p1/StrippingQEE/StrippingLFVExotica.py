###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#############################################################
#      Stripping lines for LFV direct searches              #
# Author: X/ Cid Vidal xabier.cid.vidal@cern.ch (2016)      #
#  Matthieu Marinangeli matthieu.marinangeli@cern.ch (2018) #
#############################################################

"""

LFV searches, based on functionality from StrippingA1MuMu by C. Elsasser

"""

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import MeV,picosecond
from StandardParticles import StdAllLooseMuons, StdAllLooseElectrons
from CommonParticles.Utils import *

__author__  = 'Xabier Cid Vidal, Matthieu Marinangeli'
__date__    = '22/09/2016'
__version__ = 3.0
__all__     = 'LFVExoticaConf', 'default_config'

default_config = {
  'NAME'        : 'LFVExotica',
  'BUILDERTYPE' : 'LFVExoticaConf',
  'WGs'         : [ 'QEE' ],
  'STREAMS'     : [ 'Leptonic' ],
  'CONFIG'      : {
    'Common': {
              'checkPV'  : False
              },
    'StrippingLFVExoticaPromptLine': {
              'Prescale'   : 1.0,
              'Postscale'  : 1.0,
              'Prescale_SS'   : 0.5,
              'Postscale_SS'  : 1.0,
              'Prompt_PT' : 500*MeV,
              'Prompt_P' : 10000*MeV,                                                  
              'Prompt_ProbNNmu' : 0.5,
              'Prompt_ProbNNe' : 0.25,
              'Prompt_GhostProb': 0.3,
              'Prompt_IPChi2' : 1,
              'Prompt_VChi2' : 5,
              'Prompt_XIPChi2' : 1,
              'Prompt_M' : 0,
              'Prompt_FDChi2' : 1,
              },
    'StrippingLFVExoticaDetachedLine': {
              'Prescale'   : 1.0,
              'Postscale'  : 1.0,
              'Prescale_SS'   : 1.0,
              'Postscale_SS'  : 1.0,
              'Detached_PT' : 500*MeV,
              'Detached_P' : 10000*MeV,                                                  
              'Detached_ProbNNmu' : 0.5,
              'Detached_ProbNNe' : 0.,
              'Detached_GhostProb': 0.3,
              'Detached_IPChi2' : 16,
              'Detached_VChi2' : 10,
              'Detached_XIPChi2' : 16,
              'Detached_M' : 0,
              'Detached_TAU' : 1*picosecond,
              'Detached_FDChi2' : 45,
              },
    'StrippingLFVExoticaEMuXDetachedLine': {
              'Prescale'   : 1.0,
              'Postscale'  : 1.0,
              'Prescale_SS'   : 1.0,
              'Postscale_SS'  : 1.0,
              'max_TRCHI2DV'     : 3.0,
              'Detached_PT' : 1600*MeV,
              'Detached_P' : 10000*MeV,                                                  
              'Detached_ProbNNmu' : 0.5,
              'Detached_GhostProb': 0.20,
              'Detached_IPChi2' : 25,                                                 
              'Detached_ProbNNe' : 0.,
              'Detached_VChi2' : 8,
              'Detached_M' : 0,
              'Detached_TAU' : 0.5*picosecond,
              'Detached_FDChi2' : 225,
              },        
     'StrippingLFVExoticaW2EN2EMuXDetachedLine': {
              'Prescale'   : 1.0,
              'Postscale'  : 1.0,
              'Prescale_SS'   : 1.0,
              'Postscale_SS'  : 1.0,
              'max_TRCHI2DV'     : 3.0,
              'Detached_PT' : 500*MeV,
              'Detached_P' : 10000*MeV,                                                  
              'Detached_ProbNNmu' : 0.5,
              'Detached_GhostProb': 0.20,
              'Detached_IPChi2' : 16,                                                 
              'Detached_ProbNNe' : 0.,
              'Detached_VChi2' : 8,
              'Detached_M' : 0,
              'Detached_TAU' : 0.5*picosecond,
              'Detached_FDChi2' : 225,
              'Prompt_PT' : 2000*MeV,
              'Prompt_P' : 10000*MeV,                                                                                           
              'Prompt_ProbNN' : 0.0,
              'W_M': 0*MeV,
              'Prompt_IPChi2' : 5,
              'Prompt_GhostProb': 0.30,
              },
    'StrippingLFVExoticaW2MuN2EMuXDetachedLine': {
              'Prescale'   : 1.0,
              'Postscale'  : 1.0,
              'Prescale_SS'   : 1.0,
              'Postscale_SS'  : 1.0,
              'max_TRCHI2DV'     : 3.0,
              'Detached_PT' : 500*MeV,
              'Detached_P' : 10000*MeV,                                                  
              'Detached_ProbNNmu' : 0.5,
              'Detached_GhostProb': 0.20,
              'Detached_IPChi2' : 16,                                                 
              'Detached_ProbNNe' : 0.,
              'Detached_VChi2' : 8,
              'Detached_M' : 0,
              'Detached_TAU' : 0.5*picosecond,
              'Detached_FDChi2' : 225,
              'Prompt_PT' : 1000*MeV,
              'Prompt_P' : 10000*MeV,                                                                                           
              'Prompt_ProbNN' : 0.5,
              'W_M': 0*MeV,
              'Prompt_IPChi2' : 5,
              'Prompt_GhostProb': 0.30,
              },
  }
  }


## Jet isolation stuff
##########################################
## standard jetID
from JetAccessories.JetMaker_Config import JetMakerConf
stdjets_name_noban = "StdJetsNoJetIDNoBan"
StdJetsNoJetIDNoBan = JetMakerConf(stdjets_name_noban,
                                   R = 0.7 ,
                                   PtMin = 500.,
                                   JetIDCut = False).algorithms[0]

## configure Data-On-Demand service                                                        
locations = updateDoD ( StdJetsNoJetIDNoBan )


# the daughters banning is line dependent (need to provide the location of the daughters!)
def create_stdjets(strob,line_location,stdjets_name_ban):
    
    myconf1 = JetMakerConf(stdjets_name_ban,
                           R = 0.7 ,
                           PtMin = 500.,
                           listOfParticlesToBan = [line_location],
                           JetIDCut = False).algorithms[0]
    
    if "StdJetsNoJetIDBan" in dir(strob): strob.StdJetsNoJetIDBan.append(myconf1)
    else: strob.StdJetsNoJetIDBan= [myconf1]
                                      
    ## configure Data-On-Demand service                                            
    locations = updateDoD ( myconf1 )


relInfoJetsVars = ["JETNOMU1PX","JETNOMU1PY", "JETNOMU1PZ", "JETNOMU1PT", "JETNOMU1JETWIDTH", "JETNOMU1NNTAG", "JETNOMU1MNF", "JETNOMU2PX", "JETNOMU2PY", "JETNOMU2PZ", "JETNOMU2PT", "JETNOMU2JETWIDTH", "JETNOMU2NNTAG", "JETNOMU2MNF", "JETNOMU3PX", "JETNOMU3PY", "JETNOMU3PZ", "JETNOMU3PT", "JETNOMU3JETWIDTH", "JETNOMU3NNTAG", "JETNOMU3MNF", "JETMU1PX", "JETMU1PY", "JETMU1PZ", "JETMU1PT", "JETMU1JETWIDTH", "JETMU1NNTAG", "JETMU1MNF", "JETMU2PX", "JETMU2PY", "JETMU2PZ", "JETMU2PT", "JETMU2JETWIDTH", "JETMU2NNTAG", "JETMU2MNF"]

def JetsRelInfo(stdjets_name_ban):
  
    ret = { 'Type' : 'RelInfoJetsVariables',
            'Location': 'RelatedInfoJets',
            'Variables': relInfoJetsVars,
            'UseVarsJetsWithB' : False,
            'LocationJetsNoMu' : "Phys/"+stdjets_name_ban+"/Particles",
            'LocationJetsNoRemove' : "Phys/"+stdjets_name_noban+"/Particles"
          }
        
    return ret

relInfoConeVars = ["CONEANGLE","CONEMULT","CONEP","CONEPT","CONEPASYM","CONEPTASYM","CONEDELTAETA","CONEDELTAPHI"]

def IsoConeRelInfo(angle, location, *args):
  
    ret = {'Type' : 'RelInfoConeVariables', 'Variables':relInfoConeVars, 'ConeAngle' : angle, 'Location': location}
    dl = {}
    for a in args:
      dl[a["part"]] = a["loc"]
    ret['DaughterLocations'] = dl
    
    return ret

relInfoPFVars = ["PFCHARGEDMASS","PFALLMASS","PFCHARGEDMISSPT","PFALLMISSPT","PFCHARGEDMISSPX","PFALLMISSPX","PFCHARGEDMISSPY","PFALLMISSPY"]
    
class LFVExoticaConf(LineBuilder) :
  __configuration_keys__ = default_config['CONFIG'].keys()

  def __init__(self, name, config):
    LineBuilder.__init__(self, name, config)
    
    
    stdjets_name_ban_prompt = "StdJetsNoJetIDBanDaughtersPrompt"
    stdjets_name_ban_ssprompt = "StdJetsNoJetIDBanDaughtersSameSignPrompt"
    stdjets_name_ban_detached = "StdJetsNoJetIDBanDaughtersDetached"
    stdjets_name_ban_ssdetached = "StdJetsNoJetIDBanDaughtersSameSignDetached"
    
    RelatedInfoToolsPrompt = [JetsRelInfo(stdjets_name_ban_prompt)]
    RelatedInfoToolsSSPrompt = [JetsRelInfo(stdjets_name_ban_ssprompt)]
    RelatedInfoToolsDetached = [JetsRelInfo(stdjets_name_ban_detached)]
    RelatedInfoToolsSSDetached = [JetsRelInfo(stdjets_name_ban_ssdetached)]
              
                                    
    PFrelatedinfos = {'Type' : 'RelInfoPFVariables', 'Variables':relInfoPFVars, 'Location': 'PFVariables'}
    VertexIso = {'Type': 'RelInfoVertexIsolation', 'Location':'RelInfoVtxIso' }
                                    
    ## cone related infos
                                    
    RelatedInfoToolsPrompt +=    [IsoConeRelInfo(0.5, 'ConeVariables05', {"part": '[H_30 -> ^mu+ e-]CC', 'loc': 'MuonConeInfo05'}, 
                                                                         {"part": '[H_30 -> mu+ ^e-]CC', 'loc': 'ElectronConeInfo05'}),
                                  IsoConeRelInfo(1.0, 'ConeVariables1',  {"part": '[H_30 -> ^mu+ e-]CC', 'loc': 'MuonConeInfo1'},
                                                                         {"part": '[H_30 -> mu+ ^e-]CC', 'loc': 'ElectronConeInfo1'}),
                                  IsoConeRelInfo(2.0, 'ConeVariables2',  {"part": '[H_30 -> ^mu+ e-]CC', 'loc': 'MuonConeInfo2'},
                                                                         {"part": '[H_30 -> mu+ ^e-]CC', 'loc': 'ElectronConeInfo2'})] 
                                  
    RelatedInfoToolsSSPrompt +=  [IsoConeRelInfo(0.5, 'ConeVariables05', {"part": '[H_30 -> ^mu+ e+]CC', 'loc': 'MuonConeInfo05'}, 
                                                                         {"part": '[H_30 -> mu+ ^e+]CC', 'loc': 'ElectronConeInfo05'}),
                                  IsoConeRelInfo(1.0, 'ConeVariables1',  {"part": '[H_30 -> ^mu+ e+]CC', 'loc': 'MuonConeInfo1'},
                                                                         {"part": '[H_30 -> mu+ ^e+]CC', 'loc': 'ElectronConeInfo1'}),
                                  IsoConeRelInfo(2.0, 'ConeVariables2',  {"part": '[H_30 -> ^mu+ e+]CC', 'loc': 'MuonConeInfo2'},
                                                                         {"part": '[H_30 -> mu+ ^e+]CC', 'loc': 'ElectronConeInfo2'})]
                                  
    RelatedInfoToolsDetached +=  [IsoConeRelInfo(0.5, 'ConeVariables05', {"part": '[H_30 -> ^mu+ e-]CC', 'loc': 'MuonConeInfo05'}, 
                                                                         {"part": '[H_30 -> mu+ ^e-]CC', 'loc': 'ElectronConeInfo05'}),
                                  IsoConeRelInfo(1.0, 'ConeVariables1',  {"part": '[H_30 -> ^mu+ e-]CC', 'loc': 'MuonConeInfo1'},
                                                                         {"part": '[H_30 -> mu+ ^e-]CC', 'loc': 'ElectronConeInfo1'}),
                                  IsoConeRelInfo(2.0, 'ConeVariables2',  {"part": '[H_30 -> ^mu+ e-]CC', 'loc': 'MuonConeInfo2'},
                                                                         {"part": '[H_30 -> mu+ ^e-]CC', 'loc': 'ElectronConeInfo2'})] 
                                  
    RelatedInfoToolsSSDetached += [IsoConeRelInfo(0.5, 'ConeVariables05', {"part": '[H_30 -> ^mu+ e+]CC', 'loc': 'MuonConeInfo05'}, 
                                                                         {"part": '[H_30 -> mu+ ^e+]CC', 'loc': 'ElectronConeInfo05'}),
                                  IsoConeRelInfo(1.0, 'ConeVariables1',  {"part": '[H_30 -> ^mu+ e+]CC', 'loc': 'MuonConeInfo1'},
                                                                         {"part": '[H_30 -> mu+ ^e+]CC', 'loc': 'ElectronConeInfo1'}),
                                  IsoConeRelInfo(2.0, 'ConeVariables2',  {"part": '[H_30 -> ^mu+ e+]CC', 'loc': 'MuonConeInfo2'},
                                                                         {"part": '[H_30 -> mu+ ^e+]CC', 'loc': 'ElectronConeInfo2'})]

  
    W2EN_RelatedInfos = [IsoConeRelInfo(0.5, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e-)]CC e+ ]CC', 'loc': 'MuonConeInfo05'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e-)]CC e+ ]CC', 'loc': 'ElectronConeInfo05'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e-)]CC e+ ]CC', 'loc': 'HNLConeInfo05'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e-)]CC ^e+ ]CC', 'loc': 'PromptElectronConeInfo05'}),
                         IsoConeRelInfo(1.0, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e-)]CC e+ ]CC', 'loc': 'MuonConeInfo1'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e-)]CC e+ ]CC', 'loc': 'ElectronConeInfo1'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e-)]CC e+ ]CC', 'loc': 'HNLConeInfo1'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e-)]CC ^e+ ]CC', 'loc': 'PromptElectronConeInfo1'}),
                         IsoConeRelInfo(2.0, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e-)]CC e+ ]CC', 'loc': 'MuonConeInfo2'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e-)]CC e+ ]CC', 'loc': 'ElectronConeInfo2'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e-)]CC e+ ]CC', 'loc': 'HNLConeInfo2'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e-)]CC ^e+ ]CC', 'loc': 'PromptElectronConeInfo2'})]  
                                                                
    W2MuN_RelatedInfos = [IsoConeRelInfo(0.5, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e-)]CC mu+ ]CC', 'loc': 'MuonConeInfo05'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e-)]CC mu+ ]CC', 'loc': 'ElectronConeInfo05'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e-)]CC mu+ ]CC', 'loc': 'HNLConeInfo05'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e-)]CC ^mu+ ]CC', 'loc': 'PromptMuonConeInfo05'}),
                         IsoConeRelInfo(1.0, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e-)]CC mu+ ]CC', 'loc': 'MuonConeInfo1'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e-)]CC mu+ ]CC', 'loc': 'ElectronConeInfo1'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e-)]CC mu+ ]CC', 'loc': 'HNLConeInfo1'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e-)]CC ^mu+ ]CC', 'loc': 'PromptMuonConeInfo1'}),
                         IsoConeRelInfo(2.0, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e-)]CC mu+ ]CC', 'loc': 'MuonConeInfo2'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e-)]CC mu+ ]CC', 'loc': 'ElectronConeInfo2'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e-)]CC mu+ ]CC', 'loc': 'HNLConeInfo2'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e-)]CC ^mu+ ]CC', 'loc': 'PromptMuonConeInfo2'})]                                                             
                                                                
    W2ENSS_RelatedInfos = [IsoConeRelInfo(0.5, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e+)]CC e+ ]CC', 'loc': 'MuonConeInfo05'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e+)]CC e+ ]CC', 'loc': 'ElectronConeInfo05'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e+)]CC e+ ]CC', 'loc': 'HNLConeInfo05'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e+)]CC ^e+ ]CC', 'loc': 'PromptElectronConeInfo05'}),
                         IsoConeRelInfo(1.0, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e+)]CC e+ ]CC', 'loc': 'MuonConeInfo1'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e+)]CC e+ ]CC', 'loc': 'ElectronConeInfo1'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e+)]CC e+ ]CC', 'loc': 'HNLConeInfo1'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e+)]CC ^e+ ]CC', 'loc': 'PromptElectronConeInfo1'}),
                         IsoConeRelInfo(2.0, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e+)]CC e+ ]CC', 'loc': 'MuonConeInfo2'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e+)]CC e+ ]CC', 'loc': 'ElectronConeInfo2'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e+)]CC e+ ]CC', 'loc': 'HNLConeInfo2'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e+)]CC ^e+ ]CC', 'loc': 'PromptElectronConeInfo2'})]  
                                                                
    W2MuNSS_RelatedInfos = [IsoConeRelInfo(0.5, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e+)]CC mu+ ]CC', 'loc': 'MuonConeInfo05'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e+)]CC mu+ ]CC', 'loc': 'ElectronConeInfo05'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e+)]CC mu+ ]CC', 'loc': 'HNLConeInfo05'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e+)]CC ^mu+ ]CC', 'loc': 'PromptMuonConeInfo05'}),
                         IsoConeRelInfo(1.0, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e+)]CC mu+ ]CC', 'loc': 'MuonConeInfo1'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e+)]CC mu+ ]CC', 'loc': 'ElectronConeInfo1'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e+)]CC mu+ ]CC', 'loc': 'HNLConeInfo1'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e+)]CC ^mu+ ]CC', 'loc': 'PromptMuonConeInfo1'}),
                         IsoConeRelInfo(2.0, 'ConeVariables05', {"part": '[W+ -> [(H_30 -> ^mu+ e+)]CC mu+ ]CC', 'loc': 'MuonConeInfo2'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ ^e+)]CC mu+ ]CC', 'loc': 'ElectronConeInfo2'},
                                                                {"part": '[W+ -> ^[(H_30 -> mu+ e+)]CC mu+ ]CC', 'loc': 'HNLConeInfo2'},
                                                                {"part": '[W+ -> [(H_30 -> mu+ e+)]CC ^mu+ ]CC', 'loc': 'PromptMuonConeInfo2'})]                                                                
                                                                        
    
    RelatedInfoToolsPrompt += [PFrelatedinfos, VertexIso]
    RelatedInfoToolsSSPrompt += [PFrelatedinfos, VertexIso]
    RelatedInfoToolsDetached += [PFrelatedinfos, VertexIso]
    RelatedInfoToolsSSDetached += [PFrelatedinfos, VertexIso]
    W2EN_RelatedInfos += [PFrelatedinfos, VertexIso]
    W2MuN_RelatedInfos += [PFrelatedinfos, VertexIso]

    ## [A1 -> mu+ e-]cc prompt line:
    sel_prompt = combinePrompt(name,config['StrippingLFVExoticaPromptLine'])
    
    promptline = StrippingLine(name + 'PromptLine',
                               prescale  = config['StrippingLFVExoticaPromptLine']['Prescale'],
                               postscale = config['StrippingLFVExoticaPromptLine']['Postscale'],
                               checkPV   = config['Common']['checkPV'],
                               selection = sel_prompt,
                               RelatedInfoTools = RelatedInfoToolsPrompt)
    self.registerLine(promptline)
    
    ### [A1 -> mu- e-]cc prompt line:
    sel_prompt_ss = combinePrompt(name,config['StrippingLFVExoticaPromptLine'],ss=True)
            
    sspromptline = StrippingLine(name + 'SameSignPromptLine',
                                prescale  = config['StrippingLFVExoticaPromptLine']['Prescale_SS'],
                                postscale = config['StrippingLFVExoticaPromptLine']['Postscale_SS'],
                                checkPV   = config['Common']['checkPV'],
                                selection = sel_prompt_ss,
                                RelatedInfoTools = RelatedInfoToolsSSPrompt)
    self.registerLine(sspromptline)
    
    ### [A1 -> mu+ e-]cc detached line:
    sel_detached = combineDetached(name,config['StrippingLFVExoticaDetachedLine'])

    detachedline = StrippingLine(name + 'DetachedLine',
                                prescale  = config['StrippingLFVExoticaDetachedLine']['Prescale'],
                                postscale = config['StrippingLFVExoticaDetachedLine']['Postscale'],
                                checkPV   = config['Common']['checkPV'],
                                selection = sel_detached,
                                RelatedInfoTools = RelatedInfoToolsDetached)
    self.registerLine(detachedline)

    ### [A1 -> mu- e-]cc detached line:
    sel_detached_ss = combineDetached(name,config['StrippingLFVExoticaDetachedLine'],ss=True)

    ssdetachedline = StrippingLine(name + 'SameSignDetachedLine',
                                prescale  = config['StrippingLFVExoticaDetachedLine']['Prescale_SS'],
                                postscale = config['StrippingLFVExoticaDetachedLine']['Postscale_SS'],
                                checkPV   = config['Common']['checkPV'],
                                selection = sel_detached_ss,
                                RelatedInfoTools = RelatedInfoToolsSSDetached)
    self.registerLine(ssdetachedline)
    
    ### [A1 -> mu+ e- X]cc detached line:
    sel_detached_emux = combineDetached_EMuX(name,config['StrippingLFVExoticaEMuXDetachedLine'])

    detachedline_emux = StrippingLine(name + 'EMuXDetachedLine',
                        prescale  = config['StrippingLFVExoticaEMuXDetachedLine']['Prescale'],
                        postscale = config['StrippingLFVExoticaEMuXDetachedLine']['Postscale'],
                        checkPV   = config['Common']['checkPV'],
                        selection = sel_detached_emux,
                        RelatedInfoTools = RelatedInfoToolsDetached)
    self.registerLine(detachedline_emux)

    ### [A1 -> mu- e- X]cc detached line:
    sel_detached_ss_emux = combineDetached_EMuX(name,config['StrippingLFVExoticaEMuXDetachedLine'],ss=True)

    ssdetachedline_emux = StrippingLine(name + 'EMuXSameSignDetachedLine',
                        prescale  = config['StrippingLFVExoticaEMuXDetachedLine']['Prescale_SS'],
                        postscale = config['StrippingLFVExoticaEMuXDetachedLine']['Postscale_SS'],
                        checkPV   = config['Common']['checkPV'],
                        selection = sel_detached_ss_emux,
                        RelatedInfoTools = RelatedInfoToolsSSDetached)
    self.registerLine(ssdetachedline_emux)
    
    
    ### [W+ -> e+ (HNL -> mu- e+ X)]cc detached line:
    sel_detached_N_e_emux = makeW2LeptonN2EMuX(name, config['StrippingLFVExoticaW2EN2EMuXDetachedLine'],  lepton="e", ss=False)

    N_e_detachedline_emux = StrippingLine(name + 'W2EN2EMuXDetachedLine',
              prescale  = config['StrippingLFVExoticaW2EN2EMuXDetachedLine']['Prescale'],
              postscale = config['StrippingLFVExoticaW2EN2EMuXDetachedLine']['Postscale'],
              checkPV   = config['Common']['checkPV'],
              selection = sel_detached_N_e_emux,
              RelatedInfoTools = W2EN_RelatedInfos)
    self.registerLine(N_e_detachedline_emux)
    
    ### [W+ -> mu+ (HNL -> mu- e+ X)]cc detached line:
    sel_detached_N_mu_emux = makeW2LeptonN2EMuX(name, config['StrippingLFVExoticaW2MuN2EMuXDetachedLine'], lepton="mu", ss=False)

    N_mu_detachedline_emux = StrippingLine(name + 'W2MuN2EMuXDetachedLine',
              prescale  = config['StrippingLFVExoticaW2MuN2EMuXDetachedLine']['Prescale'],
              postscale = config['StrippingLFVExoticaW2MuN2EMuXDetachedLine']['Postscale'],
              checkPV   = config['Common']['checkPV'],
              selection = sel_detached_N_mu_emux,
              RelatedInfoTools = W2MuN_RelatedInfos)
    self.registerLine(N_mu_detachedline_emux)
    
    
    ### [W+ -> e+ (HNL -> mu+ e+ X)]cc detached line:
    sel_detached_N_e_emux_SS = makeW2LeptonN2EMuX(name, config['StrippingLFVExoticaW2EN2EMuXDetachedLine'],  lepton="e", ss=True)

    N_e_detachedline_emux_SS = StrippingLine(name + 'W2EN2EMuXSSDetachedLine',
              prescale  = config['StrippingLFVExoticaW2EN2EMuXDetachedLine']['Prescale_SS'],
              postscale = config['StrippingLFVExoticaW2EN2EMuXDetachedLine']['Postscale_SS'],
              checkPV   = config['Common']['checkPV'],
              selection = sel_detached_N_e_emux_SS,
              RelatedInfoTools = W2ENSS_RelatedInfos)
    self.registerLine(N_e_detachedline_emux_SS)
    
    ### [W+ -> mu+ (HNL -> mu+ e+ X)]cc detached line:
    sel_detached_N_mu_emux_SS = makeW2LeptonN2EMuX(name, config['StrippingLFVExoticaW2MuN2EMuXDetachedLine'], lepton="mu", ss=True)

    N_mu_detachedline_emux_SS = StrippingLine(name + 'W2MuN2EMuXSSDetachedLine',
              prescale  = config['StrippingLFVExoticaW2MuN2EMuXDetachedLine']['Prescale_SS'],
              postscale = config['StrippingLFVExoticaW2MuN2EMuXDetachedLine']['Postscale_SS'],
              checkPV   = config['Common']['checkPV'],
              selection = sel_detached_N_mu_emux_SS,
              RelatedInfoTools = W2MuNSS_RelatedInfos)
    self.registerLine(N_mu_detachedline_emux_SS)

    create_stdjets(self,promptline.outputLocation(),stdjets_name_ban_prompt)
    create_stdjets(self,sspromptline.outputLocation(),stdjets_name_ban_ssprompt)
    create_stdjets(self,detachedline.outputLocation(),stdjets_name_ban_detached)
    create_stdjets(self,ssdetachedline.outputLocation(),stdjets_name_ban_ssdetached)
    

def combinePrompt(name,config,ss=False):
    
  daugh_cut={'mu': ("(PT > {Prompt_PT}) " 
                    "& (P > {Prompt_P}) " 
                    "& (BPVIPCHI2() < {Prompt_IPChi2}) " 
                    "& (TRGHOSTPROB < {Prompt_GhostProb}) " 
                    "& (PROBNNmu > {Prompt_ProbNNmu}) ").format(**config),
            'e': ("(PT > {Prompt_PT}) " 
                    "& (P > {Prompt_P}) " 
                    "& (PROBNNe > {Prompt_ProbNNe}) " 
                    "& (BPVIPCHI2() < {Prompt_IPChi2}) " 
                    "& (TRGHOSTPROB < {Prompt_GhostProb}) ").format(**config)}
    
  comb_cut = ("(APT > 2.0*{Prompt_PT}) &  (AM > {Prompt_M}) &" 
              "(ACUTDOCACHI2({Prompt_VChi2},''))").format(**config)
  
  mother_cut = ("(PT > 2*{Prompt_PT})" 
                "& (HASVERTEX)" 
                "& (VFASPF(VCHI2PDOF) < {Prompt_VChi2}) " 
                "& (BPVIPCHI2() < {Prompt_XIPChi2})" 
                "& (BPVVDCHI2 < {Prompt_FDChi2})" ).format(**config)
    
  decay = "[H_30 -> mu+ e-]cc"
  ename = "e-"
  namesel = name+"PromptSel"
  if ss:
    decay = "[H_30 -> mu+ e+]cc"
    ename = "e+" 
    namesel = name+"SSPromptSel"
  
  _combination = CombineParticles( DecayDescriptor    = decay,
                                   CombinationCut     = comb_cut,
                                   DaughtersCuts      = { 'mu+' : daugh_cut["mu"],
                                                          ename : daugh_cut["e"] },
                                   MotherCut          = mother_cut,
                                   )

  return Selection ( namesel,
                     Algorithm          = _combination,
                     RequiredSelections = [ StdAllLooseMuons, StdAllLooseElectrons]
                     )


def combineDetached(name,config,ss=False):
  
  
  daugh_cut={'mu': ("(PT > {Detached_PT}) " 
                    "& (P > {Detached_P}) " 
                    "& (MIPCHI2DV(PRIMARY) > {Detached_IPChi2}) " 
                    "& (TRGHOSTPROB < {Detached_GhostProb}) " 
                    "& (PROBNNmu > {Detached_ProbNNmu}) ").format(**config),
            'e': ("(PT > {Detached_PT}) " 
                    "& (P > {Detached_P}) " 
                    "& (PROBNNe > {Detached_ProbNNe}) " 
                    "& (MIPCHI2DV(PRIMARY) > {Detached_IPChi2}) " 
                    "& (TRGHOSTPROB < {Detached_GhostProb}) ").format(**config)}
    
  comb_cut = ("(APT > 2.0*{Detached_PT}) &  (AM > {Detached_M}) &" 
              "(ACUTDOCACHI2({Detached_VChi2},''))").format(**config)
  
  mother_cut = ("(PT > 2*{Detached_PT})" 
                "& (HASVERTEX)" 
                "& (VFASPF(VCHI2PDOF) < {Detached_VChi2}) " 
                "& (BPVIPCHI2() < {Detached_XIPChi2})" 
                "& (BPVVDCHI2 > {Detached_FDChi2})" 
                "& (BPVLTIME() > {Detached_TAU})").format(**config)
            
  decay = "[H_30 -> mu+ e-]cc"
  ename = "e-"
  namesel = name+"DetachedSel"
  if ss:
    decay = "[H_30 -> mu+ e+]cc"
    ename = "e+" 
    namesel = name+"SSDetachedSel"

  _combination = CombineParticles( DecayDescriptor    = decay,
                                   CombinationCut     = comb_cut,
                                   DaughtersCuts      = { 'mu+' : daugh_cut["mu"],
                                                          ename : daugh_cut["e"] },
                                   MotherCut          = mother_cut,
                                   )

  return Selection ( namesel,
                     Algorithm          = _combination,
                     RequiredSelections = [ StdAllLooseMuons, StdAllLooseElectrons]
                     )
                    
def combineDetached_EMuX(name,config,ss=False):
  
    daugh_cut={'mu': ("(PT > {Detached_PT}) " 
                      "& (P > {Detached_P}) " 
                      "& (MIPCHI2DV(PRIMARY) > {Detached_IPChi2}) " 
                      "& (TRGHOSTPROB < {Detached_GhostProb}) " 
                      "& (PROBNNmu > {Detached_ProbNNmu}) "
                      "& (TRCHI2DOF < {max_TRCHI2DV})").format(**config),
              'e': ("(PT > {Detached_PT}) " 
                      "& (P > {Detached_P}) " 
                      "& (PROBNNe > {Detached_ProbNNe}) " 
                      "& (MIPCHI2DV(PRIMARY) > {Detached_IPChi2}) " 
                      "& (TRGHOSTPROB < {Detached_GhostProb}) "
                      "& (TRCHI2DOF < {max_TRCHI2DV})").format(**config)}
      
    comb_cut = ("(APT > 2.*{Detached_PT}) &  (AM > {Detached_M}) &" 
                "(ACUTDOCACHI2({Detached_VChi2},''))").format(**config)

    mother_cut = ("(PT > 2.*{Detached_PT})" 
                  "& (HASVERTEX)" 
                  "& (VFASPF(VCHI2PDOF) < {Detached_VChi2}) " 
                  "& (BPVVDCHI2 > {Detached_FDChi2})" 
                  "& (BPVLTIME() > {Detached_TAU})").format(**config)
              
    decay = "[H_30 -> mu+ e-]cc"
    ename = "e-"
    namesel = name+"DetachedSel_EMuX"
    if ss:
      decay = "[H_30 -> mu+ e+]cc"
      ename = "e+" 
      namesel = name+"SSDetachedSel_EMuX"

    _combination = CombineParticles( DecayDescriptor    = decay,
                                      CombinationCut     = comb_cut,
                                      DaughtersCuts      = { 'mu+' : daugh_cut["mu"],
                                                            ename : daugh_cut["e"] },
                                      MotherCut          = mother_cut,
                                      )

    return Selection ( namesel,
                        Algorithm          = _combination,
                        RequiredSelections = [ StdAllLooseMuons, StdAllLooseElectrons]
                        )

def makeW2LeptonN2EMuX(name, config, lepton, ss=False):
  
    #lepton = e, mu

    EMuX = combineDetached_EMuX("{0}N{1}".format(lepton, name), config, ss)
    
    decay = "[W+ -> H_30 {0}+]cc".format(lepton)
    
    daugh_cut={
          "{0}+".format(lepton): ("(PT > {Prompt_PT}) " 
          "& (P > {Prompt_P}) "
          "& (PROBNN{0} > {Prompt_ProbNN}) " 
          "& (TRGHOSTPROB < {Prompt_GhostProb}) "
          "& (MIPCHI2DV(PRIMARY) < {Prompt_IPChi2}) "
          ).format(lepton, **config)}
                    
    comb_cut = ("AM > {W_M}").format(**config)
                    
    mother_cut = ("M > {W_M}").format(**config)

    _combination = CombineParticles(DecayDescriptor = decay,
                    CombinationCut = comb_cut,
                    DaughtersCuts  = daugh_cut,
                    MotherCut = mother_cut)
                                    
    namesel = name+"W2{0}N".format(lepton)
    
    if ss:
      namesel += "SS"
      
    if lepton == "e":
        lepton_loc = StdAllLooseElectrons
    elif lepton == "mu":
        lepton_loc = StdAllLooseMuons
                                    
    return Selection( namesel,
            Algorithm = _combination,
            RequiredSelections=[EMuX, lepton_loc] )

    
    
  