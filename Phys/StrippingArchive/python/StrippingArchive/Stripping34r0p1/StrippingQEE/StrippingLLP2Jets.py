###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping lines for dark pions
into displaced jets analysis
'''

__author__ = ['Igor Kostiuk', 'Wouter Hulsbergen', 'Carlos Vazquez Sierra']
__date__ = '25/01/2019'

__all__ = ('LLP2Jets_sConf','default_config')

from Gaudi.Configuration import *
from Configurables import JetVertexAlg
from StandardParticles import StdJets
from CommonParticles.Utils import updateDoD
from PhysSelPython.Wrappers import LimitSelection, FilterSelection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import GeV, mm

default_config = {
    'NAME'              : 'LLP2Jets',
    'WGs'               : ['QEE'],
    'BUILDERTYPE'       : 'LLP2Jets_sConf',
    'CONFIG'    : {
                         'MINJETS'                : 2
                 ,       'MAXJETS'                : 1.e+10
                 ,       'NDISPLONG_NOVX'         : 4
                 ,       'NDISPLONG_HM'           : 5
                 ,       'NDISPLONG_LM'           : 5
                 ,       'MIN_IPCHI2'             : 25
                 ,       'JET_PT_NOVX'            : 10*GeV
                 ,       'JET_PT_HM'              : 10*GeV
                 ,       'JET_PT_LM'              : 15*GeV
                 ,       'D0DISP'                 : 0.9*mm
                 ,       'BSRHODISP'              : 0.6*mm
                 ,       'MINGD_HM'               : 9
                 ,       'MINGD_LM'               : 7
                 ,       'MINDAUGS_LM'            : 3
                 ,       'SinglePrescale'         : 0.01
                 ,       'DoublePrescale'         : 1.0
                 ,       'SingleHighMassPrescale' : 1.0
                 ,       'SingleLowMassPrescale'  : 1.0
                 ,       'gtrk'                   : 'ISBASIC & HASTRACK & ISLONG'
                 ,       'RawBanks'               : ['Calo', 'Trigger', 'Velo', 'Tracker']
                         },
    'STREAMS' : ['BhadronCompleteEvent']
    }

### Lines stored in this file:
# StrippingLLP2JetsSingleLine
# StrippingLLP2JetsDoubleLine
# StrippingLLP2JetsSingleHighMassLine
# StrippingLLP2JetsSingleLowMassLine

class LLP2Jets_sConf(LineBuilder) :
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)
        self.name = name
        self.config = config

        self.makeLLP2Jets()

    ### Stripping lines:

    def makeLLP2Jets( self ):

        ### Single line with loose cuts

        NoVtxJets = FilterSelection( "NoVtxJets" + self.name, StdJets,
            Code = ("(PT > %(JET_PT_NOVX)s)"
                    "& (NINTREE(%(gtrk)s & (%(MIN_IPCHI2)s < MIPCHI2DV(PRIMARY))) > %(NDISPLONG_NOVX)s)")
                     % self.config )

        SingleLine = StrippingLine( self.name + "SingleLine", algos = [ NoVtxJets ],
            RequiredRawEvents = self.config['RawBanks'], prescale = self.config['SinglePrescale'] )

        ### Double line with the same cuts as the Single line

        NoVtxJetsDouble = LimitSelection( NoVtxJets, maxsize = self.config['MAXJETS'], minsize = self.config['MINJETS'] )

        DoubleLine = StrippingLine( self.name + "DoubleLine", algos = [ NoVtxJetsDouble ],
            RequiredRawEvents = self.config['RawBanks'], prescale = self.config['DoublePrescale'] )

        ### Algorithm to look for vertices in jets

        jetVertexAlg = JetVertexAlg( "JetVertex" + self.name,
                             Input = StdJets.outputLocation(), Output = "Phys/StdJetsVtxAlg/Particles" )

        StdJetVtxAlg = updateDoD( jetVertexAlg, name="StdJetsVtxAlg" )

        StdJetVtx = DataOnDemand( Location = StdJetVtxAlg.keys()[0] )

        ### Single line with cuts on the number of jets granddaughters

        VtxJetsHighMass = FilterSelection( "VtxJetsHighMass" + self.name, StdJetVtx,
            Code = ("(PT > %(JET_PT_HM)s)"
                    "& (NINTREE(%(gtrk)s & (%(MIN_IPCHI2)s < MIPCHI2DV(PRIMARY))) > %(NDISPLONG_HM)s)"
                    "& (SUMTREE( ('D0'==ABSID) & (%(D0DISP)s < abs(VFASPF(VX_BEAMSPOTRHO(%(BSRHODISP)s)))), NDAUGS) >= %(MINGD_HM)s)")
                     % self.config )

        SingleHighMassLine = StrippingLine( self.name + "SingleHighMassLine", algos = [ VtxJetsHighMass ],
            RequiredRawEvents = self.config['RawBanks'], prescale = self.config['SingleHighMassPrescale'] )

        ### Single line with cuts on the number of jets granddaughters and daughters

        VtxJetsLowMass = FilterSelection( "VtxJetsLowMass" + self.name, StdJetVtx,
            Code = ("(PT > %(JET_PT_LM)s)"
                    "& (NINTREE(%(gtrk)s & (%(MIN_IPCHI2)s < MIPCHI2DV(PRIMARY))) > %(NDISPLONG_LM)s)"
                    "& (NINGENERATION( ('D0'==ABSID) & (%(D0DISP)s< abs(VFASPF(VX_BEAMSPOTRHO(%(BSRHODISP)s)))), 1) >= %(MINDAUGS_LM)s)"
                    "& (SUMTREE( ('D0'==ABSID) & (%(D0DISP)s < abs(VFASPF(VX_BEAMSPOTRHO(%(BSRHODISP)s)))), NDAUGS ) >= %(MINGD_LM)s)")
                     % self.config )

        SingleLowMassLine = StrippingLine( self.name + "SingleLowMassLine", algos = [ VtxJetsLowMass ],
            RequiredRawEvents = self.config['RawBanks'], prescale = self.config['SingleLowMassPrescale'] )

        self.registerLine(SingleLine)
        self.registerLine(DoubleLine)
        self.registerLine(SingleHighMassLine)
        self.registerLine(SingleLowMassLine)