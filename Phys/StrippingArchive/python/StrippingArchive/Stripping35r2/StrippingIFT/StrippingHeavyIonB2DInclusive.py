###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
B->DX selection
'''

__author__ = ['Cameron Dean']
__date__ = '06/04/2020'
__version__ = '$Revision: 1.0 $'

__all__ = ( 'HeavyIonB2DInclusive',
            'makeB2DInclusive',
            'default_config')

name = 'HeavyIonB2DInclusive'

from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection, AutomaticData
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder

# Import basic particles
from StandardParticles import StdLoosePions     as pions
from StandardParticles import StdLooseKaons     as kaons
from StandardParticles import StdLooseProtons   as protons
from StandardParticles import StdLooseMuons     as muons
from StandardParticles import StdLooseElectrons as electrons

# Import intermediate decays
from StandardParticles import StdLooseD02KK, StdLooseD02KPi, StdLooseD02KPiDCS, StdLooseD02KsKK, StdLooseD02KsPiPi, StdLooseD02PiPi
from StandardParticles import StdLooseDplus2KKK, StdLooseDplus2KKPi, StdLooseDplus2KPiPi, StdLooseDplus2KPiPiOppSignPi, StdLooseDplus2PiPiPi
from StandardParticles import StdLooseDsplus2KKPi, StdLooseDsplus2KKPiOppSign

stdParticles = [pions, kaons, protons, muons, electrons]
stdParticles += [StdLooseD02KK, StdLooseD02KPi, StdLooseD02KPiDCS, StdLooseD02KsKK, StdLooseD02KsPiPi, StdLooseD02PiPi]
stdParticles += [StdLooseDplus2KKK, StdLooseDplus2KKPi, StdLooseDplus2KPiPi, StdLooseDplus2KPiPiOppSignPi, StdLooseDplus2PiPiPi] 
stdParticles += [StdLooseDsplus2KKPi, StdLooseDsplus2KKPiOppSign]

default_config = {
    'NAME'        : 'HeavyIonB2DInclusive',
    'BUILDERTYPE' : 'HeavyIonB2DInclusive',
    'WGs'         : ['IFT'],
    'STREAMS'     : ['IFT'],
    'CONFIG'      : { 'prescale'       : 1.0,
                      'trackGhostProb' : 0.4,
                      'trackCHI2'      : 5,
                      'trackPT'        : 1000,
                      'trackIPCHI2'    : 20,
                      'DCA'            : 0.15,
                      'massLow'        : 4800,
                      'massHigh'       : 6000,
                      'bPT'            : 1000,
                      'bIPCHI2'        : 10,
                      'bFDCHI2'        : 120,
                      'bDIRA'          : 0.99,
                      'bTAU'           : 0.0006
                    },
    }

class HeavyIonB2DInclusive(LineBuilder) :

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        self.B2DInclusive = makeB2DInclusive( name,
					      config['trackGhostProb'],
			                      config['trackCHI2'], 
                                              config['trackPT'], 
                                              config['trackIPCHI2'], 
                                              config['DCA'], 
                                              config['massLow'], 
                                              config['massHigh'], 
                                              config['bPT'], 
                                              config['bIPCHI2'], 
                                              config['bFDCHI2'], 
                                              config['bDIRA'], 
                                              config['bTAU'] )

        self.line = StrippingLine(name+"Line",
                                  selection = self.B2DInclusive,
                                  prescale = config['prescale'])

        self.registerLine(self.line)

def makeB2DInclusive( name, trackGhostProb, trackCHI2, trackPT, trackIPCHI2, DCA, massLow, massHigh, bPT, bIPCHI2, bFDCHI2, bDIRA, bTAU ) :

    _single_daughters_cuts = "(TRGHOSTPROB < %(trackGhostProb)s) & (TRCHI2DOF < %(trackCHI2)s) & (PT > %(trackPT)s * MeV) & (MIPCHI2DV(PRIMARY) > %(trackIPCHI2)s )" %locals()

    _d_meson_daughters_cuts = "(PT > %(trackPT)s * MeV)" %locals()

    _combination_cuts = "(AMAXDOCA('') < %(DCA)s ) & ( AM > %(massLow)s * MeV) & ( AM < %(massHigh)s * MeV )" %locals()

    _mother_cuts = "( PT > %(bPT)s ) & ( BPVIPCHI2() < %(bIPCHI2)s ) & ( BPVVDCHI2 > %(bFDCHI2)s ) & ( BPVDIRA > %(bDIRA)s ) & (BPVLTIME() > %(bTAU)s )" %locals()

    _decays = ["[ B+ -> D~0 e+ ]cc",
               "[ B+ -> D~0 mu+ ]cc",
               "[ B+ -> D~0 pi+ ]cc",
               "[ B+ -> D~0 pi+ pi+ pi- ]cc",
               "[ B+ -> D- pi+ pi+ ]cc",
               "[ B0 -> D- e+ ]cc",
               "[ B0 -> D- mu+ ]cc",
               "[ B0 -> D- pi+ ]cc",
               "[ B0 -> D~0 pi+ pi- ]cc",
               "[ B_s0 -> D_s- mu+ ]cc",
               "[ B_s0 -> D_s- e+ ]cc",
               "[ B_s0 -> D_s- pi+ ]cc",
               "[ B_s0 -> D_s- pi+ pi+ pi- ]cc",
               "  B_s0 -> D_s- D_s+ ",
               "[ B_s0 -> D_s- D+ ]cc",
               "  B_s0 -> D- D+ ",
               "  B_s0 -> D0 D~0 ",
               "[ B_s0 -> D~0 K- pi+]cc"
              ]
    
    CombineB2DInclusive = CombineParticles( DecayDescriptors = _decays,
                                            DaughtersCuts    = {
                                                                "e+"        : _single_daughters_cuts,
                                                                "mu+"       : _single_daughters_cuts,
                                                                "pi+"       : _single_daughters_cuts,
                                                                "K+"        : _single_daughters_cuts,
                                                                "D0"        : _d_meson_daughters_cuts,
                                                                "D+"        : _d_meson_daughters_cuts,
                                                                "D_s+"      : _d_meson_daughters_cuts
                                                                },
                                            CombinationCut   = _combination_cuts,
                                            MotherCut        = _mother_cuts,
                                            ReFitPVs         = False )

    getCandidates = MergedSelection( "eventCandidates", RequiredSelections = stdParticles)

    return Selection( name,
                      Algorithm = CombineB2DInclusive,
                      RequiredSelections = [ getCandidates ] )
