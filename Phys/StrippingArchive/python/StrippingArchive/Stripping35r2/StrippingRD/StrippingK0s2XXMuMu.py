###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Stripping selections for KS0 -> X X mu mu
'''

__author__  = ['Miguel Ramos Pernas']
__date__    = '10/10/2017'
__version__ = '$Revision: 0.00 $'

__all__ = ('K0s2XXMuMuConf', 'default_config')

from Gaudi.Configuration import *

from GaudiKernel.SystemOfUnits import MeV, m, mm, picosecond
from GaudiKernel.PhysicalConstants import c_light
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from StandardParticles                     import StdAllLoosePions, StdAllLooseMuons

from PhysSelPython.Wrappers      import Selection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils        import LineBuilder
from Configurables               import FilterDesktop

from math import log


default_config = {
    'NAME'        : 'K0s2XXMuMu',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'K0s2XXMuMuConf',
    'STREAMS'     : ['Dimuon'],
    'CONFIG'      : {
        # Please use the name convention following the rules in the constructor
        # The cut on the KS0 life corresponds to: (KS0 mean life)*abs(log(N_final/N_initial)
        'MuMaxGP'                : 0.4,
        'MuMaxTrChi2pdof'        : 3,
        'PiMaxGP'                : 0.1,
        'PiMinIP'                : 0.35*mm,
        'PiMaxTrChi2pdof'        : 2,
        'MinLifeTime'            : (89.54*picosecond)*abs(log(0.95)),
        'MaxMass'                : 600*MeV,
        'MaxDOCA'                : 2*mm,
        'MinDIRA'                : 0.9999,
        'MaxIP'                  : 1*mm,
        'MaxIPchi2'              : 100,
        'MaxSVchi2pdof'          : 50,
        'MaxSVZ'                 : 650*mm,
        'MinVDZ'                 : 0*mm,
        
        'K0s2pi2mu2PreScale'     : 1,
        'K0s2pi2mu2PostScale'    : 1,
        
        'K0s2pi2mu2LFVPreScale'  : 1,
        'K0s2pi2mu2LFVPostScale' : 1,
        
        'K0s2pi2mu2SSPreScale'   : 0.25,
        'K0s2pi2mu2SSPostScale'  : 1,
        
        'K0s2mu4PreScale'        : 1,
        'K0s2mu4PostScale'       : 1,
        
        'K0s2mu4SSPreScale'      : 0.4,
        'K0s2mu4SSPostScale'     : 1
        }
    }

class K0s2XXMuMuConf(LineBuilder):
    """
    Builder for KS0 -> X X mu mu processes
    """
    
    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__( self, name, config ):
        """
        The configuration is parsed so all the cuts with "max" in it will
        apply on the way:
        - PT (< {MaxPT})
        while all those with "min" will apply like
        - PT (> {MinPT})
        The constructor automatically changes the value in the dictionary
        so the part between parentheses is saved.
        
        Example:
        code = '(PT {MinPT}) & (MIPDV(PRIMARY) {MaxIP})'
        """

        LineBuilder.__init__(self, name, config)

        self.config = {}
        for k, c in config.iteritems():

            l = k.lower()
            
            if 'min' in l:
                cut = '> {}'.format(c)
            elif 'max' in l:
                cut = '< {}'.format(c)
            else:
                cut = c

            self.config[k] = cut
            
        self.SelMuons = self._filterMuons(name = 'MuonsFor' + name, sel = StdAllLooseMuons)
        self.SelPions = self._filterPions(name = 'PionsFor' + name, sel = StdAllLoosePions)
        
        #
        # K0s2PiPiMuMu
        #
        K0s2PiPiMuMu = self._makeK0s2X4('K0s2PiPiMuMu', ['KS0 -> pi+ pi- mu+ mu-'])
        self.K0s2PiPiMuMuLine = StrippingLine('K0s2PiPiMuMuLine',
                                              prescale          = self.config['K0s2pi2mu2PreScale'],
                                              postscale         = self.config['K0s2pi2mu2PostScale'],
                                              algos             = [K0s2PiPiMuMu],
                                              RequiredRawEvents = ['Muon'],
                                              MDSTFlag          = False,
                                              RelatedInfoTools  = [ { 'Type'              : 'RelInfoMuonIDPlus',
                                                                      'Variables'         : ['MU_BDT'],
                                                                      'DaughterLocations' : { 'KS0 -> ^pi+ pi- mu+ mu-' : 'Muon1BDT',
                                                                                              'KS0 -> pi+ ^pi- mu+ mu-' : 'Muon2BDT',
                                                                                              'KS0 -> pi+ pi- ^mu+ mu-' : 'Muon3BDT',
                                                                                              'KS0 -> pi+ pi- mu+ ^mu-' : 'Muon4BDT' }
                                                                      } ] )

        K0s2PiPiMuMuLFV = self._makeK0s2X4('K0s2PiPiMuMuLFV', ['[KS0 -> pi+ pi+ mu- mu-]cc'])
        self.K0s2PiPiMuMuLFVLine = StrippingLine('K0s2PiPiMuMuLFVLine',
                                                prescale          = self.config['K0s2pi2mu2LFVPreScale'],
                                                postscale         = self.config['K0s2pi2mu2LFVPostScale'],
                                                algos             = [K0s2PiPiMuMuLFV],
                                                RequiredRawEvents = ['Muon'],
                                                MDSTFlag          = False,
                                                RelatedInfoTools  = [ { 'Type'              : 'RelInfoMuonIDPlus',
                                                                        'Variables'         : ['MU_BDT'],
                                                                        'DaughterLocations' : { '[KS0 -> ^pi+ pi+ mu- mu-]CC' : 'Muon1BDT',
                                                                                                '[KS0 -> pi+ ^pi+ mu- mu-]CC' : 'Muon2BDT',
                                                                                                '[KS0 -> pi+ pi+ ^mu- mu-]CC' : 'Muon3BDT',
                                                                                                '[KS0 -> pi+ pi+ mu- ^mu-]CC' : 'Muon4BDT' }
                                                                        } ] )

        K0s2PiPiMuMuSS = self._makeK0s2X4('K0s2PiPiMuMuSS', ['[KS0 -> pi+ pi- mu+ mu+]cc',
                                                             '[KS0 -> pi+ pi+ mu+ mu-]cc',
                                                             '[KS0 -> pi+ pi+ mu+ mu+]cc'])
        self.K0s2PiPiMuMuSSLine = StrippingLine('K0s2PiPiMuMuSSLine',
                                                prescale          = self.config['K0s2pi2mu2SSPreScale'],
                                                postscale         = self.config['K0s2pi2mu2SSPostScale'],
                                                algos             = [K0s2PiPiMuMuSS],
                                                RequiredRawEvents = ['Muon'],
                                                MDSTFlag          = False,
                                                RelatedInfoTools  = [ { 'Type'              : 'RelInfoMuonIDPlus',
                                                                        'Variables'         : ['MU_BDT'],
                                                                        'DaughterLocations' : { '[KS0 -> ^pi+ [pi-]cc mu+ [mu-]cc]CC' : 'Muon1BDT',
                                                                                                '[KS0 -> pi+ ^[pi-]cc mu+ [mu-]cc]CC' : 'Muon2BDT',
                                                                                                '[KS0 -> pi+ [pi-]cc ^mu+ [mu-]cc]CC' : 'Muon3BDT',
                                                                                                '[KS0 -> pi+ [pi-]cc mu+ ^[mu-]cc]CC' : 'Muon4BDT' }
                                                                        } ] )
        
        #
        # K0s2MuMuMuMu
        #
        K0s2MuMuMuMu = self._makeK0s2X4('K0s2MuMuMuMu', ['KS0 -> mu+ mu- mu+ mu-'])
        self.K0s2MuMuMuMuLine = StrippingLine('K0s2MuMuMuMuLine',
                                              prescale          = self.config['K0s2mu4PreScale'],
                                              postscale         = self.config['K0s2mu4PostScale'],
                                              algos             = [K0s2MuMuMuMu],
                                              RequiredRawEvents = ['Muon'],
                                              MDSTFlag          = False,
                                              RelatedInfoTools  = [ { 'Type'              : 'RelInfoMuonIDPlus',
                                                                      'Variables'         : ['MU_BDT'],
                                                                      'DaughterLocations' : { 'KS0 -> ^mu+ mu- mu+ mu-' : 'Muon1BDT',
                                                                                              'KS0 -> mu+ ^mu- mu+ mu-' : 'Muon2BDT',
                                                                                              'KS0 -> mu+ mu- ^mu+ mu-' : 'Muon3BDT',
                                                                                              'KS0 -> mu+ mu- mu+ ^mu-' : 'Muon4BDT' }
                                                                      } ] )

        K0s2MuMuMuMuSS = self._makeK0s2X4('K0s2MuMuMuMuSS', ['[KS0 -> mu+ mu- mu+ mu+]cc', '[KS0 -> mu+ mu+ mu+ mu+]cc'])
        self.K0s2MuMuMuMuSSLine = StrippingLine('K0s2MuMuMuMuSSLine',
                                                prescale          = self.config['K0s2mu4SSPreScale'],
                                                postscale         = self.config['K0s2mu4SSPostScale'],
                                                algos             = [K0s2MuMuMuMuSS],
                                                RequiredRawEvents = ['Muon'],
                                                MDSTFlag          = False,
                                                RelatedInfoTools  = [ { 'Type'              : 'RelInfoMuonIDPlus',
                                                                        'Variables'         : ['MU_BDT'],
                                                                        'DaughterLocations' : { '[KS0 -> ^mu+ [mu-]cc mu+ mu+]CC' : 'Muon1BDT',
                                                                                                '[KS0 -> mu+ ^[mu-]cc mu+ mu+]CC' : 'Muon2BDT',
                                                                                                '[KS0 -> mu+ [mu-]cc ^mu+ mu+]CC' : 'Muon3BDT',
                                                                                                '[KS0 -> mu+ [mu-]cc mu+ ^mu+]CC' : 'Muon4BDT' }
                                                                        } ] )
        
        #
        # Register lines
        #
        for line in ( self.K0s2PiPiMuMuLine,
                      self.K0s2PiPiMuMuLFVLine,
                      self.K0s2PiPiMuMuSSLine,
                      self.K0s2MuMuMuMuLine,
                      self.K0s2MuMuMuMuSSLine,
                      ):
            self.registerLine(line)
            
    def _filterMuons( self, name, sel ):
        """
        Make the muon filter
        """
        code = '(TRCHI2DOF {MuMaxTrChi2pdof}) & '\
            '(TRGHOSTPROB {MuMaxGP})'.format(**self.config)

        fltr = FilterDesktop('Filter' + name, Code = code)

        return Selection(name, Algorithm = fltr, RequiredSelections = [sel])

    def _filterPions( self, name, sel ):
        """
        Make the pion filter
        """
        code = '(MIPDV(PRIMARY) {PiMinIP}) & '\
            '(TRCHI2DOF {PiMaxTrChi2pdof}) & '\
            '(TRGHOSTPROB {PiMaxGP})'.format(**self.config)
        
        fltr = FilterDesktop('Filter' + name, Code = code)

        return Selection(name, Algorithm = fltr, RequiredSelections = [sel])
        
    def _makeK0s2X4( self, name, decays ):
        """
        Define the decay of a KS0 to four daughters
        """
        
        common_dc = "ALL".format(**self.config)
        
        cc = "(AMAXDOCA('') {MaxDOCA})".format(**self.config)
        
        mc = "(M {MaxMass}) & "\
            "(BPVVDZ {MinVDZ}) & "\
            "(BPVDIRA {MinDIRA}) & "\
            "(VFASPF(VZ) {MaxSVZ}) & "\
            "(BPVLTIME() {MinLifeTime}) & "\
            "(MIPDV(PRIMARY) {MaxIP}) & "\
            "(MIPCHI2DV(PRIMARY) {MaxIPchi2}) & "\
            "(VFASPF(VCHI2PDOF) {MaxSVchi2pdof})".format(**self.config)
        
        inputs = []
        dc     = {}
        
        if any(map(lambda s: 'mu' in s, decays)):
            inputs.append(self.SelMuons)
            dc['mu+'] = common_dc
            
        if any(map(lambda s: 'pi' in s, decays)):
            inputs.append(self.SelPions)
            dc['pi+'] = common_dc
            
        combiner = CombineParticles(DecayDescriptors = decays,
                                    DaughtersCuts    = dc,
                                    CombinationCut   = cc,
                                    MotherCut        = mc
                                    )
        return Selection(name,
                         Algorithm = combiner,
                         RequiredSelections = inputs
                         )
