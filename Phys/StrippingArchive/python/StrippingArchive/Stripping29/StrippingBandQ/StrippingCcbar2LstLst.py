###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Ccbar->LstLst, including two lines:
1. Prompt line, with tight PT, PID cuts, requiring Hlt Tis, since there is no lifetime unbiased Lst trigger yet.
2. Detached line, with loose PT, PID cuts, but with IPS cuts on Kaons. 

To include it:

from StrippingSelections.StrippingCcbar2LstLst import Ccbar2LstLstConf
from StrippingSelections.StrippingCcbar2LstLst import config_default as config_Ccbar2LstLst
confCcbar2LstLst = Ccbar2LstLstConf( name = 'Ccbar2LstLst', config = config_Ccbar2LstLst )
stream.appendLines( confCcbar2LstLst.lines() )
'''

__author__=['Andrii Usachov']
__date__ = '12/01/2017'

__all__ = (
    'Ccbar2LstLstConf'
    )

default_config = {
    'NAME'        : 'Ccbar2Lst', 
    'BUILDERTYPE' : 'Ccbar2LstLstConf',
    'CONFIG' : {
        'TRCHI2DOF'        :    3.  ,
        'CcbarPT'          :    3000,   #MeV
        'KaonProbNNk'      :    0.1  ,
        'KaonPT'           :    600. , # MeV
        'KaonPTSec'        :    350. , # MeV
        'ProtonProbNNp'    :    0.1  ,
        'ProtonPT'         :    800.  , # MeV
        'ProtonPTSec'      :    500.  , # MeV
        'LstVtxChi2'       :    16.  ,
        'LstMinMass'       :  1440  ,
        'LstMaxMass'       :  1600  ,
        'CombMaxMass'      :  6100.  , # MeV, before Vtx fit
        'CombMinMass'      :  3000.  , # MeV, before Vtx fit
        'MaxMass'          :  6000.  , # MeV, after Vtx fit
        'MinMass'          :  2950.  , # MeV, after Vtx fit
        'Lst_TisTosSpecs'  : { "Hlt1Global%TIS" : 0 }
        },
    'STREAMS' : [ 'Charm' ] ,
    'WGs'     : [ 'BandQ' ]
    }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


from StandardParticles import StdNoPIDsProtons, StdNoPIDsKaons


class Ccbar2LstLstConf(LineBuilder):
    
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config




        TRCHI2DOF       =   config['TRCHI2DOF']
        CcbarPT         =   config['CcbarPT']
        KaonProbNNk     =   config['KaonProbNNk']
        KaonPT          =   config['KaonPT']
        KaonPTSec       =   config['KaonPTSec']
        ProtonProbNNp   =   config['ProtonProbNNp']
        ProtonPT        =   config['ProtonPT']
        ProtonPTSec     =   config['ProtonPTSec']
        LstVtxChi2      =   config['LstVtxChi2']
        LstMinMass      =   config['LstMinMass']
        LstMaxMass      =   config['LstMaxMass']
        CombMaxMass     =   config['CombMaxMass']
        CombMinMass     =   config['CombMinMass']
        MaxMass         =   config['MaxMass']
        MinMass         =   config['MinMass']
        Lst_TisTosSpecs =   config['Lst_TisTosSpecs']






        """
        Unbiased, require Hlt Tis
        """


        self.ProtonForLst = self.createSubSel( OutputList = "ProtonForLst" + self.name,
                                               InputList =  DataOnDemand( Location = 'Phys/StdNoPIDsProtons/Particles' ),
                                               Cuts = "(PT> %(ProtonPT)s*MeV) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s)" % self.config
                                              )
        self.KaonForLst = self.createSubSel( OutputList = "KaonForLst" + self.name,
                                             InputList =  DataOnDemand( Location = 'Phys/StdNoPIDsKaons/Particles' ),
                                             Cuts = "(PT> %(KaonPT)s*MeV) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNk> %(KaonProbNNk)s)" % self.config
                                            )
            
        self.LstForJpsiList = self.createCombinationSel( OutputList = "LstFor" + self.name,
                                                DaughterLists = [ self.ProtonForLst, self.KaonForLst],
                                                DecayDescriptor = "[Lambda(1520)0 -> p+ K-]cc",
                                                PreVertexCuts = "(in_range( %(LstMinMass)s *MeV, AM, %(LstMaxMass)s *MeV))" % self.config,
                                                PostVertexCuts = "(in_range( %(LstMinMass)s *MeV, MM, %(LstMaxMass)s *MeV)) & (VFASPF(VCHI2PDOF)<%(LstVtxChi2)s)" % self.config,
                                                reFitPVs = False
                                             )



        self.TisLstForJpsiList = self.filterTisTos( "TisLstFor" + self.name ,
                                                    LstInput = self.LstForJpsiList,
                                                    myTisTosSpecs = config['Lst_TisTosSpecs']
                                                  )




        self.DetachedProtonForLst = self.createSubSel( OutputList = "DetachedProtonForLst" + self.name,
                                              InputList =  DataOnDemand( Location = 'Phys/StdNoPIDsProtons/Particles' ),
                                              Cuts = "(PT> %(ProtonPTSec)s*MeV) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNp> %(ProtonProbNNp)s)" % self.config
                                              )
        self.DetachedKaonForLst = self.createSubSel( OutputList = "DetachedKaonForLst" + self.name,
                                              InputList =  DataOnDemand( Location = 'Phys/StdNoPIDsKaons/Particles' ),
                                              Cuts = "(PT> %(KaonPTSec)s*MeV) & (TRCHI2DOF < %(TRCHI2DOF)s) & (PROBNNk> %(KaonProbNNk)s)" % self.config
                                              )
        
        self.DetachedLstForJpsiList = self.createCombinationSel( OutputList = "DetachedLstFor" + self.name,
                                                        DaughterLists = [ self.ProtonForLst, self.KaonForLst],
                                                        DecayDescriptor = "[Lambda(1520)0 -> p+ K-]cc",
                                                        PreVertexCuts = "(in_range( %(LstMinMass)s *MeV, AM, %(LstMaxMass)s *MeV))" % self.config,
                                                        PostVertexCuts = "(in_range( %(LstMinMass)s *MeV, MM, %(LstMaxMass)s *MeV)) & (VFASPF(VCHI2PDOF)<%(LstVtxChi2)s)" % self.config,
                                                        reFitPVs = False
                                                        )
        self.makeJpsi2LstLst()
        self.makeDetachedJpsi2LstLst()

        
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )
    
    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "AALL",
                              PostVertexCuts = "ALL",
                              reFitPVs = True) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = reFitPVs)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)
    
    
    
    def makeJpsi2LstLst(self):
        Jpsi2LstLst = self.createCombinationSel( OutputList = "Jpsi2LstLst" + self.name,
                                                 DecayDescriptor = " J/psi(1S) -> Lambda(1520)0 Lambda(1520)~0",
                                                 DaughterLists = [ self.TisLstForJpsiList ],
                                                 PreVertexCuts = "(in_range( %(CombMinMass)s *MeV, AM, %(CombMaxMass)s *MeV))" % self.config,
                                                 PostVertexCuts = "(in_range( %(MinMass)s *MeV, MM, %(MaxMass)s *MeV)) & (VFASPF(VCHI2PDOF) < 16) & (PT>%(CcbarPT)s *MeV)" %self.config )
        
        Jpsi2LstLstLine = StrippingLine( self.name + "LstLine",
                                         algos = [ Jpsi2LstLst ] )
        
        self.registerLine(Jpsi2LstLstLine)
        
        
    def makeDetachedJpsi2LstLst(self):
        DetachedJpsi2LstLst = self.createCombinationSel( OutputList = "DetachedJpsi2LstLst" + self.name,
                                                         DecayDescriptor = " J/psi(1S) -> Lambda(1520)0 Lambda(1520)~0",
                                                         DaughterLists = [ self.DetachedLstForJpsiList ],
                                                         PreVertexCuts = "(in_range( %(CombMinMass)s *MeV, AM, %(CombMaxMass)s *MeV))" % self.config,
                                                         PostVertexCuts = "(in_range( %(MinMass)s *MeV, MM, %(MaxMass)s *MeV)) & (VFASPF(VCHI2PDOF) < 16 ) & (BPVDLS>10)" %self.config )
                                                        
        DetachedJpsi2LstLstLine = StrippingLine( self.name + "LstDetachedLine",
                                                 algos = [ DetachedJpsi2LstLst ] )
        self.registerLine(DetachedJpsi2LstLstLine)




    def filterTisTos(self, name,
                     LstInput,
                     myTisTosSpecs ) :
        from Configurables import TisTosParticleTagger
        
        myTagger = TisTosParticleTagger(name + "_TisTosTagger")
        myTagger.TisTosSpecs = myTisTosSpecs
        
#         To speed it up, TisTos only with tracking system)
        myTagger.ProjectTracksToCalo = False
        myTagger.CaloClustForCharged = False
        myTagger.CaloClustForNeutral = False
        myTagger.TOSFrac = { 4:0.0, 5:0.0 }

        return Selection(name + "_SelTisTos",
                         Algorithm = myTagger,
                         RequiredSelections = [ LstInput ] )

