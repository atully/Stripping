###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Hc -> p pbar mu mu
'''

__author__=['Liupan An']
__date__ = '18/05/2017'
__version__= '$Revision: 1.0 $'


__all__ = (
    'PPMuMuConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'PPMuMu',
    'BUILDERTYPE'       :  'PPMuMuConf',
    'CONFIG'    : {
        'HcProtonCuts'    : "(PROBNNp  > 0.1) & (PT > 1000*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)",
        'HcMuonCuts'      : "(PROBNNmu > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)",
        'HcComAMCuts'     : "AALL",
        'HcComN4Cuts'     : "(AM > 2.8 *GeV) & (AM < 4.2 *GeV)",
        'HcMomN4Cuts'     : "(VFASPF(VCHI2/VDOF) < 9.) & (MM>3.0 *GeV) & (MM<4.0 *GeV)",

        'HighProtonCuts'  : "(PROBNNp  > 0.2) & (PT > 1600*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 8.)",
        'HighMuonCuts'    : "(PROBNNmu > 0.2) & (PT > 800*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 8.)",
        'HighComAMCuts'   : "AALL",
        'HighComN4Cuts'   : "(AM > 3.8 *GeV)",
        'HighMomN4Cuts'   : "(VFASPF(VCHI2/VDOF) < 9.) & (MM>4.0 *GeV) & (MM<20.0 *GeV)",

        'DetachedProtonCuts'    : "(PROBNNp  > 0.1) & (PT > 1000*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 5.)",
        'DetachedMuonCuts'      : "(PROBNNmu > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 5.)",
        'DetachedComAMCuts'     : "AALL",
        'DetachedComN4Cuts'     : "(AM > 2.8 *GeV)",
        'DetachedMomN4Cuts'     : "(VFASPF(VCHI2/VDOF) < 9.) & (MM>3.0 *GeV) & (MM<20.0 *GeV)",

        'Prescale'      : 1.
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays

class PPMuMuConf(LineBuilder):
    
    __configuration_keys__ = (
        'HcProtonCuts',
        'HcMuonCuts',
        'HcComAMCuts',
        'HcComN4Cuts',
        'HcMomN4Cuts',

        'HighProtonCuts',
        'HighMuonCuts',
        'HighComAMCuts',
        'HighComN4Cuts',
        'HighMomN4Cuts',

        'DetachedProtonCuts',
        'DetachedMuonCuts',
        'DetachedComAMCuts',
        'DetachedComN4Cuts',
        'DetachedMomN4Cuts',
        'Prescale'
        )

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config

        """
        Prompt Hc -> p pbar Mu Mu 
        """

        self.SelHcProtons = self.createSubSel( OutputList = self.name + "SelHcProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseANNProtons/Particles' ), 
                                           Cuts = config['HcProtonCuts']
                                           )
        self.SelHcMuons = self.createSubSel( OutputList = self.name + "HcSelMuons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseMuons/Particles' ), 
                                           Cuts = config['HcMuonCuts']
                                           )
        self.SelHc2PPMuMu = self.createN4BodySel( OutputList = self.name + "SelHc2PPMuMu",
                                                     DaughterLists = [ self.SelHcProtons, self.SelHcMuons ],
                                                     DecayDescriptor = "h_c(1P) -> p+ p~- mu+ mu-",
                                                     ComAMCuts      = config['HcComAMCuts'],
                                                     PreVertexCuts  = config['HcComN4Cuts'], 
                                                     PostVertexCuts = config['HcMomN4Cuts']
                                                     )
        self.Hc2PPMuMuLine = StrippingLine( self.name + 'HcLine',                                                
                                               prescale  = config['Prescale'],                                               
                                               algos     = [ self.SelHc2PPMuMu ],
                                               MDSTFlag  = False
                                               )
        self.registerLine( self.Hc2PPMuMuLine )

        
        """
        Prompt B -> p pbar Mu Mu 
        """

        self.SelHighProtons = self.createSubSel( OutputList = self.name + "SelHighProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseANNProtons/Particles' ), 
                                           Cuts = config['HighProtonCuts']
                                           )
        self.SelHighMuons = self.createSubSel( OutputList = self.name + "HighSelMuons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseMuons/Particles' ), 
                                           Cuts = config['HighMuonCuts']
                                           )
        self.SelHigh2PPMuMu = self.createN4BodySel( OutputList = self.name + "SelHigh2PPMuMu",
                                                     DaughterLists = [ self.SelHighProtons, self.SelHighMuons ],
                                                     DecayDescriptor = "h_c(1P) -> p+ p~- mu+ mu-",
                                                     ComAMCuts      = config['HighComAMCuts'],
                                                     PreVertexCuts  = config['HighComN4Cuts'], 
                                                     PostVertexCuts = config['HighMomN4Cuts']
                                                     )
        self.High2PPMuMuLine = StrippingLine( self.name + 'HighLine',                                                
                                               prescale  = config['Prescale'],                                               
                                               algos     = [ self.SelHigh2PPMuMu ],
                                               MDSTFlag  = False
                                               )
        self.registerLine( self.High2PPMuMuLine )

        """
        Detached X -> p pbar Mu Mu 
        """

        self.SelDetachedProtons = self.createSubSel( OutputList = self.name + "SelDetachedProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseANNProtons/Particles' ), 
                                           Cuts = config['DetachedProtonCuts']
                                           )
        self.SelDetachedMuons = self.createSubSel( OutputList = self.name + "DetachedSelMuons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLooseMuons/Particles' ), 
                                           Cuts = config['DetachedMuonCuts']
                                           )
        self.SelDetached2PPMuMu = self.createN4BodySel( OutputList = self.name + "SelDetached2PPMuMu",
                                                     DaughterLists = [ self.SelDetachedProtons, self.SelDetachedMuons ],
                                                     DecayDescriptor = "h_c(1P) -> p+ p~- mu+ mu-",
                                                     ComAMCuts      = config['DetachedComAMCuts'],
                                                     PreVertexCuts  = config['DetachedComN4Cuts'], 
                                                     PostVertexCuts = config['DetachedMomN4Cuts']
                                                     )
        self.Detached2PPMuMuLine = StrippingLine( self.name + 'DetachedLine',                                                
                                               prescale  = config['Prescale'],                                               
                                               algos     = [ self.SelDetached2PPMuMu ],
                                               MDSTFlag  = False
                                               )
        self.registerLine( self.Detached2PPMuMuLine )


        
    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )
    
    def createN4BodySel( self, OutputList,
                         DecayDescriptor,
                         DaughterLists,
                         DaughterCuts = {} ,
                         ComAMCuts      = "AALL",
                         PreVertexCuts  = "AALL",
                         PostVertexCuts = "ALL" ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = DaVinci__N4BodyDecays ( DecayDescriptor = DecayDescriptor,      
                                           DaughtersCuts = DaughterCuts,
                                           Combination12Cut  = ComAMCuts + "&" + "( ACHI2DOCA(1,2)<20 )",
                                           Combination123Cut = ComAMCuts + "&" + "( ACHI2DOCA(1,3)<20 ) & ( ACHI2DOCA(2,3)<20 )",
                                           CombinationCut = "( ACHI2DOCA(1,4)<20 ) & ( ACHI2DOCA(2,4)<20 ) & ( ACHI2DOCA(3,4)<20 )" + " & " + PreVertexCuts,
                                           MotherCut = PostVertexCuts,
                                           ReFitPVs = False )
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)

