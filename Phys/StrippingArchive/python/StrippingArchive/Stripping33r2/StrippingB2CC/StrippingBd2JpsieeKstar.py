###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Artur Ukleja, Jibo He, Konrad Klimaszewski, Varvara Batozskaya'
__date__ = '2019/01/22'

'''
Bd->JpsieeKstar stripping selection

Exports the following stripping lines
- BetaSBd2JpsieeKstarLine           - Line _without_ Bd lifetime cut, prescaled by 0.05
- BetaSBd2JpsieeKstarDetachedLine   - Line _with_ Bd lifetime cut based on StdLooseDielectron
'''

__all__ = (
    'Bd2JpsieeKstarConf',
    'default_config'
    )

default_config = {
    'NAME'              : 'BetaSBd2JpsieeKstar',
    'BUILDERTYPE'       : 'Bd2JpsieeKstarConf',
    'CONFIG'    : {
        # BetaSBd2JpsieeKstarDetachedLine (Very tight cuts)
          'ElectronPTLoose'            :   500.    # MeV
        , 'ElectronPIDLoose'           :     1.    # adimensional
        , 'ElectronTrackCHI2pDOFLoose' :     3.    # adimensional
        , 'JpsiVertexCHI2pDOFLoose'    :    11.    # adimensional
        , 'JpsiPTLoose'                :   400.    # MeV
        , 'JpsiMassMinLoose'           :  2200.    # MeV
        , 'JpsiMassMaxLoose'           :  3500.    # MeV
        , 'KaonTrackCHI2pDOFLoose'     :     3.    # adimensional
        , 'KaonPIDLoose'               :     0.    # adimensional
        , 'PionTrackCHI2pDOFLoose'     :     3.    # adimensional
        , 'PionPIDLoose'               :     5.    # adimensional
        , 'KstPTLoose'                 :  1000.    # MeV
        , 'KstVertexCHI2pDOFLoose'     :    10.    # adimensional
        , 'KstMassWindowLoose'         :   100.    # MeV
        , 'BdVertexCHI2pDOFLoose'      :    10.    # adimensional
        , 'BdMassMinLoose'             :  4300.    # MeV
        , 'BdMassMaxLoose'             :  6000.    # MeV
        , 'LifetimeCut'                : " & (BPVLTIME()>0.3*ps)"
        , 'PrescaleLoose'              :     1.    # adamenssional
        # BetaSBd2JpsieeKstarLine
        , 'ElectronPT'            :   500.    # MeV
        , 'ElectronPID'           :     0.    # adimensional
        , 'ElectronTrackCHI2pDOF' :     5.    # adimensional
        , 'JpsiVertexCHI2pDOF'    :    15.    # adimensional
        , 'JpsiMassMin'           :  1900.    # MeV
        , 'JpsiMassMax'           :  3600.    # MeV
        , 'JpsiPT'                :   400.    # MeV
        , 'KaonTrackCHI2pDOF'     :     5.    # adimensional
        , 'KaonPID'               :    -3.    # adimensional
        , 'PionTrackCHI2pDOF'     :     5.    # adimensional
        , 'PionPID'               :    10.    # adimensional
        , 'KstPT'                 :  1000.    # MeV
        , 'KstVertexCHI2pDOF'     :    15.    # adimensional
        , 'KstMassWindow'         :   150.    # MeV
        , 'BdVertexCHI2pDOF'      :    10.    # adimensional
        , 'BdMassMin'             :  4000.    # MeV
        , 'BdMassMax'             :  6000.    # MeV
        , 'BdDIRA'                :     0.99  # adimensional
        , 'Prescale'              :     0.05   # adamenssional
        },
    'STREAMS' : [ 'Leptonic' ],
    'WGs'    : [ 'B2CC' ]
    }

from Gaudi.Configuration import *
from Configurables import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder


class Bd2JpsieeKstarConf(LineBuilder):

    __configuration_keys__ = default_config['CONFIG'].keys()

    def __init__(self, name, config) :
        LineBuilder.__init__(self, name, config)

        # if name not set outside, set it to empty 
        if name == None:
            name = ""        
        self.name = name
        DiElectrons           = DataOnDemand(Location = "Phys/StdLooseDiElectron/Particles")
        self.Bd2JpsieeKstarLine      = self._Bd2JpsieeKstarLine( DiElectrons, name, config )
        self.Bd2JpsieeKstarDetachedLine = self._Bd2JpsieeKstarDetachedLine( DiElectrons, name+"Detached", config )

        # Tests CPU time required for construction of StdLooseDiElectron
        # self.DielectronTestLine      = self._DielectronTestLine( DiElectrons, "DielectronTest", config )
        # self.registerLine( self.DielectronTestLine )

        self.registerLine( self.Bd2JpsieeKstarDetachedLine )
        self.registerLine( self.Bd2JpsieeKstarLine )


    def _Bd2JpsieeKstarLine( self, dielectron, name, config ) :

        _jpsi = FilterDesktop("FilterJpsi2eeFor"+name,
                              Code = "   (MM > %(JpsiMassMin)s *MeV)" \
                                     " & (MM < %(JpsiMassMax)s *MeV)" \
                                     " & (PT > %(JpsiPT)s *MeV)" \
                                     " & (MINTREE('e+'==ABSID,PIDe-PIDpi) > %(ElectronPID)s )" \
                                     " & (MINTREE('e+'==ABSID,PT) > %(ElectronPT)s *MeV)" \
                                     " & (MAXTREE('e+'==ABSID,TRCHI2DOF) < %(ElectronTrackCHI2pDOF)s)" \
                                     " & (VFASPF(VCHI2/VDOF) < %(JpsiVertexCHI2pDOF)s)" % config
                             )
        Jpsi = Selection("SelJpsi2eeFor"+name,
                         Algorithm = _jpsi,
                         RequiredSelections = [dielectron])

        _stdKst = DataOnDemand(Location="Phys/StdLooseKstar2Kpi/Particles")
        _kst = FilterDesktop("FilterKst2KpiFor"+name,
                             Code = \
                                    "   (PFUNA(ADAMASS('K*(892)0')) < %(KstMassWindow)s * MeV)" \
                                    " & (PT > %(KstPT)s *MeV)" \
                                    " & (MINTREE('K+'==ABSID,PIDK-PIDpi) > %(KaonPID)s )" \
                                    " & (MAXTREE('K+'==ABSID,TRCHI2DOF) < %(KaonTrackCHI2pDOF)s)" \
                                    " & (MINTREE('pi+'==ABSID,PIDK-PIDpi) < %(PionPID)s )" \
                                    " & (MAXTREE('pi+'==ABSID,TRCHI2DOF) < %(PionTrackCHI2pDOF)s)" \
                                    " & (VFASPF(VCHI2/VDOF) < %(KstVertexCHI2pDOF)s)" % config
                            )
        Kst = Selection("SelKst2KpiFor"+name,
                        Algorithm = _kst,
                        RequiredSelections = [_stdKst])

        CC = "(AM > %(BdMassMin)s *MeV) & (AM < %(BdMassMax)s *MeV)" % config
        MC = "(VFASPF(VCHI2/VDOF) < %(BdVertexCHI2pDOF)s) & (BPVDIRA > %(BdDIRA)s)" % config
        _Bd = CombineParticles("CombineBdFor"+name,
                               DecayDescriptor = "B0 -> J/psi(1S) K*(892)0",
                               CombinationCut = CC ,
                               MotherCut = MC,
                               ReFitPVs = False
                               )
        # Select Kstar first (we throw away less at the first state but
        # StdLooseKstar2Kpi is a bit quicker than StdLooseDiElectron and co)
        Bd = Selection(name,
                       Algorithm = _Bd,
                       #RequiredSelections = [Jpsi, Kst])
                       RequiredSelections = [Kst, Jpsi])

        return StrippingLine(name+"Line"
              , prescale = config['Prescale']
              , postscale = 1
              , selection = Bd
              , EnableFlavourTagging = True )#, MDSTFlag = True )


    def _Bd2JpsieeKstarDetachedLine( self, dielectron, name, config ) :
        
        _jpsi = FilterDesktop("FilterJpsi2eeFor"+name,
                              Code = "   (MM > %(JpsiMassMinLoose)s *MeV)" \
                                     " & (MM < %(JpsiMassMaxLoose)s *MeV)" \
                                     " & (PT > %(JpsiPTLoose)s *MeV)" \
                                     " & (MINTREE('e+'==ABSID,PIDe-PIDpi) > %(ElectronPIDLoose)s )" \
                                     " & (MINTREE('e+'==ABSID,PT) > %(ElectronPTLoose)s *MeV)" \
                                     " & (MAXTREE('e+'==ABSID,TRCHI2DOF) < %(ElectronTrackCHI2pDOFLoose)s)" \
                                     " & (VFASPF(VCHI2/VDOF) < %(JpsiVertexCHI2pDOFLoose)s)" % config
                             )
        Jpsi = Selection("SelJpsi2eeFor"+name,
                         Algorithm = _jpsi,
                         RequiredSelections = [dielectron])
    
        _stdKst = DataOnDemand(Location="Phys/StdLooseKstar2Kpi/Particles")
        _kst = FilterDesktop("FilterKst2KpiFor"+name,
                             Code = \
                                    "   (PFUNA(ADAMASS('K*(892)0')) < %(KstMassWindowLoose)s * MeV)" \
                                    " & (PT > %(KstPTLoose)s *MeV)" \
                                    " & (MINTREE('K+'==ABSID,PIDK-PIDpi) > %(KaonPIDLoose)s )" \
                                    " & (MAXTREE('K+'==ABSID,TRCHI2DOF) < %(KaonTrackCHI2pDOFLoose)s)" \
                                    " & (MINTREE('pi+'==ABSID,PIDK-PIDpi) < %(PionPIDLoose)s )" \
                                    " & (MAXTREE('pi+'==ABSID,TRCHI2DOF) < %(PionTrackCHI2pDOFLoose)s)" \
                                    " & (VFASPF(VCHI2/VDOF) < %(KstVertexCHI2pDOFLoose)s)" % config
                            )
        Kst = Selection("SelKst2KpiFor"+name,
                        Algorithm = _kst,
                        RequiredSelections = [_stdKst])

        CC = "(AM > %(BdMassMinLoose)s *MeV) & (AM < %(BdMassMaxLoose)s *MeV)" % config
        MC = "(VFASPF(VCHI2/VDOF) < %(BdVertexCHI2pDOFLoose)s)" % config
        _Bd = CombineParticles("CombineBdFor"+name,
                               DecayDescriptor = "B0 -> J/psi(1S) K*(892)0",
                               CombinationCut = CC , 
                               MotherCut = MC + config['LifetimeCut'],
                               ReFitPVs = True
                               )
        # Select Kstar first (we throw away less at the first state but
        # StdLooseKstar2Kpi is a bit quicker than StdLooseDiElectron and co)
        Bd = Selection(name,
                       Algorithm = _Bd,
                       #RequiredSelections = [Jpsi, Kst])
                       RequiredSelections = [Kst, Jpsi])


        return StrippingLine(name+"Line"
              , prescale = config['PrescaleLoose']
              , postscale = 1
              , selection = Bd
              , EnableFlavourTagging = True )#, MDSTFlag = True )

    def _DielectronTestLine( self, dielectron, name, config ) :
        return StrippingLine(name+"Line"
              , prescale = 1
              , postscale = 1
              , selection = dielectron
              )
