###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module for construction of Radiative Decays Stripping Selections and StrippingLines.
Provides functions to build Bd -> [K*,Kspipi]Gamma, Bs->[Phi,KsKpi]Gamma selections.
Provides class StrippingB2XGammaConf, which constructs the Selections and
StrippingLines given a configuration dictionary.
Exported selection makers: 'makePhoton', 'makeKs', 'makePhi2KK', 'makeKstar', 'makeKspipi', 'makeKsKpi', 'makeBs2PhiGamma', 'makeBd2KstGamma', 'makeBd2KspipiGamma', 'makeBs2KsKpiGamma'
"""


__author__ = ['Biplab Dey']
__date__ = '23/01/2019'
__version__ = '$Revision: 1.0 $'

__all__ = ('StrippingB2XGammaExclTDCPVConf', 'makePhoton', 'makeKs', 'makePhi2KK', 'makeKstar', 'makeKspipi', 'makeKsKpi', 'makeBs2PhiGamma', 'makeBd2KstGamma', 'makeBd2KspipiGamma', 'makeBs2KsKpiGamma', 'default_config')

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import Selection, DataOnDemand, MergedSelection
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdLooseAllPhotons
from Configurables import TisTosParticleTagger

name = 'Beauty2XGammaExclTDCPV'
default_config = {
    'NAME'        : 'Beauty2XGammaExclTDCPV',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'Beauty2XGammaExclTDCPVConf',
    'CONFIG'      : {
           'TrIPchi2'            : 9.      # Dimensionless (was 16)
          ,'TrChi2'              : 4.       # dimensionless (was 3)
          ,'PhiMassWin'          : 15.      # MeV
          ,'KstMassWin'          : 100.     # MeV  was(150)
          ,'mKspipiMax'          : 2000.    # MeV 
          ,'mKsKpiMax'           : 2300.    # MeV 
          ,'PhiVCHI2'            : 25.       # dimensionless
          ,'KstVCHI2'            : 25.       # dimensionless
          ,'BdDIRA'		 : 0.2      # rad (was 0.06)
          ,'ProbNNpi'		 : 0.05     # very nominal ProbNNpi cut for the pions
          ,'ProbNNk'		 : 0.05     # very nominal ProbNNk cut for the kaons
          ,'KsPT'                : 300.     # MeV very loose PT cut for the KS
          ,'photonPT'            : 2500.    # MeV
          ,'B_PT'                : 2500.    # MeV (was 3000)  
          ,'SumVec_PT'		 : 500.     # MeV (was 1500) hadron PT cut
          ,'MinTrack_PT'         : 250.     # MeV (was 500)
          ,'MinTrack_P'          : 3000.    # MeV 
          ,'B_APT'		 : 3000.    # MeV (was 5000)
          ,'BMassMin'            : 4000.    # MeV
          ,'BMassMax'            : 7000.    # MeV
          ,'BsPVIPchi2'          : 16.      # Dimensionless (was 9)
          ,'B0PVIPchi2'          : 16.      # Dimensionless (was 9)
          ,'BsVTXchi2'           : 16.      # Dimensionless (was 9)
          ,'B0VTXchi2'           : 16.      # Dimensionless (was 9)
          ,'GhostProb_Max'	 : 0.6	    # Dimensionless (was 0.4)
          ,'DTF_CL'	         : 1E-10    # V. loose DTF prob cut => accept convergent DTF fits only
          ,'Hlt1TISTOSLinesDict': {'Hlt1(Two)?TrackMVA(Loose)?Decision%TOS':0,
                                  }
          ,'Hlt2TISTOSLinesDict': {'Hlt2Topo(2|3|4)Body.*Decision%TOS':0,
                                   'Hlt2Radiative.*Decision%TOS':0
                                  }
          ,'Hlt1TISTOSLinesDict_Phi': {'Hlt1(Two)?TrackMVA(Loose)?Decision%TOS':0,
                                       'Hlt1B2PhiGamma_LTUNBDecision%TOS':0,
                                       'Hlt1(Phi)?IncPhi.*Decision%TOS' : 0
                                      }
          ,'Hlt2TISTOSLinesDict_Phi': {'Hlt2Topo(2|3|4)Body.*Decision%TOS':0,
                                       'Hlt2Radiative.*Decision%TOS':0,
                                       'Hlt2(Phi)?IncPhi.*Decision%TOS' : 0
                                      }

 
          ,'Bs2PhiGammaPreScale'               : 1.0
          ,'Bs2PhiGammaPostScale'              : 1.0
          ,'Bd2KstGammaPreScale'               : 1.0
          ,'Bd2KstGammaPostScale'              : 1.0
          ,'Bd2KspipiGammaPreScale'            : 1.0
          ,'Bd2KspipiGammaPostScale'           : 1.0
          ,'Bs2KsKpiGammaPreScale'             : 1.0
          ,'Bs2KsKpiGammaPostScale'            : 1.0
          },
    'STREAMS' : ['BhadronCompleteEvent']
    }

class Beauty2XGammaExclTDCPVConf(LineBuilder):
    """
    Definition of B -> X Gamma stripping
    
    Constructs B0 -> [K*,Kspipi] Gamma and Bs -> [Phi,KsKpi] Gamma Selections and StrippingLines from
    a configuration dictionary.
    Usage:
    >>> config = { .... }
    >>> gammaConf = StrippingBeauty2XGammaExclTDCPVConf('StrippingBeauty2XGammaExclTDCPVTest',config)
    >>> gammaLines = gammaConf.lines
    >>> for line in line :
    >>>  print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selPhoton                    : Photon Selection object
    selPhi2KK                    : nominal Phi -> K+K- Selection object
    selKst                       : nominal K* -> K pi Selection object
    selKspipi                    : nominal K* -> Ks pipi Selection object
    selKsKpi                     : nominal X -> Ks Kpi Selection object
    selBs2PhiGamma               : nominal Bs -> Phi Gamma Selection object with wide Bs mass window
    selBd2KstGamma               : nominal B0 -> K* Gamma object Object 
    Bs2PhiGammaLine              : Stripping line out of selBs2PhiGamma
    Bd2KstGamma                  : Stripping line out of selBd2KstGamma
    lines                  : List of lines

    Exports as class data member:
    StrippingBeauty2XGammaExclTDCPVConf.__configuration_keys__ : List of required configuration parameters.    
    """

    __configuration_keys__ = default_config['CONFIG'].keys()
    
    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        # Selection of B daughters: photon, phi and kstar
        self.name = name
        self.selPhoton = makePhoton('PhotonSel' + self.name,
                                    config['photonPT'])

        self.selKs = makeKs('KsSel' + self.name,
                                    config['KsPT'],
                                    config['ProbNNpi'],
                                    config['GhostProb_Max'])

        self.selPhi2KK = makePhi2KK('PhiSel' + self.name,
                                    config['TrIPchi2'],
                                    config['TrChi2'],
                                    config['PhiMassWin'],
                                    config['PhiVCHI2'],
                                    config['GhostProb_Max'],
                             	    config['SumVec_PT'],
                                    config['MinTrack_PT'],
                                    config['MinTrack_P'],
                                    config['ProbNNk'])
                                    
        self.selKst = makeKstar('KStarSel' + self.name,
                                config['TrIPchi2'],
                                config['TrChi2'],
                                config['KstMassWin'],
                                config['KstVCHI2'],
                                config['GhostProb_Max'],
                                config['SumVec_PT'],
                                config['MinTrack_PT'],
                                config['MinTrack_P'],
                                config['ProbNNpi'],
                                config['ProbNNk'])


        self.selKspipi = makeKspipi('KspipiSel' + self.name,
                                self.selKs,
                                config['TrIPchi2'],
                                config['TrChi2'],
                                config['mKspipiMax'],
                                config['KstVCHI2'],
                                config['GhostProb_Max'],
                                config['SumVec_PT'],
                                config['MinTrack_PT'],
                                config['MinTrack_P'],
                                config['ProbNNpi'])

        self.selKsKpi = makeKsKpi('KsKpiSel' + self.name,
                                self.selKs,
                                config['TrIPchi2'],
                                config['TrChi2'],
                                config['mKsKpiMax'],
                                config['KstVCHI2'],
                                config['GhostProb_Max'],
                                config['SumVec_PT'],
                                config['MinTrack_PT'],
                                config['MinTrack_P'],
                                config['ProbNNpi'],
                                config['ProbNNk'])


        # Bs->Phi Gamma selections
        self.selBs2PhiGamma = makeBs2PhiGamma(self.name + 'Bs2PhiGamma',
                                              self.selPhi2KK,
                                              self.selPhoton,
                                              config['BsVTXchi2'],
                                              config['BsPVIPchi2'],
                                              config['BMassMin'],
                                              config['BMassMax'],
                                              config['B_PT'],
                                              config['B_APT'],
                                              config['PhiMassWin'],
                                              config['DTF_CL'],
                                              config['Hlt1TISTOSLinesDict_Phi'],
                                              config['Hlt2TISTOSLinesDict_Phi'],
                                              )

        # Bd->Kst Gamma selections
        self.selBd2KstGamma = makeBd2KstGamma(self.name + 'Bd2KstGamma',
                                              self.selKst,
                                              self.selPhoton,
                                              config['B0VTXchi2'],
                                              config['B0PVIPchi2'],
                                              config['BMassMin'],
                                              config['BMassMax'],
                                              config['B_PT'],
                                              config['B_APT'],
                                              config['KstMassWin'],
                                              config['BdDIRA'],
                                              config['DTF_CL'],
                                              config['Hlt1TISTOSLinesDict'],
                                              config['Hlt2TISTOSLinesDict'],
                                              )

        # Bd->Kspipig Gamma selections (NEW!)
        self.selBd2KspipiGamma = makeBd2KspipiGamma(self.name + 'Bd2KspipiGamma',
                                              self.selKspipi,
                                              self.selPhoton,
                                              config['B0VTXchi2'],
                                              config['B0PVIPchi2'],
                                              config['BMassMin'],
                                              config['BMassMax'],
                                              config['B_PT'],
                                              config['B_APT'],
                                              config['BdDIRA'],
                                              config['mKspipiMax'],
                                              config['DTF_CL'],
                                              config['Hlt1TISTOSLinesDict'],
                                              config['Hlt2TISTOSLinesDict']
                                              )

        # Bs->KsKpig Gamma selections (NEW!)
        self.selBs2KsKpiGamma = makeBs2KsKpiGamma(self.name + 'Bs2KsKpiGamma',
                                              self.selKsKpi,
                                              self.selPhoton,
                                              config['B0VTXchi2'],
                                              config['B0PVIPchi2'],
                                              config['BMassMin'],
                                              config['BMassMax'],
                                              config['B_PT'],
                                              config['B_APT'],
                                              config['BdDIRA'],
                                              config['mKsKpiMax'],
                                              config['DTF_CL'],
                                              config['Hlt1TISTOSLinesDict'],
                                              config['Hlt2TISTOSLinesDict']
                                              )

        # Stripping lines
        self.Bs2PhiGammaLine = StrippingLine(self.name + 'Bs2PhiGammaLine',
                                             prescale=config['Bs2PhiGammaPreScale'],
                                             postscale=config['Bs2PhiGammaPostScale'],
                                             # RequiredRawEvents = [ "Velo","Tracker","Calo", "Muon","Rich" ],
                                             selection=self.selBs2PhiGamma
                                            )
        self.registerLine(self.Bs2PhiGammaLine)

        self.Bd2KstGammaLine = StrippingLine(self.name + 'Bd2KstGammaLine',
                                             prescale=config['Bd2KstGammaPreScale'],
                                             postscale=config['Bd2KstGammaPostScale'],
                                             # RequiredRawEvents = [ "Velo","Tracker","Calo", "Muon","Rich" ],
                                             selection=self.selBd2KstGamma
                                            )
        self.registerLine(self.Bd2KstGammaLine)
       
        self.Bd2KspipiGammaLine = StrippingLine(self.name + 'Bd2KspipiGammaLine',
                                             prescale=config['Bd2KspipiGammaPreScale'],
                                             postscale=config['Bd2KspipiGammaPostScale'],
                                             # RequiredRawEvents = [ "Velo","Tracker","Calo", "Muon","Rich" ],
                                             selection=self.selBd2KspipiGamma
                                             )
        self.registerLine(self.Bd2KspipiGammaLine)


        self.Bs2KsKpiGammaLine = StrippingLine(self.name + 'Bs2KsKpiGammaLine',
                                             prescale=config['Bs2KsKpiGammaPreScale'],
                                             postscale=config['Bs2KsKpiGammaPostScale'],
                                             # RequiredRawEvents = [ "Velo","Tracker","Calo", "Muon","Rich" ],
                                             selection=self.selBs2KsKpiGamma
                                             )
        self.registerLine(self.Bs2KsKpiGammaLine)
       


 
def makePhoton(name, photonPT):
    """Create photon Selection object starting from DataOnDemand 'Phys/StdLooseAllPhotons'.
    """
    _code = "( PT> %(photonPT)s*MeV )" % locals()
    _gammaFilter = FilterDesktop(Code=_code)
    _stdGamma = StdLooseAllPhotons
    return Selection(name, Algorithm=_gammaFilter, RequiredSelections=[_stdGamma])


def makeKs(name, KsPT, ProbNNpi, GhostProb_Max) :
    """
    Create Ks Selection object starting from DataOnDemand 'Phys/StdLooseKsDD' and 'Phys/StdVeryLooseKsLL'.
    """
    # DD Ks0's
    _KsDDFilter =  FilterDesktop(Code = "( (BPVLTIME() > 2.*ps) & (PT> %(KsPT)s*MeV) )" % locals() )
    _sel_KsDD = Selection("sel_KsDD", Algorithm=_KsDDFilter, RequiredSelections=[DataOnDemand("Phys/StdLooseKsDD/Particles")])    
    # LL Ks0's
    _preambulo = ["goodPion = ( (TRGHOSTPROB < %(GhostProb_Max)s) & (PROBNNpi > %(ProbNNpi)s) )" % locals()]
    _KsLLFilter =  FilterDesktop(Preambulo=_preambulo,Code = "( (VFASPF(VCHI2/VDOF)<25) & (PT> %(KsPT)s*MeV) & ( NINTREE( ('pi+'==ABSID) & goodPion ) == 2 ) )" % locals())
    _sel_KsLL = Selection("sel_KsLL", Algorithm=_KsLLFilter, RequiredSelections=[DataOnDemand("Phys/StdVeryLooseKsLL/Particles")])    
    return MergedSelection(name,RequiredSelections = [_sel_KsDD,_sel_KsLL])


def makePhi2KK(name, TrIPchi2Phi, TrChi2, PhiMassWin, PhiVCHI2, GhostProb_Max, SumVec_PT, MinTrack_PT, MinTrack_P, ProbNNk) :
    """
    Create and return a Phi->KK Selection object, starting from DataOnDemand 'Phys/StdLoosePhi2KK'.
    """																		
    _preambulo = ["goodKaon = ( (MIPCHI2DV(PRIMARY) > %(TrIPchi2Phi)s) & (TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(GhostProb_Max)s) & (P > %(MinTrack_P)s) & (PT > %(MinTrack_PT)s) & (PROBNNk > %(ProbNNk)s))" % locals(),
                  "goodPhi = ( (VFASPF(VCHI2/VDOF) < %(PhiVCHI2)s) & (ADMASS('phi(1020)') < 10.*%(PhiMassWin)s*MeV) & (SUMTREE(PT, ISBASIC, 0.0) > %(SumVec_PT)s))" % locals()]
    _code = "goodPhi & (NINTREE( ('K+'==ABSID) & goodKaon ) == 2) "
    _phiFilter = FilterDesktop(Preambulo=_preambulo, Code=_code)
    _stdPhi2KK = DataOnDemand(Location="Phys/StdLoosePhi2KK/Particles")
    return Selection(name, Algorithm=_phiFilter, RequiredSelections=[_stdPhi2KK])


def makeKstar(name, TrIPchi2Kst, TrChi2, KstMassWin, KstVCHI2, GhostProb_Max, SumVec_PT, MinTrack_PT, MinTrack_P, ProbNNpi, ProbNNk) :
    """
    Create and return a K*->Kpi Selection object, starting from DataOnDemand 'Phys/StdVeryLooseDetachedKst2Kpi'.
    """
    _preambulo = ["goodTrack = ( (MIPCHI2DV(PRIMARY) > %(TrIPchi2Kst)s) & (TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(GhostProb_Max)s) & (P > %(MinTrack_P)s) & (PT > %(MinTrack_PT)s) )" % locals(),
                  "goodKaon = ( goodTrack & (PROBNNk  > %(ProbNNk)s))" % locals(),
                  "goodPion = ( goodTrack & (PROBNNpi > %(ProbNNpi)s))" % locals(),
                  "goodKstar = ( (VFASPF(VCHI2/VDOF) < %(KstVCHI2)s) & (ADMASS('K*(892)0') < 3.*%(KstMassWin)s*MeV) & (SUMTREE(PT, ISBASIC, 0.0) > %(SumVec_PT)s) )" % locals()]
    _code = "goodKstar & (NINTREE( ('K+'==ABSID) & goodKaon ) == 1) & ( NINTREE( ('pi+'==ABSID) & goodPion ) == 1 )"
    _kstFilter = FilterDesktop(Preambulo=_preambulo, Code=_code)
    _stdKst2Kpi = DataOnDemand(Location="Phys/StdVeryLooseDetachedKst2Kpi/Particles")
    return Selection(name, Algorithm=_kstFilter, RequiredSelections=[_stdKst2Kpi])


def makeKspipi(name, sel_Ks, TrIPchi2Kst, TrChi2, mKspipiMax, KstVCHI2, GhostProb_Max, SumVec_PT, MinTrack_PT, MinTrack_P, ProbNNpi) :
    """
    Create and return a K*->Kspipi Selection object
    """
    _diTrackPreVertexCuts = " (AM < (%(mKspipiMax)s)) " %locals()
    _diTrackPostVertexCuts = "(HASVERTEX) & (VFASPF(VCHI2/VDOF) < %(KstVCHI2)s) " %locals()
    _pionCut = "( (MIPCHI2DV(PRIMARY) > %(TrIPchi2Kst)s) & (TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(GhostProb_Max)s) & (P > %(MinTrack_P)s) & (PT > %(MinTrack_PT)s) & (PROBNNpi > %(ProbNNpi)s) )" % locals()
    _combineDiTrack = CombineParticles( DecayDescriptor="rho(770)0 -> pi+ pi-",
                                        CombinationCut = _diTrackPreVertexCuts,
                                        MotherCut = _diTrackPostVertexCuts,
                                        DaughtersCuts = {"pi-":  _pionCut, "pi+":  _pionCut} )
    sel_pipi = Selection("2pi_combi", Algorithm=_combineDiTrack, RequiredSelections=[DataOnDemand(Location="Phys/StdAllNoPIDsPions/Particles")])
    _TriTrackPreVertexCuts = "(ASUM(PT) > %(SumVec_PT)s) & (AM < 1.5*%(mKspipiMax)s)"%locals()
    _TriTrackPostVertexCuts = "( (VFASPF(VCHI2/VDOF) < 5.*%(KstVCHI2)s) & (PT > %(SumVec_PT)s) )" %locals() # for DD KS0 vertexing is not good
    _combineTriTrack = CombineParticles( DecayDescriptor="K*_2(1430)0 -> rho(770)0 KS0",
                                         CombinationCut = _TriTrackPreVertexCuts,
                                         MotherCut = _TriTrackPostVertexCuts )
    return Selection(name, Algorithm = _combineTriTrack, RequiredSelections = [ sel_pipi, sel_Ks ] )


def makeKsKpi(name, sel_Ks, TrIPchi2Kst, TrChi2, mKsKpiMax, KstVCHI2, GhostProb_Max, SumVec_PT, MinTrack_PT, MinTrack_P, ProbNNpi, ProbNNk) :
    """
    Create and return a X->KsKpi Selection object
    @return: Selection object
    
    """
    _diTrackPreVertexCuts = " (AM < (%(mKsKpiMax)s))" %locals()
    _diTrackPostVertexCuts = "(HASVERTEX) & (VFASPF(VCHI2/VDOF) < %(KstVCHI2)s)" %locals()
    _pionCut = "( (MIPCHI2DV(PRIMARY) > %(TrIPchi2Kst)s) & (TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(GhostProb_Max)s) & (P > %(MinTrack_P)s) & (PT > %(MinTrack_PT)s) & (PROBNNpi > %(ProbNNpi)s))" % locals()
    _kaonCut = "( (MIPCHI2DV(PRIMARY) > %(TrIPchi2Kst)s) & (TRCHI2DOF < %(TrChi2)s) & (TRGHOSTPROB < %(GhostProb_Max)s) & (P > %(MinTrack_P)s) & (PT > %(MinTrack_PT)s) & (PROBNNk > %(ProbNNk)s))" % locals()
    _combineDiTrack = CombineParticles( DecayDescriptor="[rho(770)0 -> K+ pi-]cc",
                                        CombinationCut = _diTrackPreVertexCuts,
                                        MotherCut = _diTrackPostVertexCuts,
                                        DaughtersCuts = {"pi-":_pionCut,"pi+":_pionCut,"K+":_kaonCut,"K-":_kaonCut})
    sel_Kpi = Selection("kpi_combi", Algorithm=_combineDiTrack, RequiredSelections=[DataOnDemand(Location="Phys/StdAllNoPIDsPions/Particles"),
                                                                             DataOnDemand(Location="Phys/StdAllNoPIDsKaons/Particles")])

    _TriTrackPreVertexCuts = "(ASUM(PT) > %(SumVec_PT)s) & (AM < 1.2*%(mKsKpiMax)s)"%locals()
    _TriTrackPostVertexCuts = "( (VFASPF(VCHI2/VDOF) < 10.*%(KstVCHI2)s) & (PT > %(SumVec_PT)s) )" %locals() # due to DD KS0, vertexing is kept loose

    _combineTriTrack = CombineParticles( DecayDescriptor="[K*_2(1430)0 -> rho(770)0 KS0]cc",
                                         CombinationCut = _TriTrackPreVertexCuts,
                                         MotherCut = _TriTrackPostVertexCuts )
    return Selection(name, Algorithm = _combineTriTrack, RequiredSelections = [ sel_Ks, sel_Kpi ] )


def makeBs2PhiGamma(name, phiSel, gammaSel, BsVTXchi2, BsPVIPchi2, BMassMin, BMassMax, BPT, B_APT, PhiMassWin, DTF_CL, Hlt1Dict, Hlt2Dict):
    """
    Create and return a Bs -> Phi Gamma Selection object, starting with the daughters' selections.
    """  
    _combinationCut = "((AM > 0.5*%(BMassMin)s) & (AM < 2*%(BMassMax)s))"  % locals() 
    _motherCut = "(VFASPF(VCHI2/VDOF) <%(BsVTXchi2)s) & (BPVIPCHI2() < %(BsPVIPchi2)s) & (PT > %(BPT)s) & (M > 0.5*%(BMassMin)s) & (M < 2.*%(BMassMax)s) & (SUMTREE(PT, ISBASIC, 0.0) > %(B_APT)s)" % locals()
    _Bs = CombineParticles(DecayDescriptor="B_s0 -> phi(1020) gamma",
                           CombinationCut=_combinationCut,
                           MotherCut=_motherCut,
                           ReFitPVs=False)#True)
    sel_PhiGamma_init = Selection("Bs2PhiG_init", Algorithm=_Bs, RequiredSelections=[gammaSel, phiSel])
    #  add the DTF version of the cuts with a very loose requirement that the fit converges
    # PV constraint switched on, but very loose CL cut
    _code = "( (dtf_prob > %(DTF_CL)s) & (in_range(%(BMassMin)s,mB,%(BMassMax)s)) &  (abs(mX-1020) < %(PhiMassWin)s) )"  % locals()
    _preambulo = [ "dtf_prob = DTF_PROB(True )", 
                   "mB       = DTF_FUN(M, True)",
                   "mX       = DTF_FUN(CHILD(1, M), True)"
                 ]
    _PhiGammaFilter = FilterDesktop(Preambulo=_preambulo, Code=_code)
    sel_PhiGamma = Selection(name, Algorithm=_PhiGammaFilter, RequiredSelections=[sel_PhiGamma_init])
    return fullTisTosSelection(sel_PhiGamma, Hlt1Dict, Hlt2Dict)


def makeBd2KstGamma(name, kstSel, gammaSel, B0VTXchi2, B0PVIPchi2, BMassMin, BMassMax, BPT, B_APT, KstMassWin, BdDira, DTF_CL,Hlt1Dict, Hlt2Dict):
    """
    Create and return a Bd -> K* Gamma Selection object, starting with the daughters' selections.
    """  
    _combinationCut = "((AM > 0.3*%(BMassMin)s) & (AM < 2.5*%(BMassMax)s))" % locals()
    _motherCut = " (VFASPF(VCHI2/VDOF) <%(B0VTXchi2)s) & (BPVIPCHI2() < %(B0PVIPchi2)s) & (PT > %(BPT)s) & (M > 0.3*%(BMassMin)s) & (M < 2.*%(BMassMax)s) & (SUMTREE(PT, ISBASIC, 0.0) > %(B_APT)s) & (acos(BPVDIRA) < %(BdDira)s) " % locals()
    _Bd = CombineParticles(DecayDescriptor="[B0 -> K*(892)0 gamma]cc",
                           CombinationCut=_combinationCut,
                           MotherCut=_motherCut,
                           ReFitPVs=False)#True)
    sel_KstGamma_init = Selection("Bd2KstG_init", Algorithm=_Bd, RequiredSelections=[gammaSel, kstSel])
    #  add the DTF version of the cuts with a very loose requirement that the fit converges
    # PV constraint switched on, but very loose CL cut
    _code = "((dtf_prob > %(DTF_CL)s) & (in_range(%(BMassMin)s,mB,%(BMassMax)s)) &  (abs(mX-895) < %(KstMassWin)s) )"  % locals()
    _preambulo = [ "dtf_prob = DTF_PROB(True )", 
                   "mB       = DTF_FUN(M, True)",
                   "mX       = DTF_FUN(CHILD(1, M), True)"
                 ]
    _KstGammaFilter = FilterDesktop(Preambulo=_preambulo, Code=_code)
    sel_KstGamma = Selection(name, Algorithm=_KstGammaFilter, RequiredSelections=[sel_KstGamma_init])
    return fullTisTosSelection(sel_KstGamma, Hlt1Dict, Hlt2Dict)


def makeBd2KspipiGamma(name, KspipiSel, gammaSel, B0VTXchi2, B0PVIPchi2, BMassMin, BMassMax, BPT, B_APT, BdDira, mKspipiMax, DTF_CL, Hlt1Dict, Hlt2Dict):
    """
    Create and return a Bd -> Kspipi Gamma Selection object, starting with the daughters' selections.
    @return: Selection object

    """  
    _combinationCut = "((AM > 0.5*%(BMassMin)s) & (AM < 2*%(BMassMax)s))" % locals()
    _motherCut = "(VFASPF(VCHI2/VDOF) <%(B0VTXchi2)s) & (BPVIPCHI2() < %(B0PVIPchi2)s) & (PT > %(BPT)s) & (M > 0.5*%(BMassMin)s) & (M < 2.*%(BMassMax)s) & (SUMTREE(PT, ISBASIC, 0.0) > %(B_APT)s) & (acos(BPVDIRA) < %(BdDira)s)" % locals()
    _Bd = CombineParticles(DecayDescriptor="B0 -> K*_2(1430)0 gamma",
                           CombinationCut=_combinationCut,
                           MotherCut=_motherCut,
                           ReFitPVs=False)#True)
    sel_KspipiGamma_init = Selection("Bd2KspipiG_init", Algorithm=_Bd, RequiredSelections=[gammaSel, KspipiSel])

    _code = "((dtf_prob > %(DTF_CL)s) & (in_range(%(BMassMin)s,mB,%(BMassMax)s)) &  (mX < %(mKspipiMax)s))"  % locals()
    _preambulo = [ "dtf_prob = DTF_PROB(True, 'KS0' )", 
                   "mB       = DTF_FUN(M, True, 'KS0')",
                   "mX       = DTF_FUN(CHILD(1, M), True, 'KS0')"
                 ]
    _KspipiGammaFilter = FilterDesktop(Preambulo=_preambulo, Code=_code)
    sel_KspipiGamma = Selection(name, Algorithm=_KspipiGammaFilter, RequiredSelections=[sel_KspipiGamma_init])
    return fullTisTosSelection(sel_KspipiGamma, Hlt1Dict, Hlt2Dict)

def makeBs2KsKpiGamma(name, KsKpiSel, gammaSel, B0VTXchi2, B0PVIPchi2, BMassMin, BMassMax, BPT, B_APT, BdDira, mKsKpiMax, DTF_CL, Hlt1Dict, Hlt2Dict):
    """
    Create and return a Bd -> KsKpi Gamma Selection object, starting with the daughters' selections.
    @return: Selection object

    """  
    _combinationCut = "((AM > 0.5*%(BMassMin)s) & (AM < 2*%(BMassMax)s))" % locals()
    _motherCut = "(VFASPF(VCHI2/VDOF) <%(B0VTXchi2)s) & (BPVIPCHI2() < %(B0PVIPchi2)s) & (PT > %(BPT)s) & (M > 0.5*%(BMassMin)s) & (M < 2.*%(BMassMax)s)  & (SUMTREE(PT, ISBASIC, 0.0) > %(B_APT)s) & (acos(BPVDIRA) < %(BdDira)s) " % locals()
    _Bs = CombineParticles(DecayDescriptor="[B_s0 -> K*_2(1430)0 gamma]cc",
                           CombinationCut=_combinationCut,
                           MotherCut=_motherCut,
                           ReFitPVs=False)#True)
    sel_KsKpiGamma_init = Selection("Bs2KsKpiG_init", Algorithm=_Bs, RequiredSelections=[gammaSel, KsKpiSel])

    _code = "((dtf_prob > %(DTF_CL)s) & (in_range(%(BMassMin)s,mB,%(BMassMax)s)) &  (mX < %(mKsKpiMax)s))"  % locals()
    _preambulo = [ "dtf_prob = DTF_PROB(True,'KS0' )", 
                   "mB       = DTF_FUN(M, True, 'KS0')",
                   "mX       = DTF_FUN(CHILD(1, M), True, 'KS0')"
                 ]
    _KsKpiGammaFilter = FilterDesktop(Preambulo=_preambulo, Code=_code)
    sel_KsKpiGamma = Selection(name, Algorithm=_KsKpiGammaFilter, RequiredSelections=[sel_KsKpiGamma_init])
    return fullTisTosSelection(sel_KsKpiGamma, Hlt1Dict, Hlt2Dict)


# TISTOS
def fullTisTosSelection(sel, DictHlt1, DictHlt2):
    def tisTosSelection(sel, specs, taggerName):
        """Filters Selection sel to be TOS OR TIS."""

        hltTisTosFilter = TisTosParticleTagger(name+'TISTOSFilter')
        hltTisTosFilter.TisTosSpecs = specs
        hltSel = Selection(sel.name() + taggerName,
                           Algorithm=hltTisTosFilter,
                           RequiredSelections=[sel])
        return hltSel
    return tisTosSelection(tisTosSelection(sel,DictHlt1,'Hlt1TISTOS'),DictHlt2, 'Hlt2TISTOS')


# EOF
