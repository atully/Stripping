###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module for selecting Lambda_c+->Lambda pi+
'''

__author__=['Guiyun Ding', 'Jiesheng Yu', 'Yuehong Xie']
__date__ = '28/11/2016'
__version__= '$Revision: 2.0$'


__all__ = (
    'Lc2L0LLpiConf',
    'default_config'
    )

default_config = {
    'NAME'              :  'Lc2L0LLpi',
    'BUILDERTYPE'       :  'Lc2L0LLpiConf',
    'CONFIG'    : {
        'ProtonCuts'      : "(P > 2000*MeV) & (MIPCHI2DV(PRIMARY) > 9.) ",
        'PiminusCuts'      : "(PT > 50*MeV) & (P > 2000*MeV) & (MIPCHI2DV(PRIMARY) > 9.) ",
        'PionCuts'      : "(PT > 400*MeV) & (MIPCHI2DV(PRIMARY) > 4.) & (BPVIPCHI2() > 4.0) & (in_range(2000.0*MeV, P, 100000.0)) & (in_range(2.0, ETA, 5.0)) & ((PIDK - PIDpi) < 3.0) ", 
        #'PionCuts'      : "(PT > 100*MeV) & (P > 1000*MeV) & (MIPCHI2DV(PRIMARY) > 9.) ",
        #'Lambda0ComAMCuts' : "(P > 2000.0*MeV) & (PT > 500.0*MeV) & (BPVVDCHI2 > 100) & (VFASPF(VCHI2/VDOF) < 10.0)",
        'Lambda0MomN4Cuts' : """
                           (VFASPF(VCHI2) < 30.) 
                           & (ADMASS('Lambda0')<35*MeV) 
                           & (P > 2000.0*MeV)
                           & (PT > 500.0*MeV)
                           & (BPVVDCHI2 > 100)
                           & (VFASPF(VCHI2/VDOF) < 10.0)
                           """,
        'Lambda0ComCuts'   : "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))",
        'LcComCuts'     : "(ADAMASS('Lambda_c+') < 90.0*MeV) & (ACUTDOCA(0.5, ''))",
        'LcMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 10.) 
                          & (BPVDIRA> 0.999) 
                          & (BPVVDCHI2>16.)
                          """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoVertexIsolationBDT',
                      'Location'          : 'RelInfoVertexIsolationBDT'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]        
        },
    'STREAMS'           : ['Charm' ],
    'WGs'               : ['Charm'],
    }


from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles 
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
#from Configurables import DaVinci__N4BodyDecays
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N4BodyDecays
from PhysSelPython.Wrappers import MergedSelection
from Configurables import LoKi__VoidFilter
from StandardParticles import ( StdAllLoosePions, StdLooseProtons,
                                StdLoosePions, StdNoPIDsUpPions )
from StandardParticles import StdLooseAllPhotons, StdVeryLooseAllPhotons

class Lc2L0LLpiConf(LineBuilder):
    
    __configuration_keys__ = (
        'ProtonCuts',
        'PionCuts',
        'PiminusCuts',
        #'Lambda0ComAMCuts',
        'Lambda0MomN4Cuts',
        'Lambda0ComCuts',
        'LcComCuts',
        'LcMomCuts',
        'Prescale',
        'RelatedInfoTools'
        )

    
    def __init__(self, name, config ): 
        
        LineBuilder.__init__(self, name, config)
        self.name = name 
        self.config = config


        self.SelPiminus = self.createSubSel( OutputList = self.name + "SelPiminus",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLoosePions/Particles' ), 
                                           Cuts = config['PiminusCuts']
                                           )

        self.SelPions = self.createSubSel( OutputList = self.name + "SelPions",
                                           InputList =  DataOnDemand(Location = 'Phys/StdAllLoosePions/Particles' ), 
                                           Cuts = config['PionCuts']
                                           )

        self.SelProtons = self.createSubSel( OutputList = self.name + "SelProtons",
                                           InputList =  DataOnDemand(Location = 'Phys/StdLooseProtons/Particles' ), 
                                           Cuts = config['ProtonCuts']
                                           )

        
        #"""
        #Lambda0
        #"""
        ## Both LL and DD 
        #self.InputLambda0 = MergedSelection( self.name + "InputLambda0",
        #                                RequiredSelections = [DataOnDemand(Location = "Phys/StdLooseLambdaDD/Particles"),
        #                                                      DataOnDemand(Location = "Phys/StdLooseLambdaLL/Particles")] )

        #self.SelLambda0 = self.createSubSel( OutputList = self.name + "SelLambda0",
        #                                InputList = self.InputLambda0,
        #                                Cuts = config['Lambda0Cuts'] ) 

        #                                             

        """
        Lambda0-> p Pi
        """
        self.SelLambda02pPi = self.createCombinationSel( OutputList = self.name + "SelLambda02pPi",
                                                        DaughterLists = [ self.SelPiminus, self.SelProtons],
                                                        DecayDescriptor = "[Lambda0 -> p+ pi-]cc",
                                                        PreVertexCuts  = config['Lambda0ComCuts'], 
                                                        PostVertexCuts = config['Lambda0MomN4Cuts']
                                                        )
        """
        Lc-> Lambda0 Pi 
        """
        self.SelLc2Lambda0Pi_pPiPi = self.createCombinationSel( OutputList = self.name + "SelLc2Lambda0Pi_pPiPi",
                                                              DecayDescriptor = "[Lambda_c+ -> Lambda0 pi+]cc",
                                                              DaughterLists = [ self.SelLambda02pPi, self.SelPions],                    
                                                              PreVertexCuts  = config['LcComCuts'],
                                                              PostVertexCuts = config['LcMomCuts'] )

        self.SelLc2Lambda0Pi_pPiPiLine = StrippingLine( self.name + '_pPiPiLine',                                                
                                                   prescale  = config['Prescale'],
                                                 #  HLT       = config['HLTCuts'],
                                                   algos     = [ self.SelLc2Lambda0Pi_pPiPi ],
                                                   EnableFlavourTagging = False,
                                                   MDSTFlag = False,
                                                   RelatedInfoTools = self.config['RelatedInfoTools']                                                   
                                                   )

        self.registerLine( self.SelLc2Lambda0Pi_pPiPiLine )
        
        


    def createSubSel( self, OutputList, InputList, Cuts ) :
        '''create a selection using a FilterDesktop'''
        filter = FilterDesktop(Code = Cuts)
        return Selection( OutputList,
                          Algorithm = filter,
                          RequiredSelections = [ InputList ] )

    def createCombinationSel( self, OutputList,
                              DecayDescriptor,
                              DaughterLists,
                              DaughterCuts = {} ,
                              PreVertexCuts = "ALL",
                              PostVertexCuts = "ALL",
                              ReFitPVs = True ) :
        '''create a selection using a ParticleCombiner with a single decay descriptor'''
        combiner = CombineParticles( DecayDescriptor = DecayDescriptor,
                                     DaughtersCuts = DaughterCuts,
                                     MotherCut = PostVertexCuts,
                                     CombinationCut = PreVertexCuts,
                                     ReFitPVs = False)
        return Selection ( OutputList,
                           Algorithm = combiner,
                           RequiredSelections = DaughterLists)


    

