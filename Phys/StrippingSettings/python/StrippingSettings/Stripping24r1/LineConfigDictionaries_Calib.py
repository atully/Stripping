###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  2 4 r 1                        ##
##                                                                            ##
##  Configuration for Calib WG                                                ##
##  Contact person: Michael Kolpin (Michael.Kolpin@cern.ch)                   ##
################################################################################

from GaudiKernel.SystemOfUnits import *


### From S24.

CharmFromBSemiForHadronAsy = {
    "BUILDERTYPE": "CharmFromBSemiForHadronAsyAllLinesConf", 
    "CONFIG": {
        "D0to3H_3pi_DeltaMass_MAX": 350.0, 
        "D0to3H_3pi_MASS_MAX": 1400.0, 
        "D0to3H_3pi_MASS_MIN": 900.0, 
        "D0to3H_B_DIRA_MIN": 0.99, 
        "D0to3H_B_DOCACHI2_MAX": 50.0, 
        "D0to3H_B_MASS_MAX": 4900.0, 
        "D0to3H_B_MASS_MIN": 1800.0, 
        "D0to3H_B_VCHI2NDF_MAX": 15.0, 
        "D0to3H_DOCACHI2_MAX": 10.0, 
        "D0to3H_DZ": 2.0, 
        "D0to3H_K2pi_DeltaMass_MAX": 250.0, 
        "D0to3H_K2pi_MASS_MAX": 1800.0, 
        "D0to3H_K2pi_MASS_MIN": 1300.0, 
        "D0to3H_REQUIRE_TOS": True, 
        "D0to3H_SUMPT_MIN": 1800.0, 
        "D0to3H_VCHI2NDF_MAX": 3.0, 
        "GEC_nLongTrk": 250.0, 
        "GHOSTPROB_MAX": 0.35, 
        "H_PT": 250.0, 
        "K_PIDKMin": 6.0, 
        "Lc2Kpi_B_DIRA_MIN": 0.99, 
        "Lc2Kpi_B_DOCACHI2_MAX": 50.0, 
        "Lc2Kpi_B_FDCHI2_MIN": 20.0, 
        "Lc2Kpi_B_MASS_MAX": 4300.0, 
        "Lc2Kpi_B_MASS_MIN": 2200.0, 
        "Lc2Kpi_B_VCHI2NDF_MAX": 15.0, 
        "Lc2Kpi_DOCACHI2_MAX": 10.0, 
        "Lc2Kpi_DZ": 1.0, 
        "Lc2Kpi_DeltaMass_MAX": 700.0, 
        "Lc2Kpi_FDCHI2_MIN": 20.0, 
        "Lc2Kpi_MASS_MAX": 1350.0, 
        "Lc2Kpi_MASS_MIN": 800.0, 
        "Lc2Kpi_REQUIRE_TOS": True, 
        "Lc2Kpi_SUMPT_MIN": 1500.0, 
        "Lc2Kpi_VCHI2NDF_MAX": 3.0, 
        "MuPiPi_CHI2NDF": 3.0, 
        "MuPiPi_DOCACHI2_MAX": 15.0, 
        "MuPiPi_FDCHI2_MIN": 20.0, 
        "MuPi_CHI2NDOF_MAX": 3.0, 
        "MuPi_DIRA_MIN": -99.0, 
        "MuPi_DOCACHI2_MAX": 8.0, 
        "MuPi_FDCHI2_MIN": 20.0, 
        "MuPi_SUMPT_MIN": 1300.0, 
        "Mu_PT": 800.0, 
        "PiPi_CHI2NDF": 3.0, 
        "PiPi_DOCACHI2_MAX": 15.0, 
        "PiPi_MASS_MAX": 500.0, 
        "PiPi_SUMPT_MIN": 600.0, 
        "Pi_PIDKMax": 6.0, 
        "Slowpi_PIDKMax": 10.0, 
        "Slowpi_PIDeMax": 99.0, 
        "Slowpi_PTMin": 200.0, 
        "prescale_D0to3piRS": 1.0, 
        "prescale_D0to3piWS": 0.2, 
        "prescale_D0toK2piRS": 1.0, 
        "prescale_D0toK2piWS": 0.2, 
        "prescale_LbRS": 1.0, 
        "prescale_LbWS": 0.2
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

D02KPiPi0 = {
    "BUILDERTYPE": "StrippingD02KPiPi0Conf", 
    "CONFIG": {
        "D0MaxIPChi2": 9, 
        "D0MaxM": 2100, 
        "D0MinDIRA": 0.9999, 
        "D0MinM": 1600, 
        "D0MinVVDChi2": 64, 
        "D0MinVtxProb": 0.001, 
        "MergedLinePostscale": 1.0, 
        "MergedLinePrescale": 0.5, 
        "Pi0MinPT_M": 2000, 
        "Pi0MinPT_R": 1000, 
        "ResPi0MinGamCL": 0.2, 
        "ResolvedLinePostscale": 1.0, 
        "ResolvedLinePrescale": 0.5, 
        "TrackMaxGhostProb": 0.3, 
        "TrackMinIPChi2": 16, 
        "TrackMinPT_M": 300, 
        "TrackMinPT_R": 600, 
        "TrackMinTrackProb": 1e-06
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

DstarD02KShhForTrackingEff = {
    "BUILDERTYPE": "DstarD02KShh_ForTrackingEffBuilder", 
    "CONFIG": {
        "DIRA_D0_MIN": 0.999, 
        "DeltaM_MAX": 250.0, 
        "DeltaM_MIN": 0.0, 
        "FDCHI2_D0_MIN": 80.0, 
        "HLTFILTER": "(HLT_PASS_RE('Hlt2CharmHadD02HHXDst.*Decision')|HLT_PASS('Hlt2IncPhiDecision'))", 
        "Hlt2TisTosSpec": {
            "Hlt2CharmHadD02HHXDst.*Decision%TOS": 0, 
            "Hlt2IncPhiDecision%TOS": 0
        }, 
        "IPCHI2_MAX_Child_MIN": 16.0, 
        "IPCHI2_PiSlow_MAX": 9.0, 
        "KAON_PIDK_MIN": 3.0, 
        "KKprescale": 1.0, 
        "KMinusPiPlusprescale": 0.1, 
        "KPlusPiMinusprescale": 0.1, 
        "LongTrackGEC": 150, 
        "M_MAX": 1800.0, 
        "M_MIN": 0.0, 
        "PION_PIDK_MAX": 0.0, 
        "PT_Dstar_MIN": 2500.0, 
        "PairMaxDoca_Dstar": 100.0, 
        "Pair_AMINDOCA_MAX": 0.1, 
        "Pair_BPVCORRM_MAX": 3000.0, 
        "Pair_BPVVD_MIN": 0.0, 
        "Pair_SumAPT_MIN": 2500.0, 
        "PhiM_MAX": 1040.0, 
        "PhiM_MIN": 1000.0, 
        "PiPiprescale": 0.1, 
        "TrkChi2_MAX_Child_MAX": 2.25, 
        "TrkChi2_SlowPion": 2.25, 
        "TrkP_SlowPion": 3000.0, 
        "TrkPt_SlowPion": 0.0, 
        "Trk_GHOST_MAX": 0.4, 
        "Trk_PT_MIN": 600.0, 
        "Trk_P_MIN": 10000.0, 
        "VCHI2_D0_MAX": 4.0, 
        "postscale": 1.0
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

ProtonAsym = {
    "BUILDERTYPE": "ProtonAsymBuilder", 
    "CONFIG": {
        "BBPVIPCHI2": 15.0, 
        "BFDCHI2": 36.0, 
        "BMinZ": 0.2, 
        "BPTSum": 2000.0, 
        "BPartialMassMax": 4350.0, 
        "BPartialMassMin": 3500.0, 
        "BVCHI2DOF": 20.0, 
        "BuMaxWin": 6500.0, 
        "BuMinWin": 5000.0, 
        "HLTBd2LcppipiLc2Kpi": None, 
        "HLTBu2LcppipipiLc2Kpi": None, 
        "HLTLb2LcpiLc2Kpi": None, 
        "HLTLb2LcpipipiLc2Kpi": None, 
        "HLTLcst2LcpipiLc2Kpi": None, 
        "HLTSc2LcpimLc2Kpi": "HLT_PASS_RE('Hlt2.*CharmHadD02HHXDst.*Decision')", 
        "HLTSc2LcpipLc2Kpi": "HLT_PASS_RE('Hlt2.*CharmHadD02HHXDst.*Decision')", 
        "LbBPVIPCHI2": 15.0, 
        "LbFDCHI2": 36.0, 
        "LbMaxWin": 6500.0, 
        "LbMinWin": 5200.0, 
        "LbMinZ": 0.2, 
        "LbPTSum": 2000.0, 
        "LbPartialMassMax": 4700.0, 
        "LbToLc1piPartialMassMin": 3000.0, 
        "LbToLc3piPartialMassMin": 3500.0, 
        "LbVCHI2DOF": 20.0, 
        "LcMaxWin": 2366.0, 
        "LcMinWin": 2206.0, 
        "LcPartialDOCACHI2": 25.0, 
        "LcPartialFDCHI2": 36.0, 
        "LcPartialMassMax": 1360.0, 
        "LcPartialMassMin": 700.0, 
        "LcPartialPTSum": 1500.0, 
        "LcPartialVCHI2DOF": 10.0, 
        "LcstBPVIPCHI2": 15.0, 
        "LcstMaxWin": 2892.0, 
        "LcstMinWin": 2292.0, 
        "LcstMinZ": 0.2, 
        "LcstPartialMassMax": 1660.0, 
        "LcstPartialMassMin": 1300.0, 
        "LcstVCHI2DOF": 10.0, 
        "MicroDST": True, 
        "PostscaleBd2LcppipiLc2Kpi": 1.0, 
        "PostscaleBu2LcppipipiLc2Kpi": 1.0, 
        "PostscaleLb2LcpiLc2Kpi": 1.0, 
        "PostscaleLb2LcpipipiLc2Kpi": 1.0, 
        "PostscaleLcst2LcpipiLc2Kpi": 1.0, 
        "PostscaleSc2LcpimLc2Kpi": 1.0, 
        "PostscaleSc2LcpipLc2Kpi": 1.0, 
        "PrescaleBd2LcppipiLc2Kpi": 1.0, 
        "PrescaleBu2LcppipipiLc2Kpi": 1.0, 
        "PrescaleLb2LcpiLc2Kpi": 1.0, 
        "PrescaleLb2LcpipipiLc2Kpi": 1.0, 
        "PrescaleLcst2LcpipiLc2Kpi": 1.0, 
        "PrescaleSc2LcpimLc2Kpi": 1.0, 
        "PrescaleSc2LcpipLc2Kpi": 1.0, 
        "ProbNNk": 0.2, 
        "ProbNNp": 0.2, 
        "ProbNNpi": 0.2, 
        "ScBPVIPCHI2": 15.0, 
        "ScMaxWin": 2753.0, 
        "ScMinWin": 2053.0, 
        "ScMinZ": 0.2, 
        "ScPartialMassMax": 1600.0, 
        "ScPartialMassMin": 1000.0, 
        "ScVCHI2DOF": 10.0, 
        "TrackIPCHI2": 12.0, 
        "TrackP": 1000.0, 
        "TrackPT": 100.0
    }, 
    "STREAMS": {
        "Calibration": [
            "StrippingBd2LcppipiLc2KpiProtonAsymLine", 
            "StrippingBu2LcppipipiLc2KpiProtonAsymLine", 
            "StrippingLb2LcpiLc2KpiProtonAsymLine", 
            "StrippingLb2LcpipipiLc2KpiProtonAsymLine", 
            "StrippingSc2LcpipLc2KpiProtonAsymLine", 
            "StrippingSc2LcpimLc2KpiProtonAsymLine", 
            "StrippingLcst2LcpipiLc2KpiProtonAsymLine", 
            "StrippingBd2LcppipiLc2pKpiProtonAsymLine", 
            "StrippingBu2LcppipipiLc2pKpiProtonAsymLine", 
            "StrippingLb2LcpiLc2pKpiProtonAsymLine", 
            "StrippingLb2LcpipipiLc2pKpiProtonAsymLine", 
            "StrippingSc2LcpipLc2pKpiProtonAsymLine", 
            "StrippingSc2LcpimLc2pKpiProtonAsymLine", 
            "StrippingLcst2LcpipiLc2pKpiProtonAsymLine"
        ]
    }, 
    "WGs": [ "ALL" ]
}

TrackEffD0ToK3Pi = {
    "BUILDERTYPE": "TrackEffD0ToK3PiAllLinesConf", 
    "CONFIG": {
        "D0_MAX_DOCA": 0.05, 
        "D0_MIN_FD": 5.0, 
        "DoVeloDecoding": False, 
        "Dst_MAX_DTFCHI2": 3.0, 
        "Dst_MAX_M": 2035.0000000000002, 
        "HLT2": "HLT_PASS_RE('Hlt2.*CharmHad.*HHX.*Decision')", 
        "Kaon_MIN_PIDK": 7, 
        "LC_MIN_FD": 2.0, 
        "Pion_MAX_PIDK": 4, 
        "RequireDstFirst": False, 
        "Sc_MAX_DTFCHI2": 3.0, 
        "Sc_MAX_M": 2500.0, 
        "TTSpecs": {
            "Hlt2.*CharmHad.*HHX.*Decision%TOS": 0
        }, 
        "VeloFitter": "SimplifiedGeometry", 
        "VeloLineForTiming": False, 
        "VeloMINIP": 0.05
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

TrackEffD0ToKPi = {
    "BUILDERTYPE": "TrackEffD0ToKPiAllLinesConf", 
    "CONFIG": {
        "Dst_DTFCHI2_MAX": 10, 
        "Dst_M_MAX": 2100, 
        "HLT1": "HLT_PASS_RE('Hlt1TrackMVADecision')", 
        "HLT2": "HLT_PASS_RE('Hlt2TrackEff_D0.*Decision')", 
        "Kaon_MIN_PIDK": 0, 
        "Monitor": False, 
        "Pion_MAX_PIDK": 20, 
        "TTSpecs": {
            "Hlt1TrackMVADecision%TOS": 0, 
            "Hlt2TrackEff_D0.*Decision%TOS": 0
        }, 
        "Tag_MIN_PT": 1000.0, 
        "VeloMINIPCHI2": 4.0
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

TrackEffDownMuon = {
    "BUILDERTYPE": "StrippingTrackEffDownMuonConf", 
    "CONFIG": {
        "DataType": "2015", 
        "Doca": 5.0, 
        "HLT1PassOnAll": True, 
        "HLT1TisTosSpecs": {
            "Hlt1SingleMuonNoIPL0Decision%TOS": 0, 
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "HLT2PassOnAll": False, 
        "HLT2TisTosSpecs": {
            "Hlt2SingleMuon.*Decision%TOS": 0, 
            "Hlt2TrackEffDiMuonDownstream.*Decision%TUS": 0
        }, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "MassPostComb": 200.0, 
        "MassPreComb": 2000.0, 
        "MuMom": 2000.0, 
        "MuTMom": 200.0, 
        "NominalLinePostscale": 1.0, 
        "NominalLinePrescale": 0.2, 
        "SeedingMinP": 1500.0, 
        "TrChi2": 10.0, 
        "UpsilonHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHLT2TisTosSpecs": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonLinePostscale": 1.0, 
        "UpsilonLinePrescale": 1.0, 
        "UpsilonMassPostComb": 0.0, 
        "UpsilonMassPreComb": 100000.0, 
        "UpsilonMuMom": 0.0, 
        "UpsilonMuTMom": 500.0, 
        "ValidationLinePostscale": 1.0, 
        "ValidationLinePrescale": 0.0015, 
        "VertChi2": 25.0, 
        "ZHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHLT2TisTosSpecs": {
            "Hlt2SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZLinePostscale": 1.0, 
        "ZLinePrescale": 1.0, 
        "ZMassPostComb": 1500.0, 
        "ZMassPreComb": 100000.0, 
        "ZMuMaxEta": 4.5, 
        "ZMuMinEta": 2.0, 
        "ZMuMom": 0.0, 
        "ZMuTMom": 20000.0
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

TrackEffMuonTT = {
    "BUILDERTYPE": "StrippingTrackEffMuonTTConf", 
    "CONFIG": {
        "BJpsiKHlt2TriggersTOS": {
            "Hlt2TopoMu2BodyBBDTDecision%TOS": 0
        }, 
        "BJpsiKHlt2TriggersTUS": {
            "Hlt2TopoMu2BodyBBDTDecision%TUS": 0
        }, 
        "BJpsiKMINIP": 10000, 
        "BJpsiKPrescale": 1, 
        "BMassWin": 500, 
        "Hlt1PassOnAll": True, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt1Triggers": {
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "JpsiHlt2Triggers": {
            "Hlt2SingleMuon.*Decision%TOS": 0, 
            "Hlt2TrackEffDiMuonMuonTT.*Decision%TUS": 0
        }, 
        "JpsiLongMuonMinIP": 0.5, 
        "JpsiLongMuonTrackCHI2": 5, 
        "JpsiLongPT": 1300, 
        "JpsiMINIP": 3, 
        "JpsiMassWin": 500, 
        "JpsiMuonTTPT": 0, 
        "JpsiPT": 1000, 
        "JpsiPrescale": 1, 
        "LongMuonPID": 2, 
        "Postscale": 1, 
        "UpsilonHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHlt2Triggers": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonLongMuonMinIP": 0, 
        "UpsilonLongMuonTrackCHI2": 5, 
        "UpsilonLongPT": 1000, 
        "UpsilonMINIP": 10000, 
        "UpsilonMassWin": 1500, 
        "UpsilonMuonTTPT": 500, 
        "UpsilonPT": 0, 
        "UpsilonPrescale": 1, 
        "VertexChi2": 5, 
        "ZHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHlt2Triggers": {
            "Hlt2SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZLongMuonMinIP": 0, 
        "ZLongMuonTrackCHI2": 5, 
        "ZLongPT": 10000, 
        "ZMINIP": 10000, 
        "ZMassWin": 40000, 
        "ZMuonTTPT": 500, 
        "ZPT": 0, 
        "ZPrescale": 1
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

TrackEffVeloMuon = {
    "BUILDERTYPE": "StrippingTrackEffVeloMuonConf", 
    "CONFIG": {
        "HLT1PassOnAll": True, 
        "HLT1TisTosSpecs": {
            "Hlt1SingleMuonNoIPDecision%TOS": 0, 
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "HLT2PassOnAll": False, 
        "HLT2TisTosSpecs": {
            "Hlt2SingleMuon.*Decision%TOS": 0
        }, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "JpsiPt": 0.5, 
        "LongP": 7.0, 
        "MassPostComb": 400.0, 
        "MassPreComb": 1000.0, 
        "MuDLL": 1.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "TrChi2LongMu": 3.0, 
        "TrChi2VeMu": 5.0, 
        "TrP": 5.0, 
        "TrPt": 100.0, 
        "UpsilonHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHLT2TisTosSpecs": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonMassPostComb": 1500.0, 
        "UpsilonMassPreComb": 100000.0, 
        "UpsilonPostscale": 1.0, 
        "UpsilonPrescale": 1.0, 
        "UpsilonPt": 0.5, 
        "UpsilonTrP": 0.0, 
        "UpsilonTrPt": 500.0, 
        "UpsilonVertChi2": 10000.0, 
        "VertChi2": 2.0, 
        "ZHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHLT2TisTosSpecs": {
            "Hlt2SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZMassPostComb": 40000.0, 
        "ZMassPreComb": 100000.0, 
        "ZPostscale": 1.0, 
        "ZPrescale": 1.0, 
        "ZPt": 0.5, 
        "ZTrMaxEta": 4.5, 
        "ZTrMinEta": 2.0, 
        "ZTrP": 0.0, 
        "ZTrPt": 20000.0, 
        "ZVertChi2": 10000.0
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

D2KPiPi0_PartReco = {
    "BUILDERTYPE": "D2KPiPi0_PartRecoBuilder",
    "CONFIG": {
        "BDIRA": 0.999,
        "BFDCHI2HIGH": 100.0,
        "BPVVDZcut": 0.0,
        "BVCHI2DOF": 6,
        "DELTA_MASS_MAX": 190,
        "ElectronPIDe": 5.0,
        "ElectronPT": 0,
        "HadronMINIPCHI2": 9,
        "HadronPT": 800.0,
        "KLepMassHigh": 2500,
        "KLepMassLow": 500,
        "KaonPIDK": 5.0,
        "PionPIDK": -1.0,
        "Slowpion_PIDe": 5,
        "Slowpion_PT": 300,
        "Slowpion_TRGHOSTPROB": 0.35,
        "TOSFilter": {
            "Hlt2CharmHad.*HHX.*Decision%TOS": 0
        },
        "TRGHOSTPROB": 0.35
    },
    "STREAMS": [ "CharmCompleteEvent" ],
    "WGs": [ "ALL" ]
}
