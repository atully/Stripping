###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
B02Lcmu_pKpi = {
    "BUILDERTYPE": "B02Lcmu_pKpiConf", 
    "CONFIG": {
        "B0MotheCut": "(ADAMASS('B0')<500*MeV)", 
        "B0VxCut": "(VFASPF(VCHI2/VDOF) < 14.) & (BPVDIRA>0.988)", 
        "KaonCuts": "(PROBNNk  > 0.10) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)", 
        "LambdacMothCut": "(ADAMASS('Lambda_c+')<24*MeV)", 
        "LambdacVxCut": "(VFASPF(VCHI2/VDOF) < 14.) & (PT > 1*GeV) & (P > 15*GeV)", 
        "MuonCuts": "(PROBNNmu > 0.10) & (PT > 600*MeV) & (TRGHOSTPROB<0.4)", 
        "PionCuts": "(PROBNNpi > 0.18) & (PT > 200*MeV) & (TRGHOSTPROB<0.4)", 
        "Prescale": 1.0, 
        "ProtonCuts": "(PROBNNp  > 0.10) & (PT > 400*MeV) & (TRGHOSTPROB<0.4)"
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "RD" ]
}

B23Mu = {
    "BUILDERTYPE": "B23MuLinesConf", 
    "CONFIG": {
        "B23MuPrescale": 1, 
        "B23PiPrescale": 1, 
        "B2MueePrescale": 1, 
        "CommonRelInfoTools": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "BsMuMuBIsolation", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }
        ], 
        "MaxDoca": 0.3, 
        "MaxIPChi2": 25, 
        "MaxTrChi2Dof": 4.0, 
        "MaxTrGhp": 0.4, 
        "MaxVtxChi2Dof": 9, 
        "MinDira": 0.0, 
        "MinTau": 0.0, 
        "MinTrIPChi2": 25.0, 
        "MinVDChi2": 225, 
        "Postscale": 1, 
        "mDiffBuLoose": 500, 
        "mDiffBuTight": 100
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

""" moved to BnoC
B24p = {
    "BUILDERTYPE": "B24pLinesConf", 
    "CONFIG": {
        "B24pPrescale": 1, 
        "B2JpsiKpiPrescale": 1, 
        "B2PhiKhPrescale": 1, 
        "CommonRelInfoTools": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "BsMuMuBIsolation", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }
        ], 
        "MaxDoca": 0.3, 
        "MaxIPChi2": 25, 
        "MaxTrChi2Dof": 4.0, 
        "MaxTrGhp": 0.4, 
        "MaxVtxChi2Dof": 9, 
        "MinDira": 0.0, 
        "MinTau": 1.0, 
        "MinTrIPChi2": 25.0, 
        "MinVDChi2": 225, 
        "Postscale": 1, 
        "mDiffb": 500, 
        "mJpsiMax": 3200, 
        "mJpsiMin": 2990, 
        "mKstMax": 1200
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "RD" ]
}
"""

# this line didn't run in S24 nor S24r0p1 but it was included to reflect S29
B2KLLXInclusive = {
    "BUILDERTYPE": "B2KLLXInclusiveConf", 
    "CONFIG": {
        "CORRM_MAX": 15000.0, 
        "CORRM_MIN": 3000.0, 
        "DIRA": 0.995, 
        "Electron_MinIPCHI2": 16.0, 
        "Electron_PIDe": 1.0, 
        "Electron_PIDeK": 1.0, 
        "Electron_PT": 500, 
        "FlightChi2": 100.0, 
        "HLT_FILTER": "HLT_PASS_RE('Hlt2DiMuonDetachedDecision')|HLT_PASS_RE('Hlt2DiMuonDetachedHeavyDecision')|HLT_PASS_RE('Hlt2SingleMuonDecision')", 
        "IPCHI2": 9.0, 
        "Kaon_MinIPCHI2": 9.0, 
        "Kaon_PT": 500, 
        "Kaon_ProbNNK": 0.2, 
        "Kaon_ProbNNKpi": 0.1, 
        "LOWERMASS": 0.04, 
        "Muon_MinIPCHI2": 16.0, 
        "Muon_PIDmu": 0.0, 
        "Muon_PIDmuK": 0.0, 
        "Muon_PT": 500, 
        "SpdMult": 450, 
        "Track_CHI2nDOF": 3.0, 
        "Track_GhostProb": 0.5, 
        "UPPERMASS": 5000.0, 
        "VertexCHI2": 6.0, 
        "WS": False
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2KstTauTau = {
    "BUILDERTYPE": "B2KstTauXConf", 
    "CONFIG": {
        "AMAXDOCA_D": 0.2, 
        "APT_D": 800.0, 
        "B2DDSL_LinePostscale": 1, 
        "B2DDSL_LinePrescale": 1, 
        "B2KstMuMu_LinePostscale": 1, 
        "B2KstMuMu_LinePrescale": 1, 
        "B2KstMuMu_SameSign_LinePostscale": 1, 
        "B2KstMuMu_SameSign_LinePrescale": 1, 
        "B2KstTauMu_LinePostscale": 1, 
        "B2KstTauMu_LinePrescale": 1, 
        "B2KstTauMu_SameSign_LinePostscale": 1, 
        "B2KstTauMu_SameSign_LinePrescale": 0.5, 
        "B2KstTauTau_LinePostscale": 1, 
        "B2KstTauTau_LinePrescale": 1, 
        "B2KstTauTau_SameSign_LinePostscale": 1, 
        "B2KstTauTau_SameSign_LinePrescale": 1, 
        "B_COSDIRA_KMM": "0.999", 
        "DIRA_D": "0.99", 
        "DOCAMAX_KST_KMM": "0.15", 
        "FDCHI2OWNPV_KST_KMM": "120", 
        "FDCHI2_B": "80", 
        "FDCHI2_D": "16", 
        "FD_B_Max_KTM": "70", 
        "FD_B_Max_KTT": "40", 
        "FD_B_Mu": "3", 
        "FD_B_Mu_KMM": "3", 
        "FD_Kst_Mu_KMM": "3", 
        "IPCHI2_HAD_ALL_FINAL_STATE": "16", 
        "MASS_HIGH_B": 10000.0, 
        "MASS_HIGH_B_KMM": 10000.0, 
        "MASS_HIGH_D": 1900.0, 
        "MASS_HIGH_Dmother": 2030.0, 
        "MASS_HIGH_Ds": 1998.0, 
        "MASS_HIGH_Kst": "1100", 
        "MASS_LOW_B": 2000.0, 
        "MASS_LOW_B_KMM": 1500.0, 
        "MASS_LOW_D": 1840.0, 
        "MASS_LOW_Dmother": 1800.0, 
        "MASS_LOW_Ds": 1938.0, 
        "MASS_LOW_Kst": "700", 
        "MINIPCHI2_KST_KMM": "3", 
        "MINIPCHI2_K_KMM": "4", 
        "MINIPCHI2_MU_KMM": "4", 
        "MINIPCHI2_PI_KMM": "4", 
        "MaxPT_D": 800.0, 
        "PT_B_KMM": 2000.0, 
        "PT_B_KTM": 3000.0, 
        "PT_B_KTT": 3000.0, 
        "PT_D": 1000.0, 
        "PT_HAD_ALL_FINAL_STATE": "250", 
        "PT_Kst": "1000", 
        "PT_MU": "1000", 
        "PT_MU_KMM": "800", 
        "P_HAD_ALL_FINAL_STATE": "2000", 
        "RelatedInfoTools": [
            {
                "Location": "B2KstTauTau_MuonIsolationBDT", 
                "Type": "RelInfoBKsttautauMuonIsolationBDT", 
                "Variables": [
                    "BKSTTAUTAUMUONISOBDTFIRSTVALUETAUP", 
                    "BKSTTAUTAUMUONISOBDTSECONDVALUETAUP", 
                    "BKSTTAUTAUMUONISOBDTTHIRDVALUETAUP", 
                    "BKSTTAUTAUMUONISOBDTFIRSTVALUETAUM", 
                    "BKSTTAUTAUMUONISOBDTSECONDVALUETAUM", 
                    "BKSTTAUTAUMUONISOBDTTHIRDVALUETAUM"
                ]
            }, 
            {
                "Location": "B2KstTauTau_TauIsolationBDT", 
                "Type": "RelInfoBKsttautauTauIsolationBDT", 
                "Variables": [
                    "BKSTTAUTAUTAUISOBDTFIRSTVALUETAUP", 
                    "BKSTTAUTAUTAUISOBDTSECONDVALUETAUP", 
                    "BKSTTAUTAUTAUISOBDTTHIRDVALUETAUP", 
                    "BKSTTAUTAUTAUISOBDTFIRSTVALUETAUM", 
                    "BKSTTAUTAUTAUISOBDTSECONDVALUETAUM", 
                    "BKSTTAUTAUTAUISOBDTTHIRDVALUETAUM", 
                    "BKSTTAUTAUTAUISOBDTFIRSTVALUEKST", 
                    "BKSTTAUTAUTAUISOBDTSECONDVALUEKST", 
                    "BKSTTAUTAUTAUISOBDTTHIRDVALUEKST"
                ]
            }, 
            {
                "Location": "B2KstTauTau_TrackIsolationBDT", 
                "Type": "RelInfoBKsttautauTrackIsolationBDT", 
                "Variables": [
                    "BKSTTAUTAUTRKISOBDTFIRSTVALUETAUPPIM", 
                    "BKSTTAUTAUTRKISOBDTSECONDVALUETAUPPIM", 
                    "BKSTTAUTAUTRKISOBDTTHIRDVALUETAUPPIM", 
                    "BKSTTAUTAUTRKISOBDTFIRSTVALUETAUPPIP1", 
                    "BKSTTAUTAUTRKISOBDTSECONDVALUETAUPPIP1", 
                    "BKSTTAUTAUTRKISOBDTTHIRDVALUETAUPPIP1", 
                    "BKSTTAUTAUTRKISOBDTFIRSTVALUETAUPPIP2", 
                    "BKSTTAUTAUTRKISOBDTSECONDVALUETAUPPIP2", 
                    "BKSTTAUTAUTRKISOBDTTHIRDVALUETAUPPIP2", 
                    "BKSTTAUTAUTRKISOBDTFIRSTVALUETAUMPIP", 
                    "BKSTTAUTAUTRKISOBDTSECONDVALUETAUMPIP", 
                    "BKSTTAUTAUTRKISOBDTTHIRDVALUETAUMPIP", 
                    "BKSTTAUTAUTRKISOBDTFIRSTVALUETAUMPIM1", 
                    "BKSTTAUTAUTRKISOBDTSECONDVALUETAUMPIM1", 
                    "BKSTTAUTAUTRKISOBDTTHIRDVALUETAUMPIM1", 
                    "BKSTTAUTAUTRKISOBDTFIRSTVALUETAUMPIM2", 
                    "BKSTTAUTAUTRKISOBDTSECONDVALUETAUMPIM2", 
                    "BKSTTAUTAUTRKISOBDTTHIRDVALUETAUMPIM2", 
                    "BKSTTAUTAUTRKISOBDTFIRSTVALUEKSTK", 
                    "BKSTTAUTAUTRKISOBDTSECONDVALUEKSTK", 
                    "BKSTTAUTAUTRKISOBDTTHIRDVALUEKSTK", 
                    "BKSTTAUTAUTRKISOBDTFIRSTVALUEKSTPI", 
                    "BKSTTAUTAUTRKISOBDTSECONDVALUEKSTPI", 
                    "BKSTTAUTAUTRKISOBDTTHIRDVALUEKSTPI"
                ]
            }, 
            {
                "Location": "B2KstTauTau_CDFIso", 
                "Type": "RelInfoBstautauCDFIso"
            }
        ], 
        "SpdMult": "600", 
        "TRACKCHI2_HAD_ALL_FINAL_STATE": "4", 
        "TRACKCHI2_MU": "4", 
        "TRGHOPROB_HAD_ALL_FINAL_STATE": "0.4", 
        "VCHI2_B": "100", 
        "VCHI2_B_Mu": "150", 
        "VCHI2_B_Mu_KMM": "100", 
        "VCHI2_D": "16", 
        "VCHI2_Kst": "15", 
        "VDRHOmax_D": 7.0, 
        "VDRHOmin_D": 0.1, 
        "VDZ_D": 5.0
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "RD" ]
}

B2LLXBDT = {
    "BUILDERTYPE": "B2LLXBDTConf", 
    "CONFIG": {
        "BComCuts": "(in_range(3.7*GeV, AM, 6.8*GeV))", 
        "BMomCuts": "(in_range(4.0*GeV,  M, 6.5*GeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.999) & (BPVDLS>0) & (BPVIPCHI2()<400)", 
        "Bd2LLKsXmlFile": "$TMVAWEIGHTSROOT/data/Bd2eeKs_BDT_v1r0.xml", 
        "Bd2LLKstarXmlFile": "$TMVAWEIGHTSROOT/data/Bd2eeKstar_BDT_v1r0.xml", 
        "Bd2eeKsMVACut": "-0.07", 
        "Bd2eeKstarMVACut": "-0.05", 
        "Bd2mumuKsMVACut": "-0.07", 
        "Bd2mumuKstarMVACut": "-0.05", 
        "Bs2LLPhiXmlFile": "$TMVAWEIGHTSROOT/data/Bs2eePhi_BDT_v1r0.xml", 
        "Bs2eePhiMVACut": "-0.06", 
        "Bs2mumuPhiMVACut": "-0.08", 
        "Bu2LLKXmlFile": "$TMVAWEIGHTSROOT/data/Bu2eeK_BDT_v1r0.xml", 
        "Bu2eeKMVACut": "-0.05", 
        "Bu2mumuKMVACut": "-0.05", 
        "DiElectronCuts": "\n                           (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n                           & (INTREE( (ID=='e+') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (PIDe>-2) & (TRGHOSTPROB<0.5) ))\n                           & (INTREE( (ID=='e-') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (PIDe>-2) & (TRGHOSTPROB<0.5) ))\n                          ", 
        "DiMuonCuts": "\n                           (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<5.0*GeV)\n                           & (INTREE( (ID=='mu+') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) ))\n                           & (INTREE( (ID=='mu-') & (PT>200*MeV) & (MIPCHI2DV(PRIMARY)>1.) & (TRGHOSTPROB<0.5) ))\n                          ", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "KsDDCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVVDCHI2>25)", 
        "KsLLComCuts": "(ADAMASS('KS0') < 50.*MeV) & (ADOCACHI2CUT(25, ''))", 
        "KsLLCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)", 
        "KstarCuts": "(VFASPF(VCHI2/VDOF)<16) & (ADMASS('K*(892)0')< 300*MeV)", 
        "LambdaDDCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25)", 
        "LambdaLLComCuts": "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LambdaLLCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)", 
        "LambdastarComCuts": "(AM < 5.6*GeV)", 
        "LambdastarCuts": "(VFASPF(VCHI2) < 25.)", 
        "Lb2LLLambdaXmlFile": "$TMVAWEIGHTSROOT/data/Lb2eeLambda_BDT_v1r0.xml", 
        "Lb2LLPKXmlFile": "$TMVAWEIGHTSROOT/data/Lb2eePK_BDT_v1r0.xml", 
        "Lb2eeLambdaMVACut": "-0.11", 
        "Lb2eePKMVACut": "-0.05", 
        "Lb2mumuLambdaMVACut": "-0.15", 
        "Lb2mumuPKMVACut": "-0.11", 
        "LbComCuts": "(in_range(3.7*GeV, AM, 7.1*GeV))", 
        "LbMomCuts": "(in_range(4.0*GeV,  M, 6.8*GeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.999) & (BPVDLS>0) & (BPVIPCHI2()<400)", 
        "PhiCuts": "\n                          (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<1.05*GeV) & (MIPCHI2DV(PRIMARY)>2.)\n                          & (INTREE( (ID=='K+') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          & (INTREE( (ID=='K-') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          ", 
        "Pion4LPCuts": "(PROBNNpi> 0.2) & (PT>100*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>9.)", 
        "PionCuts": "(PROBNNpi> 0.2) & (PT>250*MeV) & (TRGHOSTPROB<0.4)", 
        "ProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "RelatedInfoTools": [
            {
                "Location": "VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VertexIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  StableCharged ^StableCharged)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^StableCharged  StableCharged)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l-)]CC": "TrackIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  StableCharged ^StableCharged)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^StableCharged  StableCharged)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  StableCharged  StableCharged)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  StableCharged  StableCharged)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l-)]CC": "ConeIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  StableCharged ^StableCharged)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^StableCharged  StableCharged)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  StableCharged ^StableCharged)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^StableCharged  StableCharged)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations"
            }
        ]
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2Lambda0Mu = {
    "BUILDERTYPE": "B2Lambda0MuLines", 
    "CONFIG": {
        "BDIRA": 0.99, 
        "BVCHI2DOF": 4.0, 
        "GEC_nLongTrk": 300.0, 
        "Lambda0DaugMIPChi2": 10.0, 
        "Lambda0DaugP": 2000.0, 
        "Lambda0DaugPT": 250.0, 
        "Lambda0DaugTrackChi2": 4.0, 
        "Lambda0PT": 700.0, 
        "Lambda0VertexChi2": 10.0, 
        "LambdaMuMassLowTight": 1500.0, 
        "LambdaZ": 0.0, 
        "MajoranaCutFDChi2": 100.0, 
        "MajoranaCutM": 1500.0, 
        "MuonGHOSTPROB": 0.5, 
        "MuonMINIPCHI2": 12, 
        "MuonP": 3000.0, 
        "MuonPIDK": 0.0, 
        "MuonPIDmu": 0.0, 
        "MuonPIDp": 0.0, 
        "MuonPT": 250.0, 
        "MuonTRCHI2": 4.0, 
        "XMuMassUpperHigh": 6500.0
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

# this line didn't run in S24 nor S24r0p1 but it was included to reflect S29
B2MuMuMuMu = {
    "BUILDERTYPE": "B2MuMuMuMuLinesConf", 
    "CONFIG": {
        "B23MuPiLinePostscale": 1, 
        "B23MuPiLinePrescale": 1, 
        "B2DetachedDiMuons": {
            "BPVDIRA_MIN": 0.0, 
            "BPVIPCHI2_MAX": 16, 
            "BPVVDCHI2_MIN": 50, 
            "MASS_MAX": {
                "B": "6000*MeV"
            }, 
            "MASS_MIN": {
                "B": "4600*MeV"
            }, 
            "SUMPT_MIN": "2000*MeV", 
            "VCHI2DOF_MAX": 6
        }, 
        "B2DetachedDimuonAndJpsiLinePostscale": 1, 
        "B2DetachedDimuonAndJpsiLinePrescale": 1, 
        "B2JpsiKmumuLinePostscale": 1, 
        "B2JpsiKmumuLinePrescale": 1, 
        "B2JpsiPhimumuLinePostscale": 1, 
        "B2JpsiPhimumuLinePrescale": 1, 
        "B2MuMuMuMuLinePostscale": 1, 
        "B2MuMuMuMuLinePrescale": 1, 
        "B2TwoDetachedDimuonLinePostscale": 1, 
        "B2TwoDetachedDimuonLinePrescale": 1, 
        "D2MuMuMuMuLinePostscale": 1, 
        "D2MuMuMuMuLinePrescale": 1, 
        "DetachedDiMuons": {
            "AMAXDOCA_MAX": "0.5*mm", 
            "ASUMPT_MIN": "1000*MeV", 
            "BPVVDCHI2_MIN": 16, 
            "VCHI2DOF_MAX": 16
        }
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}


B2XLL = {
    "BUILDERTYPE": "B2XLLConf", 
    "CONFIG": {
        "BDIRA": 0.9995, 
        "BFlightCHI2": 100, 
        "BIPCHI2": 25, 
        "BMassWindow": 1500, 
        "BVertexCHI2": 9, 
        "DiHadronMass": 2600, 
        "DiLeptonFDCHI2": 16, 
        "DiLeptonIPCHI2": 0, 
        "DiLeptonPT": 0, 
        "KaonIPCHI2": 9, 
        "KaonPT": 400, 
        "LeptonIPCHI2": 9, 
        "LeptonPT": 300, 
        "ProbNNe": 0.05, 
        "ProbNNk": 0.05, 
        "ProbNNmu": 0.05, 
        "ProbNNp": 0.05, 
        "ProbNNpi": 0.95, 
        "TrChi2DOF": 4, 
        "TrGhostProb": 0.4, 
        "UpperMass": 5500, 
        "eeXLinePrescale": 1, 
        "eeXSSLinePrescale": 1, 
        "meXLinePrescale": 1, 
        "meXSSLinePrescale": 1, 
        "mmXLinePrescale": 1, 
        "mmXSSLinePrescale": 1
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2XMuMu = {
    "BUILDERTYPE": "B2XMuMuConf", 
    "CONFIG": {
        "A1_Comb_MassHigh": 4200.0, 
        "A1_Comb_MassLow": 0.0, 
        "A1_Dau_MaxIPCHI2": 16.0, 
        "A1_FlightChi2": 36.0, 
        "A1_MassHigh": 4000.0, 
        "A1_MassLow": 0.0, 
        "A1_MinIPCHI2": 4.0, 
        "A1_VtxChi2": 8.0, 
        "B_Comb_MassHigh": 7100.0, 
        "B_Comb_MassLow": 4800.0, 
        "B_DIRA": 0.9999, 
        "B_Dau_MaxIPCHI2": 9.0, 
        "B_FlightCHI2": 121.0, 
        "B_IPCHI2": 16.0, 
        "B_MassHigh": 7000.0, 
        "B_MassLow": 4900.0, 
        "B_VertexCHI2": 8.0, 
        "DECAYS": [
            "B0 -> J/psi(1S) phi(1020)", 
            "[B0 -> J/psi(1S) K*(892)0]cc", 
            "B0 -> J/psi(1S) rho(770)0", 
            "[B+ -> J/psi(1S) rho(770)+]cc", 
            "B0 -> J/psi(1S) f_2(1950)", 
            "B0 -> J/psi(1S) KS0", 
            "[B0 -> J/psi(1S) D~0]cc", 
            "[B+ -> J/psi(1S) K+]cc", 
            "[B+ -> J/psi(1S) pi+]cc", 
            "[B+ -> J/psi(1S) K*(892)+]cc", 
            "[B+ -> J/psi(1S) D+]cc", 
            "[B+ -> J/psi(1S) D*(2010)+]cc", 
            "[Lambda_b0 -> J/psi(1S) Lambda0]cc", 
            "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc", 
            "B0 -> J/psi(1S) pi0", 
            "[B+ -> J/psi(1S) a_1(1260)+]cc", 
            "[B+ -> J/psi(1S) K_1(1270)+]cc", 
            "[B+ -> J/psi(1S) K_2(1770)+]cc", 
            "B0 -> J/psi(1S) K_1(1270)0", 
            "[B+ -> J/psi(1S) K_1(1400)+]cc", 
            "B0 -> J/psi(1S) K_1(1400)0", 
            "[Xi_b- -> J/psi(1S) Xi-]cc", 
            "[Omega_b- -> J/psi(1S) Omega-]cc"
        ], 
        "Dau_DIRA": -0.9, 
        "Dau_VertexCHI2": 12.0, 
        "Dimu_Dau_MaxIPCHI2": 9.0, 
        "Dimu_FlightChi2": 9.0, 
        "DimuonUPPERMASS": 7100.0, 
        "DimuonWS": True, 
        "HLT1_FILTER": None, 
        "HLT2_FILTER": None, 
        "HadronWS": False, 
        "Hadron_MinIPCHI2": 6.0, 
        "HyperonCombWindow": 65.0, 
        "HyperonMaxDocaChi2": 20.0, 
        "HyperonWindow": 50.0, 
        "K12OmegaK_CombMassHigh": 2000, 
        "K12OmegaK_CombMassLow": 400, 
        "K12OmegaK_MassHigh": 2100, 
        "K12OmegaK_MassLow": 300, 
        "K12OmegaK_VtxChi2": 10, 
        "KpiVXCHI2NDOF": 8.0, 
        "KsWINDOW": 30.0, 
        "Kstar_Comb_MassHigh": 6200.0, 
        "Kstar_Comb_MassLow": 0.0, 
        "Kstar_Dau_MaxIPCHI2": 9.0, 
        "Kstar_FlightChi2": 16.0, 
        "Kstar_MassHigh": 6200.0, 
        "Kstar_MassLow": 0.0, 
        "Kstar_MinIPCHI2": 0.0, 
        "KstarplusWINDOW": 300.0, 
        "L0DU_FILTER": None, 
        "LambdaWINDOW": 30.0, 
        "LongLivedPT": 0.0, 
        "LongLivedTau": 2, 
        "MuonNoPIDs_PIDmu": 0.0, 
        "MuonPID": -3.0, 
        "Muon_IsMuon": True, 
        "Muon_MinIPCHI2": 9.0, 
        "OmegaChi2Prob": 1e-05, 
        "Omega_CombMassWin": 150, 
        "Omega_MassWin": 100, 
        "Pi0ForOmegaMINPT": 500.0, 
        "Pi0MINPT": 800.0, 
        "RelatedInfoTools": [
            {
                "Location": "KSTARMUMUVARIABLES", 
                "Type": "RelInfoBKstarMuMuBDT", 
                "Variables": [
                    "MU_SLL_ISO_1", 
                    "MU_SLL_ISO_2"
                ]
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM", 
                    "CONEPT", 
                    "CONEP", 
                    "CONEPASYM", 
                    "CONEDELTAETA", 
                    "CONEDELTAPHI"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation", 
                "Variables": [
                    "VTXISONUMVTX", 
                    "VTXISODCHI2ONETRACK", 
                    "VTXISODCHI2MASSONETRACK", 
                    "VTXISODCHI2TWOTRACK", 
                    "VTXISODCHI2MASSTWOTRACK"
                ]
            }, 
            {
                "Location": "VtxIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "SpdMult": 600, 
        "Track_GhostProb": 0.5, 
        "UseNoPIDsHadrons": True
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

B2XTau = {
    "BUILDERTYPE": "B2XTauConf", 
    "CONFIG": {
        "AMAXDOCA_D": 0.2, 
        "APT_D": 800.0, 
        "B2DD_LinePostscale": 1, 
        "B2DD_LinePrescale": 1, 
        "B2DD_SameSign_LinePostscale": 1, 
        "B2DD_SameSign_LinePrescale": 0.5, 
        "B2DPi_LinePostscale": 1, 
        "B2DPi_LinePrescale": 1, 
        "B2DPi_SameSign_LinePostscale": 1, 
        "B2DPi_SameSign_LinePrescale": 0.5, 
        "B2TauMu_SameSign_TOSLinePostscale": 1, 
        "B2TauMu_SameSign_TOSLinePrescale": 0.5, 
        "B2TauMu_TOSLinePostscale": 1, 
        "B2TauMu_TOSLinePrescale": 1, 
        "B2TauTau_SameSign_TOSLinePostscale": 1, 
        "B2TauTau_SameSign_TOSLinePrescale": 1, 
        "B2TauTau_TOSLinePostscale": 1, 
        "B2TauTau_TOSLinePrescale": 1, 
        "B_TAUPI_2NDMINIPS": "20", 
        "DIRA_B": "0.99", 
        "DIRA_D": "0.99", 
        "FDCHI2_B": "225", 
        "FDCHI2_D": "16", 
        "FDCHI2_TAU": "4000", 
        "FD_B": "90", 
        "FD_B_MU": "35", 
        "HLT_DECISIONS_HAD": {
            "Hlt2Topo(2|3|4)Body.*Decision%TOS": 0
        }, 
        "HLT_DECISIONS_MUON": {
            "Hlt2(TopoMu|SingleMuon).*Decision%TOS": 0
        }, 
        "IPCHI2_B_CHILD_BEST": "16", 
        "IPCHI2_B_MU": "200", 
        "IPCHI2_B_TAU_CHILD_BEST": "150", 
        "IPCHI2_B_TAU_CHILD_WORSE": "16", 
        "IPCHI2_B_TAU_MU": "50", 
        "IPCHI2_HAD_ALL_FINAL_STATE": "16", 
        "IPCHI2_MU": "16", 
        "MASS_HIGH_B": "7000", 
        "MASS_HIGH_D": 2080.0, 
        "MASS_HIGH_Dmother": 2030.0, 
        "MASS_HIGH_Ds": 1998.0, 
        "MASS_HIGH_TAU": 2100.0, 
        "MASS_HIGH_TAUmother": 2000.0, 
        "MASS_LOW_B": "2000", 
        "MASS_LOW_D": 1750.0, 
        "MASS_LOW_Dmother": 1800.0, 
        "MASS_LOW_Ds": 1938.0, 
        "MASS_LOW_TAU": 400.0, 
        "MASS_LOW_TAUmother": 500.0, 
        "MCOR_HIGH_B": "10000", 
        "MCOR_LOW_B": "0", 
        "MaxPT_D": 800.0, 
        "PT_B_CHILD_BEST": "2000", 
        "PT_B_MU_PIONS_TOTAL": "2500", 
        "PT_B_PIONS_TOTAL": "7000", 
        "PT_B_TAU_CHILD_BEST": "4000", 
        "PT_B_TM": "1900", 
        "PT_B_TM_HIGH": "5000", 
        "PT_B_TT": "1900", 
        "PT_B_TT_HIGH": "2000", 
        "PT_D": 1000.0, 
        "PT_HAD_ALL_FINAL_STATE": "250", 
        "PT_MU": "1000", 
        "P_HAD_ALL_FINAL_STATE": "2000", 
        "P_MU": "6000", 
        "RelatedInfoTools": [
            {
                "Location": "MuonIsolationBDT", 
                "Type": "RelInfoBstautauMuonIsolationBDT", 
                "Variables": [
                    "BSTAUTAUMUONISOBDTFIRSTVALUE", 
                    "BSTAUTAUMUONISOBDTSECONDVALUE", 
                    "BSTAUTAUMUONISOBDTTHIRDVALUE"
                ]
            }, 
            {
                "Location": "MuonIsolation", 
                "Type": "RelInfoBstautauMuonIsolation", 
                "Variables": [
                    "BSTAUTAUMUONISOFIRSTVALUE", 
                    "BSTAUTAUMUONISOSECONDVALUE"
                ]
            }, 
            {
                "Location": "TauIsolationBDT", 
                "Type": "RelInfoBstautauTauIsolationBDT", 
                "Variables": [
                    "BSTAUTAUTAUISOBDTFIRSTVALUETAUP", 
                    "BSTAUTAUTAUISOBDTSECONDVALUETAUP", 
                    "BSTAUTAUTAUISOBDTTHIRDVALUETAUP", 
                    "BSTAUTAUTAUISOBDTFIRSTVALUETAUM", 
                    "BSTAUTAUTAUISOBDTSECONDVALUETAUM", 
                    "BSTAUTAUTAUISOBDTTHIRDVALUETAUM"
                ]
            }, 
            {
                "Location": "TauIsolation", 
                "Type": "RelInfoBstautauTauIsolation", 
                "Variables": [
                    "BSTAUTAUTAUISOFIRSTVALUETAUP", 
                    "BSTAUTAUTAUISOSECONDVALUETAUP", 
                    "BSTAUTAUTAUISOFIRSTVALUETAUM", 
                    "BSTAUTAUTAUISOSECONDVALUETAUM"
                ]
            }, 
            {
                "Location": "TrackIsolationBDT", 
                "Type": "RelInfoBstautauTrackIsolationBDT", 
                "Variables": [
                    "BSTAUTAUTRKISOBDTFIRSTVALUETAUPPIM", 
                    "BSTAUTAUTRKISOBDTSECONDVALUETAUPPIM", 
                    "BSTAUTAUTRKISOBDTTHIRDVALUETAUPPIM", 
                    "BSTAUTAUTRKISOBDTFIRSTVALUETAUPPIP1", 
                    "BSTAUTAUTRKISOBDTSECONDVALUETAUPPIP1", 
                    "BSTAUTAUTRKISOBDTTHIRDVALUETAUPPIP1", 
                    "BSTAUTAUTRKISOBDTFIRSTVALUETAUPPIP2", 
                    "BSTAUTAUTRKISOBDTSECONDVALUETAUPPIP2", 
                    "BSTAUTAUTRKISOBDTTHIRDVALUETAUPPIP2", 
                    "BSTAUTAUTRKISOBDTFIRSTVALUETAUMPIP", 
                    "BSTAUTAUTRKISOBDTSECONDVALUETAUMPIP", 
                    "BSTAUTAUTRKISOBDTTHIRDVALUETAUMPIP", 
                    "BSTAUTAUTRKISOBDTFIRSTVALUETAUMPIM1", 
                    "BSTAUTAUTRKISOBDTSECONDVALUETAUMPIM1", 
                    "BSTAUTAUTRKISOBDTTHIRDVALUETAUMPIM1", 
                    "BSTAUTAUTRKISOBDTFIRSTVALUETAUMPIM2", 
                    "BSTAUTAUTRKISOBDTSECONDVALUETAUMPIM2", 
                    "BSTAUTAUTRKISOBDTTHIRDVALUETAUMPIM2"
                ]
            }, 
            {
                "Location": "TrackIsolation", 
                "Type": "RelInfoBstautauTrackIsolation", 
                "Variables": [
                    "BSTAUTAUTRKISOFIRSTVALUETAUPPIM", 
                    "BSTAUTAUTRKISOFIRSTVALUETAUPPIP1", 
                    "BSTAUTAUTRKISOFIRSTVALUETAUPPIP2", 
                    "BSTAUTAUTRKISOFIRSTVALUETAUMPIP", 
                    "BSTAUTAUTRKISOFIRSTVALUETAUMPIM1", 
                    "BSTAUTAUTRKISOFIRSTVALUETAUMPIM2"
                ]
            }, 
            {
                "Location": "CDFIso", 
                "Type": "RelInfoBstautauCDFIso", 
                "Variables": [ "BSTAUTAUCDFISO" ]
            }, 
            {
                "Location": "ZVisoBDT", 
                "Type": "RelInfoBstautauZVisoBDT", 
                "Variables": [
                    "ZVISOTAUP", 
                    "ZVISOTAUM"
                ]
            }
        ], 
        "TRACKCHI2_HAD_ALL_FINAL_STATE": "4", 
        "TRACKCHI2_MU": "4", 
        "TRGHOPROB_HAD_ALL_FINAL_STATE": "0.4", 
        "TRGHOPROB_MU": "0.4", 
        "VCHI2_B": "90", 
        "VCHI2_B_TAU_MU": "12", 
        "VCHI2_D": "16", 
        "VDRHOmax_D": 7.0, 
        "VDRHOmin_D": 0.1, 
        "VDZ_D": 5.0
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "RD" ]
}

B2XTauMu = {
    "BUILDERTYPE": "B2XTauMuConf", 
    "CONFIG": {
        "BDIRA_K": 0.99, 
        "BDIRA_K_3pi": 0.999, 
        "BDIRA_kst": 0.95, 
        "BVCHI2DOF_K": 15.0, 
        "BVCHI2DOF_K_3pi": 4.0, 
        "BVCHI2DOF_kst": 25.0, 
        "BVCHI2DOF_phi": 10000000000.0, 
        "B_FDCHI2_K": 400.0, 
        "B_FDCHI2_kst": 80, 
        "B_MAX_MASS": 10000, 
        "B_MAX_MASS_K_3pi": 10000, 
        "B_MIN_MASS": 2000, 
        "B_MIN_MASS_K_3pi": 3000, 
        "B_PT_K": 3000.0, 
        "KMuPT_K": 1000.0, 
        "KMuSumPT_K": 2000.0, 
        "KPiPT_K": 800.0, 
        "KPiPT_kst": 500.0, 
        "KaonPIDK": 4.0, 
        "KaonPIDK_K": 5, 
        "KaonPIDK_K_3pi": 6, 
        "KaonP_K": 3.0, 
        "KaonP_K_3pi": 6.0, 
        "KaonP_kst": 2.0, 
        "KstAMassWin": 180.0, 
        "KstMassWin": 150.0, 
        "KstVCHI2DOF": 15.0, 
        "MINIPCHI2": 16.0, 
        "MINIPCHI2_K": 36.0, 
        "MuTauWS": True, 
        "MuonPT_K": 800.0, 
        "MuonPT_kst": 500.0, 
        "MuonP_K": 5.0, 
        "MuonP_K_3pi": 6.0, 
        "MuonP_kst": 2.0, 
        "PIDmu": 2.0, 
        "PhiAMassWin": 30.0, 
        "PhiMassWin": 25.0, 
        "PhiVCHI2DOF": 20.0, 
        "PionPIDK": 0.0, 
        "PionP_kst": 2.0, 
        "Prescale": 1, 
        "Prescale_WS": 0.5, 
        "RelatedInfoTools": [
            {
                "Location": "VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VertexIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "ConeVarsInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty -> (X+ -> (X0 -> X+ ^X-) l+) l+]CC ": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ -> (X0 -> X+ ^X-) l+) l-]CC ": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ -> (X0 -> ^X+ X-) l+) l+]CC ": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> (X0 -> ^X+ X-) l+) l-]CC ": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> X+ ^l+) l+]CC ": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ -> X+ ^l+) l-]CC ": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ -> X+ l+) ^l+]CC ": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ -> X+ l+) ^l-]CC ": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ -> X0 ^l+) l+]CC ": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ -> X0 ^l+) l-]CC ": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ -> X0 l+) ^l+]CC ": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ -> X0 l+) ^l-]CC ": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ -> ^X+ l+) l+]CC ": "ConeIsoInfoH", 
                    "[Beauty -> (X+ -> ^X+ l+) l-]CC ": "ConeIsoInfoH", 
                    "[Beauty -> (X0 -> X+ ^l-) l+]CC ": "ConeIsoInfoL1", 
                    "[Beauty -> (X0 -> X+ ^l-) l-]CC ": "ConeIsoInfoL1", 
                    "[Beauty -> (X0 -> X+ l-) ^l+]CC ": "ConeIsoInfoL2", 
                    "[Beauty -> (X0 -> X+ l-) ^l-]CC ": "ConeIsoInfoL2", 
                    "[Beauty -> (X0 -> ^X+ l-) l+]CC ": "ConeIsoInfoH", 
                    "[Beauty -> (X0 -> ^X+ l-) l-]CC ": "ConeIsoInfoH", 
                    "[Beauty -> (X0-> X+ X-) ^l+ l+]CC ": "ConeIsoInfoL1", 
                    "[Beauty -> (X0-> X+ X-) ^l+ l-]CC ": "ConeIsoInfoL1", 
                    "[Beauty -> (X0-> X+ X-) l+ ^l+]CC ": "ConeIsoInfoL2", 
                    "[Beauty -> (X0-> X+ X-) l+ ^l-]CC ": "ConeIsoInfoL2", 
                    "[Beauty -> (X0-> X+ ^X-) l+ l+]CC ": "ConeIsoInfoH1", 
                    "[Beauty -> (X0-> X+ ^X-) l+ l-]CC ": "ConeIsoInfoH1", 
                    "[Beauty -> (X0-> ^X+ X-) l+ l+]CC ": "ConeIsoInfoH1", 
                    "[Beauty -> (X0-> ^X+ X-) l+ l-]CC ": "ConeIsoInfoH1"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[Beauty -> (X+ -> (X0 -> X+ ^X-) l+) l+]CC ": "ConeVarsInfoH2", 
                    "[Beauty -> (X+ -> (X0 -> X+ ^X-) l+) l-]CC ": "ConeVarsInfoH2", 
                    "[Beauty -> (X+ -> (X0 -> ^X+ X-) l+) l+]CC ": "ConeVarsInfoH1", 
                    "[Beauty -> (X+ -> (X0 -> ^X+ X-) l+) l-]CC ": "ConeVarsInfoH1", 
                    "[Beauty -> (X+ -> X+ ^l+) l+]CC ": "ConeVarsInfoL1", 
                    "[Beauty -> (X+ -> X+ ^l+) l-]CC ": "ConeVarsInfoL1", 
                    "[Beauty -> (X+ -> X+ l+) ^l+]CC ": "ConeVarsInfoL2", 
                    "[Beauty -> (X+ -> X+ l+) ^l-]CC ": "ConeVarsInfoL2", 
                    "[Beauty -> (X+ -> X0 ^l+) l+]CC ": "ConeVarsInfoL1", 
                    "[Beauty -> (X+ -> X0 ^l+) l-]CC ": "ConeVarsInfoL1", 
                    "[Beauty -> (X+ -> X0 l+) ^l+]CC ": "ConeVarsInfoL2", 
                    "[Beauty -> (X+ -> X0 l+) ^l-]CC ": "ConeVarsInfoL2", 
                    "[Beauty -> (X+ -> ^X+ l+) l+]CC ": "ConeVarsInfoH", 
                    "[Beauty -> (X+ -> ^X+ l+) l-]CC ": "ConeVarsInfoH", 
                    "[Beauty -> (X0 -> X+ ^l-) l+]CC ": "ConeVarsInfoL1", 
                    "[Beauty -> (X0 -> X+ ^l-) l-]CC ": "ConeVarsInfoL1", 
                    "[Beauty -> (X0 -> X+ l-) ^l+]CC ": "ConeVarsInfoL2", 
                    "[Beauty -> (X0 -> X+ l-) ^l-]CC ": "ConeVarsInfoL2", 
                    "[Beauty -> (X0 -> ^X+ l-) l+]CC ": "ConeVarsInfoH", 
                    "[Beauty -> (X0 -> ^X+ l-) l-]CC ": "ConeVarsInfoH", 
                    "[Beauty -> (X0-> X+ X-) ^l+ l+]CC ": "ConeVarsInfoL1", 
                    "[Beauty -> (X0-> X+ X-) ^l+ l-]CC ": "ConeVarsInfoL1", 
                    "[Beauty -> (X0-> X+ X-) l+ ^l+]CC ": "ConeVarsInfoL2", 
                    "[Beauty -> (X0-> X+ X-) l+ ^l-]CC ": "ConeVarsInfoL2", 
                    "[Beauty -> (X0-> X+ ^X-) l+ l+]CC ": "ConeVarsInfoH1", 
                    "[Beauty -> (X0-> X+ ^X-) l+ l-]CC ": "ConeVarsInfoH1", 
                    "[Beauty -> (X0-> ^X+ X-) l+ l+]CC ": "ConeVarsInfoH1", 
                    "[Beauty -> (X0-> ^X+ X-) l+ l-]CC ": "ConeVarsInfoH1"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty -> (X+ -> (X0 -> X+ ^X-) l+) l+]CC ": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ -> (X0 -> X+ ^X-) l+) l-]CC ": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ -> (X0 -> ^X+ X-) l+) l+]CC ": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> (X0 -> ^X+ X-) l+) l-]CC ": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> X+ ^l+) l+]CC ": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ -> X+ ^l+) l-]CC ": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ -> X+ l+) ^l+]CC ": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ -> X+ l+) ^l-]CC ": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ -> X0 ^l+) l+]CC ": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ -> X0 ^l+) l-]CC ": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ -> X0 l+) ^l+]CC ": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ -> X0 l+) ^l-]CC ": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ -> ^X+ l+) l+]CC ": "TrackIsoBDTInfoH", 
                    "[Beauty -> (X+ -> ^X+ l+) l-]CC ": "TrackIsoBDTInfoH", 
                    "[Beauty -> (X0 -> X+ ^l-) l+]CC ": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0 -> X+ ^l-) l-]CC ": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0 -> X+ l-) ^l+]CC ": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0 -> X+ l-) ^l-]CC ": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0 -> ^X+ l-) l+]CC ": "TrackIsoBDTInfoH", 
                    "[Beauty -> (X0 -> ^X+ l-) l-]CC ": "TrackIsoBDTInfoH", 
                    "[Beauty -> (X0-> X+ X-) ^l+ l+]CC ": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0-> X+ X-) ^l+ l-]CC ": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0-> X+ X-) l+ ^l+]CC ": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0-> X+ X-) l+ ^l-]CC ": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0-> X+ ^X-) l+ l+]CC ": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X0-> X+ ^X-) l+ l-]CC ": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X0-> ^X+ X-) l+ l+]CC ": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X0-> ^X+ X-) l+ l-]CC ": "TrackIsoBDTInfoH1"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty -> (X+ -> (X0 -> X+ ^X-) l+) l+]CC ": "TrackIsoBsMMInfoH2", 
                    "[Beauty -> (X+ -> (X0 -> X+ ^X-) l+) l-]CC ": "TrackIsoBsMMInfoH2", 
                    "[Beauty -> (X+ -> (X0 -> ^X+ X-) l+) l+]CC ": "TrackIsoBsMMInfoH1", 
                    "[Beauty -> (X+ -> (X0 -> ^X+ X-) l+) l-]CC ": "TrackIsoBsMMInfoH1", 
                    "[Beauty -> (X+ -> X+ ^l+) l+]CC ": "TrackIsoBsMMInfoL1", 
                    "[Beauty -> (X+ -> X+ ^l+) l-]CC ": "TrackIsoBsMMInfoL1", 
                    "[Beauty -> (X+ -> X+ l+) ^l+]CC ": "TrackIsoBsMMInfoL2", 
                    "[Beauty -> (X+ -> X+ l+) ^l-]CC ": "TrackIsoBsMMInfoL2", 
                    "[Beauty -> (X+ -> X0 ^l+) l+]CC ": "TrackIsoBsMMInfoL1", 
                    "[Beauty -> (X+ -> X0 ^l+) l-]CC ": "TrackIsoBsMMInfoL1", 
                    "[Beauty -> (X+ -> X0 l+) ^l+]CC ": "TrackIsoBsMMInfoL2", 
                    "[Beauty -> (X+ -> X0 l+) ^l-]CC ": "TrackIsoBsMMInfoL2", 
                    "[Beauty -> (X+ -> ^X+ l+) l+]CC ": "TrackIsoBsMMInfoH", 
                    "[Beauty -> (X+ -> ^X+ l+) l-]CC ": "TrackIsoBsMMInfoH", 
                    "[Beauty -> (X0 -> X+ ^l-) l+]CC ": "TrackIsoBsMMInfoL1", 
                    "[Beauty -> (X0 -> X+ ^l-) l-]CC ": "TrackIsoBsMMInfoL1", 
                    "[Beauty -> (X0 -> X+ l-) ^l+]CC ": "TrackIsoBsMMInfoL2", 
                    "[Beauty -> (X0 -> X+ l-) ^l-]CC ": "TrackIsoBsMMInfoL2", 
                    "[Beauty -> (X0 -> ^X+ l-) l+]CC ": "TrackIsoBsMMInfoH", 
                    "[Beauty -> (X0 -> ^X+ l-) l-]CC ": "TrackIsoBsMMInfoH", 
                    "[Beauty -> (X0-> X+ X-) ^l+ l+]CC ": "TrackIsoBsMMInfoL1", 
                    "[Beauty -> (X0-> X+ X-) ^l+ l-]CC ": "TrackIsoBsMMInfoL1", 
                    "[Beauty -> (X0-> X+ X-) l+ ^l+]CC ": "TrackIsoBsMMInfoL2", 
                    "[Beauty -> (X0-> X+ X-) l+ ^l-]CC ": "TrackIsoBsMMInfoL2", 
                    "[Beauty -> (X0-> X+ ^X-) l+ l+]CC ": "TrackIsoBsMMInfoH1", 
                    "[Beauty -> (X0-> X+ ^X-) l+ l-]CC ": "TrackIsoBsMMInfoH1", 
                    "[Beauty -> (X0-> ^X+ X-) l+ l+]CC ": "TrackIsoBsMMInfoH1", 
                    "[Beauty -> (X0-> ^X+ X-) l+ l-]CC ": "TrackIsoBsMMInfoH1"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "IsoTwoBody": False, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }
        ], 
        "TrackCHI2": 3, 
        "TrackGhostProb": 0.5, 
        "XMuVCHI2DOF_K": 9.0, 
        "XMuVCHI2DOF_kst": 15.0, 
        "XMuVCHI2DOF_phi": 20.0
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingB2XTauMu_KstLine", 
            "StrippingB2XTauMu_Kst_WSLine", 
            "StrippingB2XTauMu_K_3piLine", 
            "StrippingB2XTauMu_K_3pi_WSLine"
        ], 
        "Leptonic": [
            "StrippingB2XTauMu_PhiLine", 
            "StrippingB2XTauMu_KLine", 
            "StrippingB2XTauMu_Phi_3piLine", 
            "StrippingB2XTauMu_Phi_WSLine", 
            "StrippingB2XTauMu_K_WSLine", 
            "StrippingB2XTauMu_Phi_3pi_WSLine", 
            "StrippingB2XTauMu_K_3pi_looseLine", 
            "StrippingB2XTauMu_K_3pi_loose_WSLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

BLV = {
    "BUILDERTYPE": "BLVLinesConf", 
    "CONFIG": {
        "B2DpiPrescale": 0.1, 
        "B2KhhPrescale": 1, 
        "B2LcmuPrescale": 1, 
        "B2LcpPrescale": 0.2, 
        "Bs2DspiPrescale": 0.1, 
        "CommonRelInfoTools": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "BsMuMuBIsolation", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }
        ], 
        "La2KmuPrescale": 1, 
        "Lb2DmuPrescale": 1, 
        "Lb2DsmuPrescale": 1, 
        "Lb2KmuPrescale": 1, 
        "Lb2LcpiPrescale": 0.1, 
        "MaxDoca": 0.3, 
        "MaxIPChi2": 25, 
        "MaxTrChi2Dof": 4.0, 
        "MaxTrGhp": 0.4, 
        "MaxVtxChi2Dof": 9, 
        "MinDira": 0.0, 
        "MinTauLambda": 10.0, 
        "MinTauLoose": 0.0, 
        "MinTauTight": 1.0, 
        "MinTrIPChi2": 25.0, 
        "MinVDChi2Loose": 100, 
        "MinVDChi2Tight": 225, 
        "Postscale": 1, 
        "Xib2KhmuPrescale": 1, 
        "mDiffD": 100.0, 
        "mDiffDs": 100.0, 
        "mDiffLa": 400.0, 
        "mDiffbLoose": 500.0, 
        "mDiffbStand": 400.0, 
        "mDiffbTight": 300.0, 
        "mKst": 1200.0
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "RD" ]
}

Bd2eeKstarBDT = {
    "BUILDERTYPE": "Bd2eeKstarBDTConf", 
    "CONFIG": {
        "BComMassW": 1200.0, 
        "BDIRA": 0.999, 
        "BDTCutValue": -0.95, 
        "BDTWeightsFile": "$TMVAWEIGHTSROOT/data/Bd2eeKstar_BDTG_v1r0.xml", 
        "BMassW": 1000.0, 
        "BVertexCHI2": 16.0, 
        "ElectronGhostProb": 0.5, 
        "ElectronIPCHI2": 1.0, 
        "ElectronPIDepi": -2.0, 
        "ElectronPT": 200.0, 
        "ElectronTrackCHI2pNDOF": 5.0, 
        "KaonGhostProb": 0.35, 
        "KaonIPCHI2": 4.0, 
        "KaonP": 3000.0, 
        "KaonPIDKpi": -5.0, 
        "KaonPT": 400.0, 
        "KaonTrackCHI2pNDOF": 5.0, 
        "KstarMassW": 150.0, 
        "KstarVertexCHI2": 16.0, 
        "LinePostscale": 1.0, 
        "LinePrescale": 1.0, 
        "PionGhostProb": 0.35, 
        "PionIPCHI2": 4.0, 
        "PionP": 2000.0, 
        "PionPIDpiK": 10.0, 
        "PionPT": 250.0, 
        "PionTrackCHI2pNDOF": 5.0, 
        "RelatedInfoTools": [
            {
                "DaughterLocations": {
                    "[B0 -> ( K*(892)0 -> K+ pi-) ( J/psi(1S) ->  e+^e-) ]CC": "Electron2ISO", 
                    "[B0 -> ( K*(892)0 -> K+ pi-) ( J/psi(1S) -> ^e+ e-) ]CC": "Electron1ISO", 
                    "[B0 -> ( K*(892)0 -> K+^pi-) ( J/psi(1S) ->  e+ e-) ]CC": "PionISO", 
                    "[B0 -> ( K*(892)0 ->^K+ pi-) ( J/psi(1S) ->  e+ e-) ]CC": "KaonISO"
                }, 
                "IsoTwoBody": False, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }
        ], 
        "RelatedInfoTools2": [
            {
                "DaughterLocations": {
                    "[B0 -> ( K*(892)0 -> K+ pi-) ( J/psi(1S) ->  e+^e-) ]CC": "Electron2ISO", 
                    "[B0 -> ( K*(892)0 -> K+ pi-) ( J/psi(1S) -> ^e+ e-) ]CC": "Electron1ISO", 
                    "[B0 -> ( K*(892)0 -> K+^pi-) ( J/psi(1S) ->  e+ e-) ]CC": "PionISO", 
                    "[B0 -> ( K*(892)0 ->^K+ pi-) ( J/psi(1S) ->  e+ e-) ]CC": "KaonISO"
                }, 
                "IsoTwoBody": False, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }
        ], 
        "eeCuts": "(HASVERTEX) & (VFASPF(VCHI2)<16) & (((MM<1.5*GeV)) | ((MM>2.2*GeV) & (MM<4.2*GeV)))"
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "RD" ]
}

Beauty2XGamma = {
    "BUILDERTYPE": "Beauty2XGammaConf", 
    "CONFIG": {
        "B2PhiOmega2pipipi0MPrescale": 1.0, 
        "B2PhiOmega2pipipi0RPrescale": 1.0, 
        "B2XG2pi2KsPrescale": 1.0, 
        "B2XG2piCNVDDPrescale": 1.0, 
        "B2XG2piCNVLLPrescale": 1.0, 
        "B2XG2piGGPrescale": 1.0, 
        "B2XG2piKsPrescale": 1.0, 
        "B2XG2piPrescale": 1.0, 
        "B2XG2pipi0MPrescale": 1.0, 
        "B2XG2pipi0RPrescale": 1.0, 
        "B2XG3piCNVDDPrescale": 1.0, 
        "B2XG3piCNVLLPrescale": 1.0, 
        "B2XG3piGGPrescale": 1.0, 
        "B2XG3piKsPrescale": 1.0, 
        "B2XG3piPrescale": 1.0, 
        "B2XG3pipi0MPrescale": 1.0, 
        "B2XG3pipi0RPrescale": 1.0, 
        "B2XG4piPrescale": 1.0, 
        "B2XGBMaxM": 6500.0, 
        "B2XGBMinBPVDIRA": 0.0, 
        "B2XGBMinBPVFDCHI2": 0.0, 
        "B2XGBMinM2pi": 2400.0, 
        "B2XGBMinM3pi": 2400.0, 
        "B2XGBMinM4pi": 2000.0, 
        "B2XGBMinMpi": 3200.0, 
        "B2XGBMinPT": 1000.0, 
        "B2XGBSumPtMin": 3000, 
        "B2XGBVtxChi2DOF": 9.0, 
        "B2XGBVtxMaxIPChi2": 9.0, 
        "B2XGGammaCL": 0.0, 
        "B2XGGammaCNVPTMin": 1000.0, 
        "B2XGGammaPTMin": 2000.0, 
        "B2XGLambda2piPrescale": 1.0, 
        "B2XGLambda3piPrescale": 1.0, 
        "B2XGLambdapiPrescale": 1.0, 
        "B2XGPhiOmegaMaxMass": 1300.0, 
        "B2XGPhiOmegaMinMass": 700.0, 
        "B2XGResBPVVDCHI2Min": 0.0, 
        "B2XGResMaxMass": 5000.0, 
        "B2XGResMinMass": 0.0, 
        "B2XGResMinPT": 150.0, 
        "B2XGResSumPtMin": 1000.0, 
        "B2XGResVtxChi2DOF": 9.0, 
        "B2XGTrkChi2DOF": 3.0, 
        "B2XGTrkMinIPChi2": 20.0, 
        "B2XGTrkMinP": 1000, 
        "B2XGTrkMinPT": 300.0, 
        "B2XGpiKsPrescale": 1.0, 
        "Hlt1TISTOSLinesDict": {
            "Hlt1(Phi)?IncPhi.*Decision%TOS": 0, 
            "Hlt1(Two)?TrackMVA(Loose)?Decision%TOS": 0, 
            "Hlt1B2GammaGamma.*Decision%TOS": 0, 
            "Hlt1B2PhiGamma_LTUNB.*Decision%TOS": 0
        }, 
        "Hlt2TISTOSLinesDict": {
            "Hlt2(Phi)?IncPhi.*Decision%TOS": 0, 
            "Hlt2Radiative.*Decision%TOS": 0, 
            "Hlt2Topo(2|3|4)Body.*Decision%TOS": 0
        }, 
        "Pi0MPMin": 4000.0, 
        "Pi0MPTMin": 700.0, 
        "Pi0MPTReCut": 1200.0, 
        "Pi0RPMin": 4000.0, 
        "Pi0RPTMin": 700.0, 
        "Pi0RPTReCut": 1200.0, 
        "TrackGhostProb": 0.4
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Beauty2XGammaExclusive = {
    "BUILDERTYPE": "Beauty2XGammaExclusiveConf", 
    "CONFIG": {
        "B0PVIPchi2": 9.0, 
        "B0VTXchi2": 9.0, 
        "BMassMax": 7000.0, 
        "BMassMin": 4000.0, 
        "B_APT": 3000.0, 
        "B_PT": 2000.0, 
        "Bd2KstGammaPostScale": 1.0, 
        "Bd2KstGammaPreScale": 1.0, 
        "BdDIRA": 0.06, 
        "Bs2PhiGammaPostScale": 1.0, 
        "Bs2PhiGammaPreScale": 1.0, 
        "BsPVIPchi2": 9.0, 
        "BsVTXchi2": 9.0, 
        "GhostProb_Max": 0.4, 
        "KstMassWin": 100.0, 
        "KstVCHI2": 9.0, 
        "MinTrack_P": 3000.0, 
        "MinTrack_PT": 500.0, 
        "PhiMassWin": 15.0, 
        "PhiVCHI2": 9.0, 
        "SumVec_PT": 1500.0, 
        "TrChi2": 3.0, 
        "TrIPchi2": 16.0, 
        "photonPT": 2500.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "RD" ]
}

Beauty2XGammaNoBias = {
    "BUILDERTYPE": "Beauty2XGammaNoBiasConf", 
    "CONFIG": {
        "B0MassWin": 3000.0, 
        "Bd2KstGammaPostScale": 1.0, 
        "Bd2KstGammaPreScale": 1.0, 
        "Bs2PhiGammaPostScale": 1.0, 
        "Bs2PhiGammaPreScale": 1.0, 
        "BsMassWin": 3000.0, 
        "KstMassWin": 3000.0, 
        "PhiMassWin": 400.0, 
        "TISTOSLinesDict": {
            "Hlt2RadiativeBd2KstGammaULUnbiased.*Decision%TOS": 0, 
            "Hlt2RadiativeBs2PhiGammaUnbiased.*Decision%TOS": 0
        }, 
        "TrChi2": 100.0, 
        "photonPT": 2600.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Bs2GammaGamma = {
    "BUILDERTYPE": "StrippingBs2gammagammaConf", 
    "CONFIG": {
        "BsHighMass": 6500, 
        "BsHighMassDD": 6700, 
        "BsHighMassDouble": 7000, 
        "BsHighMassNone": 6300, 
        "BsLowMass": 4300, 
        "BsLowMassDD": 4400, 
        "BsLowMassDouble": 4000, 
        "BsLowMassNone": 4800, 
        "BsNonePT": 2000, 
        "BsPT": 2000, 
        "BsVertexCHI2pDOF": 20, 
        "ConvGhostDD": 0.3, 
        "ConvGhostLL": 0.3, 
        "DDProbNNe": 0.3, 
        "HLT1None": "HLT_PASS_RE('Hlt1.*GammaGammaDecision')", 
        "HLT2DD": "HLT_PASS_RE('Hlt2RadiativeB2GammaGammaDDDecision')", 
        "HLT2Double": "HLT_PASS_RE('Hlt2RadiativeB2GammaGammaDoubleDecision')", 
        "HLT2LL": "HLT_PASS_RE('Hlt2RadiativeB2GammaGammaLLDecision')", 
        "HLT2None": "HLT_PASS_RE('Hlt2.*GammaGammaDecision')", 
        "LLProbNNe": 0.5, 
        "NoConvHCAL2ECAL": 0.1, 
        "gammaCL": 0.3, 
        "gammaConvIPCHI": 0, 
        "gammaConvMDD": 60, 
        "gammaConvPT": 2000, 
        "gammaNoneCL": 0.3, 
        "gammaNoneP": 6000, 
        "gammaNonePT": 1100, 
        "gammaP": 6000, 
        "gammaPT": 1000
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "RD" ]
}

Bs2MuMuLines = {
    "BUILDERTYPE": "Bs2MuMuLinesConf", 
    "CONFIG": {
        "BFDChi2_loose": 100, 
        "BIPChi2_loose": 64, 
        "BPVVDChi2": 121, 
        "B_BPVIPChi2": 25, 
        "B_BPVIPChi2_LTUB": 25, 
        "B_Pt": 350, 
        "B_Pt_LTUB": 500, 
        "B_maximum_decaytime_bsst": 0.2, 
        "B_minimum_decaytime_LTUB": 0.6, 
        "B_minimum_decaytime_bsst": 0.0, 
        "BdPrescale": 1, 
        "Bs2KKLTUBLinePrescale": 1, 
        "Bs2mmLTUBLinePrescale": 1, 
        "Bs2mmWideLinePrescale": 1, 
        "BsPrescale": 1, 
        "Bsst2mmLinePrescale": 1, 
        "BuPrescale": 1, 
        "DOCA": 0.3, 
        "DOCA_LTUB": 0.3, 
        "DOCA_loose": 0.5, 
        "DefaultLinePrescale": 1, 
        "DefaultPostscale": 1, 
        "JPsiLinePrescale": 1, 
        "JPsiLooseLinePrescale": 0.1, 
        "JPsiPromptLinePrescale": 0.005, 
        "LooseLinePrescale": 0.0, 
        "MuIPChi2_loose": 9, 
        "MuTrChi2_loose": 10, 
        "ProbNN": 0.4, 
        "SSPrescale": 1, 
        "SUMPT": 4500, 
        "TrackGhostProb": 0.4, 
        "TrackGhostProb_bsst": 0.3, 
        "VCHI2_VDOF": 9, 
        "VCHI2_VDOF_LTUB": 9, 
        "VCHI2_VDOF_loose": 25, 
        "daughter_IPChi2": 9, 
        "daughter_TrChi2": 4, 
        "daughter_TrChi2_LTUB": 4, 
        "daughter_TrChi2_bsst": 3, 
        "muon_PT_LTUB": 40
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingBs2MuMuLinesNoMuIDLine", 
            "StrippingBs2MuMuLinesWideMassLine", 
            "StrippingBs2MuMuLinesBu2JPsiKLine", 
            "StrippingBs2MuMuLinesBsstLine"
        ], 
        "Leptonic": [
            #"StrippingBs2MuMuLinesWideMassLine", 
            "StrippingBs2MuMuLinesBs2JPsiPhiLine", 
            "StrippingBs2MuMuLinesBs2KKLTUBLine", 
            "StrippingBs2MuMuLinesNoMuIDLine", 
            "StrippingBs2MuMuLinesSSLine", 
            "StrippingBs2MuMuLinesBd2JPsiKstLine", 
            "StrippingBs2MuMuLinesLTUBLine", 
            "StrippingBs2MuMuLinesBu2JPsiKLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

Bs2st2KKMuX = {
    "BUILDERTYPE": "Bs2st2KKMuXConf", 
    "CONFIG": {
        "Bs2PT": 50.0, 
        "Bs2st2KKJpsiPrescale": 1.0, 
        "Bs2st2KKJpsiWSPrescale": 1.0, 
        "Bs2st2KKMuPrescale": 1.0, 
        "Bs2st2KKMuWSPrescale": 1.0, 
        "DMKKJpsi": 1093.677, 
        "DMKKMu": 713.677, 
        "DZBPV": 1.0, 
        "GEC_nLongTrk": 1000.0, 
        "HLT_FILTER": "", 
        "JpsiMassWindow": 80.0, 
        "K1MinIPChi2": 9.0, 
        "K1PIDK": 16.0, 
        "K1PT": 500.0, 
        "K1PTLoose": 250.0, 
        "K2MinIPChi2": 9.0, 
        "K2P": 3000.0, 
        "K2PIDK": 0.0, 
        "K2PT": 1000.0, 
        "KJpsiFdChi2": 25.0, 
        "KJpsiMassMax": 5500.0, 
        "KJpsiMassMin": 5050.0, 
        "KJpsiVChi2Dof": 4.0, 
        "KMuFdChi2": 100.0, 
        "KMuMassMax": 5500.0, 
        "KMuMassMin": 1000.0, 
        "KMuVChi2Dof": 4.0, 
        "MuMinIPChi2": 9.0, 
        "MuP": 3000.0, 
        "MuPIDmu": 0.0, 
        "MuPT": 1000.0, 
        "RelatedInfoTools": [
            {
                "DaughterLocations": {
                    "[B*_s20 -> (B+ -> K+ [mu-]cc) ^[K-]cc]CC": "K1ISO", 
                    "[B*_s20 -> (B+ -> K+ ^[mu-]cc) [K-]cc]CC": "MuISO", 
                    "[B*_s20 -> (B+ -> ^K+ [mu-]cc) [K-]cc]CC": "K2ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS", 
                    "BSMUMUTRACKID"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }
        ]
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "RD" ]
}

Bu2LLK = {
    "BUILDERTYPE": "Bu2LLKConf", 
    "CONFIG": {
        "BDIRA": 0.9995, 
        "BFlightCHI2": 100, 
        "BIPCHI2": 25, 
        "BMassWindow": 1500, 
        "BMassWindowTau": 5000, 
        "BVertexCHI2": 9, 
        "Bu2eeLine2Prescale": 1, 
        "Bu2eeLine3Prescale": 1, 
        "Bu2eeLinePrescale": 1, 
        "Bu2meLinePrescale": 1, 
        "Bu2meSSLinePrescale": 1, 
        "Bu2mmLinePrescale": 1, 
        "Bu2mtLinePrescale": 1, #this line didn't run in S24 nor S24r0p1 but it was included to reflect S29
        "Bu2mtSSLinePrescale": 1, #this line didn't run in S24 nor S24r0p1 but it was included to reflect S29
        "DiHadronMass": 2600, 
        "DiLeptonFDCHI2": 16, 
        "DiLeptonIPCHI2": 0, 
        "DiLeptonPT": 0, 
        "K1_MassWindow_Hi": 6000, 
        "K1_MassWindow_Lo": 0, 
        "K1_SumIPChi2Had": 48.0, 
        "K1_SumPTHad": 800, 
        "K1_VtxChi2": 12, 
        "KaonIPCHI2": 9, 
        "KaonPT": 400, 
        "KstarPADOCACHI2": 30, 
        "KstarPMassWindow": 300, 
        "KstarPVertexCHI2": 25, 
        "LeptonIPCHI2": 9, 
        "LeptonPT": 300, 
        "PIDe": 0, 
        "RelatedInfoTools": [
            {
                "Location": "VertexIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VertexIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  X+  X-) (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  X+  X-) (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+ ^X-)) (X0 ->  l+  l+)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 -> ^X+  X-)) (X0 ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+ (X0 ->  X+  X-)) (X0 ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+ (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  StableCharged ^StableCharged)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoInfoL23", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoInfoL22", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoInfoL21", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoInfoL23", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoInfoL22", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoInfoL21", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^(l+ ->  X+  X-  X+))]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^(l- ->  X-  X-  X+))]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^l+)]CC": "TrackIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^StableCharged  StableCharged)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  (l- ->  X-  X-  X+))]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  l+)]CC": "TrackIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  (l- ->  X-  X-  X+))]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  l+)]CC": "TrackIsoInfoH2", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  (l- ->  X-  X-  X+))]CC": "TrackIsoInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  l+)]CC": "TrackIsoInfoH1", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l+)]CC": "TrackIsoInfoH", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l-)]CC": "TrackIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeSize": 0.5, 
                "DaughterLocations": {
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  X+  X-) (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+  X+  X-) (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+ ^X-)) (X0 ->  l+  l+)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 -> ^X+  X-)) (X0 ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+ (X0 ->  X+  X-)) (X0 ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X+ -> ^X+ (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  StableCharged ^StableCharged)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ ->  X+  X- ^X+))]CC": "ConeIsoInfoL23", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ ->  X+ ^X-  X+))]CC": "ConeIsoInfoL22", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ -> ^X+  X-  X+))]CC": "ConeIsoInfoL21", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "ConeIsoInfoL23", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "ConeIsoInfoL22", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "ConeIsoInfoL21", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^(l+ ->  X+  X-  X+))]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^(l- ->  X-  X-  X+))]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^l+)]CC": "ConeIsoInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^StableCharged  StableCharged)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  (l+ ->  X+  X-  X+))]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  (l- ->  X-  X-  X+))]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  l+)]CC": "ConeIsoInfoL1", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  StableCharged  StableCharged)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  (l+ ->  X+  X-  X+))]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  (l- ->  X-  X-  X+))]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  l+)]CC": "ConeIsoInfoH2", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  StableCharged  StableCharged)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  (l+ ->  X+  X-  X+))]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  (l- ->  X-  X-  X+))]CC": "ConeIsoInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  l+)]CC": "ConeIsoInfoH1", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l+)]CC": "ConeIsoInfoH", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l-)]CC": "ConeIsoInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoConeIsolation"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+  X+  X-) (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ ->  X+  X+  X-) (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+ ^X-)) (X0 ->  l+  l+)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 -> ^X+  X-)) (X0 ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+ (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+ (X0 ->  X+  X-)) (X0 ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X+ -> ^X+ (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  StableCharged ^StableCharged)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoBDTInfoL23", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoBDTInfoL22", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoBDTInfoL21", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoBDTInfoL23", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoBDTInfoL22", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoBDTInfoL21", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^(l+ ->  X+  X-  X+))]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^(l- ->  X-  X-  X+))]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^l+)]CC": "TrackIsoBDTInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^StableCharged  StableCharged)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  (l- ->  X-  X-  X+))]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  l+)]CC": "TrackIsoBDTInfoL1", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  (l- ->  X-  X-  X+))]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  l+)]CC": "TrackIsoBDTInfoH2", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  (l- ->  X-  X-  X+))]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  l+)]CC": "TrackIsoBDTInfoH1", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l+)]CC": "TrackIsoBDTInfoH", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l-)]CC": "TrackIsoBDTInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  StableCharged (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty ->  StableCharged (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+  X+  X-) (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X+ ->  X+  X+  X-) (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+ ^X-)) (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH3", 
                    "[Beauty -> (X+ ->  X+ (X0 -> ^X+  X-)) (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+ (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+ (X0 ->  X+  X-)) (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X+ -> ^X+ (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  StableCharged ^StableCharged)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ ->  X+  X- ^X+))]CC": "TrackIsoBs2MMInfoL23", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ ->  X+ ^X-  X+))]CC": "TrackIsoBs2MMInfoL22", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l+ -> ^X+  X-  X+))]CC": "TrackIsoBs2MMInfoL21", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "TrackIsoBs2MMInfoL23", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "TrackIsoBs2MMInfoL22", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "TrackIsoBs2MMInfoL21", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^(l+ ->  X+  X-  X+))]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^(l- ->  X-  X-  X+))]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 ->  l+ ^l+)]CC": "TrackIsoBs2MMInfoL2", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^StableCharged  StableCharged)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  (l- ->  X-  X-  X+))]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X0 ->  X+  X-) (X0 -> ^l+  l+)]CC": "TrackIsoBs2MMInfoL1", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  (l- ->  X-  X-  X+))]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X0 ->  X+ ^X-) (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH2", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  StableCharged  StableCharged)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  (l+ ->  X+  X-  X+))]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  (l- ->  X-  X-  X+))]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> (X0 -> ^X+  X-) (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH1", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l+)]CC": "TrackIsoBs2MMInfoH", 
                    "[Beauty -> ^StableCharged (X0 ->  l+  l-)]CC": "TrackIsoBs2MMInfoH"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations"
            }
        ], 
        "TauPT": 0, 
        "TauVCHI2DOF": 150, 
        "Trk_Chi2": 3, 
        "Trk_GhostProb": 0.4, 
        "UpperMass": 5500
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Bu2MajoLep = {
    'WGs' : ['RD'],
    'STREAMS' : ['Leptonic'],
    'BUILDERTYPE' : 'Bu2MajoLepConf',
    'CONFIG'      : {'MajoDaug_LTrk_GhostProb'   : 0.5,
                     'MajoDaug_IPChi2min'        : 9.0,
                     'MajoDaug_Pmin'             : 2000.0,
                     'MajoDaug_PTmin'            : 300.0,
                     'Trk_GhostProb'             : 0.5,
                     'Trk_Chi2'                  : 3.0,
                     'Majo_Mhigh'                : 5600.0,
                     'Majo_Pmin'                 : 5000.0,
                     'Majo_PTmin'                : 500.0,
                     'Majo_DD_DocaChi2'          : 25.0,
                     'Majo_LL_DocaChi2'          : 25.0,
                     'Majo_WrongMass'            : 35.0,
                     'Majo_DD_VtxChi2'           : 16.0,
                     'Majo_LL_VtxChi2'           : 16.0,
                     'Majo_DD_FDChi2'            : 25.0,
                     'Majo_LL_FDChi2'            : 25.0,
                     'Majo_FDwrtPV'              : 1.0,
                     'Bach_PTmin'                : 500.0,
                     'Bach_Pmin'                 : 3000.0,
                     'Bach_IPChi2min'            : 16.0,
                     #                     'BDaug_DD_PTsum'            : 3000.0,
                     #                     'BDaug_LL_PTsum'            : 3000.0,
                     'B_Mlow'                    : 500.0,
                     'B_Mhigh'                   : 700.0,
                     
                     'B_Pmin'                    : 5000.0,
                     'B_VtxChi2'                 : 10.0,
                     'B_Dira'                    : 0.9999,
                     'B_DD_IPCHI2wrtPV'          : 16.0,
                     'B_LL_IPCHI2wrtPV'          : 16.0,
                     'B_FDwrtPV'                 : 1.0,
                     'B_DD_FDChi2'               : 30.0,
                     'B_LL_FDChi2'               : 30.0,
                     'GEC_MaxTracks'             : 800,
                     'Prescale'                  : 1.0,
                     'Postscale'                 : 1.0,
                     "RelatedInfoTools": [
            {
                "Location": "KSTARMUMUVARIABLES",
                "Type": "RelInfoBKstarMuMuBDT",
                "Variables": ['MU_SLL_ISO_1', 'MU_SLL_ISO_2']
                },
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM", 
                    "CONEPT", 
                    "CONEP", 
                    "CONEPASYM", 
                    "CONEDELTAETA", 
                    "CONEDELTAPHI"
                    ]
                }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation", 
                "Variables": [
                    "VTXISONUMVTX", 
                    "VTXISODCHI2ONETRACK", 
                    "VTXISODCHI2MASSONETRACK", 
                    "VTXISODCHI2TWOTRACK", 
                    "VTXISODCHI2MASSTWOTRACK"
                    ]
                },
            {
                "Location": "VtxIsoBDTInfo", 
                "Type": "RelInfoVertexIsolationBDT"
                },
            ], 
    }, 
}

Bu2MuNu = {
    "BUILDERTYPE": "Bu2MuNuConf", 
    "CONFIG": {
        "IPchi2Mu": 400.0, 
        "IPchi2MuControl": 100.0, 
        "PostscaleControl": 1.0, 
        "PostscaleSignal": 1.0, 
        "PrescaleControl": 0.03, 
        "PrescaleSignal": 1.0, 
        "TrChi2Mu": 3, 
        "maxNTracks": 150, 
        "pTmaxMu": 40.0, 
        "pTminMu": 5.0, 
        "pTminMuControl": 2.5, 
        "pmaxMu": 500.0, 
        "useNN": True
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

D23Mu = {
    "BUILDERTYPE": "D23MuLinesConf", 
    "CONFIG": {
        "CommonRelInfoTools": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "BsMuMuBIsolation", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }
        ], 
        "D23MuPrescale": 1, 
        "D23PiPrescale": 0.01, 
        "D2MueePrescale": 1, 
        "MaxDoca": 0.3, 
        "MaxIPChi2": 25, 
        "MaxTrChi2Dof": 4.0, 
        "MaxTrGhp": 0.4, 
        "MaxVtxChi2Dof": 9, 
        "MinDira": 0.0, 
        "MinTau": 0.1, 
        "MinTrIPChi2": 25.0, 
        "MinVDChi2": 225, 
        "Postscale": 1, 
        "mDiffDLoose": 150, 
        "mDiffDTight": 150
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

DarkBoson = {
    "BUILDERTYPE": "DarkBosonConf", 
    "CONFIG": {
        "B2HHX": {
            "AM_MAX": "5800*MeV", 
            "AM_MIN": "4800*MeV", 
            "BPVIPCHI2_MAX": 10, 
            "BPVLTIME_MIN": "0.2*ps", 
            "HAD_MINIPCHI2_MIN": 9, 
            "PT_MIN": "1000*MeV", 
            "SUMPT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 15
        }, 
        "B2KX": {
            "AM_MAX": "5800*MeV", 
            "AM_MIN": "4800*MeV", 
            "BPVIPCHI2_MAX": 10, 
            "BPVLTIME_MIN": "0.2*ps", 
            "HAD_MINIPCHI2_MIN": 25, 
            "PT_MIN": "1000*MeV", 
            "SUMPT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 15
        }, 
        "E": {
            "MIPCHI2DV_MIN": 9, 
            "PIDe_MIN": 0, 
            "PT_MIN": "100*MeV", 
            "TRCHI2DOF_MAX": 5, 
            "TRGHP_MAX": 0.4
        }, 
        "GECNTrkMax": 250, 
        "J": {
            "ADAMASS_MAX": "100*MeV", 
            "VCHI2DOF_MAX": 12
        }, 
        "KB": {
            "MIPCHI2DV_MIN": 9, 
            "PROBNNK_MIN": 0.1, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "2000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "KBhard": {
            "MIPCHI2DV_MIN": 36, 
            "PROBNNK_MIN": 0.2, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "3000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "KDX": {
            "MIPCHI2DV_MIN": 25, 
            "PROBNNK_MIN": 0.1, 
            "PT_MIN": "125*MeV", 
            "P_MIN": "0*MeV", 
            "TRCHI2DOF_MAX": 4, 
            "TRGHP_MAX": 0.3
        }, 
        "KX": {
            "MIPCHI2DV_MIN": 25, 
            "PROBNNK_MIN": 0.1, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "3000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "MuJ": {
            "MIPCHI2DV_MIN": 25, 
            "PIDmu_MIN": -4, 
            "PT_MIN": "125*MeV", 
            "P_MIN": "0*MeV", 
            "TRCHI2DOF_MAX": 4, 
            "TRGHP_MAX": 0.3
        }, 
        "MuX": {
            "MIPCHI2DV_MIN": 9, 
            "PIDmu_MIN": -5, 
            "PT_MIN": "100*MeV", 
            "P_MIN": "0*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "PiB": {
            "MIPCHI2DV_MIN": 9, 
            "PROBNNpi_MIN": 0.2, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "2000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "PiDX": {
            "MIPCHI2DV_MIN": 25, 
            "PROBNNpi_MIN": 0.1, 
            "PT_MIN": "125*MeV", 
            "P_MIN": "0*MeV", 
            "TRCHI2DOF_MAX": 4, 
            "TRGHP_MAX": 0.3
        }, 
        "PiX": {
            "MIPCHI2DV_MIN": 36, 
            "PROBNNpi_MIN": 0.6, 
            "PT_MIN": "250*MeV", 
            "P_MIN": "3000*MeV", 
            "TRCHI2DOF_MAX": 3, 
            "TRGHP_MAX": 0.3
        }, 
        "Prescales": {
            "DD": 1.0, 
            "KK": 0.25, 
            "SS": 0.1
        }, 
        "XDD": {
            "BPVVDCHI2_MIN": 25, 
            "PT_MIN": "0*MeV", 
            "VCHI2DOF_MAX": 15
        }, 
        "XLL": {
            "BPVVDCHI2_MIN": 25, 
            "PT_MIN": "250*MeV", 
            "VCHI2DOF_MAX": 10
        }, 
        "XLLhard": {
            "BPVVDCHI2_MIN": 25, 
            "PT_MIN": "250*MeV", 
            "VCHI2DOF_MAX": 5
        }
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingB2KpiX2MuMuDDDarkBosonLine", 
            "StrippingB2KpiX2MuMuDDSSDarkBosonLine", 
            "StrippingB2KKX2MuMuDDDarkBosonLine", 
            "StrippingB2KKX2MuMuDDSSDarkBosonLine", 
            "StrippingB2KX2MuMuDDDarkBosonLine", 
            "StrippingB2KX2MuMuDDSSDarkBosonLine", 
            "StrippingB2KpiX2PiPiDDDarkBosonLine", 
            "StrippingB2KKX2PiPiDDDarkBosonLine", 
            "StrippingB2KX2PiPiDDDarkBosonLine", 
            "StrippingB2KX2PiPiDDSSDarkBosonLine", 
            "StrippingB2RhoX2MuMuDDDarkBosonLine", 
            "StrippingB2KpiX2KKDDDarkBosonLine", 
            "StrippingB2KKX2KKDDDarkBosonLine", 
            "StrippingB2KX2KKDDDarkBosonLine", 
            "StrippingB2KX2KKDDSSDarkBosonLine"
        ], 
        "Leptonic": [
            "StrippingB2KpiX2MuMuDarkBosonLine", 
            "StrippingB2KpiX2MuMuSSDarkBosonLine", 
            "StrippingB2KKX2MuMuDarkBosonLine", 
            "StrippingB2KKX2MuMuSSDarkBosonLine", 
            "StrippingB2KX2MuMuDarkBosonLine", 
            "StrippingB2KX2MuMuSSDarkBosonLine", 
            "StrippingB2KpiX2PiPiDarkBosonLine", 
            "StrippingB2KKX2PiPiDarkBosonLine", 
            "StrippingB2KX2PiPiDarkBosonLine", 
            "StrippingB2KX2PiPiSSDarkBosonLine", 
            "StrippingB2RhoX2MuMuDarkBosonLine", 
            "StrippingB2KpiX2KKDarkBosonLine", 
            "StrippingB2KKX2KKDarkBosonLine", 
            "StrippingB2KX2KKDarkBosonLine", 
            "StrippingB2KX2KKSSDarkBosonLine", 
            "StrippingB2KpiX2EEDarkBosonLine", 
            "StrippingB2KpiX2EESSDarkBosonLine", 
            "StrippingB2KKX2EEDarkBosonLine", 
            "StrippingB2KKX2EESSDarkBosonLine", 
            "StrippingB2KX2EEDarkBosonLine", 
            "StrippingB2KX2EESSDarkBosonLine", 
            "StrippingB2JKDarkBosonLine", 
            "StrippingB2JKstDarkBosonLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

DoubleSLForRX = {
    "BUILDERTYPE": "DoubleSLForRXConf", 
    "CONFIG": {
        "BDIRA": 0.995, 
        "BDOCA": 10, 
        "BMassLow": 1000.0, 
        "BMassUpp": 5400.0, 
        "BPT": 2000.0, 
        "BVCHI2": 100, 
        "DDIRA": 0.995, 
        "DFDChisq": 36.0, 
        "DIPChisq": 4.0, 
        "DMassLow": 500.0, 
        "DMassUpp": 2000.0, 
        "DPT": 800.0, 
        "DSeparation": -10, 
        "DVertexChisq": 10, 
        "DstarDeltaMass": 300, 
        "ElectronProbNN": 0.1, 
        "HLT1_FILTER": None, 
        "HLT2_FILTER": None, 
        "KaonIPChisq": 9, 
        "KaonPT": 500.0, 
        "KaonProbNN": 0.1, 
        "L0DU_FILTER": None, 
        "LeptonIPChisq": 9, 
        "LeptonPT": 800.0, 
        "MuonProbNN": 0, 
        "PionIPChisq": 4, 
        "SpdMult": 600, 
        "TrackGhostProb": 0.5, 
        "UseNoPIDParticles": False
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Hypb2L0HGamma = {
    "BUILDERTYPE": "Hypb2L0HGammaConf", 
    "CONFIG": {
        "CombMassWinOmega": 120.0, 
        "CombMassWinOmegab": 1000.0, 
        "CombMassWinXi": 60.0, 
        "CombMassWinXib": 800.0, 
        "Ghost_Prob": 0.4, 
        "MTDOCACHI2_MAX": 15.0, 
        "MassWinOmega": 70.0, 
        "MassWinXi": 30.0, 
        "MinPO": 10000.0, 
        "MinPOb": 15000.0, 
        "MinPTO": 1000.0, 
        "MinPTOb": 1000.0, 
        "MinPTXi": 500.0, 
        "MinPTXib": 500.0, 
        "MinPXi": 10000.0, 
        "MinPXib": 15000.0, 
        "Photon_PT_Min": 2500.0, 
        "Prescale": 1, 
        "TISTOSLinesDict": {
            "L0Electron.*Decision%TOS": 0, 
            "L0Photon.*Decision%TOS": 0
        }, 
        "TRACK_IPCHI2_MIN": 16.0, 
        "TRCHI2DOF_MAX": 4.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Kshort2MuMuMuMu = {
    "BUILDERTYPE": "Kshort2MuMuMuMuConf", 
    "CONFIG": {
        "DDIRA": 0.9999, 
        "DFDCHI2_mumumumu": 9, 
        "DIPCHI2_mumumumu": 20, 
        "DMAXDOCA_mumumumu": 0.2, 
        "DPT_mumumumu": 2500.0, 
        "DVCHI2DOF_mumumumu": 8, 
        "KsDauMAXIPCHI2_mumumumu": 15, 
        "KsMotherMassCut": 540.0, 
        "Kshort2MuMuMuMuLinePostscale": 1, 
        "Kshort2MuMuMuMuLinePrescale": 1, 
        "MINIPCHI2_mumumumu": 4.0, 
        "MaxDimuonMass": 260.0, 
        "MaxKsMass": 550.0, 
        "MuonMINIPCHI2": 2, 
        "MuonP": 3000.0, 
        "MuonPIDmu_mumumumu": -1, 
        "MuonPT": 500.0, 
        "MuonTRCHI2": 5, 
        "PT_mumumumu": 300
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Kshort2PiPiMuMu = {
    "BUILDERTYPE": "Kshort2PiPiMuMuConf", 
    "CONFIG": {
        "DDIRA": 0.9999, 
        "DFDCHI2_hhmumu": 9, 
        "DIPCHI2_hhmumu": 20, 
        "DMAXDOCA_hhmumu": 0.2, 
        "DPT_hhmumu": 300.0, 
        "DVCHI2DOF_hhmumu": 8, 
        "KsDauMAXIPCHI2_hhmumu": 15, 
        "KsMotherMassCut": 540.0, 
        "Kshort2PiPiMuMuLinePostscale": 1, 
        "Kshort2PiPiMuMuLinePrescale": 1, 
        "MINIPCHI2_hhmumu": 4.0, 
        "MaxDimuonMass": 260.0, 
        "MaxKsMass": 550.0, 
        "MuonMINIPCHI2": 2, 
        "MuonP": 3000.0, 
        "MuonPIDmu_hhmumu": -1, 
        "MuonPT": 500.0, 
        "MuonTRCHI2": 5, 
        "PT_hhmumu": 300, 
        "PionMINIPCHI2": 2, 
        "PionP": 2000.0, 
        "PionPT": 300.0, 
        "PionTRCHI2": 5
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Kshort2eePiPi = {
    "BUILDERTYPE": "Kshort2eePiPiConf", 
    "CONFIG": {
        "KsIP": 1, 
        "KsLifetime": 0.8953, 
        "KsMAXDOCA": 1.0, 
        "KsVtxChi2": 50, 
        "Kshort2eePiPi_eeFromTracksLinePostscale": 1, 
        "Kshort2eePiPi_eeFromTracksLinePrescale": 1, 
        "Kshort2eePiPi_eeFromTracks_TOSLinePostscale": 1, 
        "Kshort2eePiPi_eeFromTracks_TOSLinePrescale": 1, 
        "MaxKsMass": 800.0, 
        "MaxKsMass_soft": 950.0, 
        "PionGhostProb": 0.5, 
        "PionMINIPCHI2": 16, 
        "PionPIDK": 5, 
        "PionPT": 100, 
        "TISTOSDict": {
            "Hlt1(Two)?TrackMVA.*Decision%TOS": 0, 
            "Hlt1DiMuonLowMassDecision%TOS": 0, 
            "Hlt1SingleElectronNoIPDecision%TOS": 0, 
            "L0(Electron|Hadron|Muon|DiMuon|Photon)Decision%TOS": 0
        }, 
        "eGhostProb": 0.5, 
        "eMINIPCHI2": 16, 
        "eMINIPCHI2_soft": 2, 
        "ePIDe": -4, 
        "ePIDe_soft": -8, 
        "ePT": 100.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

LFV = {
    "BUILDERTYPE": "LFVLinesConf", 
    "CONFIG": {
        "B2TauMuPrescale": 1, 
        "B2eMuPrescale": 1, 
        "B2eePrescale": 1, 
        "B2hTauMuPrescale": 1, 
        "B2heMuPrescale": 1, 
        "B2pMuPrescale": 1, 
        "Bu2KJPsieePrescale": 1,
        "D2piphi2MuMuPrescale": 1,  # new, from S29
        "D2piphi2MuMuPromptPrescale": 1,    # new, from S29
        "D2piphi2eMuPrescale": 1,   # new, from S29
        "D2piphi2eMuPromptPrescale": 0.25,  # new, from S29
        "D2piphi2eePrescale": 1,    # new, from S29
        "D2piphi2eePromptPrescale": 0.2,    # new, from S29
        "JPsi2MuMuControlPrescale": 0.05, 
        "JPsi2eMuPrescale": 1, 
        "JPsi2eeControlPrescale": 0.05, 
        "Phi2MuMuControlPrescale": 1, 
        "Phi2eMuPrescale": 1, 
        "Phi2eeControlPrescale": 1, 
        "Postscale": 1, 
        "PromptJPsi2MuMuControlPrescale": 0.05, 
        "PromptJPsi2eMuPrescale": 1, 
        "PromptJPsi2eeControlPrescale": 0.05, 
        "PromptPhi2MuMuControlPrescale": 1, 
        "PromptPhi2eMuPrescale": 1, 
        "PromptPhi2eeControlPrescale": 0.7, 
        "RelatedInfoTools_B2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[B_s0 -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [mu-]cc]CC": "Electron_TrackIsoBDT", 
                    "[B_s0 -> e+ ^[mu-]cc]CC": "Muon_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }
        ], 
        "RelatedInfoTools_B2ee": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [e-]cc]CC": "Electron1_ISO", 
                    "[B_s0 -> e+ ^[e-]cc]CC": "Electron2_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B_s0 -> ^e+ [e-]cc]CC": "Electron2_TrackIsoBDT", 
                    "[B_s0 -> e+ ^[e-]cc]CC": "Electron1_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }
        ], 
        "RelatedInfoTools_B2hemu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_ISO", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_ISO", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[[B+]cc -> X [e+]cc ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars", 
                    "[[B+]cc -> X ^[e+]cc [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[[B+]cc -> ^X [e+]cc [mu-]cc]CC": "Hadron_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Bu2KJPsiee": [
            {
                "DaughterLocations": {
                    "[B+ -> ^(J/psi(1S) -> e+ e-) K+]CC": "Jpsi_ISO"
                }, 
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[B+ -> (J/psi(1S) -> ^e+ e-) K+]CC": "Electron1_ISO", 
                    "[B+ -> (J/psi(1S) -> e+ ^e-) K+]CC": "Electron2_ISO", 
                    "[B+ -> (J/psi(1S) -> e+ e-) ^K+]CC": "Kplus_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[B+ -> (J/psi(1S) -> ^e+ e-) K+]CC": "Electron1_TrackIsoBDT", 
                    "[B+ -> (J/psi(1S) -> e+ ^e-) K+]CC": "Electron2_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }
        ], 
        "RelatedInfoTools_D2piphi": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_ISO", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_ISO", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_ISO", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_ISO"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_TrackIso_BDT6vars", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_TrackIso_BDT6vars", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_TrackIso_BDT6vars", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_TrackIso_BDT6vars"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[Charm -> [(phi(1020) -> ^l+ [l-]CC)]CC pi+ ]CC": "lplus_TrackIso_BDT9vars", 
                    "[Charm -> [(phi(1020) -> l+ [^l-]CC)]CC pi+ ]CC": "lminus_TrackIso_BDT9vars", 
                    "[Charm -> [(phi(1020) -> l+ [l-]CC)]CC ^pi+ ]CC": "pi_TrackIso_BDT9vars", 
                    "[Charm -> [^(phi(1020) -> l+ [l-]CC)]CC pi+ ]CC": "phi_TrackIso_BDT9vars"
                }, 
                "IgnoreUnmatchedDescriptors": True, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_JPsi2MuMuControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_JPsi2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_JPsi2eeControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_ISO", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[J/psi(1S) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[J/psi(1S) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Phi2MuMuControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^mu+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> mu+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Phi2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [mu-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> e+ ^[mu-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Phi2eeControl": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_ISO", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT6vars", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT6vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 1, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT6varsB_v1r4.xml"
            }, 
            {
                "DaughterLocations": {
                    "[phi(1020) -> ^e+ [e-]cc]CC": "Electron_TrackIso_BDT9vars", 
                    "[phi(1020) -> e+ ^[e-]cc]CC": "Muon_TrackIso_BDT9vars"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 2, 
                "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"
            }, 
            {
                "Location": "coneInfo", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Tau2MuEtaPrime": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Tau2PhiMu": [
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu05", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus05", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus05", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi05"
                }, 
                "Location": "coneInfoTau05", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.8, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu08", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus08", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus08", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi08"
                }, 
                "Location": "coneInfoTau08", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.0, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu10", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus10", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus10", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi10"
                }, 
                "Location": "coneInfoTau10", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.2, 
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "coneInfoMu12", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "coneInfoKminus12", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "coneInfoKplus12", 
                    "[tau+ -> ^(phi(1020)->K+ K-) mu+]CC": "coneInfoPhi12"
                }, 
                "Location": "coneInfoTau12", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation", 
                "Variables": [
                    "VTXISONUMVTX", 
                    "VTXISODCHI2ONETRACK", 
                    "VTXISODCHI2MASSONETRACK", 
                    "VTXISODCHI2TWOTRACK", 
                    "VTXISODCHI2MASSTWOTRACK"
                ]
            }, 
            {
                "DaughterLocations": {
                    "[tau+ -> (phi(1020)->K+ K-) ^mu+]CC": "MuonTrackIsoBDTInfo", 
                    "[tau+ -> (phi(1020)->K+ ^K-) mu+]CC": "KminusTrackIsoBDTInfo", 
                    "[tau+ -> (phi(1020)->^K+ K-) mu+]CC": "KplusTrackIsoBDTInfo"
                }, 
                "Type": "RelInfoTrackIsolationBDT"
            }
        ], 
        "RelatedInfoTools_Upsilon2eMu": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [mu-]cc]CC": "Electron_ISO", 
                    "[Upsilon(1S) -> e+ ^[mu-]cc]CC": "Muon_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [mu-]cc]CC": "Electron_TrackIsoBDT", 
                    "[Upsilon(1S) -> e+ ^[mu-]cc]CC": "Muon_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }
        ], 
        "RelatedInfoTools_Upsilon2ee": [
            {
                "Location": "BSMUMUVARIABLES", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [
                    "BSMUMUCDFISO", 
                    "BSMUMUOTHERBMAG", 
                    "BSMUMUOTHERBANGLE", 
                    "BSMUMUOTHERBBOOSTMAG", 
                    "BSMUMUOTHERBBOOSTANGLE", 
                    "BSMUMUOTHERBTRACKS"
                ], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [e-]cc]CC": "Electron1_ISO", 
                    "[Upsilon(1S) -> e+ ^[e-]cc]CC": "Electron2_ISO"
                }, 
                "IsoTwoBody": True, 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "Variables": [
                    "BSMUMUTRACKPLUSISO", 
                    "BSMUMUTRACKPLUSISOTWO", 
                    "ISOTWOBODYQPLUS", 
                    "ISOTWOBODYMASSISOPLUS", 
                    "ISOTWOBODYCHI2ISOPLUS", 
                    "ISOTWOBODYISO5PLUS"
                ], 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }, 
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "ConeIsoInfo", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "DaughterLocations": {
                    "[Upsilon(1S) -> ^e+ [e-]cc]CC": "Electron1_TrackIsoBDT", 
                    "[Upsilon(1S) -> e+ ^[e-]cc]CC": "Electron2_TrackIsoBDT"
                }, 
                "Type": "RelInfoTrackIsolationBDT", 
                "Variables": 0
            }
        ], 
        "Tau2MuEtaPrimePrescale": 1, # this line didn't run in S24 nor S24r0p1 but it was included to reflect S29
        "Tau2MuMuePrescale": 1, # this line didn't run in S24 nor S24r0p1 but it was included to reflect S29
        "TauPrescale": 1, # this line didn't run in S24 nor S24r0p1 but it was included to reflect S29
        "Upsilon2eMuPrescale": 1, 
        "Upsilon2eePrescale": 0.6, 
        "config_B2eMu": {
            "max_ADAMASS": 1200.0, 
            "max_AMAXDOCA": 0.3, 
            "max_BPVIPCHI2": 25.0, 
            "max_TRCHI2DV": 3.0, 
            "max_TRGHOSTPROB": 0.3, 
            "min_BPVDIRA": 0.0, 
            "min_BPVVDCHI2": 225.0, 
            "min_MIPCHI2DV": 25.0
        }, 
        "config_D2piphi": {
            "comb_cuts": "in_range(1300, AM, 2400) & (AMAXDOCA('') < .3*mm)", 
            "comb_cuts_prompt": "in_range(1300, AM, 2400) & (AMAXDOCA('') < .3*mm)", 
            "mother_cuts": "(VFASPF(VCHI2/VDOF) < 3) & (MM > 1300) & (MM < 2400)& (BPVDIRA > 0)", 
            "mother_cuts_prompt": "(VFASPF(VCHI2/VDOF) < 2.5) & (MM > 1300) & (MM < 2400)& (BPVDIRA > 0) & (BPVVD > 0.3)", 
            "pi_cuts": "(PT>250*MeV) & (TRCHI2DOF < 3.) & (TRGHOSTPROB<.3)", 
            "pi_cuts_prompt": "(PT>250*MeV) & (TRCHI2DOF < 2.5) & (TRGHOSTPROB<.2) & (MIPDV(PRIMARY)>0.025) & (MIPCHI2DV(PRIMARY) < 9)"
        }, 
        "config_JPsi2eMu": {
            "max_ADAMASS": 1000.0, 
            "max_AMAXDOCA": 0.3, 
            "max_JpsiMass": 4096.0, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.3, 
            "max_VtxChi2DoF": 2.5, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_BPVDIRA": 0, 
            "min_BPVVDCHI2": 324.0, 
            "min_BPVVDCHI2_phi": 144.0, 
            "min_JpsiMass": 2200.0, 
            "min_MIPCHI2DV": 36.0, 
            "min_MIPCHI2DV_phi": 25.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 150.0, 
            "min_ProbNN": 0.3, 
            "min_ProbNN_phi2ee": 0.05, 
            "min_ProbNN_phi_e": 0.3, 
            "min_ProbNN_phi_mu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_Phi2ll_forD": {
            "max_ADAMASS": 1000.0, 
            "max_AMAXDOCA": 0.3, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.3, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_BPVDIRA": 0, 
            "min_BPVVDCHI2_phi": 9.0, 
            "min_MIPCHI2DV_phi": 9.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 150.0, 
            "min_ProbNN": 0.3, 
            "min_ProbNN_phi2ee": 0.05, 
            "min_ProbNN_phi_e": 0.3, 
            "min_ProbNN_phi_mu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_PromptJPsi2eMu": {
            "max_AMAXDOCA": 0.3, 
            "max_BPVIPCHI2": 9.0, 
            "max_BPVVDCHI2": 9.0, 
            "max_JpsiMass": 4096.0, 
            "max_MIPCHI2DV": 9.0, 
            "max_Phi2mumuMass": 1220.0, 
            "max_PhiMass": 2020.0, 
            "max_SPD": 350, 
            "max_TRCHI2DV": 3, 
            "max_TRGHOSTPROB": 0.2, 
            "max_VtxChi2DoF": 2.5, 
            "max_VtxChi2DoF_phi": 3.0, 
            "min_JpsiMass": 2200.0, 
            "min_Phi2mumuMass": 820.0, 
            "min_PhiMass": 150.0, 
            "min_ProbNN": 0.8, 
            "min_ProbNN_Control": 0.65, 
            "min_ProbNN_phi": 0.8, 
            "min_ProbNN_phi2mumu": 0.4, 
            "min_Pt": 300.0
        }, 
        "config_Tau2MuEtaPrime": {
            "config_EtaPrime2pipigamma": {
                "etap_cuts": "(PT > 500*MeV) & (VFASPF(VCHI2/VDOF) < 6.0)", 
                "etap_mass_window": "(ADAMASS('eta') < 80*MeV) | (ADAMASS('eta_prime') < 80*MeV)", 
                "gamma_cuts": "(PT > 300*MeV) & (CL > 0.1)", 
                "piminus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)", 
                "pipi_cuts": "(ACHI2DOCA(1,2)<16)", 
                "piplus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)"
            }, 
            "config_EtaPrime2pipipi": {
                "etap_cuts": "(PT > 500*MeV) & (VFASPF(VCHI2/VDOF) < 6.0)", 
                "etap_mass_window": "(ADAMASS('eta') < 80*MeV) | (ADAMASS('eta_prime') < 80*MeV)", 
                "pi0_cuts": "(PT > 250*MeV)", 
                "piminus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)", 
                "pipi_cuts": "ACHI2DOCA(1,2)<16", 
                "piplus_cuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB < 0.3) & (TRCHI2DOF < 3.0) & (MIPCHI2DV(PRIMARY) > 9.)"
            }, 
            "muplus_cuts": "(ISLONG) & (TRCHI2DOF < 3 )  & (MIPCHI2DV(PRIMARY) >  9.) & (PT > 300*MeV) & (TRGHOSTPROB < 0.3)", 
            "tau_cuts": "(BPVIPCHI2()< 100) & (VFASPF(VCHI2/VDOF)<6.) & (BPVLTIME()*c_light > 50.*micrometer) & (BPVLTIME()*c_light < 400.*micrometer) & (PT>500*MeV) & (D2DVVD(2) < 80*micrometer)", 
            "tau_mass_window": "(ADAMASS('tau+')<150*MeV)"
        }, 
        "config_Upsilon2eMu": {
            "comb_cuts": "in_range(7250, AM, 11000) & ACUTDOCA(0.3*mm,'')", 
            "e_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDe > 1)", 
            "mu_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDmu > 0)", 
            "upsilon_cuts": "(VFASPF(VCHI2) < 25) & (BPVIPCHI2() < 9) & (BPVVDCHI2 < 25)"
        }, 
        "config_Upsilon2ee": {
            "comb_cuts": "in_range(6000, AM, 11000) & ACUTDOCA(0.3*mm,'')", 
            "e_cuts": "(PT > 1000.*MeV) & (P > 8000.*MeV) & (TRCHI2DOF < 3.) & (PIDe > 1)", 
            "upsilon_cuts": "(VFASPF(VCHI2) < 25) & (BPVIPCHI2() < 9) & (BPVVDCHI2 < 25)"
        }
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingLFVJPsi2eeControlLine", 
            "StrippingLFVPromptJPsi2eeControlLine"
        ], 
        "Leptonic": [
            "StrippingLFVTau2PhiMuLine", 
            "StrippingLFVTau2eMuMuLine", 
            "StrippingLFVB2eMuLine", 
            "StrippingLFVJPsi2eMuLine", 
            "StrippingLFVPromptJPsi2eMuLine", 
            "StrippingLFVJPsi2MuMuControlLine", 
            "StrippingLFVJPsi2eeControlLine", 
            "StrippingLFVPromptJPsi2MuMuControlLine", 
            "StrippingLFVPromptJPsi2eeControlLine", 
            "StrippingLFVPhi2eMuLine", 
            "StrippingLFVPromptPhi2eMuLine", 
            "StrippingLFVPhi2MuMuControlLine", 
            "StrippingLFVPhi2eeControlLine", 
            "StrippingLFVPromptPhi2MuMuControlLine", 
            "StrippingLFVPromptPhi2eeControlLine", 
            "StrippingLFVB2eeLine", 
            "StrippingLFVB2heMuLine", 
            "StrippingLFVB2hMuLine", 
            "StrippingLFVBu2KJPsieeLine", 
            "StrippingLFVB2hTauMuLine", 
            "StrippingLFVTau2MuEtaP2pipigLine", 
            "StrippingLFVTau2MuEtaP2pipipiLine", 
            "StrippingLFVupsilon2eMuLine", 
            "StrippingLFVupsilon2eeLine", 
            "StrippingLFVD2piphi2eeLine", 
            "StrippingLFVD2piphi2eMuLine", 
            "StrippingLFVD2piphi2MuMuLine", 
            "StrippingLFVD2piphi2eePromptLine", 
            "StrippingLFVD2piphi2eMuPromptLine", 
            "StrippingLFVD2piphi2MuMuPromptLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

Lb2L0Gamma = {
    "BUILDERTYPE": "StrippingLb2L0GammaConf", 
    "CONFIG": {
        "Bd2KstGammaPrescale": 1.0, 
        "Bd2KstJpsiPrescale": 1.0, 
        "HLT1": [], 
        "HLT2": [], 
        "Kst_MassWindow": 100.0, 
        "L0": [
            "Photon", 
            "Electron", 
            "Hadron"
        ], 
        "L0_Conv": [], 
        "L0_Jpsi": [], 
        "Lambda0DD_MassWindow": 30.0, 
        "Lambda0LL_IP_Min": 0.05, 
        "Lambda0LL_MassWindow": 20.0, 
        "Lambda0_Pt_Min": 1000.0, 
        "Lambda0_VtxChi2_Max": 9.0, 
        "Lambdab_IPChi2_Max": 25.0, 
        "Lambdab_MTDOCAChi2_Max": 7.0, 
        "Lambdab_MassWindow": 1100.0, 
        "Lambdab_Pt_Min": 1000.0, 
        "Lambdab_SumPt_Min": 5000.0, 
        "Lambdab_VtxChi2_Max": 9.0, 
        "Lb2L0GammaConvertedPrescale": 1.0, 
        "Lb2L0GammaPrescale": 1.0, 
        "Lb2L0JpsiPrescale": 1.0, 
        "PhotonCnv_MM_Max": 100.0, 
        "PhotonCnv_PT_Min": 1000.0, 
        "PhotonCnv_VtxChi2_Max": 9.0, 
        "Photon_CL_Min": 0.2, 
        "Photon_PT_Min": 2500.0, 
        "Pion_P_Min": 2000.0, 
        "Pion_Pt_Min": 300.0, 
        "Proton_P_Min": 7000.0, 
        "Proton_Pt_Min": 800.0, 
        "TrackLL_IPChi2_Min": 16.0, 
        "Track_Chi2ndf_Max": 3.0, 
        "Track_GhostProb_Max": 0.4, 
        "Track_MinChi2ndf_Max": 2.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

Lc23Mu = {
    "BUILDERTYPE": "Lc23MuLinesConf", 
    "CONFIG": {
        "CommonRelInfoTools": [
            {
                "Location": "VtxIsoInfo", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "VtxIsoInfoBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "Location": "BsMuMuBIsolation", 
                "Type": "RelInfoBs2MuMuBIsolations", 
                "Variables": [], 
                "makeTrackCuts": False, 
                "tracktype": 3
            }
        ], 
        "Lc23muPrescale": 1, 
        "Lc2mueePrescale": 1, 
        "Lc2pKpiPrescale": 0.01, 
        "Lc2peePrescale": 1, 
        "Lc2pmumuPrescale": 1, 
        "MaxDoca": 0.3, 
        "MaxIPChi2": 100, 
        "MaxTrChi2Dof": 4.0, 
        "MaxTrGhp": 0.4, 
        "MaxVtxChi2": 15, 
        "MinTrIPChi2": 9, 
        "MinTrPT": 300, 
        "MinVD": 70, 
        "Postscale": 1, 
        "mDiffLcLoose": 200, 
        "mDiffLcTight": 150
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

PhiToKSKS = {
    "BUILDERTYPE": "PhiToKSKSAllLinesConf", 
    "CONFIG": {
        "D0_DOCACHI2_MAX": 20, 
        "D0_IPCHI2_MAX": 9, 
        "D0_MASS_WIN": 40, 
        "D0_PT_MIN": 800, 
        "D0_VCHI2NDOF_MAX": 6, 
        "DoDTF": True, 
        "GEC_nLongTrk": 250, 
        "GHOSTPROB_MAX": 0.35, 
        "JPsi_DOCACHI2_MAX": 20, 
        "JPsi_IPCHI2_MAX": 9, 
        "JPsi_MASS_WIN": 120, 
        "JPsi_PT_MIN": 500, 
        "JPsi_VCHI2NDOF_MAX": 6, 
        "KS_DD_DIRA_MIN": 0.999, 
        "KS_DD_FDCHI2_MIN": 100, 
        "KS_DD_FD_MIN": 10.0, 
        "KS_DD_MASS_WINDOW": 20, 
        "KS_DD_PTMIN": 400, 
        "KS_DD_VCHI2NDOF_MAX": 4, 
        "KS_LL_DIRA_MIN": 0.9999, 
        "KS_LL_FDCHI2_MIN": 100, 
        "KS_LL_FD_MIN": 10.0, 
        "KS_LL_MASS_WINDOW": 20, 
        "KS_LL_PTMIN": 400, 
        "KS_LL_VCHI2NDOF_MAX": 4, 
        "K_IPCHI2_MAX": 9, 
        "K_PIDK_MIN": 7, 
        "K_PTMIN": 200, 
        "Lambda_DIRA_MIN": 0.999, 
        "Lambda_FDCHI2_MIN": 100, 
        "Lambda_FD_MIN": 10.0, 
        "Lambda_MASS_WINDOW": 50, 
        "Lambda_PTMIN": 400, 
        "Lambda_VCHI2NDOF_MAX": 4, 
        "Mu_IPCHI2_MAX": 9, 
        "Mu_PIDmu_MIN": 0, 
        "Mu_PTMIN": 200, 
        "Phi_DOCACHI2_MAX": 20, 
        "Phi_IPCHI2_MAX": 9, 
        "Phi_MASS_MAX": 1100, 
        "Phi_PT_MIN": 800, 
        "Phi_VCHI2NDOF_MAX": 6, 
        "etaC_DOCACHI2_MAX": 20, 
        "etaC_IPCHI2_MAX": 9, 
        "etaC_MASS_WIN": 200, 
        "etaC_PT_MIN": 400, 
        "etaC_VCHI2NDOF_MAX": 6, 
        "prescale_D0ToKsKs": 0.25, 
        "prescale_EtaCToLL": 1.0, 
        "prescale_JPsiToKK": 0.002, 
        "prescale_JPsiToKsKs": 1.0, 
        "prescale_PhiToKK": 0.001, 
        "prescale_PhiToKsKs": 1.0, 
        "prescale_PhiToMuMu": 0.01
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "RD" ]
}

RareStrange = {
    "BUILDERTYPE": "RareStrangeLinesConf", 
    "CONFIG": {
        "DiElectronMaxMass": 1000, 
        "DiElectronMinIpChi2": 9, 
        "DiElectronPIDe": 2, 
        "DiElectronVtxChi2": 36, 
        "DiMuonMaxDOCA": 2, 
        "DiMuonMaxMass": 700, 
        "DiMuonMinIpChi2": 9, 
        "DiMuonMinPt": 0, 
        "DiMuonVtxChi2": 36, 
        "KDauMinIpChi2": 9.0, 
        "KDauMinIpChi2Down": 5.0, 
        "KDauMinIpChi2MassMeas": 8.0, 
        "KDauMinIpChi2MassMeasDown": 4.0, 
        "KDauTrChi2": 3.0, 
        "KMassWin": 100.0, 
        "KMassWinDown": 100.0, 
        "KMassWinMassMeas": 50.0, 
        "KMassWinMassMeasDown": 100.0, 
        "KMaxDOCA": 3.0, 
        "KMaxDOCADown": 10.0, 
        "KMaxDOCAMassMeas": 2.0, 
        "KMaxDOCAMassMeasDown": 2.0, 
        "KMaxIpChi2": 25.0, 
        "KMaxIpChi2MassMeas": 25.0, 
        "KMinDIRA": 0.98, 
        "KMinDIRADown": 0.98, 
        "KMinDIRAMassMeas": 0.9998, 
        "KMinDIRAMassMeasDown": 0.999, 
        "KMinPT": 100.0, 
        "KMinPTDown": 0.0, 
        "KMinPTMassMeas": 300.0, 
        "KMinPTMassMeasDown": 250.0, 
        "KMinVDChi2": 36.0, 
        "KMinVDChi2Down": 49.0, 
        "KMinVDChi2MassMeas": 100.0, 
        "KMinVDChi2MassMeasDown": 64.0, 
        "KPiMuMuDownPrescale": 1, 
        "KPiMuMuLFVDownPrescale": 1, 
        "KPiMuMuLFVPrescale": 1, 
        "KPiMuMuPrescale": 1, 
        "KPiPiPiDownPrescale": 0.1, 
        "KPiPiPiMassMeasDownPrescale": 1, 
        "KPiPiPiMassMeasPrescale": 1, 
        "KPiPiPiPrescale": 0.01, 
        "KVDPVMaxDown": 2500.0, 
        "KVDPVMaxMassMeasDown": 2200.0, 
        "KVDPVMinDown": 500.0, 
        "KVDPVMinMassMeasDown": 900.0, 
        "KVtxChi2": 25.0, 
        "KVtxChi2Down": 25.0, 
        "KVtxChi2MassMeas": 10.0, 
        "KVtxChi2MassMeasDown": 20.0, 
        "LambdaMassWin": 500.0, 
        "LambdaMassWinTight": 50.0, 
        "LambdaMaxDOCA": 2.0, 
        "LambdaMaxIpChi2": 36.0, 
        "LambdaMinDIRA": 0.9, 
        "LambdaMinPt": 500.0, 
        "LambdaMinTauPs": 6.0, 
        "LambdaPPiEEPrescale": 1.0, 
        "LambdaPPiPrescale": 0.01, 
        "LambdaVtxChi2": 25.0, 
        "PhiDauMinPT": 400.0, 
        "PhiKMuPrescale": 0.01, 
        "PhiMassMax": 1200, 
        "PhiMassMin": 800, 
        "PhiMaxDOCA": 0.1, 
        "PhiMinDIRA": 0.5, 
        "PhiMinPT": 700, 
        "PhiProbNNk": 0.3, 
        "PhiVtxChi2": 9, 
        "Postscale": 1, 
        "Sigma3DauTrChi2Down": 9.0, 
        "Sigma3MassWin": 500.0, 
        "Sigma3MassWinDown": 500.0, 
        "Sigma3MaxDOCA": 2.0, 
        "Sigma3MaxDOCADown": 2.0, 
        "Sigma3MaxIpChi2": 36.0, 
        "Sigma3MaxIpChi2Down": 100.0, 
        "Sigma3MinDIRA": 0.9, 
        "Sigma3MinDIRADown": 0.1, 
        "Sigma3MinPt": 0.0, 
        "Sigma3MinPtDown": 0.0, 
        "Sigma3MinTauPs": 3, 
        "Sigma3MinTauPsDown": 2, 
        "Sigma3VtxChi2": 36.0, 
        "Sigma3VtxChi2Down": 100.0, 
        "SigmaDauTrChi2Down": 9.0, 
        "SigmaDetVtxChi2": 25, 
        "SigmaMassWin": 500.0, 
        "SigmaMassWinDown": 500.0, 
        "SigmaMaxDOCA": 2.0, 
        "SigmaMaxDOCADown": 10.0, 
        "SigmaMaxIpChi2": 36.0, 
        "SigmaMaxIpChi2Down": 25.0, 
        "SigmaMinDIRA": 0.9, 
        "SigmaMinDIRADown": 0.9, 
        "SigmaMinPt": 500.0, 
        "SigmaMinPtDown": 0.0, 
        "SigmaMinTauPs": 6.0, 
        "SigmaMinTauPsDown": 7.0, 
        "SigmaMuMuMuDownPrescale": 1, 
        "SigmaMuMuMuPrescale": 1, 
        "SigmaPEEDetPrescale": 1, 
        "SigmaPEEDownPrescale": 0.1, 
        "SigmaPEEMassWinDown": 100.0, 
        "SigmaPEEPrescale": 1, 
        "SigmaPEMuPrescale": 1, 
        "SigmaPMuMuDetPrescale": 1, 
        "SigmaPMuMuDownPrescale": 1, 
        "SigmaPMuMuLFVDownPrescale": 0.1, 
        "SigmaPMuMuLFVPrescale": 1, 
        "SigmaPMuMuPrescale": 1, 
        "SigmaPPi0CalPrescale": 1.0, 
        "SigmaPPi0MassWin": 150.0, 
        "SigmaPPi0Prescale": 1, 
        "SigmaVtxChi2": 36.0, 
        "SigmaVtxChi2Down": 25.0, 
        "electronMinIpChi2": 9.0, 
        "electronMinIpChi2Down": 4.0, 
        "electronPIDe": 2.0, 
        "muon3MinIpChi2": 5.0, 
        "muon3MinIpChi2Down": 5.0, 
        "muonMinIpChi2": 9.0, 
        "muonMinIpChi2Down": 9.0, 
        "pMinPt": 500.0, 
        "pi0MinPt": 700.0, 
        "pionMinIpChi2": 9.0, 
        "protonMinIpChi2": 9.0, 
        "protonProbNNp": 0.05, 
        "protonProbNNpTight": 0.5
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

RnS = {
    "BUILDERTYPE": "RnSConf", 
    "CONFIG": {
        "DiDOCA": 0.1, 
        "IP": 0.5, 
        "IPChi2Max": 1000000, 
        "IPChi2Min": 60.0, 
        "K0s2mmLinePostscale": 1, 
        "K0s2mmLinePrescale": 1, 
        "K0s2mmSBCut": 465, 
        "K0s2mmSBLinePostscale": 1, 
        "K0s2mmSBLinePrescale": 0.1, 
        "KSdira": 0, 
        "KSip": 0.9, 
        "KSlife": 1.610411922, 
        "KSsidebmaxMass": 1000, 
        "KSsidebminMass": 600, 
        "KSsignalmaxMass": 600, 
        "KSsignalminMass": 300, 
        "L0life": 4.734283679999999, 
        "MaxIpDistRatio": 0.016666666666666666, 
        "MaxMass": 800, 
        "MinBPVDira": 0, 
        "MinVDZ": 0, 
        "MultibodyChi2dof": 50, 
        "MultibodyIP": 1, 
        "MultibodyIPChi2": 25, 
        "NoMuIDLinePostscale": 1, 
        "NoMuIDLinePrescale": 0.001, 
        "Rho": 4, 
        "SVZ": 650, 
        "SidebandLinePostscale": 1, 
        "SidebandLinePrescale": 0.2, 
        "SignalLinePostscale": 1, 
        "SignalLinePrescale": 1, 
        "TRACK_TRGHOSTPROB_MAX": 0.1, 
        "TTHits": 1, 
        "VertexChi2": 9, 
        "cosAngle": 0.999998, 
        "eIpChi2": 49, 
        "muIpChi2": 36, 
        "muTrChi2Dof": 5, 
        "piIpChi2": 100, 
        "protonIpChi2": 16
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "RD" ]
}

Tau23Mu = {
    "BUILDERTYPE": "Tau23MuLinesConf", 
    "CONFIG": {
        "Ds23PiPrescale": 0.0, 
        "Ds23PiTISPrescale": 0.0, 
        "Ds2PhiPiPrescale": 1.0, 
        "Tau25Prescale": 1.0, 
        "Tau2PMuMuPrescale": 1.0, 
        "TauPostscale": 1.0, 
        "TauPrescale": 1.0, 
        "TrackGhostProb": 0.45
    }, 
    "STREAMS": {
        "Leptonic": [
            "StrippingTau23MuTau23MuLine", 
            "StrippingTau23MuDs2PhiPiLine", 
            "StrippingTau23MuTau2PMuMuLine", 
            "StrippingTau23MuDs23PiLine", 
            "StrippingTau23MuTau25MuLine"
        ]
    }, 
    "WGs": [ "RD" ]
}

Tau2LambdaMu = {
    "BUILDERTYPE": "Tau2LambdaMuLinesConf", 
    "CONFIG": {
        "Tau2LambdaMuPrescale": 1.0, 
        "TauPostscale": 1.0, 
        "TauPrescale": 1.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "RD" ]
}

ZVTOP = {
    "BUILDERTYPE": "ZVTOP_Conf", 
    "CONFIG": {
        "B2TauTauSS_LinePostscale": 1, 
        "B2TauTauSS_LinePrescale": 1, 
        "B2TauTau_LinePostscale": 1, 
        "B2TauTau_LinePrescale": 1, 
        "B_TAUPI_2NDMINIPS": "20", 
        "DIRA_B": "0.99", 
        "DIRA_TAU": "0.98", 
        "DOCA_TAU": "0.4", 
        "FDCHI2_B": "225", 
        "FDCHI2_TAU": "16", 
        "FDRHO_TAU": "0.1", 
        "FDZ_TAU": "2.0", 
        "FD_B": "90", 
        "High_LinePostscale": 1, 
        "High_LinePrescale": 1, 
        "IPCHI2_B_CHILD_BEST": "16", 
        "IPCHI2_B_TAU_CHILD_BEST": "150", 
        "IPCHI2_B_TAU_CHILD_WORSE": "16", 
        "IPCHI2_HAD_ALL_FINAL_STATE": "40", 
        "IPCHI2_TAU": "9", 
        "MASS_HIGH_B": "7000", 
        "MASS_HIGH_TAU": "1800", 
        "MASS_LOW_B": "2000", 
        "MASS_LOW_TAU": "500", 
        "MCOR_HIGH_B": "10000", 
        "MCOR_LOW_B": "0", 
        "MIPCHI2_B": "16", 
        "MIPCHI2_B_HIGH": "16", 
        "PID_HAD_ALL_FINAL_STATE": "5", 
        "PTMAX_HAD_ALL_FINAL_STATE": "500", 
        "PT_B_CHILD_BEST": "2000", 
        "PT_B_PIONS_TOTAL": "7000", 
        "PT_B_TAU_CHILD_BEST": "4000", 
        "PT_B_TM": "1900", 
        "PT_B_TM_HIGH": "2000", 
        "PT_B_TT": "1900", 
        "PT_B_TT_HIGH": "2000", 
        "PT_HAD_ALL_FINAL_STATE": "1200", 
        "PT_TAU": "1250", 
        "P_B_CHILD_BEST": "10000", 
        "P_HAD_ALL_FINAL_STATE": "6000", 
        "TRACKCHI2_HAD_ALL_FINAL_STATE": "4", 
        "TRGHOPROB_HAD_ALL_FINAL_STATE": "0.4", 
        "VCHI2_B": "90", 
        "VCHI2_TAU": "12"
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "RD" ]
}

