###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
B2HHPi0 = {
    "BUILDERTYPE": "StrippingB2HHPi0Conf", 
    "CONFIG": {
        "BMaxIPChi2": 9, 
        "BMaxIPChi2Conv": 20, 
        "BMaxM": 6400, 
        "BMinDIRA": 0.99995, 
        "BMinM": 4200, 
        "BMinPT_M": 3000, 
        "BMinPT_R": 2500, 
        "BMinVVDChi2": 64, 
        "BMinVVDChi2Conv": 32, 
        "BMinVtxProb": 0.001, 
        "ConvLinePostscale": 1.0, 
        "ConvLinePrescale": 1.0, 
        "MergedLinePostscale": 1.0, 
        "MergedLinePrescale": 1.0, 
        "Pi0MinPT_M": 2500, 
        "Pi0MinPT_R": 1500, 
        "PiMaxGhostProb": 0.5, 
        "PiMinIPChi2": 25, 
        "PiMinP": 5000, 
        "PiMinPT": 500, 
        "PiMinTrackProb": 1e-06, 
        "ResPi0MinGamCL": 0.2, 
        "ResolvedLinePostscale": 1.0, 
        "ResolvedLinePrescale": 1.0
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BnoC" ]
}

