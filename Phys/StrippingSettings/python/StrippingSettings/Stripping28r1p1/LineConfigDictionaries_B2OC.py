###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#########################################################################################################
##                          S T R I P P I N G  2 8 r 1 p 1                                             ##
##                                                                                                     ##
##  Configuration for B2OC WG                                                                          ##
##  Contact person: Alison Tully (alison.tully@cern.ch)                                                ##
#########################################################################################################

from GaudiKernel.SystemOfUnits import *

Beauty2Charm ={
  'BUILDERTYPE' : 'Beauty2CharmConf',
  'CONFIG' : {
    "ALL" : { # Cuts made on all charged input particles in all lines (expt. upstream)
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4., 
    'TRGHP_MAX'     : 0.4
    },
    "PIDPION" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MAX'      : 20.,
    'TRGHP_MAX'     : 0.4
    },
    "PIDKAON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MIN'      : -10.,
    'TRGHP_MAX'     : 0.4
    },
    "PIDPROTON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDp_MIN'      : -10.,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xc_PION" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 1.0,
    'PIDK_MAX'      : 10.,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xc_KAON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '150*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 1.0,
    'PIDK_MIN'      : -5.0,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xc_PROTON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '400*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 1.0,
    'PIDp_MIN'      : -5.0,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xb_PION" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '300*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MAX'      : 10.,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xb_KAON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '300*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDK_MIN'      : -5.0,
    'TRGHP_MAX'     : 0.4
    },
    "Xibc_Xb_PROTON" : {
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '300*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.,
    'PIDp_MIN'      : -5.0,
    'TRGHP_MAX'     : 0.4
    },
    "UPSTREAM" : { # Cuts made on all upstream particles
    'TRCHI2DOF_MAX' : 4.,
    'PT_MIN'        : '100*MeV',
    'P_MIN'         : '1000*MeV',
    'MIPCHI2DV_MIN' : 4.
    },
    "KS0" : { # Cuts made on all K shorts
    'PT_MIN'        : '250*MeV',
    'BPVVDCHI2_MIN' : 36,
    'MM_MIN'        : '467.*MeV',
    'MM_MAX'        : '527.*MeV'
    },
    "Lambda0" : { # Cuts made on all Lambda0's
    'PT_MIN'        : '250*MeV',
    'BPVVDCHI2_MIN' : 36,
    'MM_MIN'        : '1086.*MeV',
    'MM_MAX'        : '1146.*MeV'
    },
    "Pi0" : { # Cuts made on all pi0's
    'PT_MIN'        : '500*MeV',
    'P_MIN'         : '1000*MeV',
    'CHILDCL1_MIN'  : 0.25,
    'CHILDCL2_MIN'  : 0.25,
    'FROM_B_P_MIN'  : '2000*MeV',
    'TIGHT_PT_MIN'  : '1500*MeV'
    },
    "gamma" : { # Cuts made on all photons
    'PT_MIN'     : '400*MeV',
    'CL_MIN'     : 0.25,
    'ISNOTE_MIN' : -999.0,
    'PT_VLAPH'   : '145*MeV'
    },
    "D2X" : { # Cuts made on all D's and Lc's used in all lines 
    'ASUMPT_MIN'    : '1800*MeV',
    'ADOCA12_MAX'   : '0.5*mm',
    'ADOCA13_MAX'   : '0.5*mm',
    'ADOCA23_MAX'   : '0.5*mm',
    'ADOCA14_MAX'   : '0.5*mm',
    'ADOCA24_MAX'   : '0.5*mm',
    'ADOCA34_MAX'   : '0.5*mm',
    'ADOCA15_MAX'   : '0.5*mm',
    'ADOCA25_MAX'   : '0.5*mm',
    'ADOCA35_MAX'   : '0.5*mm',
    'ADOCA45_MAX'   : '0.5*mm',
    'VCHI2DOF_MAX'  : 10,
    'BPVVDCHI2_MIN' : 36,
    'BPVDIRA_MIN'   : 0, 
    'MASS_WINDOW'   : '100*MeV'
    },
    "LC_FOR_XIBC" : { # Cuts made on Lc's/Xic(0) used in Xibc lines
    'ASUMPT_MIN'    : '1800*MeV',
    'ADOCA12_MAX'   : '0.5*mm',
    'ADOCA13_MAX'   : '0.5*mm',
    'ADOCA23_MAX'   : '0.5*mm',
    'ADOCA14_MAX'   : '0.5*mm',
    'ADOCA24_MAX'   : '0.5*mm',
    'ADOCA34_MAX'   : '0.5*mm',
    'ADOCA15_MAX'   : '0.5*mm',
    'ADOCA25_MAX'   : '0.5*mm',
    'ADOCA35_MAX'   : '0.5*mm',
    'ADOCA45_MAX'   : '0.5*mm',
    'VCHI2DOF_MAX'  : 8,
    'BPVVDCHI2_MIN' : 36,
    'BPVDIRA_MIN'   : 0, 
    'MASS_WINDOW'   : '50*MeV'
    },
    "D2X_FOR_DDX" : { # Cuts made on all D's and Lc's used in B->DDX lines 
    'ASUMPT_MIN'    : '1800*MeV',
    'ADOCA12_MAX'   : '0.5*mm',
    'ADOCA13_MAX'   : '0.5*mm',
    'ADOCA23_MAX'   : '0.5*mm',
    'ADOCA14_MAX'   : '0.5*mm',
    'ADOCA24_MAX'   : '0.5*mm',
    'ADOCA34_MAX'   : '0.5*mm',
    'ADOCA15_MAX'   : '0.5*mm',
    'ADOCA25_MAX'   : '0.5*mm',
    'ADOCA35_MAX'   : '0.5*mm',
    'ADOCA45_MAX'   : '0.5*mm',
    'VCHI2DOF_MAX'  : 8,
    'BPVVDCHI2_MIN' : 50,
    'BPVDIRA_MIN'   : 0, 
    'MASS_WINDOW'   : '100*MeV'
    },
    "B2X" : { # Cuts made on all B's and Lb's used in all lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 25,
    'BPVLTIME_MIN'  : '0.2*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '4750*MeV', # Lb->X sets this to 5200*MeV
    'AM_MAX'        : '7000*MeV', # B->Dh+-h0 sets this to 5800*MeV
    'B2CBBDT_MIN'   : 0.05
    },
    "Bc2DD" : { # Cuts made on Bc -> DD lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 20,
    'BPVLTIME_MIN'  : '0.05*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '4800*MeV', 
    'AM_MAX'        : '6800*MeV',
    'B2CBBDT_MIN'   : -999.9
    },
    "Xibc" : { # Cuts made on Xibc -> Xc hh, LcD0, Xicc h lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 8,
    'BPVIPCHI2_MAX' : 20,
    'BPVLTIME_MIN'  : '0.05*ps',
    'BPVDIRA_MIN'   : 0.99,
    'AM_MIN'        : '5500*MeV', 
    'AM_MAX'        : '9000*MeV',
    'B2CBBDT_MIN'   : -999.9
    },
    "XiccMu" : { # Cuts made on Xibc -> Xc hh, LcD0, Xicc h lines
    'SUMPT_MIN'     : '4000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 200,
    'BPVLTIME_MIN'  : '0.0*ps',
    'BPVDIRA_MIN'   : 0.99,
    'AM_MIN'        : '3000*MeV',
    'AM_MAX'        : '8000*MeV',
    'B2CBBDT_MIN'   : -999.9
    },
    "Bc2BX" : { # Cuts made on Bc -> BHH lines
    'SUMPT_MIN'     : '1000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 25,
    'BPVLTIME_MIN'  : '0.05*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '6000*MeV',
    'AM_MAX'        : '7200*MeV',
    'DZ1_MIN'       : '-1.5*mm',
    'B2CBBDT_MIN'   : -999.9
    },
    "Bc2DX" : { # Cuts made on Bc -> DX lines
    'SUMPT_MIN'     : '5000*MeV',
    'VCHI2DOF_MAX'  : 10,
    'BPVIPCHI2_MAX' : 25,
    'BPVLTIME_MIN'  : '0.05*ps',
    'BPVDIRA_MIN'   : 0.999,
    'AM_MIN'        : '5800*MeV', 
    'AM_MAX'        : '6800*MeV', 
    'B2CBBDT_MIN'   : -999.9
    },
    "Dstar" : { # Cuts made on all D*'s used in all lines 
    'ADOCA12_MAX'  : '0.5*mm',
    'VCHI2DOF_MAX'  : 10,
    'BPVVDCHI2_MIN' : 36,
    'BPVDIRA_MIN'   : 0, 
    'MASS_WINDOW'   : '600*MeV', # was 50MeV
    'DELTAMASS_MAX' : '200*MeV',
    'DELTAMASS_MIN' : '90*MeV',
    'DM_DSPH_MAX'   : '250*MeV',
    'DM_DSPH_MIN'   : '80*MeV'
    },
    "HH": { # Cuts for rho, K*, phi, XHH Dalitz analyese, etc.
    'MASS_WINDOW'   : {'KST':'150*MeV','RHO':'150*MeV','PHI':'150*MeV'},
    'DAUGHTERS'     : {'PT_MIN':'100*MeV','P_MIN':'2000*MeV'},
    'ADOCA12_MAX'  : '0.5*mm',
    'VCHI2DOF_MAX'  : 16,
    'BPVVDCHI2_MIN' : 16, 
    'BPVDIRA_MIN'   : 0,
    'ASUMPT_MIN'    : '1000*MeV',
    'pP_MIN'        : '5000*MeV' # for pH only (obviously)
    },
    "HHH": { # Cuts for PiPiPi, KPiPi analyese, etc.
    'MASS_WINDOW'   : {'A1':'3500*MeV','K1':'4000*MeV','PPH':'3600*MeV', 'PHH':'4000*MeV'},
    'KDAUGHTERS'    : {'PT_MIN':'100*MeV','P_MIN':'2000*MeV','PIDK_MIN':'-2'},
    'PiDAUGHTERS'   : {'PT_MIN':'100*MeV','P_MIN':'2000*MeV','PIDK_MAX':'10'},
    'pDAUGHTERS'    : {'PT_MIN':'100*MeV','P_MIN':'2000*MeV','PIDp_MIN':'-2'},
    'ADOCA12_MAX'   : '0.40*mm', 
    'ADOCA13_MAX'   : '0.40*mm',  
    'ADOCA23_MAX'   : '0.40*mm',    
    'VCHI2DOF_MAX'  : 8,
    'BPVVDCHI2_MIN' : 16, 
    'BPVDIRA_MIN'   : 0.98,
    'ASUMPT_MIN'    : '1250*MeV',
    'MIPCHI2DV_MIN' : 0.0,
    'BPVVDRHO_MIN'  : '0.1*mm',
    'BPVVDZ_MIN'    : '2.0*mm',
    'PTMIN1'       : '300*MeV',
    'PID'           : {'TIGHTERPI' : { 'P' : {'PIDp_MIN' : -10},
                                       'PI': {'PIDK_MAX' : 8},
                                       'K' : {'PIDK_MIN' : -10}},
                       'REALTIGHTK' : { 'P' : {'PIDp_MIN' : -10},
                                        'PI': {'PIDK_MAX' : 10},
                                        'K' : {'PIDK_MIN' : 4}}}
    },
    'PID' : {
    'P'  : {'PIDp_MIN' : -10},
    'PI' : {'PIDK_MAX' : 20},
    'K'  : {'PIDK_MIN' : -10},
    'TIGHT' : {    'P'  : {'PIDp_MIN' : -5},
                   'PI' : {'PIDK_MAX' : 10},
                   'K'  : {'PIDK_MIN' : -5}},
    'TIGHTER' : {    'P'  : {'PIDp_MIN' : 0},
                     'PI' : {'PIDK_MAX' : 10},
                     'K'  : {'PIDK_MIN' : 0}},
    'TIGHTPI' : { 'P' : {'PIDp_MIN' : -10},
                  'PI': {'PIDK_MAX' : 10},
                  'K' : {'PIDK_MIN' : -10}},
    'TIGHTER1' : {    'P'  : {'PIDp_MIN' : 0},
                     'PI' : {'PIDK_MAX' : 10},
                     'K'  : {'PIDK_MIN' : -1}},                      
    'TIGHTER2' : {    'P'  : {'PIDp_MIN' : 5},
                      'PI' : {'PIDK_MAX' : 10},
                      'K'  : {'PIDK_MIN' : 0}},
    'SPECIAL' : {    'P'  : {'PIDp_MIN' : -5},
                     'PI' : {'PIDK_MAX' : 5},
                     'K'  : {'PIDK_MIN' : 5}},
    'SPECIALPI': {'P' : {'PIDp_MIN' : -10},
                  'PI': {'PIDK_MAX' : 12},
                  'K' : {'PIDK_MIN' : -10}}
    },
    'FlavourTagging':[
    'B02Dst0DKDst02D0Pi0ResolvedD2HHHBeauty2CharmLine',
    'B02Dst0DKDst02D0GammaD2HHHBeauty2CharmLine',
    'B02DKLTUBD2HHHBeauty2CharmLine'
    ],
    'RawEvents' : [
    ],
    'MDSTChannels':[
    ],
    'RelatedInfoTools' : [
      { "Type" : "RelInfoConeVariables", 
        "ConeAngle" : 1.5, 
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
        "Location"  : 'P2ConeVar1'
      }, 
      { "Type" : "RelInfoConeVariables", 
        "ConeAngle" : 1.7, 
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
        "Location"  : 'P2ConeVar2'
      }, 
      { "Type" : "RelInfoConeVariables", 
        "ConeAngle" : 1.0, 
        "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
        "Location"  : 'P2ConeVar3'
      }, 
      { "Type" : "RelInfoVertexIsolation",
        "Location"          : "VertexIsoInfo",
        "IgnoreUnmatchedDescriptors": True,
        "DaughterLocations" : { "[Beauty -> ^X0 X+]CC":"VertexIsoInfoD0",
                                "[Beauty -> X0 ^X+]CC":"VertexIsoInfoDP",
                                "Beauty -> ^X- X+":"VertexIsoInfoDM",
                                "Beauty -> X- ^X+":"VertexIsoInfoDP" }
      },
      { "Type" : "RelInfoConeIsolation",
        "ConeSize" : 0.5,
        "IgnoreUnmatchedDescriptors": True,
        "Location" : "ConeIsoInfo",
        "DaughterLocations" : {
                    "[Beauty -> ^[X0 -> K+ pi-]CC (X+ -> X+ K- pi+)]CC":"ConeIsoInfoD0", # B+ -> D0 D+ block
                    "[Beauty -> [X0 -> K+ pi-]CC ^(X+ -> X+ K- pi+)]CC":"ConeIsoInfoDP",
                    "[Beauty -> [X0 -> ^K+ pi-]CC (X+ -> X+ K- pi+)]CC":"ConeIsoInfoD0_1",
                    "[Beauty -> [X0 -> K+ ^pi-]CC (X+ -> X+ K- pi+)]CC":"ConeIsoInfoD0_2",
                    "[Beauty -> [X0 -> K+ pi-]CC (X+ -> ^X+ K- pi+)]CC":"ConeIsoInfoDP_1",
                    "[Beauty -> [X0 -> K+ pi-]CC (X+ -> X+ ^K- pi+)]CC":"ConeIsoInfoDP_2",
                    "[Beauty -> [X0 -> K+ pi-]CC (X+ -> X+ K- ^pi+)]CC":"ConeIsoInfoDP_3" ,
                    "Beauty -> ^(X+ -> X+ K- pi+) (X- -> X- K+ pi-)":"ConeIsoInfoDP", # B+ -> D+ D- block
                    "Beauty -> (X+ -> X+ K- pi+) ^(X- -> X- K+ pi-)":"ConeIsoInfoDM",
                    "Beauty -> (X+ -> ^X+ K- pi+) (X- -> X- K+ pi-)":"ConeIsoInfoDP_1",
                    "Beauty -> (X+ -> X+ ^K- pi+) (X- -> X- K+ pi-)":"ConeIsoInfoDP_2",
                    "Beauty -> (X+ -> X+ K- ^pi+) (X- -> X- K+ pi-)":"ConeIsoInfoDP_3",
                    "Beauty -> (X+ -> X+ K- pi+) (X- -> ^X- K+ pi-)":"ConeIsoInfoDM_1",
                    "Beauty -> (X+ -> X+ K- pi+) (X- -> X- ^K+ pi-)":"ConeIsoInfoDM_2",
                    "Beauty -> (X+ -> X+ K- pi+) (X- -> X- K+ ^pi-)":"ConeIsoInfoDM_3" }
      },
    ],
    'D0INC' : {'PT_MIN' : 1000, 'IPCHI2_MIN': 100},
    "Prescales" : { # Prescales for individual lines
    'RUN_BY_DEFAULT' : True, # False = lines off by default
    'RUN_RE'         : ['.*'],  
    # Defaults are defined in, eg, Beauty2Charm_B2DXBuilder.py.  Put the full
    # line name here to override. E.g. 'B2D0HD2HHBeauty2CharmTOSLine':0.5.
    'Lb2LcDWSD2HHHPIDBeauty2CharmLine' : 0.1,
    'B2D0KPiPiD2KSHHLLWSBeauty2CharmLine' : 0.1,
    'B2D0KPiPiD2KSHHDDWSBeauty2CharmLine': 0.1,
    'B2D0PiPiPiD2KSHHLLWSBeauty2CharmLine' : 0.1,
    'B2D0PiPiPiD2KSHHDDWSBeauty2CharmLine' : 0.1,
    'B02DKLTUBD2HHHBeauty2CharmLine' : 0.04
    },
    'GECNTrkMax'   : 500
  }, 
  'STREAMS' : { 
    'BhadronCompleteEvent' : [
    # Nathan (nathan.jurik@cern.ch)
    'StrippingB2D0PiD2KSHHLLPartialDBeauty2CharmLine',
    'StrippingB2D0PiD2KSHHDDPartialDBeauty2CharmLine',
    'StrippingB2D0KD2KSHHLLPartialDBeauty2CharmLine',
    'StrippingB2D0KD2KSHHDDPartialDBeauty2CharmLine',
    
    # Eva's lines (evelina.gersabeck@cern.ch)
    'StrippingB02DKPiPiD2HHHPIDBeauty2CharmLine',
    'StrippingB02DPiPiPiD2HHHPIDBeauty2CharmLine'
    ],  
    'Bhadron' : [
    
    # New RelatedInfo tools for B -> K(*)tau nu (giulio.dujany@cern.ch)
    'StrippingB2D0DBeauty2CharmLine',
    'StrippingB02DDBeauty2CharmLine',
    
    # Nis Meinert (nis.meinert@cern.ch)
    'StrippingLb2D0Lambda0DDD02K3PiBeauty2CharmLine',
    'StrippingLb2D0Lambda0LLD02K3PiBeauty2CharmLine',

    # Wenbin (wenbin.qian@cern.ch)
    'StrippingLb2LcDWSD2HHHPIDBeauty2CharmLine',

    # Susan (haines@hep.phy.cam.ac.uk)
    'StrippingB2D0KPiPiD2KSHHLLWSBeauty2CharmLine',
    'StrippingB2D0KPiPiD2KSHHDDWSBeauty2CharmLine',
    'StrippingB2D0PiPiPiD2KSHHLLWSBeauty2CharmLine',
    'StrippingB2D0PiPiPiD2KSHHDDWSBeauty2CharmLine',

    # Anton (anton.poluektov@cern.ch)
    'StrippingB02Dst0KPiDst02D0Pi0D2HHResolvedBeauty2CharmLine',
    'StrippingB02Dst0KKDst02D0Pi0D2HHResolvedBeauty2CharmLine',
    'StrippingB02Dst0PiPiDst02D0GammaD2HHBeauty2CharmLine',
    'StrippingB02Dst0KPiDst02D0GammaD2HHBeauty2CharmLine',
    'StrippingB02Dst0KKDst02D0GammaD2HHBeauty2CharmLine',
    'StrippingB02Dst0HHWSDst02D0GammaD2HHBeauty2CharmLine',
    'StrippingB02Dst0DKDst02D0Pi0ResolvedD2HHHBeauty2CharmLine',
    'StrippingB02Dst0DKDst02D0GammaD2HHHBeauty2CharmLine',
    
    # Michele (michele.veronesi@cern.ch)
    'StrippingB02DKLTUBD2HHHBeauty2CharmLine'

]
  },
  'WGs' : [ 'B2OC' ]
}



