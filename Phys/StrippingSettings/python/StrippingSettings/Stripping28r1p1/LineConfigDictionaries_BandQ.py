###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  2 8 r 1 p 1                    ##
##                                                                            ##
##  Configuration for B&Q WG                                                  ##
##  Contact person: andrii.usachov@cern.ch                                    ##
################################################################################

Ccbar2LstLst = {
    "BUILDERTYPE": "Ccbar2LstLstConf",
    "CONFIG": {
        "CombMaxMass": 6100.0,
        "CombMinMass": 2800.0,
        "KaonPTSec": 100.0,
        "KaonProbNNk": 0.1,
        "LstMaxMass": 1600,
        "LstMinMass": 1400,
        "LstVtxChi2": 16.0,
        "MaxMass": 6000.0,
        "MinMass": 2850.0,
        "ProtonP": 5000,
        "ProtonPTSec": 100.0,
        "ProtonProbNNp": 0.1,
        "TRCHI2DOF": 5.0
    },
    "STREAMS": [ "Charm" ],
    "WGs": [ "BandQ" ]
}

Ccbar2PhiPhiPiPi = {
    "BUILDERTYPE": "Ccbar2PhiPhiPiPiConf", 
    "CONFIG": {
        "EtacComN4Cuts": "\n                          (AM > 2.7 *GeV)\n                          ", 
        "EtacMomN4Cuts": "(VFASPF(VCHI2/VDOF) < 16.) & (MM>2.7*GeV) & (BPVDLS>3)", 
        "KaonIPCHI2": 4.0, 
        "KaonPT": 100, 
        "KaonProbNNk": 0.1, 
        "PhiMassW": 30.0, 
        "PhiVtxChi2": 25.0, 
        "PionCuts": "(PROBNNpi > 0.1) & (PT > 100*MeV) & (MIPCHI2DV(PRIMARY)>4.)", 
        "Prescale": 1.0, 
        "TRCHI2DOF": 5.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

XibcBDT = {
    "BUILDERTYPE": "XibcBDTConf", 
    "CONFIG": {
        "D0Cuts": "(ADMASS('D0')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>64)", 
        "DplusCuts": "(ADMASS('D+')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>100)", 
        "HighPTKaonCuts": "(PROBNNk > 0.1) & (PT>1.0*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "HighPTPionCuts": "(PROBNNpi> 0.2) & (PT>0.5*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "HighPTProtonCuts": "(PROBNNp> 0.05) & (PT>1.0*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "LambdaDDCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25)", 
        "LambdaLLComCuts": "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LambdaLLCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)", 
        "LbComCuts": "(APT>1.0*GeV) & (ADAMASS('Lambda_b0')<80*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LbMomCuts": "(ADMASS('Lambda_b0')<60*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>64)", 
        "LcComCuts": "(APT>1.0*GeV) & (ADAMASS('Lambda_c+')<50*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LcMomCuts": "(ADMASS('Lambda_c+')<30*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>16)", 
        "PhiCuts": "\n                          (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<1.05*GeV) & (MIPCHI2DV(PRIMARY)>2.)\n                          & (INTREE( (ID=='K+') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          & (INTREE( (ID=='K-') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          ", 
        "Pion4LPCuts": "(PROBNNpi> 0.2) & (PT>100*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>9.)", 
        "PionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "ProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "TightKaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "TightPionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "TightProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "UnKaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPTKaonCuts": "(PROBNNk > 0.1) & (PT>600*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPTPionCuts": "(PROBNNpi> 0.2) & (PT>500*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPTProtonCuts": "(PROBNNp> 0.05) & (PT>750*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4)", 
        "UnProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "Xibc2D0pKMVACut": "0.05", 
        "Xibc2D0pKXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2D0pK_BDT_v1r0.xml", 
        "Xibc2LambdaPhiMVACut": "-0.1", 
        "Xibc2LambdaPhiXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2LambdaPhi_BDT_v1r0.xml", 
        "Xibc2LbKpiMVACut": "-0.3", 
        "Xibc2LbKpiXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2LbKpi_BDT_v1r0.xml", 
        "Xibc2LcKMVACut": "-0.01", 
        "Xibc2LcPiMVACut": "0.", 
        "Xibc2LcPiXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2LcPi_BDT_v1r0.xml", 
        "Xibc2pKMVACut": "0.", 
        "Xibc2pKXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2pK_BDT_v1r0.xml", 
        "XibcComCuts": "(AM>4.8*GeV)", 
        "XibcLPMomCuts": "(M>5.0*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVIPCHI2()<25)", 
        "XibcMomCuts": "(M>5.0*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVIPCHI2()<25) & (BPVDIRA> 0.99)", 
        "XibcP2D0pKpiMVACut": "-0.05", 
        "XibcP2D0pKpiXmlFile": "$TMVAWEIGHTSROOT/data/XiccP2D0pKpi_BDT_v1r0.xml", 
        "XibcP2DpKMVACut": "0.02", 
        "XibcP2DpKXmlFile": "$TMVAWEIGHTSROOT/data/XiccP2DpK_BDT_v1r0.xml", 
        "XibcP2LambdaPiMVACut": "0.1", 
        "XibcP2LambdaPiXmlFile": "$TMVAWEIGHTSROOT/data/XibcP2LambdaPi_BDT_v1r0.xml", 
        "XibcP2LcKpiMVACut": "0.10", 
        "XibcP2LcKpiXmlFile": "$TMVAWEIGHTSROOT/data/XibcP2LcKpi_BDT_v1r0.xml", 
        "XibcP2pKpiMVACut": "0.14", 
        "XibcP2pKpiXmlFile": "$TMVAWEIGHTSROOT/data/XibcP2pKpi_BDT_v1r0.xml"
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BandQ" ]
}

