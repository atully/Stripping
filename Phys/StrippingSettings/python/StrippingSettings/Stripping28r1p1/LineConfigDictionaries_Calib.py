###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
Dp2EtaPrimePip = {
    "BUILDERTYPE": "StrippingDp2EtaPrimePipConf", 
    "CONFIG": {
        "Bachelor_IPCHI2_MIN": 25.0, 
        "Bachelor_PIDK_MIN": -999.0, 
        "Bachelor_PT_MIN": 600.0, 
        "Bachelor_P_MIN": 1000.0, 
        "DTF_CHI2NDOF_MAX": 5, 
        "D_BPVDIRA_MIN": 0.999975, 
        "D_BPVLTIME_MIN": 0.00025, 
        "D_IPCHI2_MAX": 10, 
        "D_IP_MAX": 0.05, 
        "D_Mass_MAX": 2100.0, 
        "D_Mass_MIN": 1750.0, 
        "D_PT_Min": 2000.0, 
        "D_VCHI2PDOF_MAX": 5, 
        "Daug_ETA_MAX": 5.0, 
        "Daug_ETA_MIN": 2.0, 
        "Daug_IPCHI2_MIN": 25.0, 
        "Daug_PT_MIN": 500.0, 
        "Daug_P_MAX": 100000.0, 
        "Daug_P_MIN": 1000.0, 
        "Daug_TRCHI2DOF_MAX": 5, 
        "Daug_TRGHOSTPROB_MAX": 0.5, 
        "Dipion_DOCACHI2_MAX": 15, 
        "Dipion_Mass_MAX": 1200.0, 
        "Dipion_Mass_MIN": 200.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": "HLT_PASS_RE('Hlt2.*D.*Etap.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')", 
        "Neut_PT_MIN": 1000.0, 
        "Pi_PIDK_MAX": 0, 
        "PostscaleDp2PipEtaPrimePPG": 1, 
        "PrescaleDp2PipEtaPrimePPG": 1, 
        "Res_Mass_MAX": 1050.0, 
        "Res_Mass_MIN": 900.0
    }, 
    "STREAMS": [ "Calibration" ], 
    "WGs": [ "ALL" ]
}

DstarD2hhGammaCalib = {
    "BUILDERTYPE": "DstarD2hhGammaCalib", 
    "CONFIG": {
        "CombMassHigh": 2120.0, 
        "CombMassHigh_HH": 1820.0, 
        "CombMassLow": 1380.0, 
        "CombMassLow_HH": 500.0, 
        "Daug_TRCHI2DOF_MAX": 3, 
        "Dstar_AMDiff_MAX": 180.0, 
        "Dstar_MDiff_MAX": 160.0, 
        "Dstar_VCHI2VDOF_MAX": 15.0, 
        "HighPIDK": -1, 
        "Hlt1Filter": None, 
        "Hlt1Tos": {
            "Hlt1.*Track.*Decision%TOS": 0
        }, 
        "Hlt2Filter": None, 
        "Hlt2Tos": {
            "Hlt2CharmHadInclDst2PiD02HHXBDTDecision%TOS": 0, 
            "Hlt2PhiIncPhiDecision%TOS": 0
        }, 
        "LowPIDK": 5, 
        "MassHigh": 2100.0, 
        "MassHigh_HH": 1810.0, 
        "MassLow": 1400.0, 
        "MassLow_HH": 600.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxIPChi2": 20, 
        "MaxVCHI2NDOF": 10.0, 
        "MaxVCHI2NDOF_HH": 10.0, 
        "MinBPVDIRA": 0.99, 
        "MinBPVTAU": 0.0001, 
        "MinCombPT": 1500.0, 
        "MinPT": 2500.0, 
        "MinTrkIPChi2": 10, 
        "MinTrkPT": 500.0, 
        "MinVDCHI2_HH": 1000.0, 
        "MinVDCHI2_HHComb": 1000.0, 
        "PiSoft_PT_MIN": 200.0, 
        "PrescaleDstarD2KKGamma": 1, 
        "PrescaleDstarD2KPiGamma": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5, 
        "photonPT": 200.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

ElectronRecoEff = {
    "BUILDERTYPE": "StrippingElectronRecoEffLines", 
    "CONFIG": {
        "DetachedEEK": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 20, 
            "bmass_ip_constraint": -3.0, 
            "overlapCut": 0.95, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DetachedEK": {
            "AM": 5000.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 36, 
            "bCandFlightDist": 4.0
        }, 
        "DetachedMuK": {
            "AM": 5000.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 100, 
            "bCandFlightDist": 4.0
        }, 
        "DetachedMuMuK": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 20, 
            "bmass_ip_constraint": -3.0, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DoVeloDecoding": False, 
        "EtaMaxVelo": 5.1, 
        "EtaMinVelo": 1.9, 
        "Hlt1Req": {
            "DetachedEEK": "HLT_PASS_RE('Hlt1TrackMVA.*Decision')", 
            "DetachedEK": "HLT_PASS_RE('Hlt1TrackMVA.*Decision')", 
            "DetachedMuK": "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')", 
            "DetachedMuMuK": "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')"
        }, 
        "Hlt2Req": {
            "DetachedEK": "HLT_PASS_RE('Hlt2.*Topo.*Decision')", 
            "DetachedMuK": "HLT_PASS_RE('Hlt2.*Topo.*Decision')"
        }, 
        "L0Req": {
            "DetachedEEK": "L0_CHANNEL('Electron')", 
            "DetachedEK": "L0_CHANNEL_RE('Electron|Hadron')", 
            "DetachedEPi": "L0_CHANNEL('Electron')", 
            "DetachedMuK": "L0_CHANNEL_RE('Muon|Hadron')", 
            "DetachedMuMuK": "L0_CHANNEL('Muon')", 
            "DetachedMuPi": "L0_CHANNEL('Muon')"
        }, 
        "SharedChild": {
            "EtaMaxEle": 5.1, 
            "EtaMaxMu": 5.1, 
            "EtaMinEle": 1.8, 
            "EtaMinMu": 1.8, 
            "IPChi2Ele": 12, 
            "IPChi2Kaon": 12, 
            "IPChi2Mu": 12, 
            "IPChi2Pion": 36, 
            "IPEle": 0.0, 
            "IPKaon": 0.0, 
            "IPMu": 0.0, 
            "IPPion": 0.0, 
            "ProbNNe": 0.2, 
            "ProbNNk": 0.2, 
            "ProbNNmu": 0.5, 
            "ProbNNpi": 0.8, 
            "PtEle": 1200.0, 
            "PtKaon": 500.0, 
            "PtMu": 1200.0, 
            "PtPion": 1000.0, 
            "TrChi2Ele": 5, 
            "TrChi2Kaon": 5, 
            "TrChi2Mu": 5, 
            "TrChi2Pion": 5
        }, 
        "TrackGEC": 120, 
        "VeloFitter": "SimplifiedGeometry", 
        "VeloMINIP": 0.05, 
        "VeloTrackChi2": 3.5
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "ALL" ]
}

