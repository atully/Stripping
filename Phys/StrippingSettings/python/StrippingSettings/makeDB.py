#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import sys

'''
Generate a dbase module from a set of stripping line config
dictionaries.

Usage: 
  > python makeDB <Stripping> 
'''

if __name__ == '__main__' :
    from StrippingSettings.Utils import generate_dbase
    
    if 2 <= len(sys.argv):
        generate_dbase( sys.argv[1] )
    if 3 <= len(sys.argv) :
        os.system('mv stripping.tmp ' + sys.argv[2])
