###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  2 8                            ##
##                                                                            ##
##  Configuration for Calib WG                                                ##
##  Contact person: Michael Kolpin (Michael.Kolpin@cern.ch)                   ##
################################################################################

from GaudiKernel.SystemOfUnits import *

TrackEffMuonTT = {
    'WGs'         : ['ALL'],
    'STREAMS'           : [ 'Calibration' ],
    'BUILDERTYPE' : 'StrippingTrackEffMuonTTConf',
    'CONFIG'      : { 
			'JpsiMassWin'                 : 500,
			'UpsilonMassWin'              : 1500,
			'ZMassWin'                    : 40000,
			'BMassWin'                    : 500,
			'JpsiMuonTTPT'                : 0,
			'UpsilonMuonTTPT'             : 500,
			'ZMuonTTPT'                   : 500,
			'JpsiLongPT'                  : 1300,
			'UpsilonLongPT'               : 1000,
			'ZLongPT'                     : 10000,
			'JpsiPT'                      : 1000,
			'UpsilonPT'                   : 0,
			'ZPT'                         : 0,
			'JpsiLongMuonMinIP'           : 0.5,
			'UpsilonLongMuonMinIP'        : 0,
			'ZLongMuonMinIP'              : 0,
			'JpsiMINIP'                   : 3,
			'UpsilonMINIP'                : 10000, #this is a dummy
			'ZMINIP'                      : 10000, #this is a dummy
			'BJpsiKMINIP'                 : 10000, #this is a dummy
			'JpsiLongMuonTrackCHI2'       : 5,
			'UpsilonLongMuonTrackCHI2'    : 5,
			'ZLongMuonTrackCHI2'          : 5,
			'VertexChi2'                  : 5,
			'LongMuonPID'                 : 2,
            'JpsiHlt1Filter'              : 'Hlt1.*Decision',
            'JpsiHlt2Filter'              : 'Hlt2.*Decision',
			'JpsiHlt1Triggers'            :  { "Hlt1TrackMuonDecision%TOS" : 0},
            'Hlt1PassOnAll'               : True,
			'UpsilonHlt1Triggers'         :  { "Hlt1SingleMuonHighPTDecision%TOS" : 0},
			'ZHlt1Triggers'               :  { "Hlt1SingleMuonHighPTDecision%TOS" : 0},
			'JpsiHlt2Triggers'            :  { "Hlt2SingleMuon.*Decision%TOS" : 0, "Hlt2TrackEffDiMuonMuonTT.*Decision%TUS" : 0},
			'UpsilonHlt2Triggers'         :  { "Hlt2SingleMuonLowPTDecision%TOS" : 0},
			'ZHlt2Triggers'               :  { "Hlt2EWSingleMuonVHighPtDecision%TOS" : 0},
			'BJpsiKHlt2TriggersTUS'       :  { "Hlt2TopoMu2BodyBBDTDecision%TUS" : 0},
			'BJpsiKHlt2TriggersTOS'       :  { "Hlt2TopoMu2BodyBBDTDecision%TOS" : 0},
			'JpsiPrescale'                : 0,
			'UpsilonPrescale'             : 1,
			'ZPrescale'                   : 1,
			'BJpsiKPrescale'              : 1,
			'Postscale'                   : 1
                    }
    }

CharmFromBSemiForHadronAsy = {
        'WGs'         : ['ALL'],
        'BUILDERTYPE' : 'CharmFromBSemiForHadronAsyAllLinesConf',
        'CONFIG'      : {
            "prescale_LbRS" : 1.0
            ,"prescale_LbWS" : 0.2 
            ,"prescale_D0to3piRS" : 1.0
            ,"prescale_D0to3piWS" : 0.2
            ,"prescale_D0toK2piRS" : 1.0
            ,"prescale_D0toK2piWS" : 0.2 
            #### common to all modes
            ,"GEC_nLongTrk" : 250. # adimensional
            ,"GHOSTPROB_MAX" : 0.35 #adimensional
            ,"Mu_PT" : 800. # MeV
            ,"H_PT"  : 250. # MeV
            ,"Pi_PIDKMax" : 6. # adimensional
            ,"K_PIDKMin"  : 6. # adimensional
            ,"Slowpi_PIDKMax" : 10. # adimensional
            ,"Slowpi_PIDeMax" : 99. # adimensional
            ,"Slowpi_PTMin"   : 200. # MeV
            ##### specific to D0 modes
            ,"MuPi_SUMPT_MIN" : 1300. # MeV
            ,"MuPi_DOCACHI2_MAX" : 8.0 # adimensional
            ,"MuPi_CHI2NDOF_MAX" : 3.0 # adimensional
            ,"MuPi_DIRA_MIN" : -99. # adimensional
            ,"MuPi_FDCHI2_MIN" : 20. # adimensional
            ,"D0to3H_REQUIRE_TOS" : True # bool
            ,"D0to3H_DOCACHI2_MAX" : 10. # adimensional
            ,"D0to3H_VCHI2NDF_MAX" : 3.0 # adimensional
            ,"D0to3H_SUMPT_MIN" : 1800. # MeV
            ,"D0to3H_DZ" : 2.0 # mm
            ,"D0to3H_3pi_DeltaMass_MAX" : 350. # MeV
            ,"D0to3H_K2pi_DeltaMass_MAX" : 250. # MeV
            ,"D0to3H_3pi_MASS_MIN" : 900. # MeV
            ,"D0to3H_3pi_MASS_MAX" : 1400. # MeV
            ,"D0to3H_K2pi_MASS_MIN" : 1300. # MeV
            ,"D0to3H_K2pi_MASS_MAX" : 1800. # MeV
            ,"D0to3H_B_MASS_MIN" : 1800. # MeV
            ,"D0to3H_B_MASS_MAX" : 4900. # MeV
            ,"D0to3H_B_DIRA_MIN" : 0.99 # adimensional
            ,"D0to3H_B_VCHI2NDF_MAX" : 15. # adimensional
            ,"D0to3H_B_DOCACHI2_MAX" : 50. # adimensional
            #### specific to Lambda_c+ modes
            ,"PiPi_MASS_MAX" : 500.  # MeV
            ,"PiPi_DOCACHI2_MAX" : 15. # adimensional
            ,"PiPi_CHI2NDF" : 3. # adimensional
            ,"PiPi_SUMPT_MIN" : 600.  # MeV
            ,"MuPiPi_DOCACHI2_MAX" : 15. 
            ,"MuPiPi_CHI2NDF": 3.
            ,"MuPiPi_FDCHI2_MIN" : 20. # adimensional
            ,"Lc2Kpi_REQUIRE_TOS" : True # bool
            ,"Lc2Kpi_DOCACHI2_MAX" : 10. # adimensional
            ,"Lc2Kpi_VCHI2NDF_MAX" : 3.0 # adimensional
            ,"Lc2Kpi_SUMPT_MIN" : 1500.  # MeV
            ,"Lc2Kpi_FDCHI2_MIN" : 20. # adimensional
            ,"Lc2Kpi_MASS_MIN" : 800.  # MeV
            ,"Lc2Kpi_MASS_MAX" : 1350. # MeV
            ,"Lc2Kpi_DeltaMass_MAX" : 700. # MeV
            ,"Lc2Kpi_DZ" : 1.0 # mm
            ,"Lc2Kpi_B_MASS_MIN" : 2200. # MeV
            ,"Lc2Kpi_B_MASS_MAX" : 4300. # MeV
            ,"Lc2Kpi_B_FDCHI2_MIN" : 20. # adimensional
            ,"Lc2Kpi_B_DIRA_MIN" : 0.99 # adimensional
            ,"Lc2Kpi_B_DOCACHI2_MAX" : 50. # adimensional
            ,"Lc2Kpi_B_VCHI2NDF_MAX" : 15. # adimensional
            },
        'STREAMS'     : ["Calibration"]
    }

D02KPiPi0 = {
    'WGs'         : ['ALL'],
    'BUILDERTYPE' : 'StrippingD02KPiPi0Conf',
    'CONFIG'      : {   'TrackMinPT_M'         : 300       # MeV
                       ,'TrackMinPT_R'         : 600       # MeV
                       ,'TrackMinTrackProb'    : 0.000001  # unitless
                       ,'TrackMaxGhostProb'    : 0.3       # unitless
                       ,'TrackMinIPChi2'       : 16        # unitless
                       ,'Pi0MinPT_M'           : 2000      # MeV
                       ,'Pi0MinPT_R'           : 1000      # MeV
                       ,'ResPi0MinGamCL'       : 0.2       # unitless
                       ,'D0MinM'               : 1600      # MeV
                       ,'D0MaxM'               : 2100      # MeV
                       ,'D0MinVtxProb'         : 0.001     # unitless
                       ,'D0MaxIPChi2'          : 9         # unitless
                       ,'D0MinDIRA'            : 0.9999    # unitless
                       ,'D0MinVVDChi2'         : 64        # unitless
                       ,'MergedLinePrescale'   : 0.5       # unitless
                       ,'MergedLinePostscale'  : 1.        # unitless
                       ,'ResolvedLinePrescale' : 0.5       # unitless
                       ,'ResolvedLinePostscale': 1.        # unitless
                      },
    'STREAMS'     : ['Calibration']
    }

TrackEffD0ToK3Pi = {
        'WGs'         : ['ALL'],
        'BUILDERTYPE' : 'TrackEffD0ToK3PiAllLinesConf',
        'STREAMS':["Calibration"],
        'CONFIG'      : {
            "HLT2" : "HLT_PASS_RE('Hlt2.*CharmHad.*HHX.*Decision')",
            "TTSpecs" : {'Hlt2.*CharmHad.*HHX.*Decision%TOS':0},
            "VeloLineForTiming":False,
            "VeloFitter":"SimplifiedGeometry",
            "DoVeloDecoding": False,
            "RequireDstFirst":False,
            "Dst_MAX_M":2.035*GeV,
            "Dst_MAX_DTFCHI2":3.0,
            "Sc_MAX_M":2.5*GeV,
            "Sc_MAX_DTFCHI2":3.0,
            "D0_MIN_FD":5.0*mm,
            "LC_MIN_FD":2.0*mm,
            "D0_MAX_DOCA":0.05*mm,
            "VeloMINIP":0.05*mm,
            "Kaon_MIN_PIDK":7,
            "Pion_MAX_PIDK":4
            }
    }

TrackEffD0ToKPi = {
        'WGs'         : ['ALL'],
        'BUILDERTYPE' : 'TrackEffD0ToKPiAllLinesConf',
        'STREAMS':["Calibration"],
        'CONFIG'      : {
            "Monitor":False,
            "HLT1" :"HLT_PASS_RE('Hlt1TrackMVADecision')",
            "HLT2" :"HLT_PASS_RE('Hlt2TrackEff_D0.*Decision')",
            "TTSpecs" :{'Hlt1TrackMVADecision%TOS':0,'Hlt2TrackEff_D0.*Decision%TOS':0},
            "Tag_MIN_PT":1000.,
            "VeloMINIPCHI2":4.0,
            "Kaon_MIN_PIDK":0,
            "Pion_MAX_PIDK":20,
            "Dst_M_MAX":2100,
            "Dst_DTFCHI2_MAX":10
            }
    }

ElectronIDCalib = {
    'WGs'         : ['ALL'],
    'BUILDERTYPE' : 'ElectronIDConf',
    'CONFIG'      : {
        'JpsiLinePrescale'            :   0.5   ,
        'JpsiLineHltFilter'           : None    ,
        'Both_PT'                 :  500.   ,  # MeV
        'Both_P'                  : 3000.   ,  # MeV
        'Both_TRCHI2DOF'          :    5.   ,
        'Both_MIPCHI2'            :    9.   ,
        'Tag_PT'                  : 1500.   ,  # MeV
        'Tag_P'                   : 6000.   ,  # MeV
        'Tag_PIDe'                :    5.   ,
        'Tag_MIPCHI2'             :    9.   ,
        'Probe_PT'                :  500.   ,  # MeV
        'Probe_P'                 : 3000.   ,  # MeV
        'Probe_MIPCHI2'           :    9.   ,
        'eeCombMinMass'           : 2100.   ,  # MeV         
        'eeCombMaxMass'           : 4300.   ,  # MeV   
        'eeVCHI2PDOF'             :    9.   ,
        'eeMinMass'               : 2200.   ,  # MeV 
        'eeMaxMass'               : 4200.   ,  # MeV
        'JpsiLineCut'             : "(PT>2.*GeV) & (BPVDLS>50) ",
        'Bu2JpsieeKLine_Prescale'  :  1,
        'Bu2JpsieeKLine_HltFilter' : None,
        'Bu2JpsieeKLine_KaonCut'   : "(TRCHI2DOF<4) & (PT>1.0*GeV) & (PIDK >0) & (BPVIPCHI2()>9)",
        'Bu2JpsieeKLine_JpsiCut'   : "(BPVDLS>5)",
        'Bu2JpsieeKLine_BuComCut'  : "in_range(4.1*GeV,AM,6.1*GeV)",
        'Bu2JpsieeKLine_BuMomCut'  : "in_range(4.2*GeV,M, 6.0*GeV) & (VFASPF(VCHI2PDOF)<9)"
        },
    'STREAMS'     :  [ 'PID' ]
    }
