###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##################################################################################
##                          S T R I P P I N G  2 4 r 1                          ##
##                                                                              ##
##  Configuration for QEE WG                                                    ##
##  Contact person: Chitsanu Khurewathanakul (chitsanu.khurewathanakul@cern.ch) ##
##################################################################################

A1MuMu = {
    "BUILDERTYPE": "A1MuMuConf", 
    "CONFIG": {
        "A1MuMu_LinePostscale": 1.0, 
        "A1MuMu_LinePrescale": 1.0, 
        "A1MuMu_checkPV": False, 
        "DIMUON_LOW_MASS": 5000.0, 
        "PT_DIMUON_MIN": 7500.0, 
        "PT_MUON_MIN": 2500.0, 
        "P_MUON_MIN": 2500.0, 
        "TRACKCHI2_MUON_MAX": 10, 
        "VCHI2_DIMUON_MAX": 12
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "QEE" ]
}

DY2ee = {
    "BUILDERTYPE": "DY2eeConf",
    "CONFIG": {
        "DY2eeLine3Prescale": 1.0,
        "DY2eeLine4Prescale": 1.0,
        "DY2eeLinePostscale": 1.0,
        "DY3MaxMass": 20000.0,
        "DY3MinMass": 10000.0,
        "DY4MaxMass": 40000.0,
        "DY4MinMass": 20000.0,
        "ECalMin": 0.1,
        "HCalMax": 0.05,
        "PrsCalMin": 50.0,
        "ePID": 1.0,
        "p3": 10000.0,
        "p4": 10000.0,
        "pT3": 3000.0,
        "pT4": 5000.0
    },
    "STREAMS": [ "EW" ],
    "WGs": [ "QEE" ]
}

DisplVertices = {
    "BUILDERTYPE": "DisplVerticesConf",
    "CONFIG": {
        "DoubleSelection": {
            "ApplyMatterVeto": False,
            "ApplyMatterVetoOne": True,
            "MaxFractE1Track": 10.0,
            "MaxFractTrwHitBefore": 10.0,
            "MinHighestMass": 4500.0,
            "MinMass": 4000.0,
            "MinNumTracks": 4,
            "MinRho": 0.4,
            "MinSumPT": 3000.0,
            "PreScale": 1.0,
            "RawEvents": []
        },
        "FilterVelo": {
            "Apply": True,
            "MinDOCABeamLine": -2.0,
            "MinIP": 0.1,
            "MinIPChi2": -1.0,
            "MinNumTracks": 4,
            "PVLocation": "Rec/Vertex/Primary",
            "RemoveBackwardTracks": True,
            "RemovePVTracks": False,
            "RemoveVeloClones": True
        },
        "HLT": {
            "HLTPS": [
                [
                    [
                        "0x001c0028",
                        "0x002f002c"
                    ],
                    "HLT_PASS_RE('Hlt2DisplVerticesSinglePostScaledDecision')"
                ],
                [
                    [
                        "0x00340032",
                        "0x00730035"
                    ],
                    "HLT_PASS_RE('Hlt2DisplVerticesSinglePostScaledDecision')"
                ],
                [
                    [
                        "0x00750037",
                        "0x007b0038"
                    ],
                    "HLT_PASS_RE('Hlt2DisplVertices(Single|Double|SingleMV)PostScaledDecision')"
                ],
                [
                    [
                        "0x007e0039",
                        "0x0097003d"
                    ],
                    "HLT_PASS_RE('Hlt2DisplVertices(Single|Double|SingleMV)PostScaledDecision')"
                ],
                [
                    [
                        "0x00990042",
                        "0x40000000"
                    ],
                    "HLT_PASS_RE('Hlt2DisplVertices(Single|SingleLoose|Double)PSDecision')"
                ]
            ],
            "SignalLines": [
                [
                    [
                        "0x001c0028",
                        "0x002f002c"
                    ],
                    [ "Hlt2DisplVerticesSingleDecision" ]
                ],
                [
                    [
                        "0x00340032",
                        "0x00730035"
                    ],
                    [
                        "Hlt2DisplVerticesHighFDSingleDecision",
                        "Hlt2DisplVerticesHighMassSingleDecision",
                        "Hlt2DisplVerticesLowMassSingleDecision",
                        "Hlt2DisplVerticesSingleDownDecision"
                    ]
                ],
                [
                    [
                        "0x00750037",
                        "0x007b0038"
                    ],
                    [
                        "Hlt2DisplVerticesSingleDecision",
                        "Hlt2DisplVerticesSingleDownDecision",
                        "Hlt2DisplVerticesSingleHighFDPostScaledDecision",
                        "Hlt2DisplVerticesSingleHighMassPostScaledDecision"
                    ]
                ],
                [
                    [
                        "0x007e0039",
                        "0x0097003d"
                    ],
                    [
                        "Hlt2DisplVerticesSingleDecision",
                        "Hlt2DisplVerticesSingleDownDecision",
                        "Hlt2DisplVerticesSingleHighFDPostScaledDecision",
                        "Hlt2DisplVerticesSingleHighMassPostScaledDecision"
                    ]
                ],
                [
                    [
                        "0x00990042",
                        "0x40000000"
                    ],
                    [
                        "Hlt2DisplVerticesSingleDecision",
                        "Hlt2DisplVerticesSingleDownDecision",
                        "Hlt2DisplVerticesSingleHighFDDecision",
                        "Hlt2DisplVerticesSingleHighMassDecision",
                        "Hlt2DisplVerticesSingleVeryHighFDDecision"
                    ]
                ]
            ]
        },
        "HLTPS": {
            "PreScale": 0.2,
            "RawEvents": []
        },
        "JetHltSingleHighMassSelection": {
            "ConeSize": 0.7,
            "JetIDCut": None,
            "MinDOCABL": -2.0,
            "MinNJetMass": 0.0,
            "MinNJetTransvMass": None,
            "MinNumJets": 2,
            "PreScale": 0.1,
            "RawEvents": [],
            "SingleJet": False
        },
        "JetHltSingleLowMassSelection": {
            "ConeSize": 1.2,
            "JetIDCut": "( JNWITHPVINFO >= 5 ) & ( JMPT > 1800. ) & ( (PT/M) > 1.5 )",
            "MinDOCABL": 0.1,
            "MinNJetMass": 0.0,
            "MinNJetTransvMass": None,
            "MinNumJets": 1,
            "PreScale": 0.1,
            "RawEvents": [],
            "SingleJet": True
        },
        "JetID": "( 0.8 > JMPF ) & ( 0.1 < JCPF ) & ( 900. < JMPT )",
        "JetSingleHighMassSelection": {
            "ApplyMatterVeto": True,
            "ConeSize": 0.7,
            "JetIDCut": None,
            "MaxFractE1Track": 0.8,
            "MaxFractTrwHitBefore": 0.49,
            "MinDOCABL": -2.0,
            "MinMass": 5000.0,
            "MinNJetMass": 0.0,
            "MinNJetTransvMass": None,
            "MinNumJets": 2,
            "MinNumTracks": 5,
            "MinRho": 0.4,
            "MinSumPT": 7000.0,
            "PreScale": 1.0,
            "RawEvents": [],
            "SingleJet": False
        },
        "JetSingleLowMassSelection": {
            "ApplyMatterVeto": True,
            "ConeSize": 1.2,
            "JetIDCut": "( JNWITHPVINFO >= 5 ) & ( JMPT > 1800. ) & ( (PT/M) > 2.5 )",
            "MaxFractE1Track": 0.8,
            "MaxFractTrwHitBefore": 0.49,
            "MinDOCABL": 0.1,
            "MinMass": 0.0,
            "MinNJetMass": 0.0,
            "MinNJetTransvMass": None,
            "MinNumJets": 1,
            "MinNumTracks": 5,
            "MinRho": 0.4,
            "MinSumPT": 10000.0,
            "PreScale": 0.5,
            "RawEvents": [],
            "SingleJet": True
        },
        "RV2PDown": {
            "ApplyMatterVeto": False,
            "ComputeMatterVeto": False,
            "FirstPVMaxRho": 0.3,
            "FirstPVMaxZ": 500.0,
            "FirstPVMinNumTracks": 10,
            "FirstPVMinZ": -300.0,
            "MaxChi2NonVeloOnly": 5.0,
            "MaxFractE1Track": 10.0,
            "MaxFractTrwHitBefore": 10.0,
            "MinMass": 3000.0,
            "MinNumTracks": 4,
            "MinRho": 2.0,
            "MinSumPT": 0.0,
            "UseVeloTracks": False
        },
        "RV2PWithVelo": {
            "ApplyMatterVeto": False,
            "ComputeMatterVeto": True,
            "FirstPVMaxRho": 0.3,
            "FirstPVMaxZ": 500.0,
            "FirstPVMinNumTracks": 10,
            "FirstPVMinZ": -300.0,
            "MaxChi2NonVeloOnly": 5.0,
            "MaxFractE1Track": 10.0,
            "MaxFractTrwHitBefore": 10.0,
            "MinMass": 0.0,
            "MinNumTracks": 4,
            "MinRho": 0.4,
            "MinSumPT": 0.0,
            "UseVeloTracks": False
        },
        "SinglePSSelection": {
            "ApplyMatterVeto": False,
            "MaxFractE1Track": 10.0,
            "MaxFractTrwHitBefore": 10.0,
            "MinMass": 3000.0,
            "MinNumTracks": 5,
            "MinRho": 0.5,
            "MinSumPT": 0.0,
            "PreScale": 0.005,
            "RawEvents": []
        },
        "VeloGEC": {
            "Apply": True,
            "MaxPhiVectorSize": 250.0,
            "MaxVeloRatio": 0.1
        }
    },
    "STREAMS": [ "EW" ],
    "WGs": [ "QEE" ]
}

Ditau = {
    "BUILDERTYPE": "DitauConf",
    "CONFIG": {
        "CONSTRUCTORS": {
            "EX": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5,
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus",
                            "X0 -> ^X-  X+": "IsoPlus"
                        },
                        "Type": "RelInfoConeVariables"
                    }
                ],
                "checkPV": True,
                "prescale": 1.0
            },
            "EXnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5,
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus",
                            "X0 -> ^X-  X+": "IsoPlus"
                        },
                        "Type": "RelInfoConeVariables"
                    }
                ],
                "checkPV": True,
                "prescale": 0.02
            },
            "EXssnoiso": {
                "checkPV": True,
                "prescale": 0.02
            },
            "HH": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5,
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus",
                            "X0 -> ^X-  X+": "IsoPlus"
                        },
                        "Type": "RelInfoConeVariables"
                    }
                ],
                "checkPV": True,
                "prescale": 1.0
            },
            "HHnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5,
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus",
                            "X0 -> ^X-  X+": "IsoPlus"
                        },
                        "Type": "RelInfoConeVariables"
                    }
                ],
                "checkPV": True,
                "prescale": 0.02
            },
            "HHssnoiso": {
                "checkPV": True,
                "prescale": 0.02
            },
            "MX": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5,
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus",
                            "X0 -> ^X-  X+": "IsoPlus"
                        },
                        "Type": "RelInfoConeVariables"
                    }
                ],
                "checkPV": True,
                "prescale": 1.0
            },
            "MXnoiso": {
                "RelatedInfoTools": [
                    {
                        "ConeAngle": 0.5,
                        "DaughterLocations": {
                            "X0 ->  X  ^X+": "IsoMinus",
                            "X0 -> ^X-  X+": "IsoPlus"
                        },
                        "Type": "RelInfoConeVariables"
                    }
                ],
                "checkPV": True,
                "prescale": 0.05
            },
            "MXssnoiso": {
                "checkPV": True,
                "prescale": 0.05
            }
        },
        "DITAU": {
            "ee": {
                "ccuts": {
                    "min_AM": 12000.0,
                    "min_APTMAX": 9000.0
                },
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "eh1": {
                "ccuts": {
                    "extracut": "ATRUE",
                    "min_AM": 12000.0,
                    "min_APTMAX": 9000.0
                },
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    },
                    "pi": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "eh3": {
                "ccuts": {
                    "min_AM": 12000.0,
                    "min_APTMAX": 9000.0
                },
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    },
                    "tau": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "emu": {
                "ccuts": {
                    "min_AM": 12000.0,
                    "min_APTMAX": 9000.0
                },
                "dcuts": {
                    "e": {
                        "extracut": "ALL"
                    },
                    "mu": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "h1h1": {
                "ccuts": {
                    "min_AM": 16000.0,
                    "min_APTMAX": 12000.0
                },
                "dcuts": {
                    "pi": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "h1h3": {
                "ccuts": {
                    "min_AM": 16000.0,
                    "min_APTMAX": 12000.0
                },
                "dcuts": {
                    "pi": {
                        "extracut": "ALL"
                    },
                    "tau": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "h1mu": {
                "ccuts": {
                    "min_AM": 12000.0,
                    "min_APTMAX": 9000.0
                },
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    },
                    "pi": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "h3h3": {
                "ccuts": {
                    "min_AM": 16000.0,
                    "min_APTMAX": 12000.0
                },
                "dcuts": {
                    "tau": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "h3mu": {
                "ccuts": {
                    "min_AM": 12000.0,
                    "min_APTMAX": 9000.0
                },
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    },
                    "tau": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            },
            "mumu": {
                "ccuts": {
                    "min_AM": 8000.0,
                    "min_APTMAX": 4000.0
                },
                "dcuts": {
                    "mu": {
                        "extracut": "ALL"
                    }
                },
                "mcuts": {
                    "extracut": "ALL"
                }
            }
        },
        "PVRefitter": None,
        "TES_e": "Phys/StdAllNoPIDsElectrons/Particles",
        "TES_mu": "Phys/StdAllLooseMuons/Particles",
        "TES_pi": "Phys/StdAllNoPIDsPions/Particles",
        "ditau_pcomb": {
            "": "MomentumCombiner:PUBLIC"
        },
        "preambulo": "\n",
        "tau_e": {
            "ISMUONLOOSE": False,
            "InAccEcal": True,
            "InAccHcal": True,
            "extracut": "ALL",
            "max_ETA": 4.5,
            "max_HCALFrac": 0.05,
            "max_TRCHI2DOF": 5,
            "min_CaloPrsE": 50.0,
            "min_ECALFrac": 0.05,
            "min_ETA": 2.0,
            "min_PT": 4000.0
        },
        "tau_h1": {
            "ISMUONLOOSE": False,
            "InAccHcal": True,
            "extracut": "ALL",
            "max_ETA": 4.5,
            "max_TRCHI2DOF": 5,
            "min_ETA": 2.0,
            "min_HCALFrac": 0.05,
            "min_PT": 4000.0
        },
        "tau_h3": {
            "ccuts": {
                "max_AM": 1500.0,
                "min_AM": 700.0,
                "min_APTMAX": 2000.0
            },
            "dcuts": {
                "ISMUONLOOSE": False,
                "InAccHcal": True,
                "extracut": "ALL",
                "max_ETA": 4.5,
                "max_TRCHI2DOF": 5,
                "min_ETA": 2.0,
                "min_HCALFrac": 0.01,
                "min_PT": 500.0
            },
            "mcuts": {
                "max_DRTRIOMAX": 0.3,
                "max_VCHI2PDOF": 20.0,
                "min_PT": 4000.0
            }
        },
        "tau_isolation": {
            "tau_e": 0.7,
            "tau_h1": 0.8,
            "tau_h3": 0.8,
            "tau_mu": 0.8
        },
        "tau_mu": {
            "ISMUON": True,
            "extracut": "ALL",
            "max_ETA": 4.5,
            "max_TRCHI2DOF": 5,
            "min_ETA": 2.0,
            "min_PT": 4000.0
        }
    },
    "STREAMS": [ "EW" ],
    "WGs": [ "QEE" ]
}

LFVExotica = {
    "BUILDERTYPE": "LFVExoticaConf",
    "CONFIG": {
        "Detached_FDChi2": 45,
        "Detached_GhostProb": 0.3,
        "Detached_IPChi2": 16,
        "Detached_LinePostscale": 1.0,
        "Detached_LinePrescale": 1.0,
        "Detached_M": 0,
        "Detached_P": 10000.0,
        "Detached_PT": 500.0,
        "Detached_ProbNNe": 0.0,
        "Detached_ProbNNmu": 0.5,
        "Detached_SS_LinePostscale": 1.0,
        "Detached_SS_LinePrescale": 1.0,
        "Detached_TAU": 0.001,
        "Detached_VChi2": 10,
        "Detached_XIPChi2": 16,
        "Prompt_FDChi2": 1,
        "Prompt_GhostProb": 0.3,
        "Prompt_IPChi2": 1,
        "Prompt_LinePostscale": 1.0,
        "Prompt_LinePrescale": 1.0,
        "Prompt_M": 0,
        "Prompt_P": 10000.0,
        "Prompt_PT": 500.0,
        "Prompt_ProbNNe": 0.25,
        "Prompt_ProbNNmu": 0.5,
        "Prompt_SS_LinePostscale": 1.0,
        "Prompt_SS_LinePrescale": 0.5,
        "Prompt_VChi2": 5,
        "Prompt_XIPChi2": 1,
        "checkPV": False
    },
    "STREAMS": [ "Leptonic" ],
    "WGs": [ "QEE" ]
}

WMu = {
    "BUILDERTYPE": "WMuConf", 
    "CONFIG": {
        "HLT1_SingleTrackNoBias": "HLT_PASS( 'Hlt1MBNoBiasDecision' )", 
        "HLT2_Control10": "HLT_PASS_RE('Hlt2(EW)?SingleMuon(V)?High.*')", 
        "HLT2_Control4800": "HLT_PASS_RE('Hlt2(EW)?SingleMuonLow.*')", 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "STNB_Prescale": 0.2, 
        "SingMuon10_Prescale": 0.01, 
        "SingMuon10_pT": 10000.0, 
        "SingMuon48_Prescale": 0.4, 
        "SingMuon48_pT": 4800.0, 
        "WMuLow_Prescale": 0.1, 
        "WMu_Postscale": 1.0, 
        "WMu_Prescale": 1.0, 
        "pT": 20000.0, 
        "pTlow": 15000.0, 
        "pTvlow": 5000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

WRareDecay = {
    "BUILDERTYPE": "WRareDecayConf",
    "CONFIG": {
        "BPVLTIME": 0.0002,
        "BuMassWin": 400.0,
        "DpMassWin": 150.0,
        "DsMassWin": 150.0,
        "KSLTIME": 0.0005,
        "KSMassWin": 40.0,
        "KstMassWin": 120.0,
        "MesonPT": 13000.0,
        "RawEvents": [ "Calo" ],
        "RhoMassWin": 150.0,
        "TrChi2": 5.0,
        "TrGhost": 0.4,
        "TrIPChi2": 9.0,
        "W2BuGammaPostScale": 1.0,
        "W2BuGammaPreScale": 1.0,
        "W2DpGammaPostScale": 1.0,
        "W2DpGammaPreScale": 1.0,
        "W2DsGammaPostScale": 1.0,
        "W2DsGammaPreScale": 1.0,
        "W2KaonGammaPostScale": 1.0,
        "W2KaonGammaPreScale": 1.0,
        "W2KstPGammaPostScale": 1.0,
        "W2KstPGammaPreScale": 1.0,
        "W2PionGammaPostScale": 1.0,
        "W2PionGammaPreScale": 1.0,
        "W2RhoGammaPostScale": 1.0,
        "W2RhoGammaPreScale": 1.0,
        "WMassWin": 50000.0,
        "kaonPT": 400.0,
        "photonHighPT": 10000.0,
        "photonPT": 2500.0,
        "pion0PT": 800.0
    },
    "STREAMS": [ "EW" ],
    "WGs": [ "QEE" ]
}

We = {
    "BUILDERTYPE": "WeConf", 
    "CONFIG": {
        "ECalMin": 0.1, 
        "HCalMax": 0.05, 
        "PrsCalMin": 50.0, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "WeLow_Prescale": 0.1, 
        "We_Postscale": 1.0, 
        "We_Prescale": 1.0, 
        "pT": 20000.0, 
        "pTlow": 15000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

WeJets = {
    "BUILDERTYPE": "WJetsConf",
    "CONFIG": {
        "RequiredRawEvents": [ "Calo" ],
        "TOS_Jet": "Hlt2Topo.*Decision",
        "WJetsTagged_Prescale": 1.0,
        "WJets_Prescale": 1.0,
        "config_W": {
            "ECalMin": 0.1,
            "HCalMax": 0.05,
            "PrsCalMin": 50.0,
            "TOS": "Hlt2EWSingleElectronVHighPtDecision",
            "max_e_pT": 200000000.0,
            "min_e_pT": 10000.0
        },
        "dr_lepton_jet": 0.5,
        "min_jet_pT": 15000.0
    },
    "STREAMS": [ "BhadronCompleteEvent" ],
    "WGs": [ "QEE" ]
}

WmuJets = {
    "BUILDERTYPE": "WJetsConf",
    "CONFIG": {
        "RequiredRawEvents": [ "Calo" ],
        "TOS_Jet": "Hlt2Topo.*Decision",
        "WJetsTagged_Prescale": 1.0,
        "WJets_Prescale": 1.0,
        "config_W": {
            "TOS": "Hlt2EWSingleMuonVHighPtDecision",
            "max_mu_pT": 200000000.0,
            "min_mu_pT": 10000.0
        },
        "dr_lepton_jet": 0.5,
        "min_jet_pT": 15000.0
    },
    "STREAMS": [ "BhadronCompleteEvent" ],
    "WGs": [ "QEE" ]
}

Z02MuMu = {
    "BUILDERTYPE": "Z02MuMuConf", 
    "CONFIG": {
        "MMmin": 40000.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "pT": 3000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

Z02ee = {
    "BUILDERTYPE": "Z02eeConf", 
    "CONFIG": {
        "ECalMin": 0.1, 
        "HCalMax": 0.05, 
        "MMmin": 40000.0, 
        "Postscale": 1.0, 
        "Prescale": 1.0, 
        "PrsCalMin": 50.0, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "pT": 10000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}

Z0RareDecay = {
    "BUILDERTYPE": "Z0RareDecayConf",
    "CONFIG": {
        "D0MassWin": 100.0,
        "JpsiMassMax": 3200.0,
        "JpsiMassMin": 3000.0,
        "KstMassWin": 120.0,
        "MesonPT": 13000.0,
        "MuonP": -8000.0,
        "MuonPT": 650.0,
        "OmegaMassWin": 230.0,
        "PhiMassWin": 200.0,
        "Pi0Pi0PT": 10000.0,
        "RawEvents": [ "Calo" ],
        "RhoMassWin": 230.0,
        "TrChi2": 5.0,
        "UpsilonMassMin": 8500.0,
        "VtxChi2": 20.0,
        "Z2D0GammaPostScale": 1.0,
        "Z2D0GammaPreScale": 1.0,
        "Z2GammaGammaPostScale": 1.0,
        "Z2GammaGammaPreScale": 1.0,
        "Z2KstGammaPostScale": 1.0,
        "Z2KstGammaPreScale": 1.0,
        "Z2OmegaGammaPostScale": 1.0,
        "Z2OmegaGammaPreScale": 1.0,
        "Z2PhiGammaPostScale": 1.0,
        "Z2PhiGammaPreScale": 1.0,
        "Z2Pi0GammaPostScale": 1.0,
        "Z2Pi0GammaPreScale": 1.0,
        "Z2Pi0Pi0PostScale": 1.0,
        "Z2Pi0Pi0PreScale": 1.0,
        "Z2QONGammaPostScale": 1.0,
        "Z2QONGammaPreScale": 1.0,
        "Z2RhoGammaPostScale": 1.0,
        "Z2RhoGammaPreScale": 1.0,
        "ZMassWin": 60000.0,
        "photonPT": 2500.0,
        "pion0PT": 860.0
    },
    "STREAMS": [ "EW" ],
    "WGs": [ "QEE" ]
}

