###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#############################################################################################
##                          S T R I P P I N G  2 4 r 1                                     ##
##                                                                                         ##
##  Configuration for Charm WG                                                             ##
##  Contact person: Jinlin Fu & Maxime Schubiger (j.fu@cern.ch & maxime.schubiger@cern.ch) ##
#############################################################################################

from GaudiKernel.SystemOfUnits import *



#StrippingXic2HHH.py
BetacHHH = {
    "BUILDERTYPE": "StrippingXic2HHHConf", 
    "CONFIG": {
        "Comb_ADOCAMAX_MAX": 0.3, 
        "Comb_MASS_MAX": 2800.0, 
        "Comb_MASS_MIN": 2250.0, 
        "Daug_All_PT_MIN": 300.0, 
        "Daug_P_MIN": 3000.0, 
        "Daug_TRCHI2DOF_MAX": 3.0, 
        "K_IPCHI2_MIN": 4.0, 
        "PostscaleXic2KLam": 0.0, 
        "PostscaleXic2PKK": 1.0, 
        "PostscaleXic2PKPi": 0.0, 
        "PostscaleXic2PV0": 0.0, 
        "PrescaleXic2KLam": 0.0, 
        "PrescaleXic2PKK": 1.0, 
        "PrescaleXic2PKPi": 0.0, 
        "PrescaleXic2PV0": 0.0, 
        "RelatedInfoTools": [
            {
                "ConeAngle": 1.5, 
                "Location": "Cone1", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.75, 
                "Location": "Cone2", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.5, 
                "Location": "Cone3", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.25, 
                "Location": "Cone4", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.1, 
                "Location": "Cone5", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }
        ], 
        "Xic_BPVDIRA_MIN": 0.999, 
        "Xic_BPVIPCHI2_MAX": 4.0, 
        "Xic_BPVLTIME_MAX": 0.003, 
        "Xic_BPVLTIME_MIN": 0.0002, 
        "Xic_BPVVDCHI2_MIN": 0.0, 
        "Xic_PT_MIN": 2000.0, 
        "Xic_VCHI2VDOF_MAX": 4.0, 
        "pion_IPCHI2_MIN": 4.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingCharm2PPX.py
Charm2PPX = {
    "BUILDERTYPE": "StrippingCharm2PPXConf", 
    "CONFIG": {
        "Comb_ADOCACHI2_MAX": 16, 
        "Comb_MASS_MAX": 3500.0, 
        "Daug_1ofAll_BPVIPCHI2_MIN": 9.0, 
        "Daug_1ofAll_PT_MIN": 700.0, 
        "Daug_All_BPVIPCHI2_MIN": 4.0, 
        "Daug_All_CloneDist_MIN": 5000, 
        "Daug_All_MIPCHI2_MIN": 4.0, 
        "Daug_All_PT_MIN": 200.0, 
        "Daug_All_TrChostProb_MAX": 0.5, 
        "Daug_ETA_MAX": 4.9, 
        "Daug_ETA_MIN": 2.0, 
        "Daug_P_MAX": 100000.0, 
        "Daug_P_MIN": 3200.0, 
        "Hc_BPVCTAU_MIN": 0.0, 
        "Hc_BPVIPCHI2_MAX": 16, 
        "Hc_BPVVDCHI2_MIN": 9.0, 
        "Hc_PT_MIN": -1000.0, 
        "Hc_VCHI2VDOF_MAX": 25.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "HsComb_ADOCACHI2_MAX": 16, 
        "HsComb_MASS_MAX": 2125.0, 
        "HsDaug_1ofAll_BPVIPCHI2_MIN": 9.0, 
        "HsDaug_1ofAll_PT_MIN": -500.0, 
        "HsDownDaug_1ofAll_BPVIPCHI2_MIN": -9.0, 
        "HsDown_BPVVDCHI2_MIN": 4.0, 
        "HsProton_1ofAll_ProbNN_MIN": 0.5, 
        "Hs_BPVCTAU_MIN": 0.1, 
        "Hs_BPVIPCHI2_MAX": 16, 
        "Hs_BPVVDCHI2_MIN": 9.0, 
        "Hs_PT_MIN": -1000.0, 
        "Hs_VCHI2VDOF_MAX": 25.0, 
        "Kaon_ProbNN_MIN": 0.1, 
        "Pion_ProbNN_MIN": 0.1, 
        "PostscaleHc2PP": 1.0, 
        "PostscaleHc2PPK": 1.0, 
        "PostscaleHc2PPKPi": 1.0, 
        "PostscaleHc2PPKPiPi": 1.0, 
        "PostscaleHs2PPPi": 1.0, 
        "PrescaleHc2PP": 1.0, 
        "PrescaleHc2PPK": 1.0, 
        "PrescaleHc2PPKPi": 1.0, 
        "PrescaleHc2PPKPiPi": 1.0, 
        "PrescaleHs2PPPi": 1.0, 
        "Proton_1ofAll_ProbNN_MIN": 0.5, 
        "Proton_PT_MIN": 300.0, 
        "Proton_P_MIN": 10000.0, 
        "Proton_ProbNN_MIN": 0.1, 
        "Proton_ProbNNk_MAX": 0.8, 
        "Proton_ProbNNpi_MAX": 0.55
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingCharmFromBSemi.py
CharmFromBSemi = {
    "BUILDERTYPE": "CharmFromBSemiAllLinesConf", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BVCHI2DOF": 6.0, 
        "CombMassHigh_HH": 1820, 
        "CombMassLow_HH": 0, 
        "D02HHGammaAMassWin": 220, 
        "D02HHGammaMassWin": 210, 
        "D02HHHHPrescale": 1, 
        "D02HHPi0AMassWin": 220, 
        "D02HHPi0DocaCut": 6.0, 
        "D02HHPi0MassWin": 210, 
        "D02KSHHPi0AMassWin": 220, 
        "D02KSHHPi0MassWin": 210, 
        "D02KSHHPi0_D0PT": 1000, 
        "D02KSHHPi0_D0PTComb": 1000, 
        "D02KSHHPi0_PTSUMLoose": 1000, 
        "D0decaysPrescale": 1, 
        "D0radiativePrescale": 1, 
        "DDocaChi2Max": 20, 
        "DZ": 0, 
        "Ds2HHHHPrescale": 1, 
        "DsAMassWin": 100.0, 
        "DsDIRA": 0.99, 
        "DsFDCHI2": 100.0, 
        "DsIP": 7.4, 
        "DsMassWin": 80.0, 
        "DsVCHI2DOF": 6.0, 
        "DsdecaysPrescale": 1, 
        "Dstar_Chi2": 8.0, 
        "Dstar_SoftPion_PIDe": 2.0, 
        "Dstar_SoftPion_PT": 80.0, 
        "Dstar_wideDMCutLower": 0.0, 
        "Dstar_wideDMCutUpper": 170.0, 
        "Dto4hADocaChi2Max": 7, 
        "Dto4h_AMassWin": 65.0, 
        "Dto4h_MassWin": 60.0, 
        "DtoXgammaADocaChi2Max": 10, 
        "GEC_nLongTrk": 250, 
        "HLT2": "HLT_PASS_RE('Hlt2.*SingleMuon.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')", 
        "KPiPT": 250.0, 
        "KSCutDIRA": 0.99, 
        "KSCutZFDFromD": 10.0, 
        "KSDDCutFDChi2": 100, 
        "KSDDCutMass": 30, 
        "KSDDPMin": 3000, 
        "KSDDPTMin": 250, 
        "KSDaugTrackChi2": 4, 
        "KSLLCutFDChi2": 100, 
        "KSLLCutMass": 30, 
        "KSLLPMin": 2000, 
        "KSLLPTMin": 250, 
        "KSVertexChi2": 6, 
        "KaonPIDK": 4.0, 
        "KaonPIDKloose": -5, 
        "LambdaCutDIRA": 0.99, 
        "LambdaDDCutFDChi2": 100, 
        "LambdaDDCutMass": 30, 
        "LambdaDDPMin": 3000, 
        "LambdaDDPTMin": 800, 
        "LambdaDaugTrackChi2": 4, 
        "LambdaLLCutFDChi2": 100, 
        "LambdaLLCutMass": 30, 
        "LambdaLLPMin": 2000, 
        "LambdaLLPTMin": 500, 
        "LambdaVertexChi2": 6, 
        "Lc2HHHPrescale": 1, 
        "Lc2HHPrescale": 1, 
        "MINIPCHI2": 9.0, 
        "MINIPCHI2Loose": 4.0, 
        "MassHigh_HH": 1810, 
        "MassLow_HH": 0, 
        "MaxBMass": 6000, 
        "MaxConvPhDDChi": 9, 
        "MaxConvPhDDMass": 100, 
        "MaxConvPhLLChi": 9, 
        "MaxConvPhLLMass": 100, 
        "MaxVCHI2NDOF_HH": 10.0, 
        "MinBMass": 2500, 
        "MinConvPhDDPT": 800, 
        "MinConvPhLLPT": 800, 
        "MinVDCHI2_HH": 1000.0, 
        "MuonIPCHI2": 4.0, 
        "MuonPT": 800.0, 
        "PIDmu": -0.0, 
        "PTSUM": 1800.0, 
        "PTSUMLoose": 1400.0, 
        "PTSUM_HHGamma": 1800.0, 
        "PTSUM_HHPi0": 1800.0, 
        "PhotonCL": 0.25, 
        "PhotonPT": 1500, 
        "Pi0PMin": 3000, 
        "Pi0PtMin": 1000, 
        "PionPIDK": 10.0, 
        "PionPIDKTight": 4.0, 
        "TRCHI2": 4.0, 
        "TTSpecs": {
            "Hlt1.*Track.*Decision%TOS": 0, 
            "Hlt2.*SingleMuon.*Decision%TOS": 0, 
            "Hlt2Global%TIS": 0, 
            "Hlt2Topo.*Decision%TOS": 0
        }, 
        "TrGhostProbMax": 0.5, 
        "b2DstarMuXPrescale": 1
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingD2PiPi0.py
D2EtaEEGH = {
    "BUILDERTYPE": "StrippingD2EtaEEGHConf", 
    "CONFIG": {
        "Bachelor_IPCHI2_MIN": 25.0, 
        "Bachelor_PIDK_MIN": -10.0, 
        "Bachelor_PT_MIN": 1000.0, 
        "Bachelor_P_MIN": 1000.0, 
        "DTF_CHI2NDOF_MAX": 100, 
        "D_BPVLTIME_MIN": 0.00025, 
        "D_Mass_MAX": 2200.0, 
        "D_Mass_MIN": 1600.0, 
        "D_PT_Min": 3000.0, 
        "D_VCHI2PDOF_MAX": 5, 
        "Daug_ETA_MAX": 5.0, 
        "Daug_ETA_MIN": 2.0, 
        "Daug_IPCHI2_MIN": 25.0, 
        "Daug_PT_MIN": 350.0, 
        "Daug_P_MAX": 100000.0, 
        "Daug_P_MIN": 1000.0, 
        "Daug_TRCHI2DOF_MAX": 5, 
        "Daug_TRGHOSTPROB_MAX": 0.5, 
        "DiElectron_Mass_MAX": 650.0, 
        "DiElectron_PT_MIN": 350.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": "HLT_PASS_RE('Hlt2CharmHadDp2Eta.*_Eta2EmEpG.*Decision')", 
        "Neut_PT_MIN": 600.0, 
        "PostscaleD2KEtaEEG": 1, 
        "PostscaleD2PiEtaEEG": 1, 
        "PrescaleD2KEtaEEG": 1, 
        "PrescaleD2PiEtaEEG": 1, 
        "Res_Mass_MAX": 650.0, 
        "Res_Mass_MIN": 450.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingD2PiPi0.py
D2EtaH = {
    "BUILDERTYPE": "StrippingD2EtaHConf", 
    "CONFIG": {
        "Bachelor_IPCHI2_MIN": 16.0, 
        "Bachelor_PIDK_MIN": -999.0, 
        "Bachelor_PT_MIN": 600.0, 
        "Bachelor_P_MIN": 1000.0, 
        "DTF_CHI2NDOF_MAX": 5, 
        "D_BPVLTIME_MIN": 0.00025, 
        "D_Mass_MAX": 2200.0, 
        "D_Mass_MIN": 1600.0, 
        "D_PT_Min": 2000.0, 
        "D_VCHI2PDOF_MAX": 5, 
        "Daug_ETA_MAX": 5.0, 
        "Daug_ETA_MIN": 2.0, 
        "Daug_IPCHI2_MIN": 16.0, 
        "Daug_PT_MIN": 500.0, 
        "Daug_P_MAX": 100000.0, 
        "Daug_P_MIN": 1000.0, 
        "Daug_TRCHI2DOF_MAX": 5, 
        "Daug_TRGHOSTPROB_MAX": 0.5, 
        "Dipion_DOCACHI2_MAX": 15, 
        "Dipion_Mass_MAX": 750.0, 
        "Dipion_Mass_MIN": 200.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": "HLT_PASS_RE('Hlt2CharmHadDp2Eta.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')", 
        "Neut_Mass_Win": 50.0, 
        "Neut_PT_MIN": 600.0, 
        "Pi_PIDK_MAX": 0.0, 
        "PostscaleD2KEta3HM": 1, 
        "PostscaleD2KEta3HR": 1, 
        "PostscaleD2KEtaPPG": 1, 
        "PostscaleD2PiEta3HM": 1, 
        "PostscaleD2PiEta3HR": 1, 
        "PostscaleD2PiEtaPPG": 1, 
        "PrescaleD2KEta3HM": 1, 
        "PrescaleD2KEta3HR": 1, 
        "PrescaleD2KEtaPPG": 1, 
        "PrescaleD2PiEta3HM": 1, 
        "PrescaleD2PiEta3HR": 1, 
        "PrescaleD2PiEtaPPG": 1, 
        "Res_Mass_MAX": 750.0, 
        "Res_Mass_MIN": 350.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingD2PiPi0.py
D2EtaPrimeH = {
    "BUILDERTYPE": "StrippingD2EtaPrimeHConf", 
    "CONFIG": {
        "Bachelor_IPCHI2_MIN": 25.0, 
        "Bachelor_PIDK_MIN": -999.0, 
        "Bachelor_PT_MIN": 600.0, 
        "Bachelor_P_MIN": 1000.0, 
        "DTF_CHI2NDOF_MAX": 5, 
        "D_BPVLTIME_MIN": 0.00025, 
        "D_Mass_MAX": 2200.0, 
        "D_Mass_MIN": 1600.0, 
        "D_PT_Min": 2000.0, 
        "D_VCHI2PDOF_MAX": 5, 
        "Daug_ETA_MAX": 5.0, 
        "Daug_ETA_MIN": 2.0, 
        "Daug_IPCHI2_MIN": 25.0, 
        "Daug_PT_MIN": 500.0, 
        "Daug_P_MAX": 100000.0, 
        "Daug_P_MIN": 1000.0, 
        "Daug_TRCHI2DOF_MAX": 5, 
        "Daug_TRGHOSTPROB_MAX": 0.5, 
        "Dipion_DOCACHI2_MAX": 15, 
        "Dipion_Mass_MAX": 1200.0, 
        "Dipion_Mass_MIN": 200.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": "HLT_PASS_RE('Hlt2CharmHadDp2Etap.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')", 
        "Neut_Mass_Win": 50.0, 
        "Neut_PT_MIN": 1000.0, 
        "Pi_PIDK_MAX": 0, 
        "PostscaleD2KEtaPrime3HR": 1, 
        "PostscaleD2KEtaPrimePPG": 1, 
        "PostscaleD2PiEtaPrime3HR": 1, 
        "PostscaleD2PiEtaPrimePPG": 1, 
        "PrescaleD2KEtaPrime3HR": 1, 
        "PrescaleD2KEtaPrimePPG": 1, 
        "PrescaleD2PiEtaPrime3HR": 1, 
        "PrescaleD2PiEtaPrimePPG": 1, 
        "Res_Mass_MAX": 1060.0, 
        "Res_Mass_MIN": 800.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingD2HMuNu.py
D2HMuNu = {
    "BUILDERTYPE": "D2HLepNuBuilder", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFDCHI2HIGH": 100.0, 
        "BPVVDZcut": 0.0, 
        "BVCHI2DOF": 20, 
        "DELTA_MASS_MAX": 400, 
        "ElectronPIDe": 1.0, 
        "ElectronPT": 500, 
        "GEC_nLongTrk": 160.0, 
        "KLepMassHigh": 2000, 
        "KLepMassLow": 500, 
        "KaonMINIPCHI2": 9, 
        "KaonP": 3000.0, 
        "KaonPIDK": 5.0, 
        "KaonPIDmu": 5.0, 
        "KaonPIDp": 5.0, 
        "KaonPT": 800.0, 
        "MuonGHOSTPROB": 0.35, 
        "MuonPIDK": 0.0, 
        "MuonPIDmu": 3.0, 
        "MuonPIDp": 0.0, 
        "MuonPT": 500.0, 
        "SSonly": 2, 
        "Slowpion_P": 1000, 
        "Slowpion_PIDe": 5, 
        "Slowpion_PT": 300, 
        "Slowpion_TRGHOSTPROB": 0.35, 
        "TOSFilter": {
            "Hlt2CharmHad.*HHX.*Decision%TOS": 0
        }, 
        "TRGHOSTPROB": 0.35, 
        "useTOS": True
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

#StrippingD2PiPi0.py
D2PhiH = {
    "BUILDERTYPE": "StrippingD2PhiHConf", 
    "CONFIG": {
        "Bachelor_IPCHI2_MIN": 25.0, 
        "Bachelor_PIDK_MIN": -999.0, 
        "Bachelor_PT_MIN": 1000.0, 
        "Bachelor_P_MIN": 1000.0, 
        "DTF_CHI2NDOF_MAX": 9, 
        "D_BPVLTIME_MIN": 0.00025, 
        "D_Mass_MAX": 2200.0, 
        "D_Mass_MIN": 1600.0, 
        "D_PT_Min": 2000.0, 
        "D_VCHI2PDOF_MAX": 5, 
        "Daug_ETA_MAX": 5.0, 
        "Daug_ETA_MIN": 2.0, 
        "Daug_IPCHI2_MIN": 25.0, 
        "Daug_PT_MIN": 500.0, 
        "Daug_P_MAX": 100000.0, 
        "Daug_P_MIN": 1000.0, 
        "Daug_TRCHI2DOF_MAX": 5, 
        "Daug_TRGHOSTPROB_MAX": 0.5, 
        "Dipion_DOCACHI2_MAX": 15, 
        "Dipion_Mass_MAX": 1200.0, 
        "Dipion_Mass_MIN": 200.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": "HLT_PASS_RE('Hlt2CharmHadDp2.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision')", 
        "Neut_Mass_Win": 50.0, 
        "Neut_PT_MIN": 600.0, 
        "Pi_PIDK_MAX": 0, 
        "PostscaleD2KPhi3HM": 1, 
        "PostscaleD2KPhi3HR": 1, 
        "PostscaleD2PiPhi3HM": 1, 
        "PostscaleD2PiPhi3HR": 1, 
        "PrescaleD2KPhi3HM": 1, 
        "PrescaleD2KPhi3HR": 1, 
        "PrescaleD2PiPhi3HM": 1, 
        "PrescaleD2PiPhi3HR": 1, 
        "Res_Mass_MAX": 890.0, 
        "Res_Mass_MIN": 680.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingD2PiPi0.py
D2Pi0H = {
    "BUILDERTYPE": "StrippingD2Pi0HConf", 
    "CONFIG": {
        "Bachelor_IPCHI2_MIN": 25.0, 
        "Bachelor_PIDK_MIN": -10.0, 
        "Bachelor_PT_MIN": 350.0, 
        "Bachelor_P_MIN": 1000.0, 
        "DTF_CHI2NDOF_MAX": 100, 
        "D_BPVLTIME_MIN": 0.00015, 
        "D_Mass_MAX": 2200.0, 
        "D_Mass_MIN": 1600.0, 
        "D_PT_Min": 3000.0, 
        "D_VCHI2PDOF_MAX": 5, 
        "Daug_ETA_MAX": 5.0, 
        "Daug_ETA_MIN": 2.0, 
        "Daug_IPCHI2_MIN": 25.0, 
        "Daug_PT_MIN": 350.0, 
        "Daug_P_MAX": 100000.0, 
        "Daug_P_MIN": 1000.0, 
        "Daug_TRCHI2DOF_MAX": 5, 
        "Daug_TRGHOSTPROB_MAX": 0.5, 
        "DiElectron_Mass_MAX": 210.0, 
        "DiElectron_PT_MIN": 350.0, 
        "Dstar_AMDiff_MAX": 165.0, 
        "Dstar_MDiff_MAX": 160.0, 
        "Dstar_VCHI2VDOF_MAX": 100.0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": "(HLT_PASS_RE('Hlt2CharmHadD.*EmEp.*Decision') | HLT_PASS_RE('Hlt2Topo.*Decision'))", 
        "Neut_PT_MIN": 350.0, 
        "PostscaleD2KPi0EEG": 1, 
        "PostscaleD2PiPi0EEG": 1, 
        "PostscaleDst2D0PiEEG": 1, 
        "PrescaleD2KPi0EEG": 1, 
        "PrescaleD2PiPi0EEG": 1, 
        "PrescaleDst2D0PiEEG": 1, 
        "Res_Mass_MAX": 210.0, 
        "Res_Mass_MIN": 70.0, 
        "Soft_TRCHI2DOF_MAX": 5
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingD2XMuMuSS.py
D2XMuMuSS = {
    "BUILDERTYPE": "StrippingD2XMuMuSSConf", 
    "CONFIG": {
        "D02K3PiLinePostscale": 1, 
        "D02K3PiLinePrescale": 0.005, 
        "D02KKMuMuLinePostscale": 1, 
        "D02KKMuMuLinePrescale": 1, 
        "D02KKPiPiLinePostscale": 1, 
        "D02KKPiPiLinePrescale": 0.01, 
        "D02KPiMuMuLinePostscale": 1, 
        "D02KPiMuMuLinePrescale": 1, 
        "D02PiPiMuMuLinePostscale": 1, 
        "D02PiPiMuMuLinePrescale": 1, 
        "D02PiPiPiPiLinePostscale": 1, 
        "D02PiPiPiPiLinePrescale": 0.01, 
        "D22KPiLinePostscale": 1, 
        "D22KPiLinePrescale": 0.005, 
        "D2K2PiLinePostscale": 1, 
        "D2K2PiLinePrescale": 0.005, 
        "D2KEECalLinePostscale": 1, 
        "D2KEECalLinePrescale": 1, 
        "D2KEEOSLinePostscale": 1, 
        "D2KEEOSLinePrescale": 1, 
        "D2KEESSLinePostscale": 1, 
        "D2KEESSLinePrescale": 1, 
        "D2KEMuCalLinePostscale": 1, 
        "D2KEMuCalLinePrescale": 1, 
        "D2KEMuOSLinePostscale": 1, 
        "D2KEMuOSLinePrescale": 1, 
        "D2KMuEOSLinePostscale": 1, 
        "D2KMuEOSLinePrescale": 1, 
        "D2KMuESSLinePostscale": 1, 
        "D2KMuESSLinePrescale": 1, 
        "D2KMuMuCalLinePostscale": 1, 
        "D2KMuMuCalLinePrescale": 1, 
        "D2KMuMuOSLinePostscale": 1, 
        "D2KMuMuOSLinePrescale": 1, 
        "D2KMuMuSSLinePostscale": 1, 
        "D2KMuMuSSLinePrescale": 1, 
        "D2PiEECalLinePostscale": 1, 
        "D2PiEECalLinePrescale": 1, 
        "D2PiEEOSLinePostscale": 1, 
        "D2PiEEOSLinePrescale": 1, 
        "D2PiEESSLinePostscale": 1, 
        "D2PiEESSLinePrescale": 1, 
        "D2PiEMuCalLinePostscale": 1, 
        "D2PiEMuCalLinePrescale": 1, 
        "D2PiEMuOSLinePostscale": 1, 
        "D2PiEMuOSLinePrescale": 1, 
        "D2PiMuEOSLinePostscale": 1, 
        "D2PiMuEOSLinePrescale": 1, 
        "D2PiMuESSLinePostscale": 1, 
        "D2PiMuESSLinePrescale": 1, 
        "D2PiMuMuCalLinePostscale": 1, 
        "D2PiMuMuCalLinePrescale": 1, 
        "D2PiMuMuOSLinePostscale": 1, 
        "D2PiMuMuOSLinePrescale": 1, 
        "D2PiMuMuSSLinePostscale": 1, 
        "D2PiMuMuSSLinePrescale": 1, 
        "D2PiPiPiCalLinePostscale": 1, 
        "D2PiPiPiCalLinePrescale": 0.01, 
        "DDIRA": 0.9999, 
        "DFDCHI2_hhmumu": 36, 
        "DIPCHI2": 25, 
        "DIPCHI2_4bodyCS": 16, 
        "DIPCHI2_hhmumu": 20, 
        "DMAXDOCA": 0.15, 
        "DMAXDOCA_hhmumu": 0.2, 
        "DMassLow": 1763.0, 
        "DMassWin": 200.0, 
        "DMassWin_CS_hhmumu": 100.0, 
        "DPT_hhmumu": 2500.0, 
        "DVCHI2DOF": 5, 
        "DVCHI2DOF_hhmumu": 8, 
        "DdauMAXIPCHI2_hhmumu": 15, 
        "DimuonMass": 250.0, 
        "EleMINIPCHI2": 5, 
        "EleP": 2000.0, 
        "ElePT": 300.0, 
        "EleTRCHI2": 5, 
        "GhostProbCut_hhmumu": 0.5, 
        "KaonMINIPCHI2": 5, 
        "KaonP": 2000.0, 
        "KaonPIDK": -1.0, 
        "KaonPT": 300.0, 
        "KaonTRCHI2": 5, 
        "Lambdac2PEELinePostscale": 1, 
        "Lambdac2PEELinePrescale": 1, 
        "Lambdac2PEMuLinePostscale": 1, 
        "Lambdac2PEMuLinePrescale": 1, 
        "Lambdac2PEMuSSLinePostscale": 1, 
        "Lambdac2PEMuSSLinePrescale": 1, 
        "Lambdac2PKPiLinePostscale": 1, 
        "Lambdac2PKPiLinePrescale": 0.1, 
        "Lambdac2PMuELinePostscale": 1, 
        "Lambdac2PMuELinePrescale": 1, 
        "Lambdac2PMuMuLinePostscale": 1, 
        "Lambdac2PMuMuLinePrescale": 1, 
        "Lambdac2PMuMuSSLinePostscale": 1, 
        "Lambdac2PMuMuSSLinePrescale": 1, 
        "Lambdac2PPiPiLinePostscale": 1, 
        "Lambdac2PPiPiLinePrescale": 0.01, 
        "LambdacMassWin": 100.0, 
        "LambdacMassWinRare": 200.0, 
        "MINIPCHI2_4bodyCS": 3.0, 
        "MINIPCHI2_hhmumu": 2.0, 
        "MuonMINIPCHI2": 5, 
        "MuonP": 2000.0, 
        "MuonPIDmu_hhmumu": -1, 
        "MuonPT": 300.0, 
        "MuonTRCHI2": 5, 
        "PT_hhmumu": 300, 
        "PionMINIPCHI2": 5, 
        "PionP": 2000.0, 
        "PionPT": 300.0, 
        "PionTRCHI2": 5, 
        "ProtonMINIPCHI2": 6, 
        "ProtonP": 2000.0, 
        "ProtonPT": 300.0, 
        "ProtonTRCHI2": 5, 
        "RelatedInfoTools": [
            {
                "Location": "RelInfoVertexIsolation", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "RelInfoVertexIsolationBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 1.0, 
                "DaughterLocations": None, 
                "Location": "RelInfoConeVariables_1.0", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 1.5, 
                "DaughterLocations": None, 
                "Location": "RelInfoConeVariables_1.5", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 2.0, 
                "DaughterLocations": None, 
                "Location": "RelInfoConeVariables_2.0", 
                "Type": "RelInfoConeVariables"
            }, 
            {
                "ConeAngle": 0.0, 
                "DaughterLocations": None, 
                "Location": "RelInfoConeVariablesForEW_0.0", 
                "Type": "RelInfoConeVariablesForEW"
            }, 
            {
                "ConeAngle": 0.5, 
                "DaughterLocations": None, 
                "Location": "RelInfoConeVariablesForEW_0.5", 
                "Type": "RelInfoConeVariablesForEW"
            }, 
            {
                "ConeAngle": 1.0, 
                "DaughterLocations": None, 
                "Location": "RelInfoConeVariablesForEW_1.0", 
                "Type": "RelInfoConeVariablesForEW"
            }, 
            {
                "ConeAngle": 1.5, 
                "DaughterLocations": None, 
                "Location": "RelInfoConeVariablesForEW_1.5", 
                "Type": "RelInfoConeVariablesForEW"
            }, 
            {
                "DaughterLocations": None, 
                "Location": "RelInfoTrackIsolationBDT", 
                "Type": "RelInfoTrackIsolationBDT"
            }, 
            {
                "Location": "RelInfoBstautauCDFIso", 
                "Type": "RelInfoBstautauCDFIso"
            }, 
            {
                "DaughterLocations": None, 
                "IsoTwoBody": False, 
                "Location": "RelInfoBs2MuMuTrackIsolations", 
                "Type": "RelInfoBs2MuMuTrackIsolations", 
                "angle": 0.27, 
                "doca_iso": 0.13, 
                "fc": 0.6, 
                "ips": 3.0, 
                "makeTrackCuts": False, 
                "pvdis": 0.5, 
                "pvdis_h": 40.0, 
                "svdis": -0.15, 
                "svdis_h": 30.0, 
                "tracktype": 3
            }
        ]
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingD2PiPi0.py
Dst2PiD0EEG = {
    "BUILDERTYPE": "StrippingDst2PiD0EEGConf", 
    "CONFIG": {
        "DiElectron_Mass_MAX": 1914.0, 
        "DiElectron_PT_MIN": 2600.0, 
        "Dstar_AMDiff_MAX": 165.0, 
        "Dstar_MDiff_MAX": 160.0, 
        "Dstar_VCHI2VDOF_MAX": 9.0, 
        "Electron_IPCHI2_MIN": 16.0, 
        "Electron_PT_MIN": 350.0, 
        "Electron_P_MIN": 1000.0, 
        "Electron_TRCHI2DOF_MAX": 6, 
        "Electron_TRGHOSTPROB_MAX": 0.5, 
        "Hlt1Filter": None, 
        "Hlt2Filter": "HLT_PASS_RE('Hlt2CharmHadDstp2.*EmEp.*Decision')", 
        "Neut_PT_MIN": 600.0, 
        "PostscaleDst2PiD0EE": 1, 
        "PostscaleDst2PiD0EEG": 1, 
        "PrescaleDst2PiD0EE": 1, 
        "PrescaleDst2PiD0EEG": 1, 
        "Res_Mass_MAX": 1914.0, 
        "Res_Mass_MIN": 1814.0, 
        "Soft_TRCHI2DOF_MAX": 3
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingDstarD2KSHHPi0.py
DstarD2KSHHPi0 = {
    "BUILDERTYPE": "DstarD2KSHHPi0Lines", 
    "CONFIG": {
        "CombMassHigh": 2100.0, 
        "CombMassLow": 1700.0, 
        "Daug_TRCHI2DOF_MAX": 3, 
        "Dstar_AMDiff_MAX": 190.0, 
        "Dstar_MDiff_MAX": 170.0, 
        "Dstar_VCHI2VDOF_MAX": 15.0, 
        "HighPIDK": 0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "KSCutDIRA_DD": 0.999, 
        "KSCutDIRA_LL": 0.999, 
        "KSCutFDChi2_DD": 5, 
        "KSCutFDChi2_LL": 5, 
        "KSCutMass_DD": 50.0, 
        "KSCutMass_LL": 35.0, 
        "LowPIDK": 0, 
        "MassHigh": 1940.0, 
        "MassLow": 1790.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxDz_DD": 9999.0, 
        "MaxDz_LL": 9999.0, 
        "MaxVCHI2NDOF": 12.0, 
        "MinBPVDIRA": 0.9998, 
        "MinBPVTAU": 0.0001, 
        "MinCombPT": 0.0, 
        "MinDz_DD": 0.0, 
        "MinDz_LL": 0.0, 
        "MinTrkIPChi2": 3, 
        "MinTrkPT": 250.0, 
        "PhotonCL": 0.25, 
        "Pi0MassWin": 16.0, 
        "Pi0PMin": 2500.0, 
        "Pi0PtMin": 1000.0, 
        "PrescaleDstarD2KKPi0KSDD": 1, 
        "PrescaleDstarD2KKPi0KSLL": 1, 
        "PrescaleDstarD2KPiPi0KSDD": 1, 
        "PrescaleDstarD2KPiPi0KSLL": 1, 
        "PrescaleDstarD2PiPiPi0KSDD": 1, 
        "PrescaleDstarD2PiPiPi0KSLL": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingDstarD2XGamma.py
DstarD2XGamma = {
    "BUILDERTYPE": "DstarD2XGammaLines", 
    "CONFIG": {
        "CombMassHigh": 2075.0, 
        "CombMassHigh_HH": 1820.0, 
        "CombMassLow": 1665.0, 
        "CombMassLow_HH": 0.0, 
        "Daug_TRCHI2DOF_MAX": 3, 
        "Dstar_AMDiff_MAX": 180.0, 
        "Dstar_MDiff_MAX": 160.0, 
        "Dstar_VCHI2VDOF_MAX": 15.0, 
        "HighPIDK": 0, 
        "Hlt1Filter": None, 
        "Hlt2Filter": "HLT_PASS_RE('Hlt2CharmHadInclDst2PiD02HHXBDTDecision')", 
        "LowPIDK": 0, 
        "MassHigh": 2055.0, 
        "MassHigh_HH": 1810.0, 
        "MassLow": 1685.0, 
        "MassLow_HH": 0.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxMass_CNV_DD": 100.0, 
        "MaxMass_CNV_LL": 100.0, 
        "MaxVCHI2NDOF": 12.0, 
        "MaxVCHI2NDOF_HH": 16.0, 
        "MaxVCHI2_CNV_DD": 9, 
        "MaxVCHI2_CNV_LL": 9, 
        "MinBPVDIRA": 0.99995, 
        "MinBPVTAU": 0.0001, 
        "MinCombPT": 1500.0, 
        "MinPT": 6500.0, 
        "MinPT_CNV_DD": 1000.0, 
        "MinPT_CNV_LL": 1000.0, 
        "MinTrkIPChi2": 6, 
        "MinTrkPT": 250.0, 
        "MinVDCHI2_HH": 1000.0, 
        "MinVDCHI2_HHComb": 1000.0, 
        "PrescaleDstarD2KKGamma": 1, 
        "PrescaleDstarD2KKGamma_CNVDD": 1, 
        "PrescaleDstarD2KKGamma_CNVLL": 1, 
        "PrescaleDstarD2KPiGamma": 1, 
        "PrescaleDstarD2KPiGamma_CNVDD": 1, 
        "PrescaleDstarD2KPiGamma_CNVLL": 1, 
        "PrescaleDstarD2PiPiGamma": 1, 
        "PrescaleDstarD2PiPiGamma_CNVDD": 1, 
        "PrescaleDstarD2PiPiGamma_CNVLL": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5, 
        "photonPT": 1800.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

#StrippingLc2L0DDpi.py
Lambdac2L0DDpi = {
    "BUILDERTYPE": "StrippingLambdac2L0DDpiConf", 
    "CONFIG": {
        "Bach_All_BPVIPCHI2_MIN": 4.0, 
        "Bach_All_PT_MIN": 300.0, 
        "Bach_ETA_MAX": 5.0, 
        "Bach_ETA_MIN": 2.0, 
        "Bach_P_MAX": 100000.0, 
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 90.0, 
        "Comb_ADOCAMAX_MAX": 0.5, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "Lambda0_FDCHI2_MIN": 100, 
        "Lambda0_PT_MIN": 500.0, 
        "Lambda0_P_MIN": 2000.0, 
        "Lambda0_VCHI2VDOF_MAX": 10.0, 
        "Lambdac_PVDispCut": "(BPVVDCHI2 > 16.0)", 
        "Lambdac_VCHI2VDOF_MAX": 10.0, 
        "Lambdac_acosBPVDIRA_MAX": 0.14, 
        "Pi_PIDK_MAX": 3.0, 
        "PostscaleLambdac2Lambda0PiDD": 1.0, 
        "PostscaleLambdac2Lambda0PiLL": 1.0, 
        "PrescaleLambdac2Lambda0PiDD": 1.0, 
        "PrescaleLambdac2Lambda0PiLL": 1.0, 
        "Proton_PIDpPIDK_MIN": 2.0, 
        "Proton_PIDpPIDpi_MIN": 7.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingLc2L0LLpi.py
Lc2L0LLpi = {
    "BUILDERTYPE": "Lc2L0LLpiConf", 
    "CONFIG": {
        "Lambda0ComCuts": "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))", 
        "Lambda0MomN4Cuts": "\n                           (VFASPF(VCHI2) < 30.) \n                           & (ADMASS('Lambda0')<35*MeV) \n                           & (P > 2000.0*MeV)\n                           & (PT > 500.0*MeV)\n                           & (BPVVDCHI2 > 100)\n                           & (VFASPF(VCHI2/VDOF) < 10.0)\n                           ", 
        "LcComCuts": "(ADAMASS('Lambda_c+') < 90.0*MeV) & (ACUTDOCA(0.5, ''))", 
        "LcMomCuts": "\n                          (VFASPF(VCHI2/VDOF) < 10.) \n                          & (BPVDIRA> 0.999) \n                          & (BPVVDCHI2>16.)\n                          ", 
        "PiminusCuts": "(PT > 50*MeV) & (P > 2000*MeV) & (MIPCHI2DV(PRIMARY) > 9.) ", 
        "PionCuts": "(PT > 400*MeV) & (MIPCHI2DV(PRIMARY) > 4.) & (BPVIPCHI2() > 4.0) & (in_range(2000.0*MeV, P, 100000.0)) & (in_range(2.0, ETA, 5.0)) & ((PIDK - PIDpi) < 3.0) ", 
        "Prescale": 1.0, 
        "ProtonCuts": "(P > 2000*MeV) & (MIPCHI2DV(PRIMARY) > 9.) ", 
        "RelatedInfoTools": [
            {
                "Location": "RelInfoVertexIsolation", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "RelInfoVertexIsolationBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "RelInfoConeVariables_1.0", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "RelInfoConeVariables_1.5", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "RelInfoConeVariables_2.0", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }
        ]
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

#StrippingNeutralCBaryons.py
NeutralCBaryons = {
    "BUILDERTYPE": "StrippingNeutralCBaryonsConf", 
    "CONFIG": {
        "Bachelor_PT_MIN": 50.0, 
        "KaonPIDK": -5.0, 
        "LambdaDDMassWin": 5.7, 
        "LambdaDDMaxVZ": 2275.0, 
        "LambdaDDMinVZ": 400.0, 
        "LambdaDDVtxChi2Max": 5.0, 
        "LambdaDeltaZ_MIN": 5.0, 
        "LambdaLLMassWin": 5.7, 
        "LambdaLLMaxVZ": 400.0, 
        "LambdaLLMinDecayTime": 0.005, 
        "LambdaLLMinVZ": -100.0, 
        "LambdaLLVtxChi2Max": 5.0, 
        "LambdaPi_PT_MIN": 100.0, 
        "LambdaPr_PT_MIN": 500.0, 
        "PionPIDK": 10.0, 
        "ProbNNkMin": 0.1, 
        "ProbNNpMinDD": 0.05, 
        "ProbNNpMinLL": 0.1, 
        "TRCHI2DOFMax": 3.0, 
        "TrGhostProbMax": 0.25
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}
