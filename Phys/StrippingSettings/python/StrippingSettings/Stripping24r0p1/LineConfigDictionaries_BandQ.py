###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                        S T R I P P I N G  2 4 r 0 p 1                      ##
##                                                                            ##
##  Configuration for B&Q WG                                                  ##
##  Contact person: liupan.an@cern.ch                                         ##
################################################################################

from GaudiKernel.SystemOfUnits import *

#########################################################
### StrippingD02KpiForPromptCharm  
### StrippingDstarForPromptCharm                 
### StrippingDForPromptCharm                    
### StrippingDsForPromptCharm                   
### StrippingLambdaCForPromptCharm              
### StrippingLambdaC2pKKForPromptCharm          
### StrippingSigmaCForPromptCharm               
### StrippingLambdaCstarForPromptCharm          
### StrippingDiCharmForPromptCharm              
### StrippingDiMuonAndCharmForPromptCharm       
### StrippingDoubleDiMuonForPromptCharm         
### StrippingOmegaCstarForPromptCharm
### StrippingChiAndCharmForPromptCharm          
### StrippingChiAndWForPromptCharm              
### StrippingXicc+ForPromptCharm                
### StrippingXicc++ForPromptCharm               
### StrippingDsLamCForPromptCharm               
### StrippingCharmAndWForPromptCharm            
### StrippingDiMuonAndWForPromptCharm           
### -----------------------------------------------------
### Defined in:                 StrippingPromptCharm.py
### Proponent:                  Ivan.Belyaev@cern.ch
### Motivation:                 coherent stripping of stable charm hadrons
### Documentation: https://indico.cern.ch/conferenceDisplay.py?confId=270130
#########################################################
PromptCharm = {
    #
    
    'WGs'         : [ 'BandQ' ] ,
    'CONFIG'      : {
    #
    # use for simulation:
    'NOPIDHADRONS'   : False   , 
    #
    ## PT-cuts
    ## attention: with 1GeV pt-cut prescale is needed for D0,D+,D*+ and Ds
    #
    'pT(D0)'     :  1.0 * GeV ,    ## pt-cut for  prompt   D0
    'pT(D+)'     :  1.0 * GeV ,    ## pt-cut for  prompt   D+
    'pT(Ds+)'    :  1.0 * GeV ,    ## pt-cut for  prompt   Ds+
    'pT(Lc+)'    :  1.0 * GeV ,    ## pt-cut for  prompt   Lc+
    'pT(Xic0)'   :  1.0 * GeV ,    ## pt-cut for  prompt   Xic0/Omegac0
    'pT(Omgcc)'  :  1.0 * GeV ,    ## pt-cut for  prompt   Omegacc
    #
    'pT(D0->HH)' :  1.0 * GeV ,    ## pt-cut for  prompt   D0->KK,pipi models 
    #
    # Selection of basic particles
    #
    'TrackCut' : """
    ( CLONEDIST   > 5000      ) &
    ( TRGHOSTPROB < 0.5       ) &
    in_range ( 2  , ETA , 4.9 ) &
    HASRICH
    """ ,
    # 
    'PionCut'   : """
    ( PT          > 250 * MeV ) & 
    ( CLONEDIST   > 5000      ) & 
    ( TRGHOSTPROB < 0.5       ) &
    in_range ( 2          , ETA , 4.9       ) &
    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
    HASRICH                     &
    ( MIPCHI2DV()  > 9        )
    """ ,
    #
    'KaonCut'   : """
    ( PT          > 250 * MeV ) & 
    ( CLONEDIST   > 5000      ) & 
    ( TRGHOSTPROB < 0.5       ) &
    in_range ( 2          , ETA , 4.9       ) &
    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
    HASRICH                     &
    ( MIPCHI2DV()  > 9        )
    """ ,
    #
    'ProtonCut'   : """
    ( PT           > 250 * MeV ) & 
    ( CLONEDIST    > 5000      ) & 
    ( TRGHOSTPROB  < 0.5       ) & 
    in_range ( 2         , ETA , 4.9       ) &
    in_range ( 10 * GeV  , P   , 150 * GeV ) &
    HASRICH                      &
    ( MIPCHI2DV()  > 9         ) 
    """ ,
    ##
    'MuonCut'   : """
    ISMUON &
    in_range ( 2 , ETA , 4.9     ) &
    ( PT            >  550 * MeV ) &
    ( PIDmu - PIDpi >    0       ) &
    ( CLONEDIST     > 5000       )     
    """ ,
    ## cust for prompt kaon
    'PromptKaonCut'   : """
    ( CLONEDIST   > 5000         ) & 
    ( TRGHOSTPROB < 0.5          ) &
    in_range ( 2          , ETA , 4.9       ) &
    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
    HASRICH                     
    """ ,
    #
    ## PID-cuts for hadrons 
    #
    'PionPIDCut'   : " PROBNNpi > 0.1 " ,
    'KaonPIDCut'   : " PROBNNk  > 0.1 " ,
    'ProtonPIDCut' : " PROBNNp  > 0.1 " ,
    'PhotonCLCut'  : 0.05,
    ##
    #
    ## photons from chi_(c,b)
    #
    'GammaChi'        : " ( PT > 400 * MeV ) & ( CL > 0.05 ) " ,
    #
    ## W+- selection
    #
    'WCuts'           : " ( 'mu+'== ABSID ) & ( PT > 15 * GeV )" ,
    #
    # Global Event cuts
    #
    'CheckPV'         : True ,
    #
    # Technicalities:
    #
    'Preambulo'       : [
    # the D0 decay channels
    "pipi   = DECTREE ('[D0]cc -> pi- pi+   ') " ,
    "kk     = DECTREE ('[D0]cc -> K-  K+    ') " ,
    "kpi    = DECTREE ('[D0    -> K-  pi+]CC') " ,
    # number of kaons in final state (as CombinationCuts)
    "ak2    = 2 == ANUM( 'K+' == ABSID ) "       ,
    # shortcut for chi2 of vertex fit
    'chi2vx = VFASPF(VCHI2) '                    ,
    # shortcut for the c*tau
    "from GaudiKernel.PhysicalConstants import c_light" ,
    "ctau     = BPVLTIME (   9 ) * c_light "  , ## use the embedded cut for chi2(LifetimeFit)<9
    "ctau_9   = BPVLTIME (   9 ) * c_light "  , ## use the embedded cut for chi2(LifetimeFit)<9
    "ctau_16  = BPVLTIME (  16 ) * c_light "  , ## use the embedded cut for chi2(LifetimeFit)<16
    "ctau_25  = BPVLTIME (  25 ) * c_light "  , ## use the embedded cut for chi2(LifetimeFit)<25
    "ctau_100 = BPVLTIME ( 100 ) * c_light "  , ## use the embedded cut for chi2(LifetimeFit)<100
    "ctau_400 = BPVLTIME ( 400 ) * c_light "  , ## use the embedded cut for chi2(LifetimeFit)<400
    "ctau_no  = BPVLTIME (     ) * c_light "  , ## no embedded cut for chi2(lifetimeFit)
    # dimuons:
    "psi           =   ADAMASS ('J/psi(1S)') < 150 * MeV"  ,
    "psi_prime     =   ADAMASS (  'psi(2S)') < 150 * MeV"  ,
    ] ,
    # Q Values:
    'QvalueXiCK'     :  600 * MeV ,
    'QvalueXiCprime' :  250 * MeV ,
    'QvalueXiCstar'  :  150 * MeV ,
    'QvalueXiCPiK'   :  600 * MeV ,
    'QvalueXiCprimeK':  600 * MeV ,
    'QvalueLcPiK'    :  700 * MeV ,
    'QvalueOmegaCC'  : 4500 * MeV ,
    ## monitoring ?
    'Monitor'     : False ,
    ## pescales
    'D0Prescale'                   : 0.3 ,
    'D*Prescale'                   : 1.0 ,
    'DsPrescale'                   : 0.7 ,
    'D+Prescale'                   : 0.4 ,
    'LambdaCPrescale'              : 1.0 ,
    'LambdaCLooseChi2IPPrescale'   : 1.0 ,
    'XiC0Prescale'                 : 1.0 ,
    'OmegaC0Prescale'              : 1.0 ,
    'LambdaCpKKPrescale'           : 1.0 ,
    'LambdaC*Prescale'             : 1.0 ,
    'OmegaC*Prescale'              : 1.0 ,
    'XiCprimePrescale'             : 1.0 ,
    'XiC*Prescale'                 : 1.0 ,
    'OmegaC*2XiCPiKPrescale'       : 1.0 ,
    'OmegaC*2XiCprimeKPrescale'    : 1.0 ,
    'XiC**2LcPiKPrescale'          : 1.0 ,
    'OmegaCCPrescale'              : 1.0 ,
    'SigmaCPrescale'               : 1.0 ,
    'Xic02LcPiPrescale'            : 1.0 ,
    #
    'OmegaCC2XiCKpiPrescale'       : 1.0 ,
    'OmegaCC2XiCKKPrescale'        : 1.0 ,
    ##
    'D02KKPrescale'                : 1.0 ,
    'D02pipiPrescale'              : 1.0 ,
    'D*CPPrescale'                 : 1.0 ,
    ##
    'DiCharmPrescale'              : 1.0 ,
    'DiMu&CharmPrescale'           : 1.0 ,
    'DoubleDiMuonPrescale'         : 1.0 ,
    'Chi&CharmPrescale'            : 1.0 ,
    'Charm&WPrescale'              : 1.0 ,
    'DiMuon&WPrescale'             : 1.0 ,
    'Chi&WPrescale'                : 1.0 ,
    ## triple charm & friends 
    'TripleDiMuPrescale'           : 1.0 ,
    'TriCharmPrescale'             : 1.0 ,
    'DiMuonAndDiCharmPrescale'     : 1.0 ,
    'DoubleDiMuonAndCharmPrescale' : 1.0 ,
    ## ========================================================================
    } , 
    'BUILDERTYPE' :   'StrippingPromptCharmConf'            ,
    'STREAMS'     : {
    'Charm'    : [
    ## 'StrippingD02KpiForPromptCharm'                 , 
    ## 'StrippingDstarForPromptCharm'                  , 
    ## 'StrippingDForPromptCharm'                      , 
    ## 'StrippingDsForPromptCharm'                     ,
    ## 'StrippingLambdaCForPromptCharm'                ,
    ## 'StrippingLambdaCLooseChi2IPForPromptCharm'     ,
    ## 'StrippingXiC0ForPromptCharm'                   ,
    ## 'StrippingLambdaC2pKKForPromptCharm'            ,
    ## 'StrippingSigmaCForPromptCharm'                 ,
    ## 'StrippingLambdaCstarForPromptCharm'            ,
    'StrippingOmegaCstarForPromptCharm'             ,
    ## 'StrippingXiCprimeForPromptCharm'               ,
    ## 'StrippingXiCstarForPromptCharm'                ,
    ## 'StrippingOmegaCstar2XiCPiKForPromptCharm'      ,
    ## 'StrippingOmegaCstar2XiCprimeKForPromptCharm'   ,
    ## 'StrippingXiCstarstar2LambdaCPiKForPromptCharm' ,
    ## 'StrippingOmegaCC2XiCKpiForPromptCharm'         ,
    ## 'StrippingOmegaCC2XiCKKForPromptCharm'          ,
    ## 'StrippingXic02LcPiForPromptCharm'              ,
    ## 'StrippingChiAndCharmForPromptCharm'            ,
    'StrippingCharmAndWForPromptCharm'              ,
    'StrippingDiMuonAndCharmForPromptCharm'         ,
    'StrippingDiCharmForPromptCharm'                ,  
    ## triple charm and friends 
    'StrippingTriCharmForPromptCharm'               , ## added 2017-04-06
    'StrippingDiMuonAndDiCharmForPromptCharm'       , ## added 2017-04-06
    'StrippingDoubleDiMuonAndCharmForPromptCharm'   , ## added 2017-04-06
    ## ## for Eva
    ## 'StrippingD02KKForPromptCharm'                  , 
    ## 'StrippingD02pipiForPromptCharm'                ,
    ## 'StrippingDstarCPForPromptCharm'
    ] , 
    ##
    ## 'CharmCompleteEvent' : [
    ## 'StrippingOmegaC0ForPromptCharm'
    ## ] ,
    'Leptonic'           : [
    ## 'StrippingDiMuonAndWForPromptCharm'     ,
    ## 'StrippingChiAndWForPromptCharm'        ,
    ## 'StrippingDoubleDiMuonForPromptCharm'   , 
    'StrippingTripleDiMuonForPromptCharm'   , ## added 2017-04-06
    ]
    } ## STREAM 

    }



###############################################################################
##                                                                           ##
##                        Omegab2XixKpi                                      ##
##                                                                           ##
##                       Marco Pappagallo                                    ##
##                                                                           ##
###############################################################################

Omegab2XicKpi= {
    'BUILDERTYPE'       : 'Omegab2XicKpiConf',
    'CONFIG'    : {
                   # Omegab
                    'MinOmegabMass'      : 5500.     # MeV
                   ,'MaxOmegabMass'      : 6700.     # MeV
                   ,'Omegab_PT'          : 3500.     # MeV 
#                   (ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.0))>5000*MeV)
                   ,'OmegabVertChi2DOF'  : 10        # Dimensionless 
                   ,'OmegabLT'           : 0.2       # ps
                   ,'OmegabIPCHI2'       : 25        # Dimensionless
                   ,'OmegabDIRA'         : 0.999     # Dimensionless
                   ,'KaonProbNN'          : 0.1       # Dimensionless
                   ,'PionProbNN'          : 0.1       # Dimensionless
                   ,'ProtProbNN'          : 0.1       # Dimensionless
                   ,'XicPT'               : 1800      # MeV
                   ,'XicBPVVDCHI2'        : 36
                   ,'XicDIRA'             : 0.        # Dimensionless
                   ,'XicDeltaMassWin'     : 100       # MeV
                   ,'MaxXicVertChi2DOF'   : 10        # Dimensionless
                   # Pre- and postscales
                   ,'Omegab2XicKpiPreScale'               : 1.0
                   ,'Omegab2XicKpiPostScale'              : 1.0
                  },
    'STREAMS' : [ 'Bhadron' ],
    'WGs'    : [ 'BandQ' ]
    }

######################################################################
#
# Lb->eta_c K p
# Author: Liming Zhang
#
#####################################################################

Lb2EtacKp = {
    'BUILDERTYPE'       :  'Lb2EtacKpConf',
    'CONFIG'    : {
        'KaonCuts'      : "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)",
        'ProtonCuts'    : "(PROBNNp > 0.1) & (PT > 300*MeV) & (P > 10*GeV) & (TRGHOSTPROB<0.4)",        
        'PionCuts'      : "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4)",
        'EtacComAMCuts' : "(AM<3.25*GeV)",
        'EtacComN4Cuts' : """
                          (in_range(2.75*GeV, AM, 3.25*GeV))
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)
                          """,
        'EtacMomN4Cuts' : """
                           (VFASPF(VCHI2/VDOF) < 9.) 
                           & (in_range(2.8*GeV, MM, 3.2*GeV)) 
                           & (MIPCHI2DV(PRIMARY) > 0.) 
                           & (BPVVDCHI2>10) 
                           & (BPVDIRA>0.9)
                           """,
        'EtacComCuts'   : "(in_range(2.75*GeV, AM, 3.25*GeV))",
        'LambdaSComCuts': "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 4.0 *GeV) & (ADOCACHI2CUT(20., ''))",
        'LambdaSMomCuts': """
                          (MIPCHI2DV(PRIMARY) > 0.)
                          & (BPVVDCHI2 > 10.)
                          & (VFASPF(VCHI2) < 9.)
                          & (BPVDIRA>0.9)
                          """,
        'KsCuts'        : "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5)",
        'LbComCuts'     : "(ADAMASS('Lambda_b0') < 500 *MeV)",
        'LbMomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 10.) 
                          & (BPVDIRA> 0.9999) 
                          & (BPVIPCHI2()<25) 
                          & (BPVVDCHI2>250)
                          & (BPVVDRHO>0.1*mm) 
                          & (BPVVDZ>2.0*mm)
                          """,
        'Prescale'      : 1.,
        'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoVertexIsolationBDT',
                      'Location'          : 'RelInfoVertexIsolationBDT'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]        
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ'],
    }


###############################################################################
##                                                                           ##
##                        Xibc BDT                                           ##
##                                                                           ##
##                       Jibo He                                             ##
##                                                                           ##
###############################################################################

XibcBDT = {
    'BUILDERTYPE'       :  'XibcBDTConf',
    'CONFIG'    : {
        'PionCuts'      : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'KaonCuts'      : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'ProtonCuts'    : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",

        'DplusCuts'     : "(ADMASS('D+')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>100)",
        'D0Cuts'        : "(ADMASS('D0')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>64)",
    
        'LcComCuts'     : "(APT>1.0*GeV) & (ADAMASS('Lambda_c+')<50*MeV) & (ADOCACHI2CUT(30, ''))",
        'LcMomCuts'     : "(ADMASS('Lambda_c+')<30*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>16)",

        'LambdaDDCuts'   : "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25)",
        'LambdaLLComCuts': "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))",
        'LambdaLLCuts'   : "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)",

        'PhiCuts'       : """
                          (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<1.05*GeV) & (MIPCHI2DV(PRIMARY)>2.)
                          & (INTREE( (ID=='K+') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))
                          & (INTREE( (ID=='K-') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))
                          """ ,

        'UnPionCuts'    : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4)",  
        'UnKaonCuts'    : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4)",
        'UnProtonCuts'  : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)",

        'UnPTPionCuts'    : "(PROBNNpi> 0.2) & (PT>500*MeV) & (TRGHOSTPROB<0.4)",  
        'UnPTKaonCuts'    : "(PROBNNk > 0.1) & (PT>600*MeV) & (TRGHOSTPROB<0.4)",
        'UnPTProtonCuts'  : "(PROBNNp> 0.05) & (PT>750*MeV) & (TRGHOSTPROB<0.4)",    

        'TightPionCuts'   : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
        'TightKaonCuts'   : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
        'TightProtonCuts' : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",

        'HighPTPionCuts'   : "(PROBNNpi> 0.2) & (PT>0.5*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'HighPTKaonCuts'   : "(PROBNNk > 0.1) & (PT>1.0*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'HighPTProtonCuts' : "(PROBNNp> 0.05) & (PT>1.0*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",

        'Pion4LPCuts'   : "(PROBNNpi> 0.2) & (PT>100*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>9.)",
        
        'XibcComCuts'   : "(AM>4.8*GeV)",
        'XibcMomCuts'   : "(M>5.0*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVIPCHI2()<25) & (BPVDIRA> 0.99)",
        'XibcLPMomCuts' : "(M>5.0*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVIPCHI2()<25)",

        'LbComCuts'     : "(APT>1.0*GeV) & (ADAMASS('Lambda_b0')<80*MeV) & (ADOCACHI2CUT(30, ''))",
        'LbMomCuts'     : "(ADMASS('Lambda_b0')<60*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>64)",

        'XibcP2pKpiMVACut'   :  "0.14",
        'XibcP2pKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XibcP2pKpi_BDT_v1r0.xml',

        'XibcP2D0pKpiMVACut'   :  "-0.05",
        'XibcP2D0pKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2D0pKpi_BDT_v1r0.xml', # use Xicc one

        'XibcP2DpKMVACut'   :  "0.02",
        'XibcP2DpKXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2DpK_BDT_v1r0.xml',  #use Xicc one

        'XibcP2LcKpiMVACut'   :  "0.10",
        'XibcP2LcKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XibcP2LcKpi_BDT_v1r0.xml',

        'XibcP2LambdaPiMVACut'   :  "0.1",
        'XibcP2LambdaPiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XibcP2LambdaPi_BDT_v1r0.xml',

        'Xibc2pKMVACut'   :  "0.",
        'Xibc2pKXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2pK_BDT_v1r0.xml',

        'Xibc2D0pKMVACut'   :  "0.05",
        'Xibc2D0pKXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2D0pK_BDT_v1r0.xml',

        'Xibc2LambdaPhiMVACut'   :  "-0.1",
        'Xibc2LambdaPhiXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2LambdaPhi_BDT_v1r0.xml',

        'Xibc2LcPiMVACut'   :  "0.",
        'Xibc2LcKMVACut'    :  "-0.01",
        'Xibc2LcPiXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2LcPi_BDT_v1r0.xml',

        'Xibc2LbKpiMVACut'   :  "-0.3",
        'Xibc2LbKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/Xibc2LbKpi_BDT_v1r0.xml'
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ']
    }

###############################################################################
##                                                                           ##
##                        Xicc BDT                                           ##
##                                                                           ##
##                       Jibo He                                             ##
##                                                                           ##
###############################################################################
XiccBDT = {
    'NAME'              :  'XiccBDT',
    'BUILDERTYPE'       :  'XiccBDTConf',
    'CONFIG'    : {
        'PionCuts'      : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'KaonCuts'      : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",
        'ProtonCuts'    : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)",

        'DplusCuts'     : "(ADMASS('D+')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>100)",
        'D0Cuts'        : "(ADMASS('D0')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>64)",
    
        'LcComCuts'     : "(APT>1.0*GeV) & (ADAMASS('Lambda_c+')<50*MeV) & (ADOCACHI2CUT(30, ''))",
        'LcMomCuts'     : "(ADMASS('Lambda_c+')<30*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>16)",
        
        'UnPionCuts'    : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4)",  
        'UnKaonCuts'    : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4)",
        'UnProtonCuts'  : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)",  

        'TightPionCuts'   : "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
        'TightKaonCuts'   : "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
        'TightProtonCuts' : "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)",
         
        'XiccComCuts'   : "(AM<4.6*GeV) & (APT>2*GeV)",
        'XiccMomCuts'   : "(M<4.4*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.99) & (BPVIPCHI2()<25)",

        'XiccPP2LcPiMVACut'   :  "0.17",
        'XiccPP2LcPiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2LcPi_BDT_v1r0.xml',

        'XiccPP2DpMVACut'   :  "0.05",
        'XiccPP2DpXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2Dp_BDT_v1r0.xml',

        'XiccPP2DpKpiMVACut'   :  "-0.05",
        'XiccPP2DpKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2DpKpi_BDT_v1r0.xml',

        'XiccPP2D0pKpipiMVACut'   :  "-0.1",
        'XiccPP2D0pKpipiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2D0pKpipi_BDT_v1r0.xml',
        
        'XiccPP2LcKpipiMVACut'   :  "0.",
        'XiccPP2LcKpipiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccPP2LcKpipi_BDT_v1r0.xml',

        'XiccP2LcKpiMVACut'   :  "0.14",
        'XiccP2LcKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2LcKpi_BDT_v1r0.xml',

        'XiccP2D0pMVACut'   :  "0.1",
        'XiccP2D0pXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2D0p_BDT_v1r0.xml',

        'XiccP2D0pKpiMVACut'   :  "-0.05",
        'XiccP2D0pKpiXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2D0pKpi_BDT_v1r0.xml',

        'XiccP2DpKMVACut'   :  "0.",
        'XiccP2DpKXmlFile'  :  '$TMVAWEIGHTSROOT/data/XiccP2DpK_BDT_v1r0.xml',       
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ']
    }

###############################################################################
##                                                                           ##
##                        Bbbar2PhiPhi                                       ##
##                                                                           ##
##                       Simone Stracka                                      ##
##                                                                           ##
###############################################################################
Bbbar2PhiPhi = {
    'BUILDERTYPE' : 'Bbbar2PhiPhiConf',
    'CONFIG' : {
        'TRCHI2DOF'        :     3.  ,
        'KaonPIDK'         :     0.  ,
        'KaonPT'           :   650.  , # MeV
        'PhiVtxChi2'       :     9.  ,
        'PhiMassW'         :    30.  , 
        'CombMaxMass'      : 10700.  , # MeV, before Vtx fit
        'CombMinMass'      :  8800.  , # MeV, before Vtx fit
        'MaxMass'          : 10600.  , # MeV, after Vtx fit
        'MinMass'          :  8900.  , # MeV, after Vtx fit
        'Phi_TisTosSpecs'  : { "Hlt1Global%TIS" : 0 },
        },
    'STREAMS' : [ 'Charm' ] ,
    'WGs'     : [ 'BandQ' ]
    }

#########################################################
### StrippingCcbar2PpbarDetachedLine
### -----------------------------------------------------
### Defined in:                 StrippingCcbar2PpbarNew.py
### Proponent:                  sergey.barsuk@cern.ch, Jibo.He@cern.ch
### Motivation:                 Module for Charmonium->p pbar: Detached line
### Documentation:              n/a
#########################################################

Ccbar2PpbarDetached = {
    'BUILDERTYPE'       : 'Ccbar2PpbarConf',
    'CONFIG'    : {
      'LinePrescale'     :     1.  ,
      'LinePostscale'    :     1.  ,
      
      'SpdMult'          :   600.  , # dimensionless, Spd Multiplicy cut 
      'ProtonPT'         :  1000.  , # MeV
      'ProtonP'          :    -2.  , # MeV
      'ProtonTRCHI2DOF'  :     5.  ,
      'ProtonPIDppi'     :    15.  , # CombDLL(p-pi)
      'ProtonPIDpK'      :    10.  , # CombDLL(p-K)
      'ProtonIPCHI2Cut'  : " & (BPVIPCHI2()>9)",
      'CombMaxMass'      :  1.0e+6 , # MeV, before Vtx fit
      'CombMinMass'      :  2650.  , # MeV, before Vtx fit
      'MaxMass'          :  1.0e+6 , # MeV, after Vtx fit
      'MinMass'          :  2700.  , # MeV, after Vtx fit
      'VtxCHI2'          :     9.  , # dimensionless
      'CCCut'            :  " & (BPVDLS>5)"     
      },
    'STREAMS' : [ 'CharmCompleteEvent' ] ,
    'WGs'    : [ 'BandQ' ]
    }


#########################################################
### StrippingCcbar2PhiPhiDetachedLine
### -----------------------------------------------------
### Defined in:                 StrippingCcbar2PhiPhi.py
### Proponent:                  sergey.barsuk@cern.ch, Jibo.He@cern.ch
### Motivation:                 
###   Module for selecting Ccbar->PhiPhi, including two lines:
###   1. Prompt line, with tight PT, PID cuts, requiring Hlt Tis, since there 
###      is no lifetime unbiased phi trigger yet.
###   2. Detached line, with loose PT, PID cuts, but with IPS cuts on Kaons. 
### Documentation:              n/a
#########################################################
Ccbar2PhiDetached = {
    'BUILDERTYPE' : 'Ccbar2PhiPhiDetachedConf',
    'CONFIG' : {
        'TRCHI2DOF'        :     5.  ,
        'KaonPIDK'         :     5.  ,
        'KaonPT'           :   650.  , # MeV
        'PhiVtxChi2'       :    16.  ,
        'PhiMassW'         :    12.  , 
        'CombMaxMass'      :  4100.  , # MeV, before Vtx fit
        'CombMinMass'      :  2750.  , # MeV, before Vtx fit
        'MaxMass'          :  4000.  , # MeV, after Vtx fit
        'MinMass'          :  2800.  , # MeV, after Vtx fit
        'Phi_TisTosSpecs'  : { "Hlt1Global%TIS" : 0 },
        'PionCuts'         :  "(PT>0.7*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>36.) & (PIDK<10)" ,
        'KaonCuts'         :  "(PT>0.5*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>25.) & (PIDK>5)",
        'LooseKaonCuts'    :  "(PT>0.5*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>9.)"
        },
    'STREAMS' : [ 'CharmCompleteEvent' ] ,
    'WGs'     : [ 'BandQ' ]
    }

######################################################################
#
# CCbar -> p pbar pi pi line
# Author: Jibo He
#
#####################################################################

Ccbar2PPPiPi = {
    'BUILDERTYPE'       :  'Ccbar2PPPiPiConf',
    'CONFIG'    : {
        'ProtonCuts'    : "(PROBNNp  > 0.05) & (PT > 400*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 1.)",
        'PionCuts'      : "(PROBNNpi > 0.2 ) & (PT > 150*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 2.)",
        'EtacComAMCuts' : "AALL",
        'EtacComN4Cuts' : """
                          (AM > 2.7 *GeV)
                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)
                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)
                          """,
        'EtacMomN4Cuts' : "(VFASPF(VCHI2/VDOF) < 9.) & (MM>2.8 *GeV) & (BPVDLS>5)",
        'MvaCut'        : "0.19",
        'XmlFile'       : "$TMVAWEIGHTSROOT/data/Ccbar2PPPiPi_BDT_v1r0.xml",
        'Prescale'      : 1.
        },
    'STREAMS'           : ['Bhadron' ],
    'WGs'               : ['BandQ'],
    }

#############################################################################
##                                                                         ##
##                           XB2DPiP                                       ##
##                                                                         ##
##                        Liming Zhang                                     ##
##                                                                         ##
#############################################################################


XB2DPiP = {
    'WGs'               : ['BandQ'],
    'BUILDERTYPE'       : 'XB2DPiPConf',
    'STREAMS' : [ 'Bhadron' ],
    'CONFIG'    : {
                         'TRCHI2DOF'                 :       5
                 ,       'TRGHOSTPROB'               :       0.5
                 ,       'MIPCHI2'                   :       9
                 ,       'ProtonPIDp'                      :       5
                 ,       'ProtonPIDpK'                     :      -3
                 ,        'MomCuts'     : """
                          (VFASPF(VCHI2/VDOF) < 10.) 
                          & (BPVDIRA> 0.9999) 
                          & (BPVIPCHI2()<25) 
                          & (BPVVDCHI2>250)
                          & (BPVVDRHO>0.1*mm) 
                          & (BPVVDZ>2.0*mm)
                          & (MINTREE(((ABSID=='D+') | (ABSID=='D0') | (ABSID=='Lambda_c+')) , VFASPF(VZ))-VFASPF(VZ) > 0.0 *mm )
                          """
                  ,'RelatedInfoTools': [{
                      'Type'              : 'RelInfoVertexIsolation',
                      'Location'          : 'RelInfoVertexIsolation'
                  }, {
                      'Type'              : 'RelInfoVertexIsolationBDT',
                      'Location'          : 'RelInfoVertexIsolationBDT'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.0'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 1.5,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_1.5'
                  }, {
                      'Type'              : 'RelInfoConeVariables',
                      'ConeAngle'         : 2.0,
                      "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'], 
                      'Location'          : 'RelInfoConeVariables_2.0'
                  }]
                  }    
    }

###############################################################################
##                                                                           ##
##                        Ccbar2LstLst                                       ##
##                                                                           ##
##                       Andrii Usachov                                      ##
##                                                                           ##
###############################################################################
Ccbar2Lst = {
    'BUILDERTYPE' : 'Ccbar2LstLstConf',
    'CONFIG' : {
        'TRCHI2DOF'        :    3.  ,
        'CcbarPT'          :    3000,   #MeV
        'KaonProbNNk'      :    0.1  ,
        'KaonPT'           :    600. , # MeV
        'KaonPTSec'        :    350. , # MeV
        'ProtonProbNNp'    :    0.1  ,
        'ProtonPT'         :    800.  , # MeV
        'ProtonPTSec'      :    500.  , # MeV
        'LstVtxChi2'       :    16.  ,
        'LstMinMass'       :  1440  ,
        'LstMaxMass'       :  1600  ,
        'CombMaxMass'      :  6100.  , # MeV, before Vtx fit
        'CombMinMass'      :  3000.  , # MeV, before Vtx fit
        'MaxMass'          :  6000.  , # MeV, after Vtx fit
        'MinMass'          :  2950.  , # MeV, after Vtx fit
        'Lst_TisTosSpecs'  : { "Hlt1Global%TIS" : 0 }
        },
    'STREAMS' : [ 'Charm' ] ,
    'WGs'     : [ 'BandQ' ]
    }

###############################################################################
##                                                                           ##
##                        Ccbar2PhiPhiPiPi                                   ##
##                                                                           ##
##                       Andrii Usachov                                      ##
##                                                                           ##
###############################################################################
Ccbar2PhiPhiPiPi = {
    'BUILDERTYPE'       :  'Ccbar2PhiPhiPiPiConf',
    'CONFIG'    : {
        'PionCuts'      : "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>4.)",
        'Phi_TisTosSpecs'  : { "Hlt1Global%TIS" : 0 },
        'EtacComN4Cuts' : """
                          (AM > 2.7 *GeV)
                          """,  
        'EtacMomN4Cuts' : "(VFASPF(VCHI2/VDOF) < 16.) & (MM>2.8*GeV) & (BPVDLS>4)",
        'Prescale'         :     1.,     
        'TRCHI2DOF'        :     5.,
        'KaonProbNNk'      :    0.1,
        'PhiMassW'         :    30.,
        'PhiVtxChi2'       :    25.,
        'KaonPT'           :    250,
        'KaonIPCHI2'       :      4.
        },
    'STREAMS'           : ['Charm' ],
    'WGs'               : ['BandQ'],
    }
