###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  2 4 r 1                        ##
##                                                                            ##
##  Configuration for Calib WG                                                ##
##  Contact person: Michael Kolpin (Michael.Kolpin@cern.ch)                   ##
################################################################################

from GaudiKernel.SystemOfUnits import *
