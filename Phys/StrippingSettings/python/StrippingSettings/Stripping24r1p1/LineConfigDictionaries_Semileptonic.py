###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
B2DMuNuX = {
    "BUILDERTYPE": "B2DMuNuXAllLinesConf", 
    "CONFIG": {
        "B_DIRA": 0.999, 
        "B_D_DZ": -2.0, 
        "B_DocaChi2Max": 10, 
        "B_MassMax": 8000.0, 
        "B_MassMin": 2200.0, 
        "B_VCHI2DOF": 9.0, 
        "D_AMassWin": 90.0, 
        "D_BPVDIRA": 0.99, 
        "D_DocaChi2Max": 20, 
        "D_FDCHI2": 25.0, 
        "D_MassWin": {
            "Omegac": 60.0, 
            "Xic0": 60.0, 
            "default": 80.0
        }, 
        "D_VCHI2DOF": 6.0, 
        "ElectronPIDe": 3.0, 
        "ElectronPT": 300.0, 
        "GEC_nLongTrk": 250, 
        "HLT1": "HLT_PASS_RE('Hlt1.*Decision')", 
        "HLT2": "HLT_PASS_RE('Hlt2.*Decision')", 
        "HadronIPCHI2": 4.0, 
        "HadronP": 2000.0, 
        "HadronPT": 250.0, 
        "KaonP": 2000.0, 
        "KaonPIDK": -2.0, 
        "Monitor": False, 
        "MuonIPCHI2": 9.0, 
        "MuonP": 6000.0, 
        "MuonPIDmu": 0.0, 
        "MuonPT": 1000.0, 
        "PionPIDK": 10.0, 
        "ProtonP": 8000.0, 
        "ProtonPIDp": 0.0, 
        "ProtonPIDpK": 0.0, 
        "TRCHI2": 3.0, 
        "TRGHOSTPROB": 0.35, 
        "TTSpecs": {}, 
        "UseNoPIDsInputs": False, 
        "prescaleFakes": 0.02, 
        "prescales": {
            "StrippingB2DMuNuX_D0_Electron": 1.0
        }
    }, 
    "STREAMS": {
        "Charm": [
            "StrippingB2DMuNuX_D0_KK", 
            "StrippingB2DMuNuX_D0_PiPi"
        ], 
        "Semileptonic": [
            "StrippingB2DMuNuX_D0_Electron", 
            "StrippingB2DMuNuX_D0", 
            "StrippingB2DMuNuX_D0_K3Pi", 
            "StrippingB2DMuNuX_Dp_Electron", 
            "StrippingB2DMuNuX_Dp", 
            "StrippingB2DMuNuX_Ds_Electron", 
            "StrippingB2DMuNuX_Ds", 
            "StrippingB2DMuNuX_Lc_Electron", 
            "StrippingB2DMuNuX_Lc", 
            "StrippingB2DMuNuX_Omegac", 
            "StrippingB2DMuNuX_Xic", 
            "StrippingB2DMuNuX_Xic0", 
            "StrippingB2DMuNuX_D0_FakeMuon", 
            "StrippingB2DMuNuX_D0_K3Pi_FakeMuon", 
            "StrippingB2DMuNuX_Dp_FakeMuon", 
            "StrippingB2DMuNuX_Ds_FakeMuon", 
            "StrippingB2DMuNuX_Lc_FakeMuon", 
            "StrippingB2DMuNuX_Omegac_FakeMuon", 
            "StrippingB2DMuNuX_Xic_FakeMuon", 
            "StrippingB2DMuNuX_Xic0_FakeMuon"
        ]
    }, 
    "WGs": [ "Semileptonic" ]
}

