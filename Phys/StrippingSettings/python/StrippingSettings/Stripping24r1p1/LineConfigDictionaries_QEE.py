###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
WMu = {
    "BUILDERTYPE": "WMuConf", 
    "CONFIG": {
        "HLT1_SingleTrackNoBias": "HLT_PASS( 'Hlt1MBNoBiasDecision' )", 
        "HLT2_Control10": "HLT_PASS_RE('Hlt2(EW)?SingleMuon(V)?High.*')", 
        "HLT2_Control4800": "HLT_PASS_RE('Hlt2(EW)?SingleMuonLow.*')", 
        "IsoMax": 5000.0, 
        "MinIP": 0.05, 
        "RawEvents": [
            "Muon", 
            "Calo", 
            "Rich", 
            "Velo", 
            "Tracker", 
            "HC"
        ], 
        "STNB_Prescale": 0.0, 
        "SingMuon10_Prescale": 0.0, 
        "SingMuon10_pT": 10000.0, 
        "SingMuon48_Prescale": 0.0, 
        "SingMuon48_pT": 4800.0, 
        "WMuHighIP_Postscale": 1.0, 
        "WMuHighIP_Prescale": 1.0, 
        "WMuIso_Postscale": 1.0, 
        "WMuIso_Prescale": 1.0, 
        "WMuLow_Prescale": 0.0, 
        "WMu_Postscale": 0.0, 
        "WMu_Prescale": 0.0, 
        "pT": 20000.0, 
        "pT_HighIP": 15000.0, 
        "pTiso": 15000.0, 
        "pTlow": 15000.0, 
        "pTvlow": 5000.0
    }, 
    "STREAMS": [ "EW" ], 
    "WGs": [ "QEE" ]
}


