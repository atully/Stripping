###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#############################################################################################
##                          S T R I P P I N G  2 4 r 1 p 1                                 ##
##                                                                                         ##
##  Configuration for Charm WG                                                             ##
##  Contact person: Maxime Schubiger (maxime.schubiger@cern.ch)                            ##
#############################################################################################

Hc2V02H = {
    "BUILDERTYPE": "StrippingHc2V02HConf", 
    "CONFIG": {
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 120.0, 
        "Comb_ADOCAMAX_MAX": 0.3, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "Lambda0_FDCHI2_MIN_DD": 256, 
        "Lambda0_FDCHI2_MIN_LL": 256, 
        "Lambda0_PT_MIN": 250.0, 
        "Lambda0_P_MIN": 2000.0, 
        "Lambda0_VCHI2VDOF_MAX": 12.0, 
        "LambdaMinFD_DD": 0.0, 
        "LambdaMinFD_LL": 25.0, 
        "PostscaleXic2LambdaKPiDD": 1.0, 
        "PostscaleXic2LambdaKPiLL": 1.0, 
        "PostscaleXic2LambdaPiPiDD": 1.0, 
        "PostscaleXic2LambdaPiPiLL": 1.0, 
        "PrescaleXic2LambdaKPiDD": 1.0, 
        "PrescaleXic2LambdaKPiLL": 1.0, 
        "PrescaleXic2LambdaPiPiDD": 1.0, 
        "PrescaleXic2LambdaPiPiLL": 1.0, 
        "ProbNNpMin_DD": 0.1, 
        "ProbNNpMin_LL": 0.1, 
        "Xic_ADMASS_WIN": 90.0, 
        "Xic_PVDispCut_DD": "(BPVVDCHI2 > 49.0)", 
        "Xic_PVDispCut_LL": "(BPVVDCHI2 > 49.0)", 
        "Xic_VCHI2VDOF_MAX_DD": 3.0, 
        "Xic_VCHI2VDOF_MAX_LL": 3.0, 
        "Xic_acosBPVDIRA_MAX_DD": 0.0316, 
        "Xic_acosBPVDIRA_MAX_LL": 0.0316
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

Hc2V03H = {
    "BUILDERTYPE": "StrippingHc2V03HConf", 
    "CONFIG": {
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 120.0, 
        "Comb_ADOCAMAX_MAX": 0.5, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "Lambda0_FDCHI2_MIN": 49, 
        "Lambda0_PT_MIN": 250.0, 
        "Lambda0_P_MIN": 2000.0, 
        "Lambda0_VCHI2VDOF_MAX": 12.0, 
        "LambdaMinFD_DD": 0.0, 
        "LambdaMinFD_LL": 25.0, 
        "Lambdac_ADMASS_WIN": 90.0, 
        "Lambdac_PVDispCut_DD": "(BPVVDCHI2 > 9.0)", 
        "Lambdac_PVDispCut_LL": "(BPVVDCHI2 > 16.0)", 
        "Lambdac_VCHI2VDOF_MAX_DD": 5.0, 
        "Lambdac_VCHI2VDOF_MAX_LL": 5.0, 
        "Lambdac_acosBPVDIRA_MAX_DD": 0.045, 
        "Lambdac_acosBPVDIRA_MAX_LL": 0.14, 
        "PostscaleLambdac2Lambda3PiDD": 1.0, 
        "PostscaleLambdac2Lambda3PiLL": 1.0, 
        "PostscaleLambdac2LambdaK2PiDD": 1.0, 
        "PostscaleLambdac2LambdaK2PiLL": 1.0, 
        "PostscaleXic2LambdaK2PiDD": 1.0, 
        "PostscaleXic2LambdaK2PiLL": 1.0, 
        "PrescaleLambdac2Lambda3PiDD": 1.0, 
        "PrescaleLambdac2Lambda3PiLL": 1.0, 
        "PrescaleLambdac2LambdaK2PiDD": 1.0, 
        "PrescaleLambdac2LambdaK2PiLL": 1.0, 
        "PrescaleXic2LambdaK2PiDD": 1.0, 
        "PrescaleXic2LambdaK2PiLL": 1.0, 
        "ProbNNpMin_DD": 0.0, 
        "ProbNNpMin_LL": 0.1, 
        "Xic_ADMASS_WIN": 90.0, 
        "Xic_PVDispCut_DD": "(BPVVDCHI2 > 9.0)", 
        "Xic_PVDispCut_LL": "(BPVVDCHI2 > 16.0)", 
        "Xic_VCHI2VDOF_MAX_DD": 5.0, 
        "Xic_VCHI2VDOF_MAX_LL": 5.0, 
        "Xic_acosBPVDIRA_MAX_DD": 0.045, 
        "Xic_acosBPVDIRA_MAX_LL": 0.14
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

Hc2V2H = {
    "BUILDERTYPE": "StrippingHc2V2HConf", 
    "CONFIG": {
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 120.0, 
        "Comb_ADOCAMAX_MAX": 0.5, 
        "Hc_ADMASS_WIN": 90.0, 
        "Hc_PVDispCut_DDD": "(BPVVDCHI2 >  9.0)", 
        "Hc_PVDispCut_DDL": "(BPVVDCHI2 > 16.0)", 
        "Hc_PVDispCut_LLL": "(BPVVDCHI2 > 16.0)", 
        "Hc_VCHI2VDOF_MAX_DDD": 12.0, 
        "Hc_VCHI2VDOF_MAX_DDL": 12.0, 
        "Hc_VCHI2VDOF_MAX_LLL": 12.0, 
        "Hc_acosBPVDIRA_MAX_DDD": 0.14, 
        "Hc_acosBPVDIRA_MAX_DDL": 0.045, 
        "Hc_acosBPVDIRA_MAX_LLL": 0.14, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "Lambda0_FDCHI2_MIN": 49, 
        "Lambda0_PT_MIN": 250.0, 
        "Lambda0_P_MIN": 2000.0, 
        "Lambda0_VCHI2VDOF_MAX": 12.0, 
        "LambdaMinFD_DD": 0.0, 
        "LambdaMinFD_LL": 25.0, 
        "PostscaleLambdac2XiKPiDDD": 1.0, 
        "PostscaleLambdac2XiKPiDDL": 1.0, 
        "PostscaleLambdac2XiKPiLLL": 1.0, 
        "PostscaleXic2XiPiPiDDD": 1.0, 
        "PostscaleXic2XiPiPiDDL": 1.0, 
        "PostscaleXic2XiPiPiLLL": 1.0, 
        "PrescaleLambdac2XiKPiDDD": 1.0, 
        "PrescaleLambdac2XiKPiDDL": 1.0, 
        "PrescaleLambdac2XiKPiLLL": 1.0, 
        "PrescaleXic2XiPiPiDDD": 1.0, 
        "PrescaleXic2XiPiPiDDL": 1.0, 
        "PrescaleXic2XiPiPiLLL": 1.0, 
        "ProbNNpMin_DD": 0.0, 
        "ProbNNpMin_LL": 0.1, 
        "Xim_FDCHI2_MIN": 16, 
        "Xim_PT_MIN": 250.0, 
        "Xim_P_MIN": 2000.0, 
        "Xim_VCHI2VDOF_MAX": 12.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

Hc2V3H = {
    "BUILDERTYPE": "StrippingHc2V3HConf", 
    "CONFIG": {
        "Bach_P_MIN": 2000.0, 
        "Comb_ADAMASS_WIN": 120.0, 
        "Comb_ADOCAMAX_MAX": 0.5, 
        "Hlt1Filter": None, 
        "Hlt2Filter": None, 
        "Lambda0_FDCHI2_MIN": 25, 
        "Lambda0_PT_MIN": 250.0, 
        "Lambda0_P_MIN": 2000.0, 
        "Lambda0_VCHI2VDOF_MAX": 12.0, 
        "PostscaleXic2XimPiPiPiDDD": 1.0, 
        "PostscaleXic2XimPiPiPiDDL": 1.0, 
        "PostscaleXic2XimPiPiPiLLL": 1.0, 
        "PrescaleXic2XimPiPiPiDDD": 1.0, 
        "PrescaleXic2XimPiPiPiDDL": 1.0, 
        "PrescaleXic2XimPiPiPiLLL": 1.0, 
        "ProbNNpMin_DD": 0.0, 
        "ProbNNpMin_LL": 0.1, 
        "Xic_ADMASS_WIN": 90.0, 
        "Xic_PVDispCut_DDD": "(BPVVDCHI2 > 9.0)", 
        "Xic_PVDispCut_DDL": "(BPVVDCHI2 > 9.0)", 
        "Xic_PVDispCut_LLL": "(BPVVDCHI2 > 16.0)", 
        "Xic_VCHI2VDOF_MAX_DDD": 5.0, 
        "Xic_VCHI2VDOF_MAX_DDL": 5.0, 
        "Xic_VCHI2VDOF_MAX_LLL": 5.0, 
        "Xic_acosBPVDIRA_MAX_DDD": 0.14, 
        "Xic_acosBPVDIRA_MAX_DDL": 0.14, 
        "Xic_acosBPVDIRA_MAX_LLL": 0.14, 
        "Xim_FDCHI2_MIN": 9, 
        "Xim_PT_MIN": 250.0, 
        "Xim_P_MIN": 2000.0, 
        "Xim_VCHI2VDOF_MAX": 12.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

KKPiPi = {
    "BUILDERTYPE": "KKPiPiConf", 
    "CONFIG": {
        "DetachedComAMCuts": "AALL", 
        "DetachedComN4Cuts": "(AM > 1.0 *GeV) & (APT>3.0 *GeV)", 
        "DetachedKaonCuts": "(PROBNNk  > 0.4) & (PT > 250*MeV) & (TRGHOSTPROB<0.2) & (MIPCHI2DV(PRIMARY) > 4.)", 
        "DetachedMomN4Cuts": "(VFASPF(VCHI2/VDOF) < 9.) & (MM>1.5 *GeV) & (MM<3.0 *GeV) & (BPVDIRA>0.9995)", 
        "DetachedPionCuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.2) & (MIPCHI2DV(PRIMARY) > 4.)", 
        "Prescale": 1.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "Charm" ]
}

