###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
DstarD2hhGammaCalib = {
    "BUILDERTYPE": "DstarD2hhGammaCalib", 
    "CONFIG": {
        "CombMassHigh": 2120.0, 
        "CombMassHigh_HH": 1820.0, 
        "CombMassLow": 1380.0, 
        "CombMassLow_HH": 500.0, 
        "Daug_TRCHI2DOF_MAX": 3, 
        "Dstar_AMDiff_MAX": 180.0, 
        "Dstar_MDiff_MAX": 160.0, 
        "Dstar_VCHI2VDOF_MAX": 15.0, 
        "HighPIDK": -1, 
        "Hlt1Filter": None, 
        "Hlt1Tos": {
            "Hlt1.*Track.*Decision%TOS": 0
        }, 
        "Hlt2Filter": None, 
        "Hlt2Tos": {
            "Hlt2CharmHadInclDst2PiD02HHXBDTDecision%TOS": 0, 
            "Hlt2(Phi)?IncPhiDecision%TOS": 0
        }, 
        "LowPIDK": 5, 
        "MassHigh": 2100.0, 
        "MassHigh_HH": 1810.0, 
        "MassLow": 1400.0, 
        "MassLow_HH": 600.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxIPChi2": 20, 
        "MaxVCHI2NDOF": 10.0, 
        "MaxVCHI2NDOF_HH": 10.0, 
        "MinBPVDIRA": 0.99, 
        "MinBPVTAU": 0.0001, 
        "MinCombPT": 1500.0, 
        "MinPT": 2500.0, 
        "MinTrkIPChi2": 10, 
        "MinTrkPT": 500.0, 
        "MinVDCHI2_HH": 1000.0, 
        "MinVDCHI2_HHComb": 1000.0, 
        "PiSoft_PT_MIN": 200.0, 
        "PrescaleDstarD2KKGamma": 1, 
        "PrescaleDstarD2KPiGamma": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5, 
        "photonPT": 200.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

ElectronRecoEff = {
    "BUILDERTYPE": "StrippingElectronRecoEffLines", 
    "CONFIG": {
        "DetachedEEK": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 20, 
            "bCandFlightDist": 5.0, 
            "bmass_ip_constraint": -3.0, 
            "overlapCut": 0.95
        }, 
        "DetachedEK": {
            "AM": 5000.0, 
            "DIRA": 0.95, 
            "EKIP": -1.0, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2": 15, 
            "VDCHI2": 36, 
            "bCandFlightDist": 5.0
        }, 
        "DoVeloDecoding": False, 
        "EtaMaxVelo": 5.1, 
        "EtaMinVelo": 1.9, 
        "Hlt1Req": {
            "DetachedEEK": "HLT_PASS_RE('Hlt1TrackMVA.*Decision')", 
            "DetachedEK": "HLT_PASS_RE('Hlt1TrackMVA.*Decision')"
        }, 
        "Hlt2Req": {
            "DetachedEK": "HLT_PASS_RE('Hlt2.*Topo.*Decision')"
        }, 
        "SharedChild": {
            "EtaMaxEle": 3.5, 
            "EtaMaxMu": 3.5, 
            "EtaMinEle": 1.8, 
            "EtaMinMu": 1.8, 
            "IPChi2Ele": 16, 
            "IPChi2Kaon": 16, 
            "IPChi2Mu": 16, 
            "IPChi2Pion": 36, 
            "IPEle": 0.0, 
            "IPKaon": 0.0, 
            "IPMu": 0.0, 
            "IPPion": 0.0, 
            "ProbNNe": 0.2, 
            "ProbNNk": 0.2, 
            "ProbNNmu": 0.5, 
            "ProbNNpi": 0.8, 
            "PtEle": 2500.0, 
            "PtKaon": 500.0, 
            "PtMu": 3000.0, 
            "PtPion": 1000.0, 
            "TrChi2Ele": 5, 
            "TrChi2Kaon": 5, 
            "TrChi2Mu": 5, 
            "TrChi2Pion": 5
        }, 
        "TrackGEC": 120, 
        "VeloFitter": "SimplifiedGeometry", 
        "VeloMINIP": 0.05, 
        "VeloTrackChi2": 3.5
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "ALL" ]
}
