###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                          S T R I P P I N G  34r0p1                         ##
##                                                                            ##
##  Configuration for SL WG                                                   ##
##  Contact person: Anna Lupato (anna.lupato@cern.ch)                         ##
################################################################################

######################################################
# all lines
# changes: Tully, Reiss, Ely (all lines required)
#####################################################

B2DMuNuX = {
    "BUILDERTYPE": "B2DMuNuXAllLinesConf", 
    "CONFIG": {
        "B_DIRA": 0.999, 
        "B_D_DZ": -2.0, 
        "B_DocaChi2Max": 10, 
        "B_MassMax": 8000.0, 
        "B_MassMin": 2200.0, 
        "B_VCHI2DOF": 9.0, 
        "D_MassMax": 1700.0,
        "D_MassMin": 700.0,
        "D_AMassWin": 90.0, 
        "D_BPVDIRA": 0.99, 
        "D_DocaChi2Max": 20, 
        "D_FDCHI2": 25.0, 
        "D_MassWin": {
            "Omegac": 60.0, 
            "Xic0": 60.0, 
            "default": 80.0
        }, 
        "D_VCHI2DOF": 6.0, 
        "ElectronPIDe": 3.0, 
        "ElectronPT": 300.0, 
        "GEC_nLongTrk": 250, 
        "HLT1": "HLT_PASS_RE('Hlt1.*Decision')", 
        "HLT2": "HLT_PASS_RE('Hlt2.*Decision')", 
        "HadronIPCHI2": 4.0, 
        "HadronP": 2000.0, 
        "HadronPT": 250.0, 
        "KaonP": 2000.0, 
        "KaonPIDK": -2.0, 
        "Monitor": False, 
        "MuonIPCHI2": 9.0, 
        "MuonP": 6000.0, 
        "MuonPIDmu": 0.0, 
        "MuonPT": 1000.0, 
        "PionPIDK": 10.0, 
        "ProtonP": 8000.0, 
        "ProtonPIDp": 0.0, 
        "ProtonPIDpK": 0.0, 
        "TRCHI2": 3.0, 
        "TRGHOSTPROB": 0.35, 
        "TTSpecs": {}, 
        "UseNoPIDsInputs": False, 
        "prescaleFakes": 0.02, 
        "prescales": {
            "StrippingB2DMuNuX_D0_Electron": 1.0
        }
    }, 
    "STREAMS": {
        "Charm": [
            "StrippingB2DMuNuX_D0_KK", 
            "StrippingB2DMuNuX_D0_PiPi"
        ], 
        "Semileptonic": [
            "StrippingB2DMuNuX_D0_Electron", 
            "StrippingB2DMuNuX_D0", 
            "StrippingB2DMuNuX_D0_KMuNu", 
            "StrippingB2DMuNuX_D0_K3Pi", 
            "StrippingB2DMuNuX_Dp_Electron", 
            "StrippingB2DMuNuX_Dp", 
            "StrippingB2DMuNuX_Ds_Electron", 
            "StrippingB2DMuNuX_Ds", 
            "StrippingB2DMuNuX_Lc_Electron", 
            "StrippingB2DMuNuX_Lc", 
            "StrippingB2DMuNuX_Omegac", 
            "StrippingB2DMuNuX_Xic", 
            "StrippingB2DMuNuX_Xic0", 
            "StrippingB2DMuNuX_D0_FakeMuon", 
            "StrippingB2DMuNuX_D0_K3Pi_FakeMuon", 
            "StrippingB2DMuNuX_Dp_FakeMuon", 
            "StrippingB2DMuNuX_Ds_FakeMuon", 
            "StrippingB2DMuNuX_Lc_FakeMuon", 
            "StrippingB2DMuNuX_Omegac_FakeMuon", 
            "StrippingB2DMuNuX_Xic_FakeMuon", 
            "StrippingB2DMuNuX_Xic0_FakeMuon"
        ]
    }, 
    "WGs": [ "Semileptonic" ]
}

####################################
# all lines
# author: Michel De Cian
#####################################

B2Dst0MuNu = {
    "BUILDERTYPE": "B2Dst0MuNuAllLinesConf", 
    "CONFIG": {
        "HLT_FILTER": "", 
        "TTSpecs": {}
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}

####################################################
#    "StrippingB2PPbarMuForTauMuLine"
#    "StrippingB2PPbarMuForTauMuTopoLine",                       
#    "StrippingB2PPbarMuForTauMuSSLine",                         
#    "StrippingB2PPbarMuForTauMufakePLine",                    
#    "StrippingB2PPbarMuForTauMuSSfakePLine",                      
#    "StrippingB2PPbarMuForTauMufakeMuLine",                     
#    "StrippingB2PPbarMuForTauMuSSfakeMuLine"
#
#     author: Mark Smith
###################################################

B2PPbarMuForTauMu = {
    "BUILDERTYPE": "B2PPbarMuForTauMuBuilder", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFDCHI2HIGH": 50.0, 
        "BVCHI2DOF": 15.0, 
        "DECAYS": [
            "[B- -> J/psi(1S) mu-]cc"
        ], 
        "FAKEPDECAYS": [
            "[B- -> N(1440)0 mu-]cc", 
            "[B+ -> N(1440)0 mu+]cc"
        ], 
        "GEC_nSPDHits": 600.0, 
        "MuonGHOSTPROB": 0.35, 
        "MuonMINIPCHI2": 16.0, 
        "MuonP": 3000.0, 
        "MuonTRCHI2": 4.0, 
        "PPbarDIRA": 0.999, 
        "PPbarFDCHI2HIGH": 25.0, 
        "PPbarVCHI2DOF": 8.0, 
        "ProtonMINIPCHI2": 9.0, 
        "ProtonP": 15000.0, 
        "ProtonPIDK": 2.0, 
        "ProtonPIDp": 2.0, 
        "ProtonPT": 800.0, 
        "ProtonTRCHI2": 3.0, 
        "TRGHOSTPROB": 0.35, 
        "pMuMassLower": 1000.0, 
        "ppMuPT": 1500.0
    }, 
    "STREAMS": { "Semileptonic":[
    "StrippingB2PPbarMuForTauMuLine",  
    "StrippingB2PPbarMuForTauMuTopoLine",                       
    "StrippingB2PPbarMuForTauMuSSLine",                         
    "StrippingB2PPbarMuForTauMufakePLine",                    
    "StrippingB2PPbarMuForTauMuSSfakePLine",                      
    "StrippingB2PPbarMuForTauMufakeMuLine",                     
    "StrippingB2PPbarMuForTauMuSSfakeMuLine"
    ]
    }, 
    "WGs": [ "Semileptonic" ]
}
############################################################################
## Lines defined in StrippingB2XTauNu.py

# StrippingB0d2DstarTauNuNonPhysTauForB2XTauNuAllLines     
# StrippingB0d2DTauNuNonPhysTauForB2XTauNuAllLines            
# StrippingBu2D0TauNuNonPhysTauForB2XTauNuAllLines            
# StrippingBc2JpsiTauNuNonPhysTauForB2XTauNuAllLines          
# StrippingLb2LcTauNuNonPhysTauForB2XTauNuAllLines            
# StrippingBs2DsTauNuNonPhysTauForB2XTauNuAllLines            
# StrippingLb2pTauNuForB2XTauNuAllLines                   
# StrippingLb2pTauNuWSForB2XTauNuAllLines  
#StrippingBu2Dstar02Pi0D0TauNuForB2XTauNuAllLines       
#StrippingBu2Dstar02Pi0D0TauNuWSForB2XTauNuAllLines      
#StrippingBu2Dstar02Pi0D0TauNuNonPhysForB2XTauNuAllLines
#StrippingBu2Dstar02GammaD0TauNuForB2XTauNuAllLines         
#StrippingBu2Dstar02GammaD0TauNuWSForB2XTauNuAllLines   
#StrippingBu2Dstar02GammaD0TauNuNonPhysForB2XTauNuAllLines
#StrippingB0d2DdoubleStar2PiDstar02Pi0D0TauNuForB2XTauNuAllLines
#StrippingB0d2DdoubleStar2PiDstar02Pi0D0TauNuWSForB2XTauNuAllLines
#StrippingB0d2DdoubleStar2PiDstar02Pi0D0TauNuNonPhysForB2XTauNuAllLines
#StrippingB0d2DdoubleStar2PiDstar02GammaD0TauNuForB2XTauNuAllLines
#StrippingB0d2DdoubleStar2PiDstar02GammaD0TauNuWSForB2XTauNuAllLines
#StrippingB0d2DdoubleStar2PiDstar02GammaD0TauNuNonPhysForB2XTauNuAllLines
#StrippingBu2DdoubleStar0TauNuForB2XTauNuAllLines            
#StrippingBu2DdoubleStar0TauNuWSForB2XTauNuAllLines         
#StrippingBu2DdoubleStar0TauNuNonPhysTauForB2XTauNuAllLines
#
#author: Luke S Smead and Guy Wromser
############################################################################

B2XTauNuAllLines = {
    "BUILDERTYPE": "B2XTauNuAllLinesConf", 
    "CONFIG": {
        "B_BPVDIRA": 0.995, 
        "B_DOCAMAX": 0.15, 
        "B_DeltaM_high": 300.0, 
        "B_DeltaM_low": -2579.0, 
        "B_upperDeltaM_high": 1721.0, 
        "B_upperDeltaM_low": 720.0, 
        "D0_BPVVDCHI2": 36.0, 
        "D0_DIRA": 0.995, 
        "D0_DOCAMAX": 0.5, 
        "D0_MassW": 40.0, 
        "D0_PT": 1200.0, 
        "D0_VCHI2": 10.0, 
        "DInvVertD": 1.0, 
        "D_BPVVDCHI2": 50.0, 
        "D_DIRA": 0.995, 
        "D_K_IPCHI2": 10.0, 
        "D_K_PIDK": -3, 
        "D_K_PT": 150.0, 
        "D_K_TRCHI2DOF": 30.0, 
        "D_MIPCHI2": 10.0, 
        "D_MassW": 40.0, 
        "D_PT": 1600.0, 
        "D_Pi_IPCHI2": 10.0, 
        "D_Pi_PIDK": 50.0, 
        "D_Pi_PT": 150.0, 
        "D_Pi_TRCHI2": 3.0, 
        "D_VCHI2": 10.0, 
        "DdoubleStar0_DeltaM_high": 450.0, 
        "DdoubleStar0_DeltaM_low": 350.0, 
        "DdoubleStar0_MassW": 300.0, 
        "DdoubleStar0_PT": 500.0, 
        "DdoubleStar0_Pi_IPCHI2": 4.0, 
        "DdoubleStar0_Pi_PIDK": 8.0, 
        "DdoubleStar0_Pi_TRCHI2": 3.0, 
        "DdoubleStar0_VCHI2": 25.0, 
        "DdoubleStar2PiDstar02GammaD0_DeltaM_high": 450.0, 
        "DdoubleStar2PiDstar02GammaD0_DeltaM_low": 350.0, 
        "DdoubleStar2PiDstar02GammaD0_MassW": 300.0, 
        "DdoubleStar2PiDstar02GammaD0_PT": 500.0, 
        "DdoubleStar2PiDstar02GammaD0_Pi_IPCHI2": 4.0, 
        "DdoubleStar2PiDstar02GammaD0_Pi_PIDK": 8.0, 
        "DdoubleStar2PiDstar02GammaD0_Pi_TRCHI2": 3.0, 
        "DdoubleStar2PiDstar02GammaD0_VCHI2": 25.0, 
        "DdoubleStar2PiDstar02Pi0D0_DeltaM_high": 450.0, 
        "DdoubleStar2PiDstar02Pi0D0_DeltaM_low": 350.0, 
        "DdoubleStar2PiDstar02Pi0D0_MassW": 300.0, 
        "DdoubleStar2PiDstar02Pi0D0_PT": 500.0, 
        "DdoubleStar2PiDstar02Pi0D0_Pi_IPCHI2": 4.0, 
        "DdoubleStar2PiDstar02Pi0D0_Pi_PIDK": 8.0, 
        "DdoubleStar2PiDstar02Pi0D0_Pi_TRCHI2": 3.0, 
        "DdoubleStar2PiDstar02Pi0D0_VCHI2": 25.0, 
        "Dplus_K_PIDK": 3, 
        "Dplus_K_PT": 1500.0, 
        "Dplus_K_TRPCHI2": 1e-08, 
        "Dplus_Pi_TRPCHI2": 1e-08, 
        "Ds_BPVVDCHI2": 36.0, 
        "Ds_K_PT": 1500.0, 
        "Dstar02GammaD0_DeltaM_high": 250.0, 
        "Dstar02GammaD0_DeltaM_low": 0.0, 
        "Dstar02GammaD0_MassW": 50.0, 
        "Dstar02GammaD0_PT": 1250.0, 
        "Dstar02GammaD0_Pi_PT": 400.0, 
        "Dstar02GammaD0_VCHI2": 25.0, 
        "Dstar02Pi0D0_DeltaM_high": 200.0, 
        "Dstar02Pi0D0_DeltaM_low": 120.0, 
        "Dstar02Pi0D0_MassW": 50.0, 
        "Dstar02Pi0D0_PT": 1250.0, 
        "Dstar02Pi0D0_Pi_PT": 200.0, 
        "Dstar02Pi0D0_VCHI2": 25.0, 
        "Dstar_DeltaM_high": 160.0, 
        "Dstar_DeltaM_low": 135.0, 
        "Dstar_MassW": 50.0, 
        "Dstar_PT": 1250.0, 
        "Dstar_VCHI2": 25.0, 
        "Jpsi_MassW": 80.0, 
        "Jpsi_PT": 2000.0, 
        "Jpsi_VCHI2": 9.0, 
        "LbInvVertD": 1.0, 
        "Lc_BPVVDCHI2": 50.0, 
        "Lc_DIRA": 0.995, 
        "Lc_K_IPCHI2": 10.0, 
        "Lc_K_PIDK": 3.0, 
        "Lc_K_PT": 150.0, 
        "Lc_K_TRCHI2DOF": 3.0, 
        "Lc_K_TRPCHI2": 1e-08, 
        "Lc_MIPCHI2": 10.0, 
        "Lc_MassW": 30.0, 
        "Lc_PT": 1200.0, 
        "Lc_Pi_IPCHI2": 10.0, 
        "Lc_Pi_PIDK": 50.0, 
        "Lc_Pi_PT": 150.0, 
        "Lc_Pi_TRCHI2": 3.0, 
        "Lc_Pi_TRPCHI2": 1e-08, 
        "Lc_VCHI2": 10.0, 
        "Lc_p_IPCHI2": 10.0, 
        "Lc_p_PIDp": 5.0, 
        "Lc_p_PT": 150.0, 
        "Lc_p_TRCHI2DOF": 3.0, 
        "Lc_p_TRPCHI2": 1e-08, 
        "Muon_PT": 1000.0, 
        "Muon_TRCHI2DOF": 3.0, 
        "Postscale": 1.0, 
        "Prescale_B0d2DTauNu": 1.0, 
        "Prescale_B0d2DdoubleStar2PiDstar02GammaD0TauNu": 1.0, 
        "Prescale_B0d2DdoubleStar2PiDstar02Pi0D0TauNu": 1.0, 
        "Prescale_B0d2DstarTauNu": 1.0, 
        "Prescale_B0d2DstarWSSlowTauNu": 1.0, 
        "Prescale_B0s2DsTauNu": 1.0, 
        "Prescale_Bc2JpsiTauNu": 1.0, 
        "Prescale_Bu2D0TauNu": 1.0, 
        "Prescale_Bu2DdoubleStar0TauNu": 1.0, 
        "Prescale_Bu2Dstar02GammaD0TauNu": 1.0, 
        "Prescale_Bu2Dstar02Pi0D0TauNu": 1.0, 
        "Prescale_Lb2LcTauNu": 1.0, 
        "Prescale_Lb2pTauNu": 1.0, 
        "Prescale_NonPhys": 1.0, 
        "TRGHP": 0.4, 
        "TRGHP_slowPi": 0.6, 
        "TisTosSpecs": {
            "Hlt1.*Decision%TOS": 0
        }, 
        "p_IPCHI2": 25.0, 
        "p_PE": 15000, 
        "p_PIDp": 0.6, 
        "p_PT": 1200.0, 
        "p_TRCHI2DOF": 3.0, 
        "slowPi_PT": 50.0, 
        "slowPi_TRCHI2DOF": 30.0
    }, 
    "STREAMS": { "BhadronCompleteEvent": [
    "StrippingB0d2DstarTauNuNonPhysTauForB2XTauNuAllLines",    
    "StrippingB0d2DTauNuNonPhysTauForB2XTauNuAllLines",            
    "StrippingBu2D0TauNuNonPhysTauForB2XTauNuAllLines",            
    "StrippingBc2JpsiTauNuNonPhysTauForB2XTauNuAllLines",          
    "StrippingLb2LcTauNuNonPhysTauForB2XTauNuAllLines",            
    "StrippingBs2DsTauNuNonPhysTauForB2XTauNuAllLines",            
    "StrippingLb2pTauNuForB2XTauNuAllLines",                   
    "StrippingLb2pTauNuWSForB2XTauNuAllLines",
    "StrippingBu2Dstar02Pi0D0TauNuForB2XTauNuAllLines",
    "StrippingBu2Dstar02Pi0D0TauNuWSForB2XTauNuAllLines",      
    "StrippingBu2Dstar02Pi0D0TauNuNonPhysForB2XTauNuAllLines",
    "StrippingBu2Dstar02GammaD0TauNuForB2XTauNuAllLines",         
    "StrippingBu2Dstar02GammaD0TauNuWSForB2XTauNuAllLines",   
    "StrippingBu2Dstar02GammaD0TauNuNonPhysForB2XTauNuAllLines",
    "StrippingB0d2DdoubleStar2PiDstar02Pi0D0TauNuForB2XTauNuAllLines",
    "StrippingB0d2DdoubleStar2PiDstar02Pi0D0TauNuWSForB2XTauNuAllLines",
    "StrippingB0d2DdoubleStar2PiDstar02Pi0D0TauNuNonPhysForB2XTauNuAllLines",
    "StrippingB0d2DdoubleStar2PiDstar02GammaD0TauNuForB2XTauNuAllLines",
    "StrippingB0d2DdoubleStar2PiDstar02GammaD0TauNuWSForB2XTauNuAllLines",
    "StrippingB0d2DdoubleStar2PiDstar02GammaD0TauNuNonPhysForB2XTauNuAllLines",
    "StrippingBu2DdoubleStar0TauNuForB2XTauNuAllLines",            
    "StrippingBu2DdoubleStar0TauNuWSForB2XTauNuAllLines",         
    "StrippingBu2DdoubleStar0TauNuNonPhysTauForB2XTauNuAllLines"
    
    ]
                 }, 
    "WGs": [ "Semileptonic" ]
}
####################################
# all lines
# author: Michel De Cian
#####################################

B2XuENu = {
    "BUILDERTYPE": "B2XuENuBuilder", 
    "CONFIG": {
        "BCorrMHigh": 7000.0, 
        "BCorrMLow": 2500.0, 
        "BDIRA": 0.99, 
        "BDIRAMed": 0.994, 
        "BDIRATight": 0.999, 
        "BFDCHI2ForPhi": 50.0, 
        "BFDCHI2HIGH": 100.0, 
        "BFDCHI2Tight": 120.0, 
        "BVCHI2DOF": 6.0, 
        "BVCHI2DOFTight": 4.0, 
        "ElectronGHOSTPROB": 0.35, 
        "ElectronMINIPCHI2": 25, 
        "ElectronP": 3000.0, 
        "ElectronPIDK": 0.0, 
        "ElectronPIDe": 3.0, 
        "ElectronPIDp": 0.0, 
        "ElectronPT": 1000.0, 
        "ElectronPTTight": 1500.0, 
        "ElectronPTight": 6000.0, 
        "ElectronTRCHI2": 4.0, 
        "Enu": 1850.0, 
        "EnuK": 2000.0, 
        "GEC_nLongTrk": 250.0, 
        "KEMassLowTight": 1500.0, 
        "KS0DaugMIPChi2": 50.0, 
        "KS0DaugP": 2000.0, 
        "KS0DaugPT": 100.0, 
        "KS0DaugTrackChi2": 4.0, 
        "KS0EMassLowTight": 3000.0, 
        "KS0MIPChi2": 0.0, 
        "KS0PT": 250.0, 
        "KS0VertexChi2": 4.0, 
        "KS0Z": 0.0, 
        "KSLLCutFDChi2": 0.0, 
        "KSLLMassHigh": 536.0, 
        "KSLLMassLow": 456.0, 
        "KSMajoranaCutFDChi2": 100.0, 
        "KaonMINIPCHI2": 36, 
        "KaonP": 3000.0, 
        "KaonPIDK": 5.0, 
        "KaonPIDK_phi": 2.0, 
        "KaonPIDmu": 5.0, 
        "KaonPIDmu_phi": -2.0, 
        "KaonPIDp": 5.0, 
        "KaonPIDp_phi": -2.0, 
        "KaonPT": 400.0, 
        "KaonPTTight": 1000.0, 
        "KaonPTight": 10000.0, 
        "KaonTRCHI2": 4.0, 
        "KsLLCutFD": 20.0, 
        "KsLLMaxDz": 650.0, 
        "KstarChPionMINIPCHI2": 25.0, 
        "KstarChPionP": 3000.0, 
        "KstarChPionPIDK": 2.0, 
        "KstarChPionPT": 250.0, 
        "KstarChPionTRCHI2": 4.0, 
        "KstarEMassLowTight": 2500.0, 
        "KstarMassWindow": 1200, 
        "KstarPT": 800, 
        "NoPrescales": False, 
        "OmegaChPionMINIPCHI2": 25, 
        "OmegaChPionPIDK": 0, 
        "OmegaChPionPT": 300, 
        "OmegaMassWindow": 150, 
        "OmegaPi0PT": 500, 
        "OmegaVCHI2DOF": 6, 
        "PhiDIRA": 0.9, 
        "PhiE_MCORR": 2500.0, 
        "PhiMINIPCHI2": 9, 
        "PhiPT": 600.0, 
        "PhiUpperMass": 2200.0, 
        "PhiVCHI2DOF": 6, 
        "PiENu_prescale": 0.2, 
        "PionMINIPCHI2": 36, 
        "PionP": 3000.0, 
        "PionPIDK": -2.0, 
        "PionPT": 300.0, 
        "PionPTTight": 800.0, 
        "PionPTight": 10000.0, 
        "PionTRCHI2": 4.0, 
        "RhoChPionMINIPCHI2": 36.0, 
        "RhoChPionPIDK": -2.0, 
        "RhoChPionPT": 400.0, 
        "RhoChPionTRCHI2": 4.0, 
        "RhoDIRA": 0.98, 
        "RhoEMassLowTight": 2000.0, 
        "RhoLeadingPionP": 5000.0, 
        "RhoLeadingPionPT": 900.0, 
        "RhoMINIPCHI2": 50, 
        "RhoMassWindow": 1500.0, 
        "RhoMassWindowMax1SB": 620.0, 
        "RhoMassWindowMax2SB": 1200.0, 
        "RhoMassWindowMin1SB": 300.0, 
        "RhoMassWindowMin2SB": 920.0, 
        "RhoPT": 1000.0, 
        "RhoVCHI2DOF": 4, 
        "TRGHOSTPROB": 0.5, 
        "XEMassUpper": 5500.0, 
        "XEMassUpperHigh": 6500.0
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}
####################################
# all lines
# last changes: Michel De Cian
#####################################

B2XuMuNu = {
    "BUILDERTYPE": "B2XuMuNuBuilder", 
    "CONFIG": {
        "BCorrMHigh": 7000.0, 
        "BCorrMLow": 2500.0, 
        "BDIRA": 0.99, 
        "BDIRAMed": 0.994, 
        "BDIRATight": 0.999, 
        "BFDCHI2ForPhi": 50.0, 
        "BFDCHI2HIGH": 100.0, 
        "BFDCHI2Tight": 120.0, 
        "BVCHI2DOF": 6.0, 
        "BVCHI2DOFTight": 4.0, 
        "Enu": 1850.0, 
        "EnuK": 2000.0, 
        "GEC_nLongTrk": 250.0, 
        "KMuMassLowTight": 1500.0, 
        "KS0DaugMIPChi2": 50.0, 
        "KS0DaugP": 2000.0, 
        "KS0DaugPT": 100.0, 
        "KS0DaugTrackChi2": 4.0, 
        "KS0MIPChi2": 0.0, 
        "KS0MuMassLowTight": 3000.0, 
        "KS0PT": 250.0, 
        "KS0VertexChi2": 4.0, 
        "KS0Z": 0.0, 
        "KSLLCutFDChi2": 0.0, 
        "KSLLMassHigh": 536.0, 
        "KSLLMassLow": 456.0, 
        "KSMajoranaCutFDChi2": 100.0, 
        "KaonMINIPCHI2": 36, 
        "KaonP": 3000.0, 
        "KaonPIDK": 5.0, 
        "KaonPIDK_phi": 2.0, 
        "KaonPIDmu": 5.0, 
        "KaonPIDmu_phi": -2.0, 
        "KaonPIDp": 5.0, 
        "KaonPIDp_phi": -2.0, 
        "KaonPT": 400.0, 
        "KaonPTTight": 1000.0, 
        "KaonPTight": 10000.0, 
        "KaonTRCHI2": 4.0, 
        "KsLLCutFD": 20.0, 
        "KsLLMaxDz": 650.0, 
        "KstarChPionMINIPCHI2": 25.0, 
        "KstarChPionP": 3000.0, 
        "KstarChPionPIDK": 2.0, 
        "KstarChPionPT": 250.0, 
        "KstarChPionTRCHI2": 4.0, 
        "KstarMassWindow": 1200, 
        "KstarMuMassLowTight": 2500.0, 
        "KstarPT": 800, 
        "MuonGHOSTPROB": 0.35, 
        "MuonMINIPCHI2": 25, 
        "MuonP": 3000.0, 
        "MuonPIDK": 0.0, 
        "MuonPIDmu": 3.0, 
        "MuonPIDp": 0.0, 
        "MuonPT": 1000.0, 
        "MuonPTTight": 1500.0, 
        "MuonPTight": 6000.0, 
        "MuonTRCHI2": 4.0, 
        "NoPrescales": False, 
        "OmegaChPionMINIPCHI2": 25, 
        "OmegaChPionPIDK": 0, 
        "OmegaChPionPT": 300, 
        "OmegaMassWindow": 150, 
        "OmegaPi0PT": 500, 
        "OmegaVCHI2DOF": 6, 
        "PhiDIRA": 0.9, 
        "PhiMINIPCHI2": 9, 
        "PhiMu_MCORR": 2500.0, 
        "PhiPT": 600.0, 
        "PhiUpperMass": 2200.0, 
        "PhiVCHI2DOF": 6, 
        "PiMuNu_prescale": 0.2, 
        "PionMINIPCHI2": 36, 
        "PionP": 3000.0, 
        "PionPIDK": -2.0, 
        "PionPT": 300.0, 
        "PionPTTight": 800.0, 
        "PionPTight": 10000.0, 
        "PionTRCHI2": 4.0, 
        "RhoChPionMINIPCHI2": 36.0, 
        "RhoChPionPIDK": -2.0, 
        "RhoChPionPT": 400.0, 
        "RhoChPionTRCHI2": 4.0, 
        "RhoDIRA": 0.98, 
        "RhoLeadingPionP": 5000.0, 
        "RhoLeadingPionPT": 900.0, 
        "RhoMINIPCHI2": 50, 
        "RhoMassWindow": 1500.0, 
        "RhoMassWindowMax1SB": 620.0, 
        "RhoMassWindowMax2SB": 1200.0, 
        "RhoMassWindowMin1SB": 300.0, 
        "RhoMassWindowMin2SB": 920.0, 
        "RhoMuMassLowTight": 2000.0, 
        "RhoPT": 1000.0, 
        "RhoVCHI2DOF": 4, 
        "TRGHOSTPROB": 0.5, 
        "XMuMassUpper": 5500.0, 
        "XMuMassUpperHigh": 6500.0
    }, 
    "STREAMS": [ "Semileptonic" ], 
    "WGs": [ "Semileptonic" ]
}

####################################
#  Strippingb2LcMuXLb2LcMuNuX_Lc2L0PiLine                    
#  Strippingb2LcMuXFakeLb2LcMuNuX_Lc2L0PiLine   
#  author: Michel De Cian
#####################################
Lb2LcMuNuX_Lc2L0Pi = {
    "BUILDERTYPE": "Lb2LcMuNuX_Lc2L0PiConf", 
    "CONFIG": {
        "FakePrescale": 0.1, 
        "GEC_nLongTrk": 250, 
        "GhostProb": 0.35, 
        "HLT1": "HLT_PASS_RE('Hlt1.*Decision')", 
        "HLT2": "HLT_PASS_RE('Hlt2.*Decision')", 
        "LambdaLL_ADMASS": 30.0, 
        "LambdaLL_BPVVDCHI2": 100.0, 
        "LambdaLL_P": 2000.0, 
        "LambdaLL_PT": 500.0, 
        "LambdaLL_VCHI2DOF": 6.0, 
        "Lb_DIRA": 0.999, 
        "Lb_DocaChi2Max": 10, 
        "Lb_Lc_DZ": -2.0, 
        "Lb_MassMax": 8000.0, 
        "Lb_MassMin": 2200.0, 
        "Lb_VCHI2DOF": 9.0, 
        "Lc_AMassWin": 90.0, 
        "Lc_BPVDIRA": 0.99, 
        "Lc_DocaChi2Max": 20, 
        "Lc_FDCHI2": 25.0, 
        "Lc_MassWin": 80.0, 
        "Lc_VCHI2DOF": 6.0, 
        "Monitor": False, 
        "MuonIPCHI2": 9.0, 
        "MuonP": 6000.0, 
        "MuonPIDmu": 0.0, 
        "MuonPT": 1000.0, 
        "PionIPCHI2": 4.0, 
        "PionP": 2000.0, 
        "PionPIDK": 10.0, 
        "PionPT": 250.0, 
        "TRCHI2": 3.0, 
        "TTSpecs": {}, 
        "UseNoPIDsInputs": False
    }, 
    "STREAMS": { "Semileptonic":[
 "Strippingb2LcMuXLb2LcMuNuX_Lc2L0PiLine",               
 "Strippingb2LcMuXFakeLb2LcMuNuX_Lc2L0PiLine" 
   ]
                 }, 
    "WGs": [ "Semileptonic" ]
}




