###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Line config dictionaries for MiniBias.

Stripping1Point4TeV definitions.
Dictionaries have the name of the line builder instance.
"""


MiniBias = {
    'BUILDERTYPE'	:	'MiniBiasConf',
    'CONFIG'	: {
    'L0AnyLine_Prescale': 1.,
    'L0AnyLine_RE': "(HLT_PASS_RE('Hlt1L0Any.*Decision'))",
    'NoBiasLine_Prescale': 1.,
    'NoBiasLine_RE': "(HLT_PASS_RE('Hlt1MB.*Bias.*Decision'))"
    },
    'WGs' : [ 'ALL' ],
    'STREAMS' : [ 'AllStreams' ]
    }

