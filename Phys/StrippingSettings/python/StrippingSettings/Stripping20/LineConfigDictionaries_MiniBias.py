###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Line config dictionaries for MiniBias.
"""


MiniBias = {
    'BUILDERTYPE'	:	'MiniBiasConf',
    'CONFIG'   :{ "NoBiasLine_RE"       : "(HLT_PASS('Hlt1MBNoBiasDecision'))",
                  "NoBiasLine_Rate" : 1,
                  "NoBiasLine_Limiter" : "Hlt1MBNoBiasODINFilter",
                  "L0AnyRLLine_RE"        : "(HLT_PASS('Hlt1L0AnyRateLimitedDecision'))",
                  "L0AnyRLLine_Rate"  : 1,
                  "L0AnyRLLine_Limiter" : "Hlt1L0AnyRateLimitedPostScaler",
                  "L0AnyPSLine_RE"      : "(HLT_PASS_RE('Hlt1L0Any.*Decision'))",
                  "L0AnyPSLine_PS"  : 0.1,
                  }, 
    'WGs' : [ 'ALL' ],
    'STREAMS' : [ 'MiniBias' ]
    }

