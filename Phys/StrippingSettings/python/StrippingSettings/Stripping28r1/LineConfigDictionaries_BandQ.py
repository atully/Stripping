###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################################################
##                          S T R I P P I N G  2 8 r 1                        ##
##                                                                            ##
##  Configuration for B&Q WG                                                  ##
##  Contact person: andrii.usachov@cern.ch                                    ##
################################################################################

Bbbar2PhiPhi = {
    "BUILDERTYPE": "Bbbar2PhiPhiConf", 
    "CONFIG": {
        "CombMaxMass": 10700.0, 
        "CombMinMass": 8800.0, 
        "KaonPIDK": 0.0, 
        "KaonPT": 650.0, 
        "MaxMass": 10600.0, 
        "MinMass": 8900.0, 
        "PhiMassW": 30.0, 
        "PhiVtxChi2": 9.0, 
        "Phi_TisTosSpecs": {
            "Hlt1Global%TIS": 0
        }, 
        "TRCHI2DOF": 3.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

Bc2Ds1Gamma = {
    "BUILDERTYPE": "Bc2Ds1GammaConf", 
    "CONFIG": {
        "Bc2Ds1GammaPostScale": 1.0, 
        "Bc2Ds1GammaPreScale": 1.0, 
        "BcPVIPchi2": 16.0, 
        "Bc_PT": 1000, 
        "CTAU_Bc": 75, 
        "Ds1DeltaMassWin": 580, 
        "KaonProbNN": 0.1, 
        "MaxBcMass": 7500.0, 
        "MaxDs1VertChi2DOF": 10, 
        "MinBcMass": 4800.0, 
        "TrGhostProb": 0.5, 
        "photonPT": 1500.0, 
        "pionProbNN": 0.1
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "BandQ" ]
}

Bc2JpsiHBDT = {
    "BUILDERTYPE": "Bc2JpsiHBDTConf", 
    "CONFIG": {
        "BDTCutValue": 0.6, 
        "BDTWeightsFile": "$TMVAWEIGHTSROOT/data/Bc2JpsiH_BDTG_v1r0.xml", 
        "BcComCuts": "(in_range(5.8*GeV, AM, 7.0*GeV))", 
        "BcMomCuts": "(VFASPF(VCHI2/VDOF)<16) \n                             & (in_range(6.0*GeV, DTF_FUN(M,True,strings( ['J/psi(1S)'])), 6.75*GeV))\n                             & (BPVIPCHI2()<25) \n                            ", 
        "JpsiCuts": "((MM>3.0*GeV) & (MM<3.2*GeV) & (VFASPF(VCHI2PDOF)<16))", 
        "LinePostscale": 1.0, 
        "LinePrescale": 1.0, 
        "MuonCuts": "(MINTREE('mu+'==ABSID,PT)>500*MeV) & (MAXTREE('mu+'==ABSID,TRCHI2DOF)<3) & (MINTREE('mu+'==ABSID,PIDmu)>0.)", 
        "PionCuts": "((TRGHOSTPROB<0.6) & (PT>1.0*GeV))"
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "BandQ" ]
}

Bc2JpsiMu = {
    "BUILDERTYPE": "Bc2JpsiMuXConf", 
    "CONFIG": {
        "BcLowerMass": 3200.0, 
        "BcPT": 6000.0, 
        "BcUpperMass": 6400.0, 
        "BcVtxCHI2": 9.0, 
        "LinePostscale": 1.0, 
        "LinePrescale": 1.0, 
        "MuMuMassWindow": 150.0, 
        "MuMuPT": -10.0, 
        "MuMuParticleName": "'J/psi(1S)'", 
        "MuMuVtxCHI2": 9.0, 
        "MuonBcP": -5.0, 
        "MuonBcPT": 2500.0, 
        "MuonBcTRCHI2DOF": 5.0, 
        "MuonP": -5.0, 
        "MuonPT": 1400.0, 
        "MuonTRCHI2DOF": 5.0
    }, 
    "STREAMS": [ "Leptonic" ], 
    "WGs": [ "BandQ" ]
}

Bc3h = {
    "BUILDERTYPE": "Bc3hConf", 
    "CONFIG": {
        "Bc3kPrescale": 1.0, 
        "Bc3kpiPrescale": 1.0, 
        "Bc3piPrescale": 1.0, 
        "Bc3ppkPrescale": 1.0, 
        "Bc3pppiPrescale": 1.0, 
        "CTAU": 0.14, 
        "CTAU_BC": 0.08, 
        "CheckPV": True, 
        "FILTER": None, 
        "HLT1": None, 
        "HLT2": None, 
        "KaonCut": "\n    ( CLONEDIST   > 5000   ) & \n    ( TRGHOSTPROB < 0.4    ) & \n    ( PT          > 500 * MeV               ) & \n    in_range ( 2          , ETA , 4.9       ) &\n    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &\n    HASRICH                  &\n    ( PROBNNk      > 0.20  ) &\n    ( MIPCHI2DV()  > 9.    ) \n    ", 
        "L0DU": None, 
        "ODIN": None, 
        "PionCut": "\n    ( CLONEDIST   > 5000   ) & \n    ( TRGHOSTPROB < 0.4    ) &\n    ( PT          > 500 * MeV               ) & \n    in_range ( 2          , ETA , 4.9       ) &\n    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &\n    HASRICH                  &\n    ( PROBNNpi     > 0.15  ) &\n    ( MIPCHI2DV()  > 9.    )\n    ", 
        "Preambulo": [
            "chi2vx = VFASPF(VCHI2)   ", 
            "from GaudiKernel.PhysicalConstants import c_light", 
            "ctau      = BPVLTIME ( 25 ) * c_light ", 
            "ctau_9    = BPVLTIME (  9 ) * c_light ", 
            "ctau_16   = BPVLTIME ( 16 ) * c_light ", 
            "mbp_acut  = in_range ( 5.050 * GeV , AM , 5.550 * GeV ) ", 
            "mbc_acut  = in_range ( 6.000 * GeV , AM , 6.600 * GeV ) ", 
            "mbp_cut   = in_range ( 5.100 * GeV ,  M , 5.500 * GeV ) ", 
            "mbc_cut   = in_range ( 6.050 * GeV ,  M , 6.550 * GeV ) "
        ], 
        "ProtonCut": "\n    ( CLONEDIST   > 5000    ) & \n    ( TRGHOSTPROB < 0.4     ) & \n    ( PT          > 500 * MeV              ) & \n    in_range (  2        , ETA , 4.9       ) &\n    in_range ( 10 * GeV  , P   , 150 * GeV ) &\n    HASRICH                   &\n    ( PROBNNp      > 0.15   ) &\n    ( MIPCHI2DV()  > 4.     ) \n    "
    }, 
    "STREAMS": {
        "Bhadron": [
            "StrippingBc3piForBc3h", 
            "StrippingBc3kForBc3h", 
            "StrippingBc3kpiForBc3h", 
            "StrippingBc3pppiForBc3h", 
            "StrippingBc3ppkForBc3h"
        ]
    }, 
    "WGs": [ "BandQ" ]
}

CC2DD = {
    "BUILDERTYPE": "CC2DDConf", 
    "CONFIG": {
        "CCMassCut": "(AM>0)", 
        "CCMaxD0ChildPT": "1500*MeV", 
        "CCMaxD0MinTreeIpChi2": "0.", 
        "CCMaxD0TreePT": "1200*MeV", 
        "CCVtxChi2Ndof": 10, 
        "D0Bpvdira": -10.0, 
        "D0Bpvdls": 4.0, 
        "D0MassWin": "60*MeV", 
        "D0PT": "1000*MeV", 
        "D0VtxChi2Ndof": 8, 
        "D0daughterBpvIpChi2": 4.0, 
        "D0daughterKaonProbNNk": 0.1, 
        "D0daughterP": "5*GeV", 
        "D0daughterPT": "600*MeV", 
        "D0daughterPionProbNNpi": 0.1, 
        "D0daughterTrkChi2": 3, 
        "D0daughterTrkGhostProb": 0.3, 
        "DpmBpvdira": -10.0, 
        "DpmBpvdls": 4.0, 
        "DpmMassWin": "60*MeV", 
        "DpmPT": "1000*MeV", 
        "DpmVtxChi2Ndof": 8, 
        "DpmdaughterBpvIpChi2": 4.0, 
        "DpmdaughterKaonProbNNk": 0.1, 
        "DpmdaughterP": "5*GeV", 
        "DpmdaughterPT": "500*MeV", 
        "DpmdaughterPionProbNNpi": 0.1, 
        "DpmdaughterTrkChi2": 3, 
        "DpmdaughterTrkGhostProb": 0.3, 
        "LcBpvdira": -10.0, 
        "LcBpvdls": 4.0, 
        "LcMassWin": "60*MeV", 
        "LcPT": "1000*MeV", 
        "LcVtxChi2Ndof": 8, 
        "LcdaughterBpvIpChi2": 4.0, 
        "LcdaughterKaonProbNNk": 0.1, 
        "LcdaughterP": "5*GeV", 
        "LcdaughterPT": "500*MeV", 
        "LcdaughterPionProbNNpi": 0.1, 
        "LcdaughterProtonProbNNp": 0.15, 
        "LcdaughterTrkChi2": 3, 
        "LcdaughterTrkGhostProb": 0.3, 
        "XcBpvdira": -10.0, 
        "XcBpvdls": -10.0, 
        "XcMassWin": "60*MeV", 
        "XcPT": "1000*MeV", 
        "XcVtxChi2Ndof": 8, 
        "XcdaughterBpvIpChi2": 2.0, 
        "XcdaughterKaonProbNNk": 0.1, 
        "XcdaughterP": "5*GeV", 
        "XcdaughterPT": "500*MeV", 
        "XcdaughterPionProbNNpi": 0.1, 
        "XcdaughterProtonProbNNp": 0.15, 
        "XcdaughterTrkChi2": 3, 
        "XcdaughterTrkGhostProb": 0.3
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "BandQ" ]
}

CC2DDcontrol = {
    "BUILDERTYPE": "CC2DDConf", 
    "CONFIG": {
        "CCMassCut": "(AM>0)", 
        "CCMaxD0ChildPT": "1500*MeV", 
        "CCMaxD0MinTreeIpChi2": "0.", 
        "CCMaxD0TreePT": "1200*MeV", 
        "CCVtxChi2Ndof": 10, 
        "D0Bpvdira": -10.0, 
        "D0Bpvdls": 4.0, 
        "D0MassWin": "60*MeV", 
        "D0PT": "1000*MeV", 
        "D0VtxChi2Ndof": 8, 
        "D0daughterBpvIpChi2": 4.0, 
        "D0daughterKaonProbNNk": 0.0, 
        "D0daughterP": "5*GeV", 
        "D0daughterPT": "600*MeV", 
        "D0daughterPionProbNNpi": 0.0, 
        "D0daughterTrkChi2": 3, 
        "D0daughterTrkGhostProb": 0.4, 
        "DpmBpvdira": -10.0, 
        "DpmBpvdls": 4.0, 
        "DpmMassWin": "60*MeV", 
        "DpmPT": "1000*MeV", 
        "DpmVtxChi2Ndof": 8, 
        "DpmdaughterBpvIpChi2": 4.0, 
        "DpmdaughterKaonProbNNk": 0.0, 
        "DpmdaughterP": "5*GeV", 
        "DpmdaughterPT": "500*MeV", 
        "DpmdaughterPionProbNNpi": 0.0, 
        "DpmdaughterTrkChi2": 3, 
        "DpmdaughterTrkGhostProb": 0.4, 
        "LcBpvdira": -10.0, 
        "LcBpvdls": 4.0, 
        "LcMassWin": "60*MeV", 
        "LcPT": "1000*MeV", 
        "LcVtxChi2Ndof": 8, 
        "LcdaughterBpvIpChi2": 4.0, 
        "LcdaughterKaonProbNNk": 0.0, 
        "LcdaughterP": "5*GeV", 
        "LcdaughterPT": "500*MeV", 
        "LcdaughterPionProbNNpi": 0.0, 
        "LcdaughterProtonProbNNp": 0.15, 
        "LcdaughterTrkChi2": 3, 
        "LcdaughterTrkGhostProb": 0.4, 
        "XcBpvdira": -10.0, 
        "XcBpvdls": -10.0, 
        "XcMassWin": "60*MeV", 
        "XcPT": "1000*MeV", 
        "XcVtxChi2Ndof": 8, 
        "XcdaughterBpvIpChi2": 2.0, 
        "XcdaughterKaonProbNNk": 0.0, 
        "XcdaughterP": "5*GeV", 
        "XcdaughterPT": "500*MeV", 
        "XcdaughterPionProbNNpi": 0.0, 
        "XcdaughterProtonProbNNp": 0.15, 
        "XcdaughterTrkChi2": 3, 
        "XcdaughterTrkGhostProb": 0.4
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

Ccbar2Lst = {
    "BUILDERTYPE": "Ccbar2LstLstConf", 
    "CONFIG": {
        "CcbarPT": 3000, 
        "CombMaxMass": 6100.0, 
        "CombMinMass": 3000.0, 
        "KaonPT": 600.0, 
        "KaonPTSec": 350.0, 
        "KaonProbNNk": 0.1, 
        "LstMaxMass": 1600, 
        "LstMinMass": 1440, 
        "LstVtxChi2": 16.0, 
        "Lst_TisTosSpecs": {
            "Hlt1Global%TIS": 0
        }, 
        "MaxMass": 6000.0, 
        "MinMass": 2950.0, 
        "ProtonPT": 800.0, 
        "ProtonPTSec": 500.0, 
        "ProtonProbNNp": 0.1, 
        "TRCHI2DOF": 3.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

Ccbar2PPPiPi = {
    "BUILDERTYPE": "Ccbar2PPPiPiConf", 
    "CONFIG": {
        "EtacComAMCuts": "AALL", 
        "EtacComN4Cuts": "\n                          (AM > 2.7 *GeV)\n                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)\n                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)\n                          ", 
        "EtacMomN4Cuts": "(VFASPF(VCHI2/VDOF) < 9.) & (MM>2.8 *GeV) & (BPVDLS>5)", 
        "MvaCut": "0.19", 
        "PionCuts": "(PROBNNpi > 0.2 ) & (PT > 150*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 2.)", 
        "Prescale": 1.0, 
        "ProtonCuts": "(PROBNNp  > 0.05) & (PT > 400*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 1.)", 
        "XmlFile": "$TMVAWEIGHTSROOT/data/Ccbar2PPPiPi_BDT_v1r0.xml"
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BandQ" ]
}

Ccbar2Phi = {
    "BUILDERTYPE": "Ccbar2PhiPhiConf", 
    "CONFIG": {
        "CombMaxMass": 4100.0, 
        "CombMinMass": 2750.0, 
        "KaonCuts": "(PT>0.5*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>25.) & (PIDK>5)", 
        "KaonPIDK": 5.0, 
        "KaonPT": 650.0, 
        "LooseKaonCuts": "(PT>0.5*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>9.)", 
        "MaxMass": 4000.0, 
        "MinMass": 2800.0, 
        "PhiMassW": 12.0, 
        "PhiVtxChi2": 16.0, 
        "Phi_TisTosSpecs": {
            "Hlt1Global%TIS": 0
        }, 
        "PionCuts": "(PT>0.7*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>36.) & (PIDK<10)", 
        "TRCHI2DOF": 5.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

Ccbar2PhiDetached = {
    "BUILDERTYPE": "Ccbar2PhiPhiDetachedConf", 
    "CONFIG": {
        "CombMaxMass": 4100.0, 
        "CombMinMass": 2750.0, 
        "KaonCuts": "(PT>0.5*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>25.) & (PIDK>5)", 
        "KaonPIDK": 5.0, 
        "KaonPT": 650.0, 
        "LooseKaonCuts": "(PT>0.5*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>9.)", 
        "MaxMass": 4000.0, 
        "MinMass": 2800.0, 
        "PhiMassW": 12.0, 
        "PhiVtxChi2": 16.0, 
        "Phi_TisTosSpecs": {
            "Hlt1Global%TIS": 0
        }, 
        "PionCuts": "(PT>0.7*GeV) & (TRCHI2DOF<5) & (MIPCHI2DV(PRIMARY)>36.) & (PIDK<10)", 
        "TRCHI2DOF": 5.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "BandQ" ]
}

Ccbar2PhiPhiPiPi = {
    "BUILDERTYPE": "Ccbar2PhiPhiPiPiConf", 
    "CONFIG": {
        "EtacComN4Cuts": "\n                          (AM > 2.7 *GeV)\n                          ", 
        "EtacMomN4Cuts": "(VFASPF(VCHI2/VDOF) < 16.) & (MM>2.8*GeV) & (BPVDLS>4)", 
        "KaonIPCHI2": 4.0, 
        "KaonPT": 250, 
        "KaonProbNNk": 0.1, 
        "PhiMassW": 30.0, 
        "PhiVtxChi2": 25.0, 
        "Phi_TisTosSpecs": {
            "Hlt1Global%TIS": 0
        }, 
        "PionCuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>4.)", 
        "Prescale": 1.0, 
        "TRCHI2DOF": 5.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

Ccbar2Ppbar = {
    "BUILDERTYPE": "Ccbar2PpbarConf", 
    "CONFIG": {
        "CCCut": " & (PT>6*GeV)", 
        "CombMaxMass": 4100.0, 
        "CombMinMass": 2750.0, 
        "LinePostscale": 1.0, 
        "LinePrescale": 1.0, 
        "MaxMass": 4000.0, 
        "MinMass": 2800.0, 
        "ProtonIPCHI2Cut": "", 
        "ProtonP": 10.0, 
        "ProtonPIDpK": 15.0, 
        "ProtonPIDppi": 20.0, 
        "ProtonPT": 1950.0, 
        "ProtonTRCHI2DOF": 4.0, 
        "SpdMult": 300.0, 
        "VtxCHI2": 9.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

Ccbar2PpbarDetached = {
    "BUILDERTYPE": "Ccbar2PpbarConf", 
    "CONFIG": {
        "CCCut": " & (BPVDLS>5)", 
        "CombMaxMass": 1000000.0, 
        "CombMinMass": 2650.0, 
        "LinePostscale": 1.0, 
        "LinePrescale": 1.0, 
        "MaxMass": 1000000.0, 
        "MinMass": 2700.0, 
        "ProtonIPCHI2Cut": " & (BPVIPCHI2()>9)", 
        "ProtonP": -2.0, 
        "ProtonPIDpK": 10.0, 
        "ProtonPIDppi": 15.0, 
        "ProtonPT": 1000.0, 
        "ProtonTRCHI2DOF": 5.0, 
        "SpdMult": 600.0, 
        "VtxCHI2": 9.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "BandQ" ]
}

Ccbar2PpbarExclusive = {
    "BUILDERTYPE": "Ccbar2PpbarConf", 
    "CONFIG": {
        "CCCut": "", 
        "CombMaxMass": 1000000.0, 
        "CombMinMass": 0.0, 
        "LinePostscale": 1.0, 
        "LinePrescale": 1.0, 
        "MaxMass": 1000000.0, 
        "MinMass": 0.0, 
        "ProtonIPCHI2Cut": "", 
        "ProtonP": -2.0, 
        "ProtonPIDpK": 15.0, 
        "ProtonPIDppi": 20.0, 
        "ProtonPT": 550.0, 
        "ProtonTRCHI2DOF": 5.0, 
        "SpdMult": 20.0, 
        "VtxCHI2": 9.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

CharmAssociative = {
    "BUILDERTYPE": "StrippingCharmAssociativeConf", 
    "CONFIG": {
        "CheckPV": True, 
        "ChiAndDiMuonPrescale": 1.0, 
        "ChiAndWPrescale": 1.0, 
        "DiChiPrescale": 1.0, 
        "DiMuonAndGammaPrescale": 1.0, 
        "DiMuonAndWPrescale": 1.0, 
        "DoubleDiMuonPrescale": 1.0, 
        "GammaChi": " ( PT > 400 * MeV ) & ( CL > 0.05 ) ", 
        "Monitor": False, 
        "MuonCuts": " ISMUON & ( PT > 650 * MeV ) & ( TRCHI2DOF < 5 ) ", 
        "PhotonCuts": " PT > 3.0 * GeV  ", 
        "Preambulo": [
            "chi2vx = VFASPF(VCHI2) ", 
            "from GaudiKernel.PhysicalConstants import c_light", 
            "psi             = ADAMASS ( 'J/psi(1S)'  ) < 125 * MeV", 
            "psi_prime       = ADAMASS (   'psi(2S)'  ) < 125 * MeV", 
            "mu2_tight       = ( chi2vx < 10    ) & ( MINTREE ( 'mu+' == ABSID , PT ) > 900 * MeV ) ", 
            "dimu_tight      = ( PT > 3.0 * GeV ) & mu2_tight ", 
            "psi_tight       = ( ADMASS ( 'J/psi(1S)' ) < 110 * MeV ) & dimu_tight ", 
            "psi_prime_tight = ( ADMASS (   'psi(2S)' ) < 110 * MeV ) & dimu_tight ", 
            "dimuon_heavy    = ( M > 4.9 * GeV ) & dimu_tight ", 
            "dimuon_tight    = psi_tight | psi_prime_tight | dimuon_heavy "
        ], 
        "WCuts": " ( 'mu+'== ABSID ) & ( PT > 15 * GeV )"
    }, 
    "STREAMS": {
        "Leptonic": [
            "StrippingDiMuonAndGammaForCharmAssociative", 
            "StrippingDoubleDiMuonForCharmAssociative", 
            "StrippingChiAndDiMuonForCharmAssociative", 
            "StrippingDiChiForCharmAssociative", 
            "StrippingDiMuonAndWForCharmAssociative", 
            "StrippingChiAndWForCharmAssociative"
        ]
    }, 
    "WGs": [ "BandQ" ]
}

ChiCJPsiGammaConv = {
    "BUILDERTYPE": "StrippingChiCJPsiGammaConvConf", 
    "CONFIG": {
        "GammaEEMass": 100.0, 
        "GammaEEPt": 580.0, 
        "JPsiMassMax": 3.2, 
        "JPsiMassMin": 3.0, 
        "JPsiPTMin": 2, 
        "JPsiVertexChi2": 25.0, 
        "JPsi_PIDmu": 0.0, 
        "MuPMin": 8000, 
        "MuPTMin": 900, 
        "PostscaleChi": 1.0, 
        "PrescaleChi": 1.0, 
        "UpsilonMassMax": 12.9, 
        "UpsilonMassMin": 9.0, 
        "UpsilonPTMin": 0.9, 
        "UpsilonVertexChi2": 25.0, 
        "Upsilon_PIDmu": 0.0, 
        "eDLLe": 0.0, 
        "mMaxChiBFit": 13.0, 
        "mMaxChiBRaw": 13.2, 
        "mMaxChiCFit": 4.7, 
        "mMaxChiCRaw": 4.8, 
        "mMinChiBFit": 9.5, 
        "mMinChiBRaw": 9.4, 
        "mMinChiCFit": 3.0, 
        "mMinChiCRaw": 2.9, 
        "trackChi2": 100000
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingChiCJPsiGammaConvChibLine", 
            "StrippingChiCJPsiGammaConvChicLine"
        ]
    }, 
    "WGs": [ "BandQ" ]
}

DiMuonInherit = {
    "BUILDERTYPE": "DiMuonInherit", 
    "CONFIG": {
        "Debug": False, 
        "Lines": {
            "FullDSTDiMuonDiMuonHighMassLine": {
                "Cuts": {
                    "Mass": "MM > 8500 *MeV", 
                    "MuonP": "MINTREE('mu+'==ABSID,P ) > 8000.0 *MeV", 
                    "MuonPIDmu": "MINTREE('mu+'==ABSID,PIDmu) > 0.0", 
                    "MuonPt": "MINTREE('mu+'==ABSID,PT) > 1000.0 *MeV"
                }, 
                "Inherit": "VirtualBase", 
                "Prescale": 1.0
            }, 
            "FullDSTDiMuonDiMuonHighMassSameSignLine": {
                "Cuts": {
                    "Mass": "MM > 8500 *MeV", 
                    "MuonP": "MINTREE('mu+'==ABSID,P ) > 8000.0 *MeV", 
                    "MuonPt": "MINTREE('mu+'==ABSID,PT) > 1000.0 *MeV"
                }, 
                "Inherit": "VirtualBase", 
                "InputDiMuon": "StdLooseDiMuonSameSign", 
                "Prescale": 0.2
            }, 
            "FullDSTDiMuonDiMuonNoPVLine": {
                "Cuts": {
                    "BPVVDZ": "BPVVDZ < -1 *mm", 
                    "Mass": "MM > 2900"
                }, 
                "Inherit": "VirtualBase", 
                "Prescale": 1.0, 
                "maxPV": 0.5
            }, 
            "FullDSTDiMuonJpsi2MuMuDetachedLine": {
                "Cuts": {
                    "Detachement": "((BPVDLS>3) | (BPVDLS<-3))", 
                    "Mass": "(MM > 2996.916) & (MM < 3196.916)", 
                    "MuonPIDmu": "MINTREE('mu+'==ABSID,PIDmu) > 0.0", 
                    "MuonPt": "MINTREE('mu+'==ABSID,PT) > 500.0 *MeV"
                }, 
                "Inherit": "VirtualBase", 
                "InputDiMuon": "StdLooseJpsi2MuMu", 
                "Prescale": 1.0, 
                "RequiredRawEvents": [
                    "Trigger", 
                    "Muon", 
                    "Calo", 
                    "Rich", 
                    "Velo", 
                    "Tracker"
                ], 
                "checkPV": True
            }, 
            "FullDSTDiMuonJpsi2MuMuTOSLine": {
                "Cuts": {
                    "JpsiPt": "PT > 3 *GeV", 
                    "Mass": "(MM > 3010) & (MM < 3170)", 
                    "MuonP": "MINTREE('mu+'==ABSID,P) > 10 *GeV", 
                    "MuonPIDmu": "MINTREE('mu+'==ABSID,PIDmu) > 0.0", 
                    "TOSHlt1": "TOS('Hlt1DiMuonHighMassDecision', 'Hlt1TriggerTisTos')", 
                    "TOSHlt2": "TOS('Hlt2DiMuonJPsiHighPTDecision', 'Hlt2TriggerTisTos')", 
                    "TOSL0": "TOS('L0.*Mu.*Decision', 'L0TriggerTisTos')"
                }, 
                "Inherit": "VirtualBase", 
                "InputDiMuon": "StdLooseJpsi2MuMu", 
                "Prescale": 1.0, 
                "RequiredRawEvents": None
            }, 
            "FullDSTDiMuonPsi2MuMuDetachedLine": {
                "Cuts": {
                    "Mass": "ADMASS('psi(2S)') < 100.0 *MeV"
                }, 
                "Inherit": "FullDSTDiMuonJpsi2MuMuDetachedLine", 
                "InputDiMuon": "StdLooseDiMuon", 
                "Prescale": 1.0, 
                "RequiredRawEvents": None
            }, 
            "FullDSTDiMuonPsi2MuMuTOSLine": {
                "Cuts": {
                    "Mass": "ADMASS('psi(2S)') < 100.0", 
                    "MuonPIDmu": "MINTREE('mu+'==ABSID,PIDmu) > 0.0", 
                    "MuonPt": "MINTREE('mu+'==ABSID,PT) > 1 *GeV", 
                    "TOSHlt1": "TOS('Hlt1DiMuonHighMassDecision', 'Hlt1TriggerTisTos')", 
                    "TOSHlt2": "TOS('Hlt2DiMuonPsi2SHighPTDecision', 'Hlt2TriggerTisTos')", 
                    "TOSL0": "TOS('L0.*Mu.*Decision', 'L0TriggerTisTos')"
                }, 
                "Inherit": "FullDSTDiMuonJpsi2MuMuTOSLine", 
                "InputDiMuon": "StdLooseDiMuon", 
                "Prescale": 1.0, 
                "RequiredRawEvents": None
            }, 
            "MicroDSTDiMuonDiMuonIncLine": {
                "Cuts": {
                    "Mass": "MM > 3000*MeV"
                }, 
                "Inherit": "VirtualBase", 
                "InputDiMuon": "StdLooseDiMuon", 
                "Prescale": 0.03
            }, 
            "MicroDSTDiMuonDiMuonIncTISLine": {
                "Cuts": {
                    "Mass": "MM > 3000*MeV", 
                    "TISHlt2": "TIS('Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Lumi)(?!Transparent)(?!PassThrough).*Decision', 'Hlt2TriggerTisTos')"
                }, 
                "Inherit": "VirtualBase", 
                "InputDiMuon": "StdLooseDiMuon", 
                "Prescale": 1.0
            }, 
            "MicroDSTDiMuonDiMuonInchighPTLine": {
                "Cuts": {
                    "JpsiPt": "PT > 6 *GeV", 
                    "Mass": "MM > 3000*MeV"
                }, 
                "Inherit": "VirtualBase", 
                "InputDiMuon": "StdLooseDiMuon", 
                "Prescale": 1.0
            }, 
            "MicroDSTDiMuonDiMuonSameSignLine": {
                "Cuts": None, 
                "Inherit": "MicroDSTDiMuonDiMuonIncLine", 
                "InputDiMuon": "StdLooseDiMuonSameSign", 
                "Prescale": 0.1
            }, 
            "MicroDSTDiMuonDiMuonSameSignTISLine": {
                "Cuts": None, 
                "Inherit": "MicroDSTDiMuonDiMuonIncTISLine", 
                "InputDiMuon": "StdLooseDiMuonSameSign", 
                "Prescale": 1.0
            }, 
            "MicroDSTDiMuonDiMuonSameSignhighPTLine": {
                "Cuts": None, 
                "Inherit": "MicroDSTDiMuonDiMuonInchighPTLine", 
                "InputDiMuon": "StdLooseDiMuonSameSign", 
                "Prescale": 1.0
            }, 
            "VirtualBase": {
                "Cuts": {
                    "JpsiVtx": "VFASPF(VCHI2PDOF)< 20.0", 
                    "MuonPt": "MINTREE('mu+'==ABSID,PT) > 650.0 *MeV"
                }, 
                "Inherit": None, 
                "InputDiMuon": "StdLooseDiMuon", 
                "Prescale": 0.0, 
                "RequiredRawEvents": None, 
                "TOScut": None, 
                "checkPV": False, 
                "maxPV": None
            }
        }
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingFullDSTDiMuonPsi2MuMuTOSLine", 
            "StrippingFullDSTDiMuonJpsi2MuMuTOSLine", 
            "StrippingFullDSTDiMuonPsi2MuMuDetachedLine", 
            "StrippingFullDSTDiMuonJpsi2MuMuDetachedLine", 
            "StrippingFullDSTDiMuonDiMuonNoPVLine", 
            "StrippingFullDSTDiMuonDiMuonHighMassLine", 
            "StrippingFullDSTDiMuonDiMuonHighMassSameSignLine"
        ], 
        "Leptonic": [
            "StrippingMicroDSTDiMuonDiMuonSameSignLine", 
            "StrippingMicroDSTDiMuonDiMuonIncLine", 
            "StrippingMicroDSTDiMuonDiMuonSameSignhighPTLine", 
            "StrippingMicroDSTDiMuonDiMuonInchighPTLine", 
            "StrippingMicroDSTDiMuonDiMuonSameSignTISLine", 
            "StrippingMicroDSTDiMuonDiMuonIncTISLine"
        ]
    }, 
    "WGs": [ "BandQ" ]
}

HeavyBaryon = {
    "BUILDERTYPE": "HeavyBaryonsConf", 
    "CONFIG": {
        "DLSForLongLived": 5.0, 
        "JpsiMassWindow": 80.0, 
        "KaonPIDK": -5.0, 
        "OmegaMassWindow": 30.0, 
        "OmegabminusMassWindow": 500.0, 
        "PionPIDK": 5.0, 
        "TRCHI2DOF": 4.0, 
        "XiMassWindow": 30.0, 
        "XibminusMassWindow": 300.0, 
        "XibzeroMassWindow": 500.0
    }, 
    "STREAMS": [ "Dimuon" ], 
    "WGs": [ "BandQ" ]
}

Lb2EtacKp = {
    "BUILDERTYPE": "Lb2EtacKpConf", 
    "CONFIG": {
        "EtacComAMCuts": "(AM<3.25*GeV)", 
        "EtacComCuts": "(in_range(2.75*GeV, AM, 3.25*GeV))", 
        "EtacComN4Cuts": "\n                          (in_range(2.75*GeV, AM, 3.25*GeV))\n                          & ( (ACHILD(PT,1)+ACHILD(PT,2)+ACHILD(PT,3)+ACHILD(PT,4) ) > 2.5 *GeV)\n                          & ( (ACHILD(MIPCHI2DV(), 1) + ACHILD(MIPCHI2DV(), 2) + ACHILD(MIPCHI2DV(), 3) + ACHILD(MIPCHI2DV(), 4))>30)\n                          ", 
        "EtacMomN4Cuts": "\n                           (VFASPF(VCHI2/VDOF) < 9.) \n                           & (in_range(2.8*GeV, MM, 3.2*GeV)) \n                           & (MIPCHI2DV(PRIMARY) > 0.) \n                           & (BPVVDCHI2>10) \n                           & (BPVDIRA>0.9)\n                           ", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT > 300*MeV) & (TRGHOSTPROB<0.4)", 
        "KsCuts": "(ADMASS('KS0') < 30.*MeV) & (BPVDLS>5)", 
        "LambdaSComCuts": "(ACHILD(PT,1)+ACHILD(PT,2) > 900.*MeV) & (AM < 4.0 *GeV) & (ADOCACHI2CUT(20., ''))", 
        "LambdaSMomCuts": "\n                          (MIPCHI2DV(PRIMARY) > 0.)\n                          & (BPVVDCHI2 > 10.)\n                          & (VFASPF(VCHI2) < 9.)\n                          & (BPVDIRA>0.9)\n                          ", 
        "LbComCuts": "(ADAMASS('Lambda_b0') < 500 *MeV)", 
        "LbMomCuts": "\n                          (VFASPF(VCHI2/VDOF) < 10.) \n                          & (BPVDIRA> 0.9999) \n                          & (BPVIPCHI2()<25) \n                          & (BPVVDCHI2>250)\n                          & (BPVVDRHO>0.1*mm) \n                          & (BPVVDZ>2.0*mm)\n                          ", 
        "PionCuts": "(PROBNNpi > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4)", 
        "Prescale": 1.0, 
        "ProtonCuts": "(PROBNNp > 0.1) & (PT > 300*MeV) & (P > 10*GeV) & (TRGHOSTPROB<0.4)", 
        "RelatedInfoTools": [
            {
                "Location": "RelInfoVertexIsolation", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "RelInfoVertexIsolationBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "RelInfoConeVariables_1.0", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "RelInfoConeVariables_1.5", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "RelInfoConeVariables_2.0", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }
        ]
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BandQ" ]
}

Omegab2XicKpi = {
    "BUILDERTYPE": "Omegab2XicKpiConf", 
    "CONFIG": {
        "KaonProbNN": 0.1, 
        "MaxOmegabMass": 6700.0, 
        "MaxXicVertChi2DOF": 10, 
        "MinOmegabMass": 5500.0, 
        "Omegab2XicKpiPostScale": 1.0, 
        "Omegab2XicKpiPreScale": 1.0, 
        "OmegabDIRA": 0.999, 
        "OmegabIPCHI2": 25, 
        "OmegabLT": 0.2, 
        "OmegabVertChi2DOF": 10, 
        "Omegab_PT": 3500.0, 
        "PionProbNN": 0.1, 
        "ProtProbNN": 0.1, 
        "XicBPVVDCHI2": 36, 
        "XicDIRA": 0.0, 
        "XicDeltaMassWin": 100, 
        "XicPT": 1800
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BandQ" ]
}

PPMuMu = {
    "BUILDERTYPE": "PPMuMuConf", 
    "CONFIG": {
        "DetachedComAMCuts": "AALL", 
        "DetachedComN4Cuts": "(AM > 2.8 *GeV)", 
        "DetachedMomN4Cuts": "(VFASPF(VCHI2/VDOF) < 9.) & (MM>3.0 *GeV) & (MM<20.0 *GeV)", 
        "DetachedMuonCuts": "(PROBNNmu > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 5.)", 
        "DetachedProtonCuts": "(PROBNNp  > 0.1) & (PT > 1000*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) > 5.)", 
        "HcComAMCuts": "AALL", 
        "HcComN4Cuts": "(AM > 2.8 *GeV) & (AM < 4.2 *GeV)", 
        "HcMomN4Cuts": "(VFASPF(VCHI2/VDOF) < 9.) & (MM>3.0 *GeV) & (MM<4.0 *GeV)", 
        "HcMuonCuts": "(PROBNNmu > 0.1) & (PT > 250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)", 
        "HcProtonCuts": "(PROBNNp  > 0.1) & (PT > 1000*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 10.)", 
        "HighComAMCuts": "AALL", 
        "HighComN4Cuts": "(AM > 3.8 *GeV)", 
        "HighMomN4Cuts": "(VFASPF(VCHI2/VDOF) < 9.) & (MM>4.0 *GeV) & (MM<20.0 *GeV)", 
        "HighMuonCuts": "(PROBNNmu > 0.2) & (PT > 800*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 8.)", 
        "HighProtonCuts": "(PROBNNp  > 0.2) & (PT > 1600*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY) < 8.)", 
        "Prescale": 1.0
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BandQ" ]
}

XB2DPiP = {
    "BUILDERTYPE": "XB2DPiPConf", 
    "CONFIG": {
        "MIPCHI2": 9, 
        "MomCuts": "\n                          (VFASPF(VCHI2/VDOF) < 10.) \n                          & (BPVDIRA> 0.9999) \n                          & (BPVIPCHI2()<25) \n                          & (BPVVDCHI2>250)\n                          & (BPVVDRHO>0.1*mm) \n                          & (BPVVDZ>2.0*mm)\n                          & (MINTREE(((ABSID=='D+') | (ABSID=='D0') | (ABSID=='Lambda_c+')) , VFASPF(VZ))-VFASPF(VZ) > 0.0 *mm )\n                          ", 
        "ProtonPIDp": 5, 
        "ProtonPIDpK": -3, 
        "RelatedInfoTools": [
            {
                "Location": "RelInfoVertexIsolation", 
                "Type": "RelInfoVertexIsolation"
            }, 
            {
                "Location": "RelInfoVertexIsolationBDT", 
                "Type": "RelInfoVertexIsolationBDT"
            }, 
            {
                "ConeAngle": 1.0, 
                "Location": "RelInfoConeVariables_1.0", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 1.5, 
                "Location": "RelInfoConeVariables_1.5", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 2.0, 
                "Location": "RelInfoConeVariables_2.0", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }
        ], 
        "TRCHI2DOF": 5, 
        "TRGHOSTPROB": 0.5
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BandQ" ]
}

Xibc = {
    "BUILDERTYPE": "XibcBuilder", 
    "CONFIG": {
        "GlobalGhostProb_Max": 0.4, 
        "HighMassBaryon_MassLowEdge": "4.5*GeV", 
        "HighMassBaryon_MinAPT": "1*GeV", 
        "JpsiForHighMassBaryon_MassWin": "40*MeV", 
        "JpsiForHighMassBaryon_MuonPIDmu": 0, 
        "JpsiForHighMassBaryon_PT": "1.5*GeV", 
        "JpsiKp_CtrlLine_Prescale": 1.0, 
        "JpsiKp_Jpsi_MassWin": 40, 
        "JpsiKp_Jpsi_MinPT": 700, 
        "JpsiKp_K_MaxTrackGhostProb": 0.4, 
        "JpsiKp_K_MinP": 2000, 
        "JpsiKp_K_MinPT": 300, 
        "JpsiKp_K_MinProbNNk": 0.02, 
        "JpsiKp_K_MinTrackPvalue": 0.1, 
        "JpsiKp_MassLbThreshold": 5750, 
        "JpsiKp_MassMax": 8000, 
        "JpsiKp_MassMin": 5200, 
        "JpsiKp_MaxVertexChi2": 10, 
        "JpsiKp_MinTAU": "0.10*ps", 
        "JpsiKp_mu_MaxTrackGhostProb": 0.4, 
        "JpsiKp_mu_MinPIDmu": 0, 
        "JpsiKp_p_MaxTrackGhostProb": 0.4, 
        "JpsiKp_p_MinP": 2000, 
        "JpsiKp_p_MinProbNNp": 0.05, 
        "JpsiKp_p_MinPt": 300, 
        "JpsiKp_p_MinTrackPvalue": 0.1, 
        "JpsiProtonForHighMassBaryonCosth": 1, 
        "Jpsi_ENDVERTEXCHI2": 10, 
        "Jpsi_MassWindowLarge": "150*MeV", 
        "Jpsi_MassWindowTight": "50*MeV", 
        "Kaon4Lambdac_PT": "450*MeV", 
        "Kaon4Lambdac_ProbNNk": 0.02, 
        "Kaon4Lambdac_TRPCHI2": 0, 
        "Kaon4Lambdac_minP": "0*GeV", 
        "Lambda0_APT": "700*MeV", 
        "Lambda0_DownPionTrackPvalue": 0, 
        "Lambda0_DownProtonTrackPvalue": 0, 
        "Lambda0_ENDVERTEXCHI2": 10, 
        "Lambda0_MassWindowLarge": "180*MeV", 
        "Lambda0_MassWindowTight": "30*MeV", 
        "Lambda0_minFD": "1*mm/GeV", 
        "Lambdac_BPVDIRA": 0.98, 
        "Lambdac_ENDVERTEXCHI2": 5, 
        "Lambdac_MassWindowLarge": "120*MeV", 
        "Lambdac_MassWindowTight": "30*MeV", 
        "Lambdac_MaxADOCA": "0.5*mm", 
        "Lambdac_MinAPT": "1500*MeV", 
        "Lambdac_minTAU": -0.0001, 
        "LongProtons4Lambda0_MINIPCHI2": 2, 
        "LongTrackGEC": 150, 
        "Muon4Jpsi_PIDmu": 0, 
        "Muon4Jpsi_PT": "650*MeV", 
        "Muon4Jpsi_TRPCHI2": 0, 
        "Pion4Lambdac_PT": "250*MeV", 
        "Pion4Lambdac_ProbNNpi": 0.2, 
        "Pion4Lambdac_TRPCHI2": 0, 
        "Pions4Lambda0_MINIPCHI2": 0.0, 
        "Pions4Lambda0_PT": "0*MeV", 
        "Pions4Lambda0_ProbNNpi": 0.2, 
        "Pions4Lambdac_MINIPCHI2": 0.0, 
        "Pions4Xic0_PT": "200*MeV", 
        "Pions4Xic0_ProbNNpi": 0.2, 
        "Pions4Xic0_TRPCHI2": 0, 
        "Pions4Ximinus_MINIPCHI2": 0, 
        "Pions4Ximinus_PT": "0*MeV", 
        "Pions4Ximinus_ProbNNpi": 0.2, 
        "Pions4Ximinus_TRPCHI2": 0, 
        "Protons4Lambda0_PT": "600*MeV", 
        "Protons4Lambda0_ProbNNp": 0.02, 
        "Protons4Lambdac_PT": "450*MeV", 
        "Protons4Lambdac_ProbNNp": 0.05, 
        "Protons4Lambdac_TRPCHI2": 0, 
        "Protons4Lambdac_minP": "0*GeV", 
        "ProtonsForHighMassBaryon_P": "5*GeV", 
        "ProtonsForHighMassBaryon_PT": "1.5*GeV", 
        "ProtonsForHighMassBaryon_ProbNNp": 0.1, 
        "ProtonsForHighMassBaryon_TRPCHI2": 0.0, 
        "Xibc0_ENDVERTEXCHI2": 20, 
        "Xibc0_MassWindow": "1.5*GeV", 
        "Xibc2LcJpsiPrescale": 0.2, 
        "Xibc_ENDVERTEXCHI2": 7, 
        "Xibc_MINPVIP": 1000.0, 
        "Xibc_MassWindow": "2*GeV", 
        "Xic0_APT": "0*MeV", 
        "Xic0_ENDVERTEXCHI2": 20, 
        "Xic0_MassWindowLarge": "600*MeV", 
        "Xic0_MassWindowTight": "100*MeV", 
        "Ximinus_APT": "800*MeV", 
        "Ximinus_ENDVERTEXCHI2": 20, 
        "Ximinus_FlightDistance": "0.1*mm/GeV", 
        "Ximinus_MassWindowLarge": "120*MeV", 
        "Ximinus_MassWindowTight": "40*MeV"
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingXibcXibc2LcJpsi"
             ]
    }, 
    "WGs": [ "BandQ" ]
}

XibcBDT = {
    "BUILDERTYPE": "XibcBDTConf", 
    "CONFIG": {
        "D0Cuts": "(ADMASS('D0')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>64)", 
        "DplusCuts": "(ADMASS('D+')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>100)", 
        "HighPTKaonCuts": "(PROBNNk > 0.1) & (PT>1.0*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "HighPTPionCuts": "(PROBNNpi> 0.2) & (PT>0.5*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "HighPTProtonCuts": "(PROBNNp> 0.05) & (PT>1.0*GeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "LambdaDDCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25)", 
        "LambdaLLComCuts": "(ADAMASS('Lambda0')<50*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LambdaLLCuts": "(ADMASS('Lambda0') < 30.*MeV) & (BPVVDCHI2>25) & (VFASPF(VCHI2) < 25.)", 
        "LbComCuts": "(APT>1.0*GeV) & (ADAMASS('Lambda_b0')<80*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LbMomCuts": "(ADMASS('Lambda_b0')<60*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>64)", 
        "LcComCuts": "(APT>1.0*GeV) & (ADAMASS('Lambda_c+')<50*MeV) & (ADOCACHI2CUT(30, ''))", 
        "LcMomCuts": "(ADMASS('Lambda_c+')<30*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>16)", 
        "PhiCuts": "\n                          (HASVERTEX) & (VFASPF(VCHI2)<16) & (MM<1.05*GeV) & (MIPCHI2DV(PRIMARY)>2.)\n                          & (INTREE( (ID=='K+') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          & (INTREE( (ID=='K-') & (PT>200*MeV) & (TRGHOSTPROB<0.4) ))\n                          ", 
        "Pion4LPCuts": "(PROBNNpi> 0.2) & (PT>100*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>9.)", 
        "PionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "ProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "TightKaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "TightPionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "TightProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "UnKaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPTKaonCuts": "(PROBNNk > 0.1) & (PT>600*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPTPionCuts": "(PROBNNpi> 0.2) & (PT>500*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPTProtonCuts": "(PROBNNp> 0.05) & (PT>750*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4)", 
        "UnProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "Xibc2D0pKMVACut": "0.05", 
        "Xibc2D0pKXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2D0pK_BDT_v1r0.xml", 
        "Xibc2LambdaPhiMVACut": "-0.1", 
        "Xibc2LambdaPhiXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2LambdaPhi_BDT_v1r0.xml", 
        "Xibc2LbKpiMVACut": "-0.3", 
        "Xibc2LbKpiXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2LbKpi_BDT_v1r0.xml", 
        "Xibc2LcKMVACut": "-0.01", 
        "Xibc2LcPiMVACut": "0.", 
        "Xibc2LcPiXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2LcPi_BDT_v1r0.xml", 
        "Xibc2pKMVACut": "0.", 
        "Xibc2pKXmlFile": "$TMVAWEIGHTSROOT/data/Xibc2pK_BDT_v1r0.xml", 
        "XibcComCuts": "(AM>4.8*GeV)", 
        "XibcLPMomCuts": "(M>5.0*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVIPCHI2()<25)", 
        "XibcMomCuts": "(M>5.0*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVIPCHI2()<25) & (BPVDIRA> 0.99)", 
        "XibcP2D0pKpiMVACut": "-0.05", 
        "XibcP2D0pKpiXmlFile": "$TMVAWEIGHTSROOT/data/XiccP2D0pKpi_BDT_v1r0.xml", 
        "XibcP2DpKMVACut": "0.02", 
        "XibcP2DpKXmlFile": "$TMVAWEIGHTSROOT/data/XiccP2DpK_BDT_v1r0.xml", 
        "XibcP2LambdaPiMVACut": "0.1", 
        "XibcP2LambdaPiXmlFile": "$TMVAWEIGHTSROOT/data/XibcP2LambdaPi_BDT_v1r0.xml", 
        "XibcP2LcKpiMVACut": "0.10", 
        "XibcP2LcKpiXmlFile": "$TMVAWEIGHTSROOT/data/XibcP2LcKpi_BDT_v1r0.xml", 
        "XibcP2pKpiMVACut": "0.14", 
        "XibcP2pKpiXmlFile": "$TMVAWEIGHTSROOT/data/XibcP2pKpi_BDT_v1r0.xml"
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BandQ" ]
}

XicHHHOld = {
    "BUILDERTYPE": "StrippingXic2HHHOldConf", 
    "CONFIG": {
        "Comb_ADOCAMAX_MAX": 0.3, 
        "Comb_MASS_MAX": 2800.0, 
        "Comb_MASS_MIN": 2250.0, 
        "Daug_All_PT_MIN": 300.0, 
        "Daug_P_MIN": 3000.0, 
        "Daug_TRCHI2DOF_MAX": 4.0, 
        "K_IPCHI2_MIN": 9.0, 
        "PostscaleXic2KLam": 0.0, 
        "PostscaleXic2PKK": 1.0, 
        "PostscaleXic2PKPi": 0.0, 
        "PostscaleXic2PV0": 1.0, 
        "PrescaleXic2KLam": 0.0, 
        "PrescaleXic2PKK": 1.0, 
        "PrescaleXic2PKPi": 0.0, 
        "PrescaleXic2PV0": 1.0, 
        "RelatedInfoTools": [
            {
                "ConeAngle": 1.5, 
                "Location": "Cone1", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.75, 
                "Location": "Cone2", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.5, 
                "Location": "Cone3", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.25, 
                "Location": "Cone4", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }, 
            {
                "ConeAngle": 0.1, 
                "Location": "Cone5", 
                "Type": "RelInfoConeVariables", 
                "Variables": [
                    "CONEANGLE", 
                    "CONEMULT", 
                    "CONEPTASYM"
                ]
            }
        ], 
        "Xic_BPVDIRA_MIN": 0.999, 
        "Xic_BPVIPCHI2_MAX": 12.0, 
        "Xic_BPVLTIME_MAX": 0.005, 
        "Xic_BPVLTIME_MIN": -0.005, 
        "Xic_BPVVDCHI2_MIN": 0.0, 
        "Xic_PT_MIN": 2000.0, 
        "Xic_VCHI2VDOF_MAX": 8.0, 
        "pion_IPCHI2_MIN": 9.0
    }, 
    "STREAMS": [ "Charm" ], 
    "WGs": [ "BandQ" ]
}

XiccBDT = {
    "BUILDERTYPE": "XiccBDTConf", 
    "CONFIG": {
        "D0Cuts": "(ADMASS('D0')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>64)", 
        "DplusCuts": "(ADMASS('D+')<75*MeV) & (VFASPF(VCHI2/VDOF) < 25.) & (PT>1.0*GeV) & (BPVVDCHI2>100)", 
        "KaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "LcComCuts": "(APT>1.0*GeV) & ((ADAMASS('Lambda_c+')<50*MeV) | (ADAMASS('Xi_c+')<50*MeV)) & (ADOCACHI2CUT(30, ''))", 
        "LcMomCuts": "((ADMASS('Lambda_c+')<20*MeV) | (ADMASS('Xi_c+')<20*MeV)) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVVDCHI2>16)", 
        "PionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "ProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>1.)", 
        "TightKaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "TightPionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "TightProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4) & (MIPCHI2DV(PRIMARY)>2.)", 
        "UnKaonCuts": "(PROBNNk > 0.1) & (PT>250*MeV) & (TRGHOSTPROB<0.4)", 
        "UnPionCuts": "(PROBNNpi> 0.2) & (PT>200*MeV) & (TRGHOSTPROB<0.4)", 
        "UnProtonCuts": "(PROBNNp> 0.05) & (PT>300*MeV) & (TRGHOSTPROB<0.4)", 
        "XiccComCuts": "(AM<4.6*GeV) & (APT>2*GeV)", 
        "XiccMomCuts": "(M<4.4*GeV) & (VFASPF(VCHI2/VDOF) < 25.) & (BPVDIRA> 0.99) & (BPVIPCHI2()<25)", 
        "XiccP2D0pKpiMVACut": "-0.05", 
        "XiccP2D0pKpiXmlFile": "$TMVAWEIGHTSROOT/data/XiccP2D0pKpi_BDT_v1r0.xml", 
        "XiccP2D0pMVACut": "0.1", 
        "XiccP2D0pXmlFile": "$TMVAWEIGHTSROOT/data/XiccP2D0p_BDT_v1r0.xml", 
        "XiccP2DpKMVACut": "0.", 
        "XiccP2DpKXmlFile": "$TMVAWEIGHTSROOT/data/XiccP2DpK_BDT_v1r0.xml", 
        "XiccP2LcKpiMVACut": "0.16", 
        "XiccP2LcKpiXmlFile": "$TMVAWEIGHTSROOT/data/XiccP2LcKpi_BDT_v1r0.xml", 
        "XiccPP2D0pKpipiMVACut": "-0.1", 
        "XiccPP2D0pKpipiXmlFile": "$TMVAWEIGHTSROOT/data/XiccPP2D0pKpipi_BDT_v1r0.xml", 
        "XiccPP2DpKpiMVACut": "-0.05", 
        "XiccPP2DpKpiXmlFile": "$TMVAWEIGHTSROOT/data/XiccPP2DpKpi_BDT_v1r0.xml", 
        "XiccPP2DpMVACut": "0.05", 
        "XiccPP2DpXmlFile": "$TMVAWEIGHTSROOT/data/XiccPP2Dp_BDT_v1r0.xml", 
        "XiccPP2LcKpipiMVACut": "0.", 
        "XiccPP2LcKpipiXmlFile": "$TMVAWEIGHTSROOT/data/XiccPP2LcKpipi_BDT_v1r0.xml", 
        "XiccPP2LcPiMVACut": "0.18", 
        "XiccPP2LcPiXmlFile": "$TMVAWEIGHTSROOT/data/XiccPP2LcPi_BDT_v1r0.xml"
    }, 
    "STREAMS": [ "Bhadron" ], 
    "WGs": [ "BandQ" ]
}

PromptCharm = {
    "BUILDERTYPE": "StrippingPromptCharmConf", 
    "CONFIG": {
        "Charm&WPrescale": 1.0, 
        "CheckPV": True, 
        "Chi&CharmPrescale": 1.0, 
        "Chi&WPrescale": 1.0, 
        "D*CPPrescale": 1.0, 
        "D*Prescale": 1.0, 
        "D+Prescale": 0.4, 
        "D02KKPrescale": 1.0, 
        "D02pipiPrescale": 1.0, 
        "D0Prescale": 0.3, 
        "DiCharmPrescale": 1.0, 
        "DiMu&CharmPrescale": 1.0, 
        "DiMuon&WPrescale": 1.0, 
        "DiMuonAndDiCharmPrescale": 1.0, 
        "DoubleDiMuonAndCharmPrescale": 1.0, 
        "DoubleDiMuonPrescale": 1.0, 
        "DsPrescale": 0.7, 
        "GammaChi": " ( PT > 400 * MeV ) & ( CL > 0.05 ) ", 
        "KaonCut": "\n    ( PT          > 250 * MeV ) & \n    ( CLONEDIST   > 5000      ) & \n    ( TRGHOSTPROB < 0.5       ) &\n    in_range ( 2          , ETA , 4.9       ) &\n    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &\n    HASRICH                     &\n    ( MIPCHI2DV()  > 9        )\n    ", 
        "KaonPIDCut": " PROBNNk  > 0.1 ", 
        "LambdaC*Prescale": 1.0, 
        "LambdaCLooseChi2IPPrescale": 1.0, 
        "LambdaCPrescale": 1.0, 
        "LambdaCpKKPrescale": 1.0, 
        "Monitor": False, 
        "MuonCut": "\n    ISMUON &\n    in_range ( 2 , ETA , 4.9     ) &\n    ( PT            >  550 * MeV ) &\n    ( PIDmu - PIDpi >    0       ) &\n    ( CLONEDIST     > 5000       )     \n    ", 
        "NOPIDHADRONS": False, 
        "OmegaC*2XiCPiKPrescale": 1.0, 
        "OmegaC*2XiCprimeKPrescale": 1.0, 
        "OmegaC*Prescale": 1.0, 
        "OmegaC0Prescale": 1.0, 
        "OmegaCC2XiCKKPrescale": 1.0, 
        "OmegaCC2XiCKpiPrescale": 1.0, 
        "OmegaCCPrescale": 1.0, 
        "PhotonCLCut": 0.05, 
        "PionCut": "\n    ( PT          > 250 * MeV ) & \n    ( CLONEDIST   > 5000      ) & \n    ( TRGHOSTPROB < 0.5       ) &\n    in_range ( 2          , ETA , 4.9       ) &\n    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &\n    HASRICH                     &\n    ( MIPCHI2DV()  > 9        )\n    ", 
        "PionPIDCut": " PROBNNpi > 0.1 ", 
        "Preambulo": [
            "pipi   = DECTREE ('[D0]cc -> pi- pi+   ') ", 
            "kk     = DECTREE ('[D0]cc -> K-  K+    ') ", 
            "kpi    = DECTREE ('[D0    -> K-  pi+]CC') ", 
            "ak2    = 2 == ANUM( 'K+' == ABSID ) ", 
            "chi2vx = VFASPF(VCHI2) ", 
            "from GaudiKernel.PhysicalConstants import c_light", 
            "ctau     = BPVLTIME (   9 ) * c_light ", 
            "ctau_9   = BPVLTIME (   9 ) * c_light ", 
            "ctau_16  = BPVLTIME (  16 ) * c_light ", 
            "ctau_25  = BPVLTIME (  25 ) * c_light ", 
            "ctau_100 = BPVLTIME ( 100 ) * c_light ", 
            "ctau_400 = BPVLTIME ( 400 ) * c_light ", 
            "ctau_no  = BPVLTIME (     ) * c_light ", 
            "psi           =   ADAMASS ('J/psi(1S)') < 150 * MeV", 
            "psi_prime     =   ADAMASS (  'psi(2S)') < 150 * MeV"
        ], 
        "PromptKaonCut": "\n    ( CLONEDIST   > 5000         ) & \n    ( TRGHOSTPROB < 0.5          ) &\n    in_range ( 2          , ETA , 4.9       ) &\n    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &\n    HASRICH                     \n    ", 
        "ProtonCut": "\n    ( PT           > 250 * MeV ) & \n    ( CLONEDIST    > 5000      ) & \n    ( TRGHOSTPROB  < 0.5       ) & \n    in_range ( 2         , ETA , 4.9       ) &\n    in_range ( 10 * GeV  , P   , 150 * GeV ) &\n    HASRICH                      &\n    ( MIPCHI2DV()  > 9         ) \n    ", 
        "ProtonPIDCut": " PROBNNp  > 0.1 ", 
        "QvalueLcPiK": 700.0, 
        "QvalueOmegaCC": 4500.0, 
        "QvalueXiCK": 600.0, 
        "QvalueXiCPiK": 600.0, 
        "QvalueXiCprime": 250.0, 
        "QvalueXiCprimeK": 600.0, 
        "QvalueXiCstar": 150.0, 
        "SigmaCPrescale": 1.0, 
        "TrackCut": "\n    ( CLONEDIST   > 5000      ) &\n    ( TRGHOSTPROB < 0.5       ) &\n    in_range ( 2  , ETA , 4.9 ) &\n    HASRICH\n    ", 
        "TriCharmPrescale": 1.0, 
        "TripleDiMuPrescale": 1.0, 
        "WCuts": " ( 'mu+'== ABSID ) & ( PT > 15 * GeV )", 
        "XiC**2LcPiKPrescale": 1.0, 
        "XiC*Prescale": 1.0, 
        "XiC0Prescale": 1.0, 
        "XiCprimePrescale": 1.0, 
        "Xic02LcPiPrescale": 1.0, 
        "pT(D+)": 1000.0, 
        "pT(D0)": 1000.0, 
        "pT(D0->HH)": 1000.0, 
        "pT(Ds+)": 1000.0, 
        "pT(Lc+)": 1000.0, 
        "pT(Omgcc)": 1000.0, 
        "pT(Xic0)": 1000.0
    }, 
    "STREAMS": {
        "Charm": [
            "StrippingD02KpiForPromptCharm", 
            "StrippingDstarForPromptCharm", 
            "StrippingDForPromptCharm", 
            "StrippingDsForPromptCharm", 
            "StrippingLambdaCForPromptCharm", 
            "StrippingLambdaC2pKKForPromptCharm",
            "StrippingLambdaCLooseChi2IPForPromptCharm", 
            "StrippingXiC0ForPromptCharm", 
            "StrippingSigmaCForPromptCharm", 
            "StrippingLambdaCstarForPromptCharm", 
            "StrippingOmegaCstarForPromptCharm", 
            "StrippingXiCstarstar2LambdaCPiKForPromptCharm", 
            "StrippingXic02LcPiForPromptCharm", 
            "StrippingChiAndCharmForPromptCharm", 
            "StrippingCharmAndWForPromptCharm", 
            "StrippingDiMuonAndCharmForPromptCharm", 
            "StrippingDiCharmForPromptCharm", 
            "StrippingTriCharmForPromptCharm", 
            "StrippingDiMuonAndDiCharmForPromptCharm", 
            "StrippingDoubleDiMuonAndCharmForPromptCharm", 
            "StrippingD02KKForPromptCharm", 
            "StrippingD02pipiForPromptCharm", 
            "StrippingDstarCPForPromptCharm"
        ], 
        "Leptonic": [
            "StrippingDiMuonAndWForPromptCharm", 
            "StrippingChiAndWForPromptCharm", 
            "StrippingDoubleDiMuonForPromptCharm", 
            "StrippingTripleDiMuonForPromptCharm"
        ]
    }, 
    "WGs": [ "BandQ" ]
}

